//websocket stuff
var Server;
var host;
var IPValidPattern = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/g;
var RTCPeerConnection = /*window.RTCPeerConnection ||*/ window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
	//fancy wesocket
	var FancyWebSocket = function(url)	{
		var callbacks = {};
		var ws_url = url;
		var conn;

		this.bind = function(event_name, callback){
			callbacks[event_name] = callbacks[event_name] || [];
			callbacks[event_name].push(callback);
			return this;// chainable
		};

		this.send = function(event_name, event_data){
			this.conn.send( event_data );
			return this;
		};

		this.connect = function() {
			if ( typeof(MozWebSocket) == 'function' )
				this.conn = new MozWebSocket(url);
			else
				this.conn = new WebSocket(url);

			// dispatch to the right handlers
			this.conn.onmessage = function(evt){
				dispatch('message', evt.data);
			};

			this.conn.onclose = function(){dispatch('close',null)}
			this.conn.onopen = function(){dispatch('open',null)}
		};

		this.disconnect = function() {
			this.conn.close();
		};

		var dispatch = function(event_name, message){
			var chain = callbacks[event_name];
			if(typeof chain == 'undefined') return; // no callbacks for this event
			for(var i = 0; i < chain.length; i++){
				chain[i]( message )
			}
		}
	};


function userAdd(userId, userName, mac) {
	console.log('USERADD: '+mac);
	$('.avatar[data-mac="'+mac+'"]').addClass('websoketActive');
	$('.avatar[data-mac="'+mac+'"]').removeClass('websoketActiveLogOff');
}
function userRemove(userId, mac) {
	console.log('USERREMOVE:'+mac);
	$(".console.logged_user span[userid='"+userId+"']").remove();
	$('.avatar[data-mac="'+mac+'"]').addClass('websoketActiveLogOff');
	$('.avatar[data-mac="'+mac+'"]').removeClass('websoketActive');
}

function godzillaEvent() {

	$("body").css({"background-image": "url(img/giphy.gif)"});
	setTimeout(function () {
		$("body").css({"background-image": "none"});
	}, 6000);
	$(".avatar.websoketActive, .avatar.websoketActiveLogOff").each(function(index){
		messagewrite("OH MY GOD, IT'S GODZILLA", $(this).attr("data-mac"));		
	})
}

function ashTagEvent(ashtag, rooms) {
	console.log(rooms);
	$(".rooms").html("");
	$(".rooms").append('<li><a class="public_ashtag_button">Public</a></li>');
	for(i in rooms){
		if (rooms[i].hashTagName != ""  ){
			if($(".rooms .ashtag_button:contains("+rooms[i].hashTagName+")").length <= 0){
				$(".rooms").append('<li><a class="ashtag_button">'+rooms[i].hashTagName+"</a></li>");
			}
		}
		$('.avatar[data-mac="'+rooms[i].macAddress+'"]').attr('data-room', rooms[i].hashTagName);
	}
	
	var myMac = $("#macaddress").text();
	if ($('.avatar[data-mac="'+myMac+'"]').attr('data-room') == ashtag)
	{
		$('.avatar[data-room="'+ashtag+'"]').show();
	}
	else
	{
		$('.avatar[data-room="'+ashtag+'"]').hide();
	}

	//console.log(macAddressList);
	//var arr = ["48:d2:24:39:f5:5f"];

	

	/*for (var i = 0; i < rooms.length; i++) {
		$('.avatar[data-mac="'+rooms[i]+'"]').attr('data-room', rooms[])
	}*/
//
	//ashtag = ashtag.replace('#','');
	//var myMac = $("#macaddress").text();
//
	//if ($('.avatar[data-mac="'+myMac+'"]').attr('data-room') != '')
	//{
		//$('.avatar[data-mac="'+myMac+'"]').attr('data-room', ashtag);
	//}
//		
//	
//
	//for (var i = 0; i < macAddressList.length; i++) {
		//console.log("CONTINUACHEVENGO");
		//if ($('.avatar[data-mac="'+macAddressList[i]+'"]').attr('data-room') == ashtag && macAddressList[i] != myMac)
		//{
//
		//}else
			//$('.avatar[data-mac="'+macAddressList[i]+'"]').show();
	//}
	
		//$('.avatar').hide();
		//for (var i = 0; i < macAddressList.length; i++) {
			//$('.avatar[data-mac="'+macAddressList[i]+'"]').show();		
		//};
	//} else {
		//for (var i = 0; i < macAddressList.length; i++) {
			//$('.avatar[data-mac="'+macAddressList[i]+'"]').hide();		
		//};
	//}


	//ashtagtmparr = ashtag.split("#");
	//ashtag = ashtagtmparr[ashtagtmparr.length - 1];
	//alert(ashtag);
	//$('.avatar[data-mac="'+mac+'"]').addClass(ashtag);
	//$('.avatar').not('.'+ashtag).css("display", "none");
	//$('.avatar.'+ashtag).css("display", "block");
	//$(".rooms").append("<p>"+ashtag+"</p>");
}

function send( text, chatId ) {
	var message2send = { };
	message2send.messageType = "commonText";
	if (text.toLowerCase().indexOf("godzilla") > -1){
		message2send.messageType = "textEventGodzilla";
	} 
	if (text.toLowerCase().indexOf("#") == 0  ){
		//controllare che l'ashtag non esista
		message2send.messageType = "setAshTag";
		$('.avatar').hide();
	}
	message2send.messageText = text;
	message2send.chatId = chatId;

	var json = JSON.stringify(message2send);
	Server.send( 'message', json);
}

function initChat() {
	var message2send = { };
	message2send.messageType = "initChat";
	message2send.messageText = "";
	message2send.macAddress = $("#macaddress").text();
	message2send.chatId = "1";
	message2send.userName = $("username").text();

	var json = JSON.stringify(message2send);
	Server.send( 'message', json);
}

function messagewrite(text, macaddress) {
	$('.avatar[data-mac="'+macaddress+'"] .message .values').text(text);
	$('.avatar[data-mac="'+macaddress+'"] .message').addClass('show');
	$('.avatar[data-mac="'+macaddress+'"]').addClass('maxImportance');

	pushMessageToChatLog(macaddress,text);

	if($('.avatar[data-mac="'+macaddress+'"] .message').data('timeout') != null){
		clearTimeout($('.avatar[data-mac="'+macaddress+'"] .message').data('timeout'));
	}
	$('.avatar[data-mac="'+macaddress+'"] .message').data('timeout',setTimeout(function(){
		$('.avatar[data-mac="'+macaddress+'"] .message').removeClass('show');
		$('.avatar[data-mac="'+macaddress+'"]').removeClass('maxImportance');
	}, 4000));
}

$(document).ready(function() {

	$.ajax({
	  url: "ajax/getHost"
	}).success(function(response) {
	  host = response;
	});

	if(!IPValidPattern.test(host)){
		host = "10.10.30.49";
	}
	while ($("username").text() == "null"){
		$("username").text(prompt("Username?"));
	}
	if ($("username").text() == "null"){
		$("body").remove();
	}

	console.log('Connecting...');


	//getPrivateIp();



	Server = new FancyWebSocket('ws://'+host+':9300');

	timeoutSentMessage = null;

	$('.message').keypress(function(e) {
		if ( e.keyCode == 13 && this.value ) {
			console.log( 'You: ' + this.value );
			send( this.value, $(this).attr("chatId"));

			$('.user_active .message .values').text(this.value);
			$('.user_active .message').addClass('show');
			pushMessageToChatLog($('.user_active').attr('data-mac'),this.value);
			$(this).val('');
			if(timeoutSentMessage != null){
				clearTimeout(timeoutSentMessage);
			}
			timeoutSentMessage = setTimeout(function(){
				$('.user_active .message').removeClass('show');
			}, 4000);
		}
	});

	//Let the user know we're connected
	Server.bind('open', function() {
		console.log( "Connected." );
		console.log( "Initializing..." );
		initChat();
	});

	//OH NOES! Disconnection occurred.
	Server.bind('close', function( data ) {
		console.log( "Disconnected." );
		alert('OOPS. Server is restarting. One moment.');
		window.location.href=window.location.href
	});

	//console.Log any messages sent from server
	Server.bind('message', function( payload ) {
		//console.console.log(payload);
		var objResponse = jQuery.parseJSON( payload );
		if (objResponse.responseType == "userJoin") {
			userAdd(objResponse.userId, objResponse.userName, objResponse.macAddress);
			console.log(objResponse.message);
		} else if (objResponse.responseType == "userLeft") {
			userRemove(objResponse.userId, objResponse.macAddress);
			console.log(objResponse.message);
		} else  if (objResponse.responseType == "simpleMessage"){
			console.log(objResponse.message);
			messagewrite(objResponse.message, objResponse.macAddress)
		} else  if (objResponse.responseType == "addChatAshTag"){
			ashTagEvent(objResponse.hashTag, objResponse.rooms);
		} else  if (objResponse.responseType == "eventGodzilla"){
			godzillaEvent();
		} else {
			console.console.log("Unknown responseType");
		};
	});

	Server.connect();

	$('body').delegate(".ashtag_button","click",function () {
		$(this).addClass('active').parent().siblings().find('a').removeClass('active');
		send($(this).text(), 1);
	});
});

function pushMessageToChatLog(macAddress,message){
	var html = '<li data-mac="'+macAddress+'">';
	var currentdate = new Date();
	var datetime = "" + currentdate.getDate() + "/"
	                + (currentdate.getMonth()+1)  + "/"
	                + currentdate.getFullYear() + " @ "
	                + currentdate.getHours() + ":"
	                + currentdate.getMinutes() + ":"
	                + currentdate.getSeconds();
	var background = $('.avatar[data-mac="'+macAddress+'"]').css('background-image');
	html += '	<div class="avatar_rip" style="background-image:'+background+'"></div>';
	html += '	<div class="message_rip"><span>'+message+'</span><span class="label">'+datetime+'</span></div>';
	html += '<li>'
	$('.chatRoom ul.chat_history').prepend(html);
	/*
	var objDiv = document.getElementById("chatRoom");
	objDiv.scrollTop = objDiv.scrollHeight;
	*/
}


// get local IP
// NOTE: window.RTCPeerConnection is "not a constructor" in FF22/23
//function getPrivateIp(){
	//var rtc = new RTCPeerConnection({iceServers:[]});
	//if (1 || window.mozRTCPeerConnection) {      // FF [and now Chrome!] needs a channel/stream to proceed
	    //rtc.createDataChannel('', {reliable:false});
	//};
//
	//rtc.onicecandidate = function (evt) {
	    //// convert the candidate to SDP so we can run it through our general parser
	    //// see https://twitter.com/lancestout/status/525796175425720320 for details
	    //if (evt.candidate) grepSDP("a="+evt.candidate.candidate);
	//};
	//rtc.createOffer(function (offerDesc) {
	    //grepSDP(offerDesc.sdp);
	    //rtc.setLocalDescription(offerDesc);
	//}, function (e) { console.warn("offer failed", e); });
//
//
	//var addrs = Object.create(null);
	//addrs["0.0.0.0"] = false;
	//function updateDisplay(newAddr) {
//
		////Invio l'IP al server
		////$.ajax({
			////url:"ajax/registerHost",
			////type: "POST",
			////data: { 'private_ip' : newAddr },
			////cache: false,
			////async: true,
			////success:function(result){
				////console.console.log(result.data['private_ip']);
			////},
			////error: function(richiesta,stato,errori){
				////console.console.log("AJAX ERROR: "+stato+" "+errori);
			////}
		////});
//
	    //if (newAddr in addrs) return;
	    //else addrs[newAddr] = true;
	    //var displayAddrs = Object.keys(addrs).filter(function (k) { return addrs[k]; });
//
	    //document.getElementById('list').textContent = displayAddrs.join(" or perhaps ") || "n/a";
	//}
//
	//function grepSDP(sdp) {
	    //var hosts = [];
	    //sdp.split('\r\n').forEach(function (line) { // c.f. http://tools.ietf.org/html/rfc4566#page-39
	        //if (~line.indexOf("a=candidate")) {     // http://tools.ietf.org/html/rfc4566#section-5.13
	            //var parts = line.split(' '),        // http://tools.ietf.org/html/rfc5245#section-15.1
	                //addr = parts[4],
	                //type = parts[7];
	            //if (type === 'host') updateDisplay(addr);
	        //} else if (~line.indexOf("c=")) {       // http://tools.ietf.org/html/rfc4566#section-5.7
	            //var parts = line.split(' '),
	                //addr = parts[2];
	            //console.console.log('QUI: '+addr);
	            //updateDisplay(addr);
	        //}
	    //});
	//}
//}

