<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
	<base href="<?php echo(base_url());?>">
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	<script src="js/custom.js"></script>
	<style type="text/css">
		body{
			color: white;
			background-color: black;
		}
		.custom_scrollbar::-webkit-scrollbar {
			width: 16px;
		}
		.custom_scrollbar::-webkit-scrollbar-thumb {
			background-color: rgba(255,255,255,0.4);
			-webkit-box-shadow: inset 1px 1px 0 rgba(255,255,255,0.10),inset 0 -1px 0 rgba(255,255,255,0.07);
		}
		.custom_scrollbar::-webkit-scrollbar-button {
			width: 0;
			height: 0;
			display: none;
		}
		.custom_scrollbar::-webkit-scrollbar-corner {
			background-color: transparent;
		}
		.chatbox{
			background-color: black;
			color: white;
			display: inline-block;
			border: 1px solid white;
			width: 75%;
			height: 20rem;
			padding: 2px;
			resize: none;
		}
		.logged_user{
			right: 0;
			vertical-align: top;
			display: inline-block;
			border: 1px solid white;
			width: 20%;
			height: 20rem;
			padding: 2px;
		}
		.message{
			vertical-align: top;
			box-sizing: content-box;
			background-color: black;
			color: white;
			display: inline-block;
			border: 1px solid white;
			width: 100%;
			margin-top: 3%;
			outline: none;
			padding: 2px;
		}
		.chat{
			width: 50rem;
			margin: auto;
			margin-top: 5rem;
		}
		span{
			display: block;
		}
	</style>
</head>
<body>
	<username style="display: none">null</username>
	<h1 id="list" style="color: white; display:none">-</h1>
	<div class="chat">
		<h1>Chat 1</h1>
		<textarea class="custom_scrollbar chatbox" readonly='readonly' chatId="1"></textarea>
		<div class="custom_scrollbar logged_user" chatId="1">
		</div>
		<input type='text' class="message" chatId="1"/>
	</div>
</body>
</html>