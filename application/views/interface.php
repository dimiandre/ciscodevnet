<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<title>C#at - Empsol srl</title>
		<base href="<?php echo(base_url());?>">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		<link rel="stylesheet/less" type="text/css" href="css/style.less" />
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="js/custom.js"></script>
		<script src="js/less.min.js" charset="utf-8"></script>
	</head>
	<body>
		<div class="body-container">
			<header>
				<strong>Empsol srl - CISCO CMX</strong>
				<input type='text' name="sendMessage" maxlength="200" class="message" chatId="1" placeholder="Send a message..."/>
				<?php
					/*
					echo('<pre>');
					print_r($this->_ci_cached_vars);
					echo('</pre>');
					*/
				?>
			</header>
			<div id="macaddress" class="hidden" ><?php echo $macAddress; ?></div>
			<username><?php echo $macAddress; ?></username>
		</div>
		<div class="canvas">
			<div class="avatargroup">
				<div class="avatar popIn user_active" data-mac="<?php echo($macAddress);?>" data-x="0" data-y="0" <?php echo($img != '' ? 'style="background-image: url('.$img.');"' : '');?>>
					<form method="post" action="upload" enctype="multipart/form-data">
						<input type="file" name="avatar_image"/>
						<button type="submit">Invia</button>
						<script type="text/javascript">
							$('input[name="avatar_image"]').change(function(){
								$(this).parent().submit();
							});
						</script>
					</form>
					<div class="message"><span class="label">Message:</span><span class="values"></span></div>
					<div class="tooltip">
						<span class="label">MacAddress</span><span class="values"><?php echo($macAddress);?></span>
						<span class="label">Coordinates</span><span class="values">X <?php echo 0;?> : Y <?php echo 0;?></span>
					</div>
				</div>
				<?php
					foreach ($arrayPeople as $macAddressP => $coordinates) {
						?>
							<div class="avatar popIn hide" data-mac="<?php echo($macAddressP);?>" data-x="<?php echo $coordinates['x'];?>" data-y="<?php echo $coordinates['y'];?>" <?php echo($coordinates['img'] != '' ? 'style="background-image: url('.$coordinates['img'].');"' : '');?>>
								<div class="message"><span class="label">Message:</span><span class="values"></span></div>
								<div class="tooltip">
									<span class="label">MacAddress</span><span class="values"><?php echo($macAddressP);?></span>
									<span class="label">Coordinates</span><span class="values cordinates">X <?php echo($coordinates['x']);?> : Y <?php echo $coordinates['y'];?></span>
								</div>
							</div>
						<?php
					}
				?>
			</div>
			<div class="backgroundgroup">
				<?php $circle_size = 150;?>
				<div class="circle" data-size="<?php echo($circle_size*1);?>"></div>
				<div class="circle x2" data-size="<?php echo($circle_size*2);?>"></div>
				<div class="circle x3" data-size="<?php echo($circle_size*3);?>"></div>
				<div class="circle x4" data-size="<?php echo($circle_size*4);?>"></div>
				<div class="circle radarAnimation" data-size="<?php echo($circle_size*4);?>"></div>
			</div>

			<!--<div class="toolsButtons">
				<div class="tagButtons">
					<div class="button resetTag" title="Reset Hashtags">#</div>
				</div>
			</div>-->

			<div class="interfaceButtons">

				<div class="zoomButtons">

					<div class="button zoomIn" title="Zoom In">+</div>
					<div class="button reset" title="Zoom reset">=</div>
					<div class="button zoomOut" title="Zoom Out">-</div>
				</div>
			</div>
			<div class="interfaceCount">
				<div>
					<input type="checkbox" name="show_no_websocket" checked="checked"/> Show inactive Host
					<script type="text/javascript">
						jQuery(document).ready(function($) {
							$('[name="show_no_websocket"]').change(function(){
								if($(this).is(':checked')){
									$('.canvas').removeClass('hideInactive');
								}else{
									$('.canvas').addClass('hideInactive');
								}
							});
						});
					</script>
				</div>
				<div>
					<?php
						$count = count($arrayPeople);
						echo('<span>'.$count.'</span> devices detected');
					?>
				</div>
			</div>
		</div>
		<div class="chatRoom" id="chatRoom">
			<h3>Rooms</h3>
			<ul class="rooms">
			</ul>
			<h3>Chat History</h3>
			<ul class="chat_history">
			</ul>
		</div>
		<script>
			screenHeight = 0;
			screenWidth = 0;
			scale_factor = 2;

			scale_factor_original = scale_factor;

			setPositionAtCordinates = function(div,x,y,img){
				var maxCordX = screenWidth/2;
				var maxCordY = screenHeight/2;

				var newY = maxCordY+((y*scale_factor*(-1)));
				var newX = maxCordX+(x*scale_factor);

				//Converto in percentuale
				var percX = newX*100/screenWidth;
				var percY = newY*100/screenHeight;

				div.css({left:percX+'%',top:percY+'%'});
				div.find('.cordinates').text('X '+parseInt(newX)+' : Y '+parseInt(newY)+'');
				if(img != '' && img != undefined){
					div.css({'background-image':'url('+img+')'});
				}
			};
			resizeCircles = function(){
				$('.circle').each(function(index){
					if($(this).is('.radarAnimation')){
						$(this).css({
							'width': parseInt($(this).attr('data-size'))*3,
							'height': parseInt($(this).attr('data-size'))*3
						});
					}else{
						$(this).css({
							'width': parseInt($(this).attr('data-size'))*scale_factor,
							'height': parseInt($(this).attr('data-size'))*scale_factor
						});
					}
				});
			}

			$('.zoomButtons .button').click(function(){
				if($(this).is('.zoomIn')){
					scale_factor += 0.1;
				}else{
					if($(this).is('.reset')){
						scale_factor = scale_factor_original;
					}else{
						if(scale_factor >= 0.2){
							scale_factor -= 0.1;
						}
					}
				}
				resizeCircles();
				$(window).trigger('resize');
			});

			$(window).resize(function(){
				screenHeight = $('.canvas').height();
				screenWidth = $('.canvas').width();
				resizeCircles();
				$('.avatar').each(function(index){
					setPositionAtCordinates($(this),parseInt($(this).attr('data-x')),parseInt($(this).attr('data-y')));
				});
			}).trigger('resize');

			elementi_iniziali = $('.avatar.hide').length;
			var index = 0;
			var speed = parseInt(2000/$('.avatar.hide').length);
			var intervallo = setInterval(function(){
				$('.avatar.hide:eq('+index+')').removeClass('hide');
				index++;
				if(index+1 == elementi_iniziali){
					clearInterval(intervallo);
					$('.avatar.hide').removeClass('hide');
				}
			}, speed);

			setInterval(function(){
				$.ajax({
					url:"ajax/getDistances",
					type: "POST",
					data: {},
					cache: false,
					async: true,
					success:function(result){
						if( typeof someVar === 'array' ) {
							$('.interfaceCount span').text(result.count());
						}
						for(i in result){
							if($('[data-mac="'+i+'"]').length > 0){
								//C'è già la posizione
								setPositionAtCordinates($('.avatar[data-mac="'+i+'"]'),parseInt(result[i]['x']),parseInt(result[i]['y']),result[i]['img']);
							}else{
								//Bisogna creare l'elemento
								var image_url = result[i]['img'] != '' ? 'style="background-image: url('+result[i]['img']+');"' : '';

								var html = '<div class="avatar popIn hide" data-mac="'+i+'" data-x="'+parseInt(result[i]['x'])+'" data-y="'+parseInt(result[i]['y'])+'" '+image_url+'>';
								html+= '	<div class="message"><span class="label">Message:</span><span class="values"></span></div>';
								html+= '	<div class="tooltip">';
								html+= '		<span class="label">MacAddress</span><span class="values">'+i+'</span>';
								html+= '		<span class="label">Coordinates</span><span class="values cordinates">X '+parseInt(result[i]['x'])+' : Y '+parseInt(result[i]['y'])+'</span>';
								html+= '	</div>';
								html+= '</div>';
								$('.avatargroup').append('</div>');
								$('.avatargroup .avatar.hide').removeClass('hide');
							}
						}
					},
					error: function(richiesta,stato,errori){
						console.log("AJAX ERROR: "+stato+" "+errori);
					}
				});
			}, 2500);

			$('body').delegate('.avatar','click',function(){
				if($(this).is('.active')){
					$(this).removeClass('active');
				}else{
					$(this).addClass('active');
				}
			});
		</script>
		<img src="img/giphy.gif" style="width: 0; height:0; opacity: 0;">
	</body>
</html>