<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Ajax extends MY_Controller {


	function __construct(){
		parent::__construct();
		$this->load->model("host");
		$this->load->model("host_info");
	}

	public function registerHost($data = NULL)	{
		if(!is_null($data)){
			$_POST['private_ip'] = $data;
		}
		if (isset($_POST['private_ip'])) {

			$this->load->model(array('group',('group_info')));
			$group = new Group_info($_SERVER['REMOTE_ADDR']);
			$host = new Host_info($_POST['private_ip'], $group->get_id_group());

			$array = array(
				'private_ip' => $host->get_private_id(),
				'host' => $host,
				'group' => $group
			);
			$this->output
				->set_content_type('application/json')
				->set_output(
					json_encode(array(
						'code' => 0,
						'message' => 'Success',
						'data' => $array
					))
				);
		} else {
			$this->output
				->set_content_type('application/json')
				->set_output(
					json_encode(array(
						'code' => -1,
						'message' => 'Ajax Call Aborted CODE 1',
						'data' => array()
					))
				);
		}
	}
	public function getHost(){
		$this->output
			->set_content_type('application/json')
			->set_output(
				json_encode(array(
					'code' => 0,
					'message' => 'Success',
					'data' => array('10.10.30.49')
				))
			);
	}
}