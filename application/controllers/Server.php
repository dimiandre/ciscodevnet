<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Server extends MY_Controller {
	
	
	function __construct(){
		parent::__construct();
		$this->load->library("WebSocket");
	}

	public function run() {
		// prevent the server from timing out
		set_time_limit(0);
		
		// start the server
		$this->websocket->wsBind('message', 'wsOnMessage');
		$this->websocket->wsBind('open', 'wsOnOpen');
		$this->websocket->wsBind('close', 'wsOnClose');
		// for other computers to connect, you will probably need to change this to your LAN IP or external IP,
		// alternatively use: gethostbyaddr(gethostbyname($_SERVER['SERVER_NAME']))
		//$this->websocket->wsStartServer(gethostbyaddr(gethostbyname($_SERVER['HOSTNAME'])), 9300);
		$this->websocket->wsStartServer(WS_HOST, 9300);
	}
	public function stop(){
		//$this->websocket->wsStopServer('192.168.0.108', 9300);
		$this->websocket->wsStopServer(WS_HOST, 9300);
	}
	public function getHost(){
		echo '89.186.94.197';
	}
}