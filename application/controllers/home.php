<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	public function index(){
		$this->load->model(array('group',('group_info')));
		$group = new Group_info($_SERVER['REMOTE_ADDR']);

		$this->startPage();
		$this->load->view('registerJs');
		$this->endPage();
	}

	public function chatroom()	{
		$this->load->view('chatroom');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */