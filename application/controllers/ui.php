<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ui extends MY_Controller {


	public function index(){
		//$this->load->view('haked');
		$data = array("macAddress" => $this->getClientMAC($_SERVER['REMOTE_ADDR']), "currentIp" => $_SERVER['REMOTE_ADDR'], "arrayPeople" => $this->getDistances(false), "img" => $this->getImg(), "debug" => $this->debug_text );

		$this->load->view('interface', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */