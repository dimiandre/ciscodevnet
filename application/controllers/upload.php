<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends MY_Controller {
	public function index() {
		$config['upload_path'] = '/var/www/html/img/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_width']  = '1920';
		$config['max_height']  = '1080';
		$config['file_name'] = md5($this->getClientMAC($_SERVER['REMOTE_ADDR'])).".jpg";
		$config['overwrite'] = true;
		
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('avatar_image'))
		{
			$error = array('error' => $this->upload->display_errors());
			echo "<pre>";
			var_dump($error);
			echo "</pre>";
			//$this->load->view('upload_form', $error);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			
			header("location: index.php");
			header('Cache-Control: no-cache');
			header('Pragma: no-cache');
			die();
			//$this->load->view('upload_success', $data);
		}
	}
}