<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
	Based on PHP WebSocket Server 0.2
	 - http://code.google.com/p/php-websocket-server/
	 - http://code.google.com/p/php-websocket-server/wiki/Scripting

	WebSocket Protocol 07
	 - http://tools.ietf.org/html/draft-ietf-hybi-thewebsocketprotocol-07
	 - Supported by Firefox 6 (30/08/2011)

	Whilst a big effort is made to follow the protocol documentation, the current script version may unknowingly differ.
	Please report any bugs you may find, all feedback and questions are welcome!
*/


class WebSocket {
	// maximum amount of clients that can be connected at one time
	const WS_MAX_CLIENTS = 100;

	// maximum amount of clients that can be connected at one time on the same IP v4 address
	const WS_MAX_CLIENTS_PER_IP = 15;

	// amount of seconds a client has to send data to the server, before a ping request is sent to the client,
	// if the client has not completed the opening handshake, the ping request is skipped and the client connection is closed
	const WS_TIMEOUT_RECV = 10;

	// amount of seconds a client has to reply to a ping request, before the client connection is closed
	const WS_TIMEOUT_PONG = 5;

	// the maximum length, in bytes, of a frame's payload data (a message consists of 1 or more frames), this is also internally limited to 2,147,479,538
	const WS_MAX_FRAME_PAYLOAD_RECV = 100000;

	// the maximum length, in bytes, of a message's payload data, this is also internally limited to 2,147,483,647
	const WS_MAX_MESSAGE_PAYLOAD_RECV = 500000;




	// internal
	const WS_FIN =  128;
	const WS_MASK = 128;

	const WS_OPCODE_CONTINUATION = 0;
	const WS_OPCODE_TEXT =         1;
	const WS_OPCODE_BINARY =       2;
	const WS_OPCODE_CLOSE =        8;
	const WS_OPCODE_PING =         9;
	const WS_OPCODE_PONG =         10;

	const WS_PAYLOAD_LENGTH_16 = 126;
	const WS_PAYLOAD_LENGTH_63 = 127;

	const WS_READY_STATE_CONNECTING = 0;
	const WS_READY_STATE_OPEN =       1;
	const WS_READY_STATE_CLOSING =    2;
	const WS_READY_STATE_CLOSED =     3;

	const WS_STATUS_NORMAL_CLOSE =             1000;
	const WS_STATUS_GONE_AWAY =                1001;
	const WS_STATUS_PROTOCOL_ERROR =           1002;
	const WS_STATUS_UNSUPPORTED_MESSAGE_TYPE = 1003;
	const WS_STATUS_MESSAGE_TOO_BIG =          1004;

	const WS_STATUS_TIMEOUT = 3000;

	const USER_NAME_PLACEHOLDER = "[[USERNAME]]";

	// global vars
	public $wsClients       = array();
	public $wsRead          = array();
	public $wsClientCount   = 0;
	public $wsClientIPCount = array();
	public $wsOnEvents      = array();
	public $wsPoolMessage	= array();

	/*
		$this->wsClients[ integer ClientID ] = array(
			0 => resource  Socket,						// client socket
			1 => string    MessageBuffer,				// a blank string when there's no incoming frames
			2 => integer   ReadyState,					// between 0 and 3
			3 => integer   LastRecvTime,				// set to time() when the client is added
			4 => int/false PingSentTime,				// false when the server is not waiting for a pong
			5 => int/false CloseStatus,					// close status that wsOnClose() will be called with
			6 => integer   IPv4,						// client's IP stored as a signed long, retrieved from ip2long()
			7 => int/false FramePayloadDataLength,		// length of a frame's payload data, reset to false when all frame data has been read (cannot reset to 0, to allow reading of mask key)
			8 => integer   FrameBytesRead,				// amount of bytes read for a frame, reset to 0 when all frame data has been read
			9 => string    FrameBuffer,					// joined onto end as a frame's data comes in, reset to blank string when all frame data has been read
			10 => integer  MessageOpcode,				// stored by the first frame for fragmented messages, default value is 0
			11 => integer  MessageBufferLength			// the payload data length of MessageBuffer
			12 => integer  Relative ChatID				// the chat id which connection refers to, default to 0
			13 => string   Relative UserName			// the username which connection refers to, default to ''
			14 => boolean  Initialized Chat				// set 1 if chat has initialized, default to 0
		)

		$wsRead[ integer ClientID ] = resource Socket         // this one-dimensional array is used for socket_select()
															  // $wsRead[ 0 ] is the socket listening for incoming client connections

		$wsClientCount = integer ClientCount                  // amount of clients currently connected

		$wsClientIPCount[ integer IP ] = integer ClientCount  // amount of clients connected per IP v4 address
	*/

	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->model("wsclient");
		$this->CI->load->model("group");
	}


	// server state functions
	function wsStartServer($host, $port) {
		if (isset($this->wsRead[0])) return false;

		if (!$this->wsRead[0] = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {
			return false;
		}
		if (!socket_set_option($this->wsRead[0], SOL_SOCKET, SO_REUSEADDR, 1)) {
			socket_close($this->wsRead[0]);
			return false;
		}
		if (!socket_bind($this->wsRead[0], $host, $port)) {
			socket_close($this->wsRead[0]);
			return false;
		}
		if (!socket_listen($this->wsRead[0], 10)) {
			socket_close($this->wsRead[0]);
			return false;
		}

		$write = array();
		$except = array();

		$nextPingCheck = time() + 1;
		while (isset($this->wsRead[0])) {
			$changed = $this->wsRead;
			$result = socket_select($changed, $write, $except, 1);

			if ($result === false) {
				socket_close($this->wsRead[0]);
				return false;
			}
			elseif ($result > 0) {
				foreach ($changed as $clientID => $socket) {
					if ($clientID != 0) {
						// client socket changed
						$buffer = '';
						$bytes = @socket_recv($socket, $buffer, 4096, 0);

						if ($bytes === false) {
							// error on recv, remove client socket (will check to send close frame)
							$this->wsSendClientClose($clientID, self::WS_STATUS_PROTOCOL_ERROR);
						}
						elseif ($bytes > 0) {
							// process handshake or frame(s)
							if (!$this->wsProcessClient($clientID, $buffer, $bytes)) {
								$this->wsSendClientClose($clientID, self::WS_STATUS_PROTOCOL_ERROR);
							}
						}
						else {
							// 0 bytes received from client, meaning the client closed the TCP connection
							$this->wsRemoveClient($clientID);
						}
					}
					else {
						// listen socket changed
						$client = socket_accept($this->wsRead[0]);
						if ($client !== false) {
							// fetch client IP as integer
							$clientIP = '';
							$result = socket_getpeername($client, $clientIP);
							$clientIP = ip2long($clientIP);

							if ($result !== false && $this->wsClientCount < self::WS_MAX_CLIENTS && (!isset($this->wsClientIPCount[$clientIP]) || $this->wsClientIPCount[$clientIP] < self::WS_MAX_CLIENTS_PER_IP)) {
								$this->wsAddClient($client, $clientIP);
							}
							else {
								socket_close($client);
							}
						}
					}
				}
			}

			if (time() >= $nextPingCheck) {
				$this->wsCheckIdleClients();
				$nextPingCheck = time() + 1;
			}
		}

		return true; // returned when wsStopServer() is called
	}
	function wsStopServer() {
		// check if server is not running
		if (!isset($this->wsRead[0])) return false;

		// close all client connections
		foreach ($this->wsClients as $clientID => $client) {
			// if the client's opening handshake is complete, tell the client the server is 'going away'
			if ($client->get_ReadyState() != self::WS_READY_STATE_CONNECTING) {
				$this->wsSendClientClose($clientID, self::WS_STATUS_GONE_AWAY);
			}
			socket_close($client->get_Socket());
		}

		// close the socket which listens for incoming clients
		socket_close($this->wsRead[0]);

		// reset variables
		$this->wsRead          = array();
		$this->wsClients       = array();
		$this->wsClientCount   = 0;
		$this->wsClientIPCount = array();

		return true;
	}

	// client timeout functions
	function wsCheckIdleClients() {
		$time = time();
		foreach ($this->wsClients as $clientID => $client) {
			if ($client->get_ReadyState() != self::WS_READY_STATE_CLOSED) {
				// client ready state is not closed
				if ($client->get_PingSentTime() !== false) {
					// ping request has already been sent to client, pending a pong reply
					if ($time >= $client->get_PingSentTime() + self::WS_TIMEOUT_PONG) {
						// client didn't respond to the server's ping request in self::WS_TIMEOUT_PONG seconds
						$this->wsSendClientClose($clientID, self::WS_STATUS_TIMEOUT);
						$this->wsRemoveClient($clientID);
					}
				}
				elseif ($time >= $client->get_LastRecvTime() + self::WS_TIMEOUT_RECV) {
					// last data was received >= self::WS_TIMEOUT_RECV seconds ago
					if ($client->get_ReadyState() != self::WS_READY_STATE_CONNECTING) {
						// client ready state is open or closing
						$this->wsClients[$clientID]->set_PingSentTime(time());
						$this->wsSendClientMessage($clientID, self::WS_OPCODE_PING, '');
					}
					else {
						// client ready state is connecting
						$this->wsRemoveClient($clientID);
					}
				}
			}
		}
	}

	// client existence functions
	function wsAddClient($socket, $clientIP) {
		// increase amount of clients connected
		$this->wsClientCount++;

		// increase amount of clients connected on this client's IP
		if (isset($this->wsClientIPCount[$clientIP])) {
			$this->wsClientIPCount[$clientIP]++;
		}
		else {
			$this->wsClientIPCount[$clientIP] = 1;
		}

		// fetch next client ID
		$clientID = $this->wsGetNextClientID();

		// store initial client data
		$this->wsClients[$clientID] = new wsclient(array("Socket" => $socket, "ReadyState" => self::WS_READY_STATE_CONNECTING, "LastRecvTime" => time(), "IPv4" => $clientIP));

		// store socket - used for socket_select()
		$this->wsRead[$clientID] = $socket;
	}
	function wsRemoveClient($clientID) {
		// fetch close status (which could be false), and call wsOnClose
		$closeStatus = $this->wsClients[$clientID]->get_CloseStatus();
		if ( array_key_exists('close', $this->wsOnEvents) )
			foreach ( $this->wsOnEvents['close'] as $func )
				$this->$func($clientID, $closeStatus);

		// close socket
		$socket = $this->wsClients[$clientID]->get_Socket();
		socket_close($socket);

		// decrease amount of clients connected on this client's IP
		$clientIP = $this->wsClients[$clientID]->get_IPv4();
		if ($this->wsClientIPCount[$clientIP] > 1) {
			$this->wsClientIPCount[$clientIP]--;
		}
		else {
			unset($this->wsClientIPCount[$clientIP]);
		}

		// decrease amount of clients connected
		$this->wsClientCount--;

		// remove socket and client data from arrays
		unset($this->wsRead[$clientID], $this->wsClients[$clientID]);
	}

	// client data functions
	function wsGetNextClientID() {
		$i = 1; // starts at 1 because 0 is the listen socket
		while (isset($this->wsRead[$i])) $i++;
		return $i;
	}

	// useless function?
	function wsGetClientSocket($clientID) {
		return $this->wsClients[$clientID]->get_Socket();
	}

	// client read functions
	function wsProcessClient($clientID, &$buffer, $bufferLength) {
		if ($this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_OPEN) {
			// handshake completed
			$result = $this->wsBuildClientFrame($clientID, $buffer, $bufferLength);
		}
		elseif ($this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CONNECTING) {
			// handshake not completed
			$result = $this->wsProcessClientHandshake($clientID, $buffer);
			if ($result) {
				$this->wsClients[$clientID]->set_ReadyState(self::WS_READY_STATE_OPEN);

				$data = ''; //to prevent fault
				if ( array_key_exists('open', $this->wsOnEvents) )
					foreach ( $this->wsOnEvents['open'] as $func )
						$this->$func($clientID);
			}
		}
		else {
			// ready state is set to closed
			$result = false;
		}

		return $result;
	}
	function wsBuildClientFrame($clientID, &$buffer, $bufferLength) {
		// increase number of bytes read for the frame, and join buffer onto end of the frame buffer
		$this->wsClients[$clientID]->add_FrameBytesRead($bufferLength);
		$this->wsClients[$clientID]->concat_FrameBuffer($buffer);

		// check if the length of the frame's payload data has been fetched, if not then attempt to fetch it from the frame buffer
		if ($this->wsClients[$clientID]->get_FramePayloadDataLength() !== false || $this->wsCheckSizeClientFrame($clientID) == true) {
			// work out the header length of the frame
			$headerLength = ($this->wsClients[$clientID]->get_FramePayloadDataLength() <= 125 ? 0 : ($this->wsClients[$clientID]->get_FramePayloadDataLength() <= 65535 ? 2 : 8)) + 6;

			// check if all bytes have been received for the frame
			$frameLength = $this->wsClients[$clientID]->get_FramePayloadDataLength() + $headerLength;
			if ($this->wsClients[$clientID]->get_FrameBytesRead() >= $frameLength) {
				// check if too many bytes have been read for the frame (they are part of the next frame)
				$nextFrameBytesLength = $this->wsClients[$clientID]->get_FrameBytesRead() - $frameLength;
				if ($nextFrameBytesLength > 0) {
					$this->wsClients[$clientID]->sub_FrameBytesRead($nextFrameBytesLength);
					$nextFrameBytes = substr($this->wsClients[$clientID]->get_FrameBuffer(), $frameLength);
					$this->wsClients[$clientID]->set_FrameBuffer(substr($this->wsClients[$clientID]->get_FrameBuffer(), 0, $frameLength));
				}

				// process the frame
				$result = $this->wsProcessClientFrame($clientID);

				// check if the client wasn't removed, then reset frame data
				if (isset($this->wsClients[$clientID])) {
					$this->wsClients[$clientID]->set_FramePayloadDataLength(false);
					$this->wsClients[$clientID]->set_FrameBytesRead(0);
					$this->wsClients[$clientID]->set_FrameBuffer('');
				}

				// if there's no extra bytes for the next frame, or processing the frame failed, return the result of processing the frame
				if ($nextFrameBytesLength <= 0 || !$result) return $result;

				// build the next frame with the extra bytes
				return $this->wsBuildClientFrame($clientID, $nextFrameBytes, $nextFrameBytesLength);
			}
		}

		return true;
	}
	function wsCheckSizeClientFrame($clientID) {
		// check if at least 2 bytes have been stored in the frame buffer
		if ($this->wsClients[$clientID]->get_FrameBytesRead() > 1) {
			// fetch payload length in byte 2, max will be 127
			$payloadLength = ord(substr($this->wsClients[$clientID]->get_FrameBuffer(), 1, 1)) & 127;

			if ($payloadLength <= 125) {
				// actual payload length is <= 125
				$this->wsClients[$clientID]->set_FramePayloadDataLength($payloadLength);
			}
			elseif ($payloadLength == 126) {
				// actual payload length is <= 65,535
				if (substr($this->wsClients[$clientID]->get_FrameBuffer(), 3, 1) !== false) {
					// at least another 2 bytes are set
					$payloadLengthExtended = substr($this->wsClients[$clientID]->get_FrameBuffer(), 2, 2);
					$array = unpack('na', $payloadLengthExtended);
					$this->wsClients[$clientID]->set_FramePayloadDataLength($array['a']);
				}
			}
			else {
				// actual payload length is > 65,535
				if (substr($this->wsClients[$clientID]->get_FrameBuffer(), 9, 1) !== false) {
					// at least another 8 bytes are set
					$payloadLengthExtended = substr($this->wsClients[$clientID]->get_FrameBuffer(), 2, 8);

					// check if the frame's payload data length exceeds 2,147,483,647 (31 bits)
					// the maximum integer in PHP is "usually" this number. More info: http://php.net/manual/en/language.types.integer.php
					$payloadLengthExtended32_1 = substr($payloadLengthExtended, 0, 4);
					$array = unpack('Na', $payloadLengthExtended32_1);
					if ($array['a'] != 0 || ord(substr($payloadLengthExtended, 4, 1)) & 128) {
						$this->wsSendClientClose($clientID, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}

					// fetch length as 32 bit unsigned integer, not as 64 bit
					$payloadLengthExtended32_2 = substr($payloadLengthExtended, 4, 4);
					$array = unpack('Na', $payloadLengthExtended32_2);

					// check if the payload data length exceeds 2,147,479,538 (2,147,483,647 - 14 - 4095)
					// 14 for header size, 4095 for last recv() next frame bytes
					if ($array['a'] > 2147479538) {
						$this->wsSendClientClose($clientID, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}

					// store frame payload data length
					$this->wsClients[$clientID]->set_FramePayloadDataLength($array['a']);
				}
			}

			// check if the frame's payload data length has now been stored
			if ($this->wsClients[$clientID]->get_FramePayloadDataLength() !== false) {

				// check if the frame's payload data length exceeds self::WS_MAX_FRAME_PAYLOAD_RECV
				if ($this->wsClients[$clientID]->get_FramePayloadDataLength() > self::WS_MAX_FRAME_PAYLOAD_RECV) {
					$this->wsClients[$clientID]->set_FramePayloadDataLength(false);
					$this->wsSendClientClose($clientID, self::WS_STATUS_MESSAGE_TOO_BIG);
					return false;
				}

				// check if the message's payload data length exceeds 2,147,483,647 or self::WS_MAX_MESSAGE_PAYLOAD_RECV
				// doesn't apply for control frames, where the payload data is not internally stored
				$controlFrame = (ord(substr($this->wsClients[$clientID]->get_FrameBuffer(), 0, 1)) & 8) == 8;
				if (!$controlFrame) {
					$newMessagePayloadLength = $this->wsClients[$clientID]->get_MessageBufferLength() + $this->wsClients[$clientID]->get_FramePayloadDataLength();
					if ($newMessagePayloadLength > self::WS_MAX_MESSAGE_PAYLOAD_RECV || $newMessagePayloadLength > 2147483647) {
						$this->wsSendClientClose($clientID, self::WS_STATUS_MESSAGE_TOO_BIG);
						return false;
					}
				}

				return true;
			}
		}

		return false;
	}
	function wsProcessClientFrame($clientID) {
		// store the time that data was last received from the client
		$this->wsClients[$clientID]->set_LastRecvTime(time());

		// fetch frame buffer
		$buffer = $this->wsClients[$clientID]->get_FrameBuffer();

		// check at least 6 bytes are set (first 2 bytes and 4 bytes for the mask key)
		if (substr($buffer, 5, 1) === false) return false;

		// fetch first 2 bytes of header
		$octet0 = ord(substr($buffer, 0, 1));
		$octet1 = ord(substr($buffer, 1, 1));

		$fin = $octet0 & self::WS_FIN;
		$opcode = $octet0 & 15;

		$mask = $octet1 & self::WS_MASK;
		if (!$mask) return false; // close socket, as no mask bit was sent from the client

		// fetch byte position where the mask key starts
		$seek = $this->wsClients[$clientID]->get_FramePayloadDataLength() <= 125 ? 2 : ($this->wsClients[$clientID]->get_FramePayloadDataLength() <= 65535 ? 4 : 10);

		// read mask key
		$maskKey = substr($buffer, $seek, 4);

		$array = unpack('Na', $maskKey);
		$maskKey = $array['a'];
		$maskKey = array(
			$maskKey >> 24,
			($maskKey >> 16) & 255,
			($maskKey >> 8) & 255,
			$maskKey & 255
		);
		$seek += 4;

		// decode payload data
		if (substr($buffer, $seek, 1) !== false) {
			$data = str_split(substr($buffer, $seek));
			foreach ($data as $key => $byte) {
				$data[$key] = chr(ord($byte) ^ ($maskKey[$key % 4]));
			}
			$data = implode('', $data);
		}
		else {
			$data = '';
		}

		// check if this is not a continuation frame and if there is already data in the message buffer
		if ($opcode != self::WS_OPCODE_CONTINUATION && $this->wsClients[$clientID]->get_MessageBufferLength() > 0) {
			// clear the message buffer
			$this->wsClients[$clientID]->set_MessageBufferLength(0);
			$this->wsClients[$clientID]->set_MessageBuffer('');
		}

		// check if the frame is marked as the final frame in the message
		if ($fin == self::WS_FIN) {
			// check if this is the first frame in the message
			if ($opcode != self::WS_OPCODE_CONTINUATION) {
				// process the message
				return $this->wsProcessClientMessage($clientID, $opcode, $data, $this->wsClients[$clientID]->get_FramePayloadDataLength());
			}
			else {
				// increase message payload data length
				$this->wsClients[$clientID]->add_MessageBufferLength($this->wsClients[$clientID]->get_FramePayloadDataLength());

				// push frame payload data onto message buffer
				$this->wsClients[$clientID]->concat_MessageBuffer($data);

				// process the message
				$result = $this->wsProcessClientMessage($clientID, $this->wsClients[$clientID]->get_MessageOpcode(), $this->wsClients[$clientID]->get_MessageBuffer(), $this->wsClients[$clientID]->get_MessageBufferLength());

				// check if the client wasn't removed, then reset message buffer and message opcode
				if (isset($this->wsClients[$clientID])) {
					$this->wsClients[$clientID]->set_MessageBuffer('')
						->set_MessageOpcode(0)
						->set_MessageBufferLength(0);
				}

				return $result;
			}
		}
		else {
			// check if the frame is a control frame, control frames cannot be fragmented
			if ($opcode & 8) return false;

			// increase message payload data length
			$this->wsClients[$clientID]->add_MessageBufferLength($this->wsClients[$clientID]->get_FramePayloadDataLength());

			// push frame payload data onto message buffer
			$this->wsClients[$clientID]->concat_MessageBuffer($data);

			// if this is the first frame in the message, store the opcode
			if ($opcode != self::WS_OPCODE_CONTINUATION) {
				$this->wsClients[$clientID]->set_MessageOpcode($opcode);
			}
		}

		$this->wsClients[$clientID]->set_FrameBuffer($buffer);
		return true;
	}
	function wsProcessClientMessage($clientID, $opcode, &$data, $dataLength) {
		// check opcodes
		if ($opcode == self::WS_OPCODE_PING) {
			// received ping message
			return $this->wsSendClientMessage($clientID, self::WS_OPCODE_PONG, $data);
		}
		elseif ($opcode == self::WS_OPCODE_PONG) {
			// received pong message (it's valid if the server did not send a ping request for this pong message)
			if ($this->wsClients[$clientID]->get_PingSentTime() !== false) {
				$this->wsClients[$clientID]->set_PingSentTime(false);
			}
		}
		elseif ($opcode == self::WS_OPCODE_CLOSE) {
			// received close message
			if (substr($data, 1, 1) !== false) {
				$array = unpack('na', substr($data, 0, 2));
				$status = $array['a'];
			}
			else {
				$status = false;
			}

			if ($this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CLOSING) {
				// the server already sent a close frame to the client, this is the client's close frame reply
				// (no need to send another close frame to the client)
				$this->wsClients[$clientID]->set_ReadyState(self::WS_READY_STATE_CLOSED);
			}
			else {
				// the server has not already sent a close frame to the client, send one now
				$this->wsSendClientClose($clientID, self::WS_STATUS_NORMAL_CLOSE);
			}

			$this->wsRemoveClient($clientID);
		}
		elseif ($opcode == self::WS_OPCODE_TEXT || $opcode == self::WS_OPCODE_BINARY) {
			if ( array_key_exists('message', $this->wsOnEvents) )
				foreach ( $this->wsOnEvents['message'] as $func )
					$this->$func($clientID, $data, $dataLength, $opcode == self::WS_OPCODE_BINARY);
		}
		else {
			// unknown opcode
			return false;
		}

		return true;
	}
	function wsProcessClientHandshake($clientID, &$buffer) {
		// fetch headers and request line
		$sep = strpos($buffer, "\r\n\r\n");
		if (!$sep) return false;

		$headers = explode("\r\n", substr($buffer, 0, $sep));
		$headersCount = sizeof($headers); // includes request line
		if ($headersCount < 1) return false;

		// fetch request and check it has at least 3 parts (space tokens)
		$request = &$headers[0];
		$requestParts = explode(' ', $request);
		$requestPartsSize = sizeof($requestParts);
		if ($requestPartsSize < 3) return false;

		// check request method is GET
		if (strtoupper($requestParts[0]) != 'GET') return false;

		// check request HTTP version is at least 1.1
		$httpPart = &$requestParts[$requestPartsSize - 1];
		$httpParts = explode('/', $httpPart);
		if (!isset($httpParts[1]) || (float) $httpParts[1] < 1.1) return false;

		// store headers into a keyed array: array[headerKey] = headerValue
		$headersKeyed = array();
		for ($i=1; $i<$headersCount; $i++) {
			$parts = explode(':', $headers[$i]);
			if (!isset($parts[1])) return false;

			$headersKeyed[trim($parts[0])] = trim($parts[1]);
		}

		// check Host header was received
		if (!isset($headersKeyed['Host'])) return false;

		// check Sec-WebSocket-Key header was received and decoded value length is 16
		if (!isset($headersKeyed['Sec-WebSocket-Key'])) return false;
		$key = $headersKeyed['Sec-WebSocket-Key'];
		if (strlen(base64_decode($key)) != 16) return false;

		// check Sec-WebSocket-Version header was received and value is 7
		if (!isset($headersKeyed['Sec-WebSocket-Version']) || (int) $headersKeyed['Sec-WebSocket-Version'] < 7) return false; // should really be != 7, but Firefox 7 beta users send 8

		// work out hash to use in Sec-WebSocket-Accept reply header
		$hash = base64_encode(sha1($key.'258EAFA5-E914-47DA-95CA-C5AB0DC85B11', true));

		// build headers
		$headers = array(
			'HTTP/1.1 101 Switching Protocols',
			'Upgrade: websocket',
			'Connection: Upgrade',
			'Sec-WebSocket-Accept: '.$hash
		);
		$headers = implode("\r\n", $headers)."\r\n\r\n";

		// send headers back to client
		$socket = $this->wsClients[$clientID]->get_Socket();

		$left = strlen($headers);
		do {
			$sent = @socket_send($socket, $headers, $left, 0);
			if ($sent === false) return false;

			$left -= $sent;
			if ($sent > 0) $headers = substr($headers, $sent);
		}
		while ($left > 0);

		return true;
	}

	// client write functions
	function wsSendClientMessage($clientID, $opcode, $message) {
		// check if client ready state is already closing or closed
		if ($this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CLOSING || $this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CLOSED) return true;

		// fetch message length
		$messageLength = strlen($message);

		// set max payload length per frame
		$bufferSize = 4096;

		// work out amount of frames to send, based on $bufferSize
		$frameCount = ceil($messageLength / $bufferSize);
		if ($frameCount == 0) $frameCount = 1;

		// set last frame variables
		$maxFrame = $frameCount - 1;
		$lastFrameBufferLength = ($messageLength % $bufferSize) != 0 ? ($messageLength % $bufferSize) : ($messageLength != 0 ? $bufferSize : 0);

		// loop around all frames to send
		for ($i=0; $i<$frameCount; $i++) {
			// fetch fin, opcode and buffer length for frame
			$fin = $i != $maxFrame ? 0 : self::WS_FIN;
			$opcode = $i != 0 ? self::WS_OPCODE_CONTINUATION : $opcode;

			$bufferLength = $i != $maxFrame ? $bufferSize : $lastFrameBufferLength;

			// set payload length variables for frame
			if ($bufferLength <= 125) {
				$payloadLength = $bufferLength;
				$payloadLengthExtended = '';
				$payloadLengthExtendedLength = 0;
			}
			elseif ($bufferLength <= 65535) {
				$payloadLength = self::WS_PAYLOAD_LENGTH_16;
				$payloadLengthExtended = pack('n', $bufferLength);
				$payloadLengthExtendedLength = 2;
			}
			else {
				$payloadLength = self::WS_PAYLOAD_LENGTH_63;
				$payloadLengthExtended = pack('xxxxN', $bufferLength); // pack 32 bit int, should really be 64 bit int
				$payloadLengthExtendedLength = 8;
			}

			// set frame bytes
			$buffer = pack('n', (($fin | $opcode) << 8) | $payloadLength) . $payloadLengthExtended . substr($message, $i*$bufferSize, $bufferLength);

			// send frame
			$socket = $this->wsClients[$clientID]->get_Socket();

			$left = 2 + $payloadLengthExtendedLength + $bufferLength;
			do {
				$sent = @socket_send($socket, $buffer, $left, 0);
				if ($sent === false) return false;

				$left -= $sent;
				if ($sent > 0) $buffer = substr($buffer, $sent);
			}
			while ($left > 0);
		}

		return true;
	}
	function wsSendClientClose($clientID, $status=false) {
		// check if client ready state is already closing or closed
		if ($this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CLOSING || $this->wsClients[$clientID]->get_ReadyState() == self::WS_READY_STATE_CLOSED) return true;

		// store close status
		$this->wsClients[$clientID]->set_CloseStatus($status);

		// send close frame to client
		$status = $status !== false ? pack('n', $status) : '';
		$this->wsSendClientMessage($clientID, self::WS_OPCODE_CLOSE, $status);

		// set client ready state to closing
		$this->wsClients[$clientID]->set_ReadyState(self::WS_READY_STATE_CLOSING);
	}

	// client non-internal functions
	function wsClose($clientID) {
		return $this->wsSendClientClose($clientID, self::WS_STATUS_NORMAL_CLOSE);
	}
	function wsSend($clientID, $message, $binary=false) {
		return $this->wsSendClientMessage($clientID, $binary ? self::WS_OPCODE_BINARY : self::WS_OPCODE_TEXT, $message);
	}

	function log( $message )
	{
		echo date('Y-m-d H:i:s: ') . $message . "\n";
	}

	function wsBind( $type, $func )
	{
		if ( !isset($this->wsOnEvents[$type]) )
			$this->wsOnEvents[$type] = array();
		$this->wsOnEvents[$type][] = $func;
	}

	function wsUnbind( $type='' )
	{
		if ( $type ) unset($this->wsOnEvents[$type]);
		else $this->wsOnEvents = array();
	}


	/***
	 *       *     (         )                      (               )     *               )  (           
	 *     (  `    )\ )   ( /(          (           )\ )  *   )  ( /(   (  `       (   ( /(  )\ )        
	 *     )\))(  (()/(   )\()) (       )\      (  (()/(` )  /(  )\())  )\))(      )\  )\())(()/(   (    
	 *    ((_)()\  /(_))|((_)\  )\    (((_)     )\  /(_))( )(_))((_)\  ((_)()\   (((_)((_)\  /(_))  )\   
	 *    (_()((_)(_))  |_ ((_)((_)   )\___  _ ((_)(_)) (_(_())   ((_) (_()((_)  )\___  ((_)(_))_  ((_)  
	 *    |  \/  ||_ _| | |/ / | __| ((/ __|| | | |/ __||_   _|  / _ \ |  \/  | ((/ __|/ _ \ |   \ | __| 
	 *    | |\/| | | |    ' <  | _|   | (__ | |_| |\__ \  | |   | (_) || |\/| |  | (__| (_) || |) || _|  
	 *    |_|  |_||___|  _|\_\ |___|   \___| \___/ |___/  |_|    \___/ |_|  |_|   \___|\___/ |___/ |___| 
	 *                                                                                                   
	 */

	public function get_wsPoolMessage(){ 
		return $this->wsPoolMessage; 
	}
	public function set_wsPoolMessage($wsPoolMessage){ 
		$this->wsPoolMessage = $wsPoolMessage; 
	}

	function wsOnMessage($clientID, $JSONdata, $messageLength, $binary) {
		//$Server = Server::get_server();
		$data = json_decode($JSONdata);
		//$message = addslashes($data->messageText);
		$message = (string)$data->messageText;
		$ip = long2ip( $this->wsClients[$clientID]->get_IPv4());
		$currentChat = $this->wsClients[$clientID]->get_ChatID();
		$userName = $this->wsClients[$clientID]->get_UserName();
		$macAddress = $this->wsClients[$clientID]->get_MacAddress();
		$currentHash = $this->wsClients[$clientID]->get_HashTagName();

		if($data->messageType == "commonText"){

			$response= array();
			$response["chatId"] = $data->chatId;
			$response["responseType"] = "simpleMessage";
			$response["userName"] = $userName;
			$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			// check if message length is 0
			if ($messageLength == 0) {
				$this->wsClose($clientID);
				return;
			}

			//The speaker is the only person in the room. Don't let them feel lonely.
			if ( sizeof($this->wsClients) == 1 ){
				$response["message"] = "There isn't anyone else in the room, but I'll still listen to you. --Your Trusty Server";
				$this->wsSend($clientID, json_encode($response));
			}
			else{
				//Send the message to everyone but the person who said it
				foreach ( $this->wsClients as $id => $client ){
					if ($client->get_ChatID() == $currentChat){
						if ( $id != $clientID ){
							if($currentHash == $client->get_HashTagName()) {
								$response["message"] = strip_tags($message);
								$this->wsSend($id, json_encode($response));
							}
						}
					}
				}
			}
		} else if ($data->messageType == "initChat" && $this->wsClients[$clientID]->get_Initialized_Chat() == 0){
			// set client chat id
			$wsPoolMessage = $this->get_wsPoolMessage();


			if (isset($data->chatId) && is_numeric($data->chatId)) {
				if ($this->setChatId($clientID, $data->chatId)){
					$this->log("Chat Set -> ".print_r($this->wsClients[$clientID]->get_ChatID(), true));
				} else {
					$this->log("Unable to set ChatID!");
				}
			} else {
				$this->log("Invalid ChatID!");
			}
			// set client username in chat
			if (is_string($data->userName)) {
				if ($this->setUserName($clientID, $data->userName)){
					$this->log("UserName Set -> ".print_r($this->wsClients[$clientID]->get_UserName(), true));
				} else {
					$this->log("Unable to set UserName!");
				}
			} else {
				$this->log("Invalid UserName!");
			}
			// set client macaddress in chat
			if (is_string($data->macAddress)) {
				if ($this->setMacAddress($clientID, $data->macAddress)){
					$this->log("MAC Address Set -> ".print_r($this->wsClients[$clientID]->get_MacAddress(), true));
				} else {
					$this->log("Unable to set MAC Address!");
				}
			} else {
				$this->log("Invalid MAC Address!");
			}

			

			if (is_array($wsPoolMessage)) {
				if (count($wsPoolMessage) !=0) {
					foreach ($wsPoolMessage as $pmessage) {
						if(!isset($pmessage['macAddress'])){
							$pmessage['macAddress'] = $this->wsClients[$clientID]->get_MacAddress();
						}
						$tmpClientId = $pmessage['clientID'];
						unset($pmessage['clientID']);
						$pmessage = $this->replaceUsernamePlacheHolder($this->wsClients[$clientID]->get_UserName(), $pmessage);
						$this->wsSend($tmpClientId, json_encode($pmessage));
					}
					$this->set_wsPoolMessage(array());
				}
			}


			$response= array();
			$response["chatId"] = $data->chatId;
			$response["hashTag"] = $this->wsClients[$clientID]->get_HashTagName();
			$response["responseType"] = "addChatAshTag";
			$response["userName"] = $userName;
			$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			$response["rooms"] = array();
			$i=0;
			foreach ($this->wsClients as $id => $client) {
				$response["rooms"][$i]["macAddress"] = $client->get_MacAddress();
				$response["rooms"][$i]["hashTagName"] = $client->get_HashTagName();
				$i++;
			}
			unset($i);
			$this->wsSend($clientID, json_encode($response));

			//$this->wsClients[$clientID]->set_InternalIPv4($data->internalIP);
			$this->log($this->wsClients[$clientID]->get_MacAddress());
			//$this->log($this->wsClients[$clientID]->get_InternalIPv4());
			$this->wsClients[$clientID]->set_Initialized_Chat(1);
		} else if ($data->messageType == "initChat" && $this->wsClients[$clientID]->get_Initialized_Chat() == 1){
			$this->log("User '".$this->wsClients[$clientID]->get_UserName()."' (ID: ".$clientID.") has tried double initChat");
		} else if($data->messageType == "setAshTag"){
			$this->wsClients[$clientID]->set_HashTagName($message);
			$response= array();
			$response["chatId"] = $data->chatId;
			$response["hashTag"] = $this->wsClients[$clientID]->get_HashTagName();
			$response["responseType"] = "addChatAshTag";
			$response["userName"] = $userName;
			$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			$response["rooms"] = array();
			$i=0;
			foreach ($this->wsClients as $id => $client) {
				$response["rooms"][$i]["macAddress"] = $client->get_MacAddress();
				$response["rooms"][$i]["hashTagName"] = $client->get_HashTagName();
				$i++;
			}
			unset($i);
			//Send the message to everyone but the person who said it
			foreach ( $this->wsClients as $id => $client ){
				$response["message"] = strip_tags($message);
				$this->wsSend($id, json_encode($response));
			}
		} else if($data->messageType == "textEventGodzilla"){
			$response= array();
			$response["chatId"] = $data->chatId;
			$response["responseType"] = "eventGodzilla";
			$response["userName"] = $userName;
			$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			//Send the message to everyone but the person who said it
			foreach ( $this->wsClients as $id => $client ){
				if ($client->get_ChatID() == $currentChat){
					if($currentHash == $client->get_HashTagName()) {
						$response["message"] = strip_tags($message);
						$this->wsSend($id, json_encode($response));
					}
				}
			}
		} else {
			$this->log("Unknown message type!");
		}
	}

	// when a client connects
	function wsOnOpen($clientID)
	{
		//$Server = Server::get_server();
		$response = array();
		$ip = long2ip( $this->wsClients[$clientID]->get_IPv4());
		$pool = array();
		//$macAddress = $this->wsClients[$clientID]->get_MacAddress();

		$this->log( "$ip ($clientID) has connected." );

		//da risolvere somehow
		//foreach ($this->wsClients as $id => $client) {
		//	if ( $id != $clientID && $clientID > $id ){
		//		while ($client->get_Initialized_Chat() == 0 ){
		//			$this->log($id."--> wait!<br/>");
		//			//usleep(5000);
		//		}
		//	}
		//}
		//Send a join notice to everyone //but the person who joined
		foreach ( $this->wsClients as $id => $client ) {
			$response["chatId"] = 0;
			$response["responseType"] = "userJoin";
			$response["userName"] = self::USER_NAME_PLACEHOLDER;
			//$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			if ( $id != $clientID ){
				$response["message"] = self::USER_NAME_PLACEHOLDER." has joined the room.";
				$response["clientID"] = $id;
				array_push($pool, $response);
				//$this->wsSend($id, json_encode($response));
			} else {
				$response["message"] = "You have joined the room.";
				$response["clientID"] = $clientID;
				array_push($pool, $response);
				//$this->wsSend($id, json_encode($response));
				foreach ($this->wsClients as $otherId => $existingClient) {
					if ( $id != $otherId ){
						$existingUserName = $existingClient->get_UserName();
						$response["userName"] = $existingUserName;
						$response["userId"] = $otherId;
						$response["macAddress"] = $existingClient->get_MacAddress();
						$response["message"] = $existingUserName." was already in the room.";
						$response["clientID"] = $clientID;
						array_push($pool, $response);
						//$this->wsSend($id, json_encode($response));
					}
				}
			}
		}
		$this->log("<pre>".print_r($pool,true)."</pre>");
		$this->set_wsPoolMessage($pool);
	}

	// when a client closes or lost connection
	function wsOnClose($clientID, $status) {
		//$Server = Server::get_server();
		$response = array();
		$ip = long2ip( $this->wsClients[$clientID]->get_IPv4());
		$userName = $this->wsClients[$clientID]->get_UserName();
		$macAddress = $this->wsClients[$clientID]->get_MacAddress();

		$this->log( "$ip ($clientID) has disconnected." );

		//Send a user left notice to everyone in the room
		foreach ( $this->wsClients as $id => $client ){
			$response["chatId"] = 0;
			$response["responseType"] = "userLeft";
			$response["userName"] = $userName;
			$response["macAddress"] = $macAddress;
			$response["userId"] = $clientID;
			$response["message"] = $userName." has left the room.";
			$this->wsSend($id, json_encode($response));
		}
	}

	function setChatID($clientID=NULL, $data=NULL)	{
		if(is_numeric($clientID)){
			if(is_numeric($data)){
				//store chatID
				$this->wsClients[$clientID]->set_ChatID($data);
				return true;
			}     
		}
		return false;
	}

	function setUserName($clientID=NULL, $data=NULL)	{
		if(is_numeric($clientID)){
			if(is_string($data) && $data != ''){
				//store chatID
				$this->wsClients[$clientID]->set_UserName($data);
				return true;
			}
		}
		return false;
	}

	function setMacAddress($clientID=NULL, $data=NULL)	{
		if(is_numeric($clientID)){
			if(is_string($data) && $data != ''){
				//store chatID
				$this->wsClients[$clientID]->set_MacAddress($data);
				return true;
			}
		}
		return false;
	}
	
	function replaceUsernamePlacheHolder($replace, $subject, $search = self::USER_NAME_PLACEHOLDER){ 
    	return json_decode(str_replace($search, $replace,  json_encode($subject))); 
	}
}

/* End of file WebSocket.php */