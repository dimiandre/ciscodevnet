<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	function __construct(){
		parent::__construct();
	}
	var $debug_text = "";

	public function head(){
		$this->load->view('layout/head');
	}
	public function header(){
		$data = array();
		$this->load->view('layout/header',$data);
	}

	//load only body closing tag and html closing tag
	public function foot(){
		$this->load->view('layout/foot');
	}
	public function footer(){
		$this->load->view('layout/footer');
	}

	//load head and header
	public function startPage(){
		$this->head();
		$this->header();
	}
	//load foot and footer
	public function endPage(){
		$this->footer();
		$this->foot();
	}

	public function getClientMAC($ip)	{
		$username = $password = "learning";
		$location='https://10.10.20.11/api/contextaware/v1/location/clients/'.$ip;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$location);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERPWD,"$username:$password");
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST , false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Accept: Application/Json'
		    ));

		$result = curl_exec ($ch); 

		$object_result = json_decode($result, false);

		if (is_object($object_result->WirelessClientLocation) || is_object($object_result)){
			return $object_result->WirelessClientLocation->macAddress;
		} else {
			return false;
		}
	}

	public function getImg($macAddress = NULL) {
		if(is_null($macAddress)){
			$macAddress = $this->getClientMAC($_SERVER['REMOTE_ADDR']);
		} else {
			$this->debug_text = md5($macAddress);
		}
		$file = "img/".md5($macAddress).".jpg";
		
		if (file_exists($file)){
			return $file;
		} else {
			return "";
		}
	}

	public function getClientData($ipOrMac){
		$username = $password='learning';

		$location='https://10.10.20.11/api/contextaware/v1/location/clients/'.$ipOrMac;

		$ch = curl_init ();
		curl_setopt($ch, CURLOPT_URL,$location);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERPWD,"$username:$password");
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST , false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Accept: Application/Json'
		    ));

		$result = curl_exec ($ch); 

		return json_decode($result, false);
	}

	public function getDistances($print_response = true) {
		$username = $password='learning';



		// Get My Coords

		$userIp = $_SERVER['REMOTE_ADDR'];

		$myData = $this->getClientData($userIp);

		if (is_object($myData) && is_object($myData->WirelessClientLocation) && is_object($myData->WirelessClientLocation->MapCoordinate)) {
			$x = $myData->WirelessClientLocation->MapCoordinate->x;
			$y = $myData->WirelessClientLocation->MapCoordinate->y;
		}

		if (!isset($x) || !isset($y))
			return;

		$location='https://10.10.20.11/api/contextaware/v1/location/clients/';

		$ch = curl_init ();
		curl_setopt($ch, CURLOPT_URL,$location);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_USERPWD,"$username:$password");
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST , false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		    'Accept: Application/Json'
		    ));

		$result = curl_exec ($ch); 

		$result = json_decode($result, false);

		$response = array();

		foreach ($result->Locations->entries as $client)
		{
			if (is_object($client) && is_object($client->MapCoordinate) ) {

				if (!isset($client->MapCoordinate->x))
					continue;

				if (!isset($client->MapCoordinate->y))
					continue;

				if (!isset($client->macAddress))
					continue;

				//$allowedMac = array (
					//"bc:f5:ac:f8:7c:0f",
					//"6c:40:08:97:fe:c4",
					//"48:d2:24:39:f5:5f",
					//"78:4b:87:77:07:de");
//
				//if (!in_array($client->macAddress, $allowedMac))
					//continue;

				if ($client->macAddress == $myData->WirelessClientLocation->macAddress)
					continue;

				$x2 = $client->MapCoordinate->x;
				$y2 = $client->MapCoordinate->y;

				//$distance = sqrt(($x2 - $x)*($x2 - $x) + ($y2 - $y) * ($y2 - $y));
				//$response[$client->macAddress]['distance'] = $distance;
				$response[$client->macAddress]['x'] = ($x2 - $x)*10;
				$response[$client->macAddress]['y'] = ($y2 - $y)*10;

				if (isset($client->ipAddress[0]) || isset($client->ipAddress[1])) {
					if (count(explode(".", $client->ipAddress[0])) > 0 ) {
						$ip =  $client->ipAddress[0];
					}
					else if (count(explode(".", $client->ipAddress[1])) > 0 ) {
						$ip =  $client->ipAddress[1];
					} else {
						$ip = "none";
					}
				} else {
					$ip = "none";
				}


				$response[$client->macAddress]['ipAddress'] = $ip;
				$response[$client->macAddress]['img'] = $this->getImg($client->macAddress);

			}
		}

		//usort($response, function ($elem1, $elem2) {
			//if (strcmp($elem1['img'], $elem2['img']) > 1) {
				# code...
		//	}
		//     return ;
		//});

		if ($print_response) {
			$this->output
				->set_content_type('application/json')
				->set_output(
					json_encode($response)
				);
		} else {
			return $response;
		}
	}
}
