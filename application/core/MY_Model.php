<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {

	function __construct($data = NULL){
		parent::__construct();
		$this->load->database();
		if(is_object($data) || is_array($data)){
			$this->assignData($data);
		}
	}

	function assignData($data){
		foreach($this as $keyThis=>$elementThis){
			foreach($data as $keyData=>$dataElement){
				if($keyThis == $keyData){
					$this->$keyThis = $dataElement;
				}
			}
		}
	}
}