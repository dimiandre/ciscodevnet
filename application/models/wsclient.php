<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class wsclient extends MY_Model {
	var $Socket; 							//0 		// client socket
	var $MessageBuffer = "";				//1 		// a blank string when there's no incoming frames
	var $ReadyState = 0;					//2 		// between 0 and 3
	var $LastRecvTime = 0;					//3 		// set to time() when the client is added
	var $PingSentTime = false;				//4 		// false when the server is not waiting for a pong
	var $CloseStatus = 0;					//5 		// close status that wsOnClose() will be called with
	var $IPv4 = 0;							//6 		// client's IP stored as a signed long, retrieved from ip2long()
	var $FramePayloadDataLength = false;	//7 		// length of a frame's payload data, reset to false when all frame data has been read (cannot reset to 0, to allow reading of mask key)
	var $FrameBytesRead = 0;				//8 		// amount of bytes read for a frame, reset to 0 when all frame data has been read
	var $FrameBuffer = "";					//9 		// joined onto end as a frame's data comes in, reset to blank string when all frame data has been read
	var $MessageOpcode = 0;					//10 		// stored by the first frame for fragmented messages, default value is 0
	var $MessageBufferLength = 0;			//11 		// the payload data length of MessageBuffer
	var $ChatID = 0;						//12 		// the chat id which connection refers to, default to 0
	var $UserName = "";						//13 		// the username which connection refers to, default to ''
	var $Initialized_Chat = false;			//14 		// set 1 if chat has initialized, default to 0
	var $InternalIPv4 = 0;					//15 		// client's internal IP stored as a signed long
	var $MacAddress = "";					//16 		// client's MacAddress (as string)
	var $HashTagName = "";					//17 		// client's HashTagChatRoom

	function __construct($data = NULL){
		parent::__construct();
		if(is_array($data)){
			foreach ($data as $key => $value) {
				$func = "set_".$key;
				$this->$func($value);
			}
		}
	}

	public function get_Socket(){ 
		return $this->Socket; 
	}
	public function set_Socket($Socket){ 
		$this->Socket = $Socket; 
	}
	public function get_MessageBuffer(){ 
		return $this->MessageBuffer; 
	}
	public function set_MessageBuffer($MessageBuffer){ 
		$this->MessageBuffer = $MessageBuffer; 
	}
	public function concat_MessageBuffer($MessageBuffer){ 
		$this->MessageBuffer .= $MessageBuffer; 
	}
	public function get_ReadyState(){ 
		return $this->ReadyState; 
	}
	public function set_ReadyState($ReadyState){ 
		$this->ReadyState = $ReadyState; 
	}
	public function get_LastRecvTime(){ 
		return $this->LastRecvTime; 
	}
	public function set_LastRecvTime($LastRecvTime){ 
		$this->LastRecvTime = $LastRecvTime; 
	}
	public function get_PingSentTime(){ 
		return $this->PingSentTime; 
	}
	public function set_PingSentTime($PingSentTime){ 
		$this->PingSentTime = $PingSentTime; 
	}
	public function get_CloseStatus(){ 
		return $this->CloseStatus; 
	}
	public function set_CloseStatus($CloseStatus){ 
		$this->CloseStatus = $CloseStatus; 
	}
	public function get_IPv4(){ 
		return $this->IPv4; 
	}
	public function set_IPv4($IPv4){ 
		$this->IPv4 = $IPv4; 
	}
	public function get_FramePayloadDataLength(){ 
		return $this->FramePayloadDataLength; 
	}
	public function set_FramePayloadDataLength($FramePayloadDataLength){ 
		$this->FramePayloadDataLength = $FramePayloadDataLength; 
	}
	public function get_FrameBytesRead(){ 
		return $this->FrameBytesRead; 
	}
	public function set_FrameBytesRead($FrameBytesRead){ 
		$this->FrameBytesRead = $FrameBytesRead; 
	}
	public function add_FrameBytesRead($FrameBytesRead){ 
		$this->FrameBytesRead += $FrameBytesRead; 
	}
	public function sub_FrameBytesRead($FrameBytesRead){ 
		$this->FrameBytesRead -= $FrameBytesRead; 
	}
	public function get_FrameBuffer(){ 
		return $this->FrameBuffer; 
	}
	public function set_FrameBuffer($FrameBuffer){ 
		$this->FrameBuffer = $FrameBuffer; 
	}
	public function concat_FrameBuffer($FrameBuffer){ 
		$this->FrameBuffer .= $FrameBuffer; 
	}
	public function get_MessageOpcode(){ 
		return $this->MessageOpcode; 
	}
	public function set_MessageOpcode($MessageOpcode){ 
		$this->MessageOpcode = $MessageOpcode; 
	}
	public function get_MessageBufferLength(){ 
		return $this->MessageBufferLength; 
	}
	public function set_MessageBufferLength($MessageBufferLength){ 
		$this->MessageBufferLength = $MessageBufferLength; 
	}
	public function add_MessageBufferLength($MessageBufferLength){ 
		$this->MessageBufferLength += $MessageBufferLength; 
	}
	public function get_ChatID(){ 
		return $this->ChatID; 
	}
	public function set_ChatID($ChatID){ 
		$this->ChatID = $ChatID; 
	}
	public function get_UserName(){ 
		return $this->UserName; 
	}
	public function set_UserName($UserName){ 
		$this->UserName = $UserName; 
	}
	public function get_Initialized_Chat(){ 
		return $this->Initialized_Chat; 
	}
	public function set_Initialized_Chat($Initialized_Chat){ 
		$this->Initialized_Chat = $Initialized_Chat; 
	}
	public function get_InternalIPv4(){ 
		return $this->InternalIPv4; 
	}
	public function set_InternalIPv4($InternalIPv4){ 
		$this->InternalIPv4 = $InternalIPv4; 
	}
	public function get_MacAddress(){ 
		return $this->MacAddress; 
	}
	public function set_MacAddress($MacAddress){ 
		$this->MacAddress = $MacAddress; 
	}
	public function get_HashTagName(){ 
		return $this->HashTagName; 
	}
	public function set_HashTagName($HashTagName){ 
		$this->HashTagName = $HashTagName; 
	}

	
	
}

/* End of file wsClient.php */
/* Location: ./application/models/wsClient.php */