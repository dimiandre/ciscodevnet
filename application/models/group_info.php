<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group_info extends Group {

	var $hosts = array();

	function __construct($data = NULL){
		parent::__construct($data);
		$this->load->model(array('host','host_info'));
		$this->load_hosts();
	}

	public function get_hosts(){ return $this->hosts; }
	public function set_hosts($hosts){ $this->hosts = $hosts; }

	public function load_hosts(){
		$db_table = Host::dbTable;
		$data = $this->db
			->from($db_table)
			->where('id_group',$this->get_id_group())
			->get()
			->result();
		foreach($data as &$row){
			$row = new Host($row);
		}
		$this->set_hosts($data);
	}
}