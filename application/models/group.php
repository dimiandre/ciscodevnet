<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Group extends MY_Model {

	var $id_group = NULL;
	var $public_ip = '';

	const dbTable = 'groups';

	function __construct($data = NULL){
		parent::__construct($data);
		if(is_valid_ip($data)){
			$this->retrive_by_public_ip($data);
		}
	}

	public function get_id_group(){ return $this->id_group; }
	public function set_id_group($id_group){ $this->id_group = $id_group; }
	public function get_public_ip(){ return $this->public_ip; }
	public function set_public_ip($public_ip){ $this->public_ip = $public_ip; }

	public function retrive_by_public_ip($public_ip){
		$this->load_by_public_ip($public_ip);
		if(!$this->is_valid_group()){
			$this->create_with_public_id($public_ip);
		}
	}

	public function load_by_public_ip($public_ip){
		$this->assignData(
			$this->db
				->from(self::dbTable)
				->where('public_ip',$public_ip)
				->get()
				->first_row()
		);
	}

	function is_valid_group(){
		return is_numeric($this->get_id_group());
	}

	function create_with_public_id($public_ip){
		$this->set_public_ip($public_ip);
		$esito = $this->db->insert(self::dbTable, $this);
		if($esito){
			$this->set_id_group($this->db->insert_id());
		}
		return $this->get_id_group();
	}
}