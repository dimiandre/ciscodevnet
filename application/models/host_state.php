<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Host_state extends MY_Model {

	var $id_host_state = NULL;
	var $code = '';
	var $label =  '';

	const dbTable = 'host_states';

	function __construct($data = NULL){
		parent::__construct($data);
	}

	public function get_id_host_state(){ return $this->id_host_state; }
	public function set_id_host_state($id_host_state){ $this->id_host_state = $id_host_state; }
	public function get_code(){ return $this->code; }
	public function set_code($code){ $this->code = $code; }
	public function get_label(){ return $this->label; }
	public function set_label($label){ $this->label = $label; }
}