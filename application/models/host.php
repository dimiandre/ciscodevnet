<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Host extends MY_Model {

	var $id_host = NULL;
	var $id_group = NULL;
	var $private_ip = '';
	var $username = '';
	var $id_host_state = 1;

	const dbTable = 'hosts';

	function __construct($data = NULL,$id_group = NULL){
		parent::__construct($data);
		if(is_valid_ip($data) && is_numeric($id_group)){
			$this->retrive_by_private_ip($data,$id_group);
		}
	}

	public function get_id_host(){ return $this->id_host; }
	public function set_id_host($id_host){ $this->id_host = $id_host; }
	public function get_id_group(){ return $this->id_group; }
	public function set_id_group($id_group){ $this->id_group = $id_group; }
	public function get_private_ip(){ return $this->private_ip; }
	public function set_private_ip($private_ip){ $this->private_ip = $private_ip; }
	public function get_username(){ return $this->username; }
	public function set_username($username){ $this->username = $username; }
	public function get_id_host_state(){ return $this->id_host_state; }
	public function set_id_host_state($id_host_state){ $this->id_host_state = $id_host_state; }

	public function retrive_by_private_ip($private_ip,$id_group){
		$this->load_by_private_ip($private_ip,$id_group);
		if(!$this->is_valid_host()){
			$this->create_with_private_id($private_ip,$id_group);
		}
	}
	public function load_by_private_ip($private_ip,$id_group){
		$this->assignData(
			$this->db
				->from(self::dbTable)
				->where('private_ip',$private_ip)
				->where('id_group',$id_group)
				->get()
				->first_row()
		);
	}

	function is_valid_host(){
		return is_numeric($this->get_id_host());
	}

	function set_username_if_null(){
		if(is_null($this->get_username()) || strlen($this->get_username()) <= 0){
			$this->set_username($this->get_private_ip());
		}
	}

	function create_with_private_id($private_ip,$id_group){
		$this->set_private_ip($private_ip);
		$this->set_username_if_null();
		$this->set_id_group($id_group);
		$esito = $this->db->insert(self::dbTable, $this);
		if($esito){
			$this->set_id_host($this->db->insert_id());
		}
		return $this->get_id_host();
	}
}