/**
 * filename:        jabbwerwerx.js
 * created at:      2009/10/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
;(function() {
    if ('jabberwerx' in window) {
        return;
    }

    var jabberwerx;
    /**
     * @namespace
     * @minimal
     * @description
     * Cisco AJAX XMPP Library is an easy to use, AJAX-based XMPP client. This namespace
     * contains the core, non-UI classes and methods.
     *
     * To use these features you must have an account on an XMPP server.
     *
     * ## Events
     * The Cisco AJAX XMPP Library eventing mechanism is implemented in
     * {@link jabberwerx.EventDispatcher} and {@link jabberwerx.EventNotifier}.
     * Any possible event that may be fired will trigger on
     * {@link jabberwerx.globalEvents}. See
     * <a href="../jabberwerxEvents.html">JabberWerx Events</a> for all
     * possible events.
     *
     * ## Configuration
     * The following configuration options are available:
     * <table>
     * <tr>
     *  <th>Name</th>
     *  <th>Default</th>
     *  <th>Description</th>
     * </tr>
     * <tr>
     *  <td>persistDuration</td>
     *  <td>30</td>
     *  <td>The number of seconds that persisted data is considered to still
     *  be valid.</td>
     * </tr>
     * <tr>
     *  <td>capabilityFeatures</td>
     *  <td>[]</td>
     *  <td>The base capabilities for clients, not including those defined
     *  by enabled controllers</td>
     * </tr>
     * <tr>
     *  <td>capabilityIdentity</td>
     *  <td><pre class="code">{
     *      category: "client",
     *      type: "pc",
     *      name: "JabberWerx AJAX",
     *      node: "http://jabber.cisco.com/jabberwerx"
     *}</pre></td>
     *  <td>The identity for clients' capabilities.</td>
     * </tr>
     * <tr>
     *  <td>unsecureAllowed</td>
     *  <td>false</td>
     *  <td><tt>true</tt> if plaintext authentication is allowed over
     *  unencrypted or unsecured HTTP channels</td>
     * </tr>
     * <tr>
     *  <td>baseReconnectCountdown</td>
     *  <td>30</td>
     *  <td>base number of seconds between a disconnect occurring and a
     * reconnect been initiated. The actual reconnect period will be the
     * {baseReconnectCountdown} +/- x%, where x is a random number between
     * 0 and 10. If {baseReconnectCountdown} is 0 then a reconnect will
     * never be attempted. {baseReconnectCountdown}  is also used as a persist
     * password flag. If 0 password is never persisted and is cleared from memory
     * as soon as possible (immediately after a connect ATTEMPT. IF > 0, password
     * is persisted obfuscated and the password remains in memory (accessable through
     * the client.connectParams object)
     </td>
     * </tr>
     * <tr>
     *  <td>bindRetryCountdown</td>
     *  <td>30</td>
     *  <td>Number of seconds timeout between sending a bind resource request
     * and receiving the response from server. If no response from server after this timeout period,
     * a reconnect operation will be processed.
     * If {bindRetryCountdown} is less than or equal 0, the default value 30 seconds will be used.
     * </td>
     * </tr>
     * <tr>
     *  <td>enabledMechanisms</td>
     *  <td>["DIGEST-MD5", "PLAIN"]</td>
     *  <td>The list of SASL mechanism to enable by default.</td>
     * </tr>
     * </table>
     *
     * To set any of these options, create an object called `jabberwerx_config`
     * in the global namespace, like this:
     *
     *      jabberwerx_config = {
     *          persistDuration: 30,
     *          unsecureAllowed: false,
     *          capabilityFeatures: ['http://jabber.org/protocol/caps',
     *                          'http://jabber.org/protocol/chatstates',
     *                          'http://jabber.org/protocol/disco#info',
     *                          'http://jabber.org/protocol/muc',
     *                          'http://jabber.org/protocol/muc#user'],
     *          capabilityIdentity: {
     *                  category: 'client',
     *                  type: 'pc',
     *                  name: 'JabberWerx AJAX',
     *                  node: 'http://jabber.cisco.com/jabberwerx'},
     *          baseReconnectCountdown: 30,
     *          bindRetryCountdown: 30,
     *          enabledMechanisms: ["DIGEST-MD5", "PLAIN"]
     *      };
     *
     * This code must be evaluated **before** including this file, jabberwerx.js.
     */
    jabberwerx = {
        /**
         * JabberWerx Version
         * @property {String} version
         * @type String
         */
    version: '8.5.0.22677',

        /**
         * Internal config settings. These may be overwritten by the user at init time
         * by setting properties on a global object named jabberwerx_config.
         *
         * @property _config
         */
        _config: {
            /** The age past which a saved session is expired. */
            persistDuration: 30,
            /** Dictates if unsecure connections are allowed. A connection is considered unsecure if it
             * is SASL PLAIN or JEP-0078 password over http. */
            unsecureAllowed: false,
            /** Default feature list for entity capabilities (XEP-115) */
            capabilityFeatures: [],
            /** Default primary binding url */
            httpBindingURL: "/httpbinding",
            /** Default secondary binding url */
            httpBindingURL_secondary: "/httpbinding",
            /** Default base reconnect period */
            baseReconnectCountdown: 30,
            /** Default bind retry period */
            bindRetryCountdown: 30,
            /** Default SASL Mechanisms to enable */
            enabledMechanisms: ["DIGEST-MD5", "PLAIN"],
            /** Is service discovery enabled on client **/
            serviceDiscoveryEnabled: false
        },

        /**
         * @private
         * Returns the url for the currently configured install location.
         *
         * @type String
         * @returns The url for the currently configured install location.
         */
        _getInstallURL: function() {
            return this._getInstallPath();
        },

        /**
         * @private
         * Returns the url for the currently configured install location.
         *
         * @type String
         * @returns The url for the currently configured install location.
         */
        _getInstallPath: function() {
            var p = this._config.installPath;
            if (!p) {
                var target = String(arguments[0] || "jabberwerx") + ".js";

                p = jabberwerx.$("script[src$=" + target + "]").slice(0,1).attr("src");
                p = p.substring(0, p.indexOf(target));
            }

            return p.charAt(p.length - 1) == '/' ? p : p + '/';
        },

        /**
         * Converts an XMPP-formatted date/time string into a Javascript Date object.
         *
         * @param   {String} timestamp The timestamp string to parse from
         * @returns  {Date} The date object representing the timestamp
         * @throws  {TypeError} If {timestamp} cannot be parsed
         */
        parseTimestamp: function(timestamp) {
            var result = /^([0-9]{4})(?:-?)([0-9]{2})(?:-?)([0-9]{2})T([0-9]{2}):([0-9]{2}):([0-9]{2}(?:\.([0-9]+))?)(?:(Z|[-+]?[0-9]{2}:[0-9]{2})?)$/.exec(timestamp);
            if (!result) {
                throw new TypeError("timestamp string not recognized");
            }

            var ts, offset = 0;
            ts = Date.UTC(  Number(result[1]),
                            Number(result[2]) - 1,
                            Number(result[3]),
                            Number(result[4]),
                            Number(result[5]),
                            Number(result[6]),
                            Number(result[7] || "0"));

            if (result[8] && result[8] != "Z") {
                result = /^([-+]?[0-9]{2}):([0-9]{2})$/.exec(result[8]);
                if (result) {
                    offset += Number(result[1]) * 3600000;
                    offset += Number(result[2]) * 60000;
                }
            }

            return new Date(ts - offset);
        },
        /**
         * Converts a Date object into an XMPP-formatted date/time string.
         *
         * @param   {Date} ts The date object to generate from
         * @param   {Boolean} [legacy] <tt>true</tt> if the date/time string
         *          should conform to the legacy format in XEP-0091
         * @returns {String} The date/time string
         * @throws  {TypeError} If {ts} is not a Date object
         */
        generateTimestamp: function(ts, legacy) {
            var padFN = function(val, amt) {
                var result = "";
                if (amt > 1) {
                    result = arguments.callee(parseInt(val / 10), amt - 1);
                }
                return result + String(parseInt(val % 10));
            };

            if (!(ts && ts instanceof Date)) {
                throw new TypeError("Expected Date object");
            }
            var date = [
                    padFN(ts.getUTCFullYear(), 4),
                    padFN(ts.getUTCMonth() + 1, 2),
                    padFN(ts.getUTCDate(), 2)];
            var time = [
                    padFN(ts.getUTCHours(), 2),
                    padFN(ts.getUTCMinutes(), 2),
                    padFN(ts.getUTCSeconds(), 2)];

            if (legacy) {
                return date.join("") + "T" + time.join(":");
            } else {
                return date.join("-") + "T" + time.join(":") + "Z";
            }
        },


        /**
         * Internal JabberWerx init method. Clients do not need to call this.
         *
         * @private
         */
        _init: function() {
            this._inited = true;

            // copy user config into internal config
            if (typeof jabberwerx_config != 'undefined') {
                for (var name in jabberwerx_config) {
                    var val = jabberwerx_config[name];
                    if (jabberwerx.$.isArray(val) && val.concat) {
                        val = val.concat();
                    }
                    this._config[name] = val;
                }
            }

            // can't do this before auto-loading JWCore...
            jabberwerx.InsufficientServerDetailsError = jabberwerx.util.Error.extend('A server name and HTTP proxy address are needed to connect.');

            jabberwerx.$(document).ready(function() {
                /*DEBUG-BEGIN*/
                jabberwerx.util.debug.on = window.jabberwerx._config.debug.on;
                for (var streamName in window.jabberwerx._config.debug.streams) {
                    jabberwerx.util.setDebugStream(streamName, window.jabberwerx._config.debug.streams[streamName]);
                }
                /*DEBUG-END*/
            });

            //jabberwerx.$(window).unload(function() { poppet.unload(); });
        },

        /**
         * The method invoked when the storage engine is ready to go.
         *
         * @private
         */
        _handleStorageReady: function(engine) {
            if (this._storageReady) { return; }

            if (engine && !engine.isReady) {
                engine.ready(this._handleStorageReady);
            }

            this._storageReady = true;
            if (!this._deferredCalls) {
                this._deferredCalls = [];
            }
            for (var i = 0 ; i < this._deferredCalls.length; i++) {
                var info = this._deferredCalls[i];
                var target = info.target || this;
                target[info.methodName].apply(target, info.arguments);
            }
            this._deferredCalls = [];
        },

        /**
         * Disconnects from the server and clears any stored chat sessions.
         */
        reset: function() {
            if (this.client) {
                this.client.disconnect();
            }
            if (jabberwerx._config.persistSessions) {
                jabberwerx.util.clearGraph();
            }
        },

        /**
         * Performs a "reduce" on the given object (array or otherwise). This
         * method iterates over the items in &lt;obj&gt;, calling &lt;fn&gt;
         * with the current item from &lt;obj&gt; and the current &lt;value&gt;.
         *
         * The signature for &lt;fn&gt; is expected to take two arguments: The
         * current item from &lt;obj&gt; to evaluate, and the current value of
         * the reduction. &lt;fn&gt; is expected to return the updated value.
         *
         * For example, the following sums all of the items in an array:
         * <pre class="code"
         * var value = jabberwerx.reduce([0,1,2],
         *                              function(item, value) {
         *                                     return item + value;
         *                              });
         * </pre>
         *
         * @param {Object|Array} obj The object to reduce
         * @param {Function} fn The callback to perform the reduction
         * @param [value] The initial value for reduction; may be undefined
         * @throws {TypeError} If {fn} is not a function object.
         * @returns The reduction value
         */
        reduce: function(obj, fn, value) {
            if (!jabberwerx.$.isFunction(fn)) {
                throw new TypeError("expected function object");
            }

            jabberwerx.$.each(obj, function(idx, item) {
                value = fn(item, value);
                return true;
            });

            return value;
        },
        /**
         * Removes duplicate items from the given array.
         *
         * @param {Array} arr The array to make unique
         * @returns {Array} The provided array {arr}
         */
        unique: function(arr) {
            if (!jabberwerx.$.isArray(arr)) {
                return arr;
            }

            var specified = {};
            for (var idx = arr.length - 1; idx >= 0; idx--) {
                var item = arr[idx];
                if (!specified[item]) {
                    specified[item] = true;
                } else {
                    arr.splice(idx, 1);
                }
            }

            return arr;
        },
        /**
         * Check whether the given node {o} is a TextNode
         *
         * @param {Node} o The object to check against
         * @returns {Boolean} <tt>true</tt> if {o} is a TextNode
         */
        isText: function(o) {
            return (
                //typeof TextNode === "object" ? o instanceof TextNode : //DOM2
                o && o.ownerDocument && o.nodeType == 3 && typeof o.nodeName == "string"
            );
        },

        /**
         * Check whether the given NODE {o} is an element node
         *
         * @param {Node} o The node to check against
         * @returns {Boolean} <tt>true</tt> if {o} is an element node
         */
        isElement: function(o) {
            return (
                //typeof Element === "object" ? o instanceof Element : //DOM2
                o &&
                (o.ownerDocument !== undefined) &&
                (o.nodeType == 1) &&
                (typeof o.nodeName == "string")
            );
        },

        /**
         * Check whether the given node {o} is a document node
         *
         * @param {Node} o The node to check against
         * @returns {Boolean} <tt>true</tt> if {o} is a document node
         */
        isDocument: function(o) {
            return (
                //typeof Document === "object" ? o instanceof Document : //DOM2
                o &&
                (o.documentElement !== undefined) &&
                (o.nodeType == 9) &&
                (typeof o.nodeName == "string")
            );
        },

        client: null,
        _inited: false,

        _deferredCalls: [],
        _storageReady: false
    };

    jabberwerx._config.debug = {
    /*DEBUG-BEGIN*/
        streams: {
            rawStanzaLogging: false,
            connectionStatus: false,
            clientStatus: false,
            entityLifeCycle: false,
            stanzaSelectors: false, // a LOT of info ...
            persistence: false,
            observers: false,
            collectionControllers: false
        },
    /*DEBUG-END*/
        on: true
    };

    /**
     * @class
     * @minimal
     * Utility class to build DOMs programmatically. This class is used to
     * create most of the XMPP data.
     *
     * @property {jabberwerx.NodeBuilder} parent The parent node builder
     * @property {Element} data The current data
     * @property {Document} document The document (used for creating nodes)
     *
     * @description
     * <p>Creates a new jabberwerx.NodeBuilder</p>
     *
     * <p>The value of {data} may be an Element or a String. If it is a string,
     * it is expected to be an expanded name in one of the following forms:</p>
     *
     * <ol>
     *<li>{namespace-uri}prefix:local-name</li>
     * <li>{namespace-uri}local-name</li>
     * <li>prefix:local-name</li>
     * <li>local-name</li>
     * </ol>
     *
     * @param   {Element|String} [data] The context element, or the expanded-name
     *          of the root element.
     * @throws  {TypeError} If {data} is defined, not an element, not a
     *          Document, or not a valid expanded name
     */
    jabberwerx.NodeBuilder = function(data) {
        var parent, doc, ns = null;

        if (data instanceof jabberwerx.NodeBuilder) {
            // NOT FOR EXTERNAL USE: support a hierarchy of
            // NodeBuilders.
            this.parent = parent = arguments[0];
            data = arguments[1];
            doc = parent.document;
            ns = parent.namespaceURI;
        }

        if (jabberwerx.isDocument(data)) {
            doc = data;
            data = doc.documentElement;
            ns = data.namespaceURI || data.getAttribute("xmlns") || ns;
        } else if (jabberwerx.isElement(data)) {
            if (!doc) {
                doc = data.ownerDocument;
            } else if (data.ownerDocument !== doc) {
                data = (doc.importNode) ?
                       doc.importNode(data, true) :
                       data.cloneNode(true);
            }

            if (parent && parent.data) {
                parent.data.appendChild(data);
            }
            if (!doc.documentElement) {
                doc.appendChild(data);
            }

            ns = data.namespaceURI || data.getAttribute("xmlns") || ns;
        } else if (data) {
            if (!doc) {
                doc = this._createDoc();
            }

            var ename, ln, pre;

            ename = this._parseName(data, ns);
            ns = ename.namespaceURI;
            data = this._createElem(doc, ns, ename.localName, ename.prefix);
        } else if (!parent) {
            doc = this._createDoc();
        }

        this.document = doc;
        this.data = data;
        this.namespaceURI = ns;
    };

    /* @extends jabberwerx.NodeBuilder.prototype */
    jabberwerx.NodeBuilder.prototype = {
        /**
         * Adds or updates an attribute to this NodeBuilder's data.
         *
         * <p><b>NOTE:</b> namespaced attributes are not supported</p>
         *
         * @param {String} name The name of the attribute
         * @param {String} val The attribute value
         * @returns {jabberwerx.NodeBuilder} This builder
         * @throws  {TypeError} if {name} is not valid
         */
        attribute: function(name, val) {
            var ename = this._parseName(name);

            if (ename.prefix && ename.prefix != "xml" && ename.prefix != "xmlns") {
                var xmlns = "xmlns:" + ename.prefix;

                if (this.data.getAttribute(xmlns) === null) {
                    this.data.setAttribute(xmlns, ename.namespaceURI || "");
                }
            } else if (ename.prefix != "xml" && ename.prefix != "xmlns" && ename.namespaceURI !== null) {
                throw new TypeError("namespaced attributes not supported");
            }

            this.data.setAttribute(ename.qualifiedName, val);

            return this;
        },
        /**
         * Appends a new text node to this NodeBuilder's data.
         *
         * @param {String} val The text node value
         * @returns {jabberwerx.NodeBuilder} this builder
         */
        text: function(val) {
            if (!val) {
                return this;
            }

            var txt = this.document.createTextNode(val);
            this.data.appendChild(txt);

            return this;
        },
        /**
         * Appends a new element to this NodeBuilder's data, with the given
         * name and attributes. The created element is automatically appended
         * to this NodeBuilder's data.
         *
         * <p>If expanded-name uses form 2 (local-name, no namespace), then the
         * namespace for the parent is used.</p>
         *
         * @param {String} name The expanded name of the new element
         * @param {Object} [attrs] A hashtable of attribute names to
         *        attribute values
         * @returns {jabberwerx.NodeBuilder} The builder for the new element,
         *           with the current builder as its parent.
         * @throws  {TypeError} if {name} is not a valid expanded name
         */
        element: function(name, attrs) {
            if (!attrs) {
                attrs = {};
            }
            if (typeof(name) != "string") {
                throw new TypeError("name is not a valid expanded name");
            }
            var builder = new jabberwerx.NodeBuilder(this, name);
            for (var key in attrs) {
                if (key == 'xmlns') { continue; }
                builder.attribute(key, attrs[key]);
            }

            return builder;
        },

        /**
         * Appends the given node to this NodeBuilder's data:
         * <ul>
         * <li>If {n} is a document, its documentElement is appended to
         * this NodeBuilder's data and a NodeBuilder wrapping that element
         * is returned</li>
         * <li>If {n} is an element, it is cloned and appended to this
         * NodeBuilder's data and a NodeBuilder wrapping the cloned element
         * is returned</li>
         * <li>If {n} is a TextNode, its value is appended to this
         * NodeBuilder's data and this NodeBuilder is returned</li>
         * <li>Otherwise, a TypeError is thrown</li>
         * <ul>
         *
         * @param   {Node} n The node to append
         * @returns  {jabberwerx.NodeBuilder} The builder appropriate for {node}
         * @throws   {TypeError} If {node} is invalid
         */
        node: function(n) {
            if (!n) {
                throw new TypeError("node must exist");
            }

            if (jabberwerx.isDocument(n)) {
                n = n.documentElement;
            }

            if (jabberwerx.isElement(n)) {
                return new jabberwerx.NodeBuilder(this, n);
            } else if (jabberwerx.isText(n)) {
                return this.text(n.nodeValue);
            } else {
                throw new TypeError("Node must be an XML node");
            }

            return this;
        },

        /**
         * <p>Appends the given value as parsed XML to this NodeBuilder's
         * data.</p>
         *
         * @param {String} val The XML to parse and append
         * @returns {jabberwerx.NodeBuilder} This NodeBuilder
         */
        xml: function(val) {
            var wrapper = (this.namespaceURI) ?
                          "<wrapper xmlns='" + this.namespaceURI + "'>" :
                          "<wrapper>";
            wrapper += val + "</wrapper>";
            var parsed = this._parseXML(wrapper);
            var that = this;

            jabberwerx.$(parsed).contents().each(function() {
                if (jabberwerx.isElement(this)) {
                    new jabberwerx.NodeBuilder(that, this);
                } else if (jabberwerx.isText(this)) {
                    that.text(this.nodeValue);
                }
            });

            return this;
        },

        /**
         * @private
         * <ol>
         * <li>{namespace-uri}prefix:local-name</li>
         * <li>{namespace-uri}local-name</li>
         * <li>prefix:local-name</li>
         * <li>local-name</li>
         * </ol>
         */
        _parseName: function(name, ns) {
            var ptn = /^(?:\{(.*)\})?(?:([^\s{}:]+):)?([^\s{}:]+)$/;
            var m = name.match(ptn);

            if (!m) {
                throw new TypeError("name '" + name + "' is not a valid ename");
            }

            var retval = {
                namespaceURI: m[1],
                localName: m[3],
                prefix: m[2]
            };

            if (!retval.localName) {
                throw new TypeError("local-name not value");
            }

            retval.qualifiedName = (retval.prefix) ?
                    retval.prefix + ":" + retval.localName :
                    retval.localName;

            if (!retval.namespaceURI) {
                // IE work-around, since RegExp returns "" if:
                //  1) it evaluates to "" OR
                //  2) it is missing!
                if (name.indexOf("{}") == 0) {
                    retval.namespaceURI = "";
                } else {
                    retval.namespaceURI = ns || null;
                }
            }

            return retval;
        },
        /**
         * @private
         */
        _createDoc: function() {
            var doc;
            try {
                if (document.implementation && document.implementation.createDocument) {
                    doc = document.implementation.createDocument(null, null, null);
                } else {
                    doc = new ActiveXObject("Msxml2.DOMDocument");
                    doc.async = false;
                }
            } catch (ex) {
                //DEBUG-BEGIN
                jabberwerx.util.debug.log("could not create XML Document: " + ex);
                //DEBUG-END
                throw ex;
            }

            return doc;
        },

        /**
         * @private
         */
        _parseXML: function(xmlstr) {
            var tmp;
            try { //safari throws DOMException for certain types of invalid xml
                tmp = this._createDoc();
                tmp.loadXML(xmlstr);
            } catch (ex) {
                try {
                    tmp = (new DOMParser()).parseFromString(xmlstr,"text/xml");
                }
                catch(ex) {
                    // whoops!
                    jabberwerx.util.debug.log("exception: " + ex + " trying to parse xml: '" + xmlstr + "'");
                    throw new TypeError("Exception trying to parse xml: '" + xmlstr + "'");
                }
            }
            var ret = tmp.documentElement;
             //check elem to see if a parse error occurred
            if (!ret || //ie
               (ret.nodeName == "parsererror") || //mozilla
               (jabberwerx.$("parsererror", ret).length > 0)) {// safari
                jabberwerx.util.debug.log("parse error trying to parse xml: '" + xmlstr + "'");
                throw new TypeError("Parse error trying to parse: " + xmlstr);
            }
            return ret;
        },


        /**
         * @private
         */
        _createElem: function(doc, ns, ln, pre) {
            var parent = this.parent;
            var elem;
            var qn = pre ? (pre + ":" + ln ) : ln;

            var str;
            if (pre) {
                str = "xmlns:" + pre + "='" + (ns || "") + "'";
            } else {
                str = (ns !== undefined && ns !== null) ? "xmlns='" + ns + "'" : "";
            }
            str = "<" + qn + " " + str + "/>";

            elem = this._parseXML(str);
            if (doc.importNode) {
                elem = doc.importNode(elem, true);
            }

            if (parent && parent.data) {
                if (parent.namespaceURI == ns) {
                    elem.removeAttribute("xmlns");
                }

                parent.data.appendChild(elem);
            } else if (!doc.documentElement) {
                doc.appendChild(elem);
            }
            return elem;
        }
    };

    /**
     * @namespace
     * @description
     * @minimal
     * Namespace for XHTML-IM functions and constants.
     */
    jabberwerx.xhtmlim = {};

    /**
    *    @minimal
    *    An array of css style properties that may be included in
    *    xhtml-im. Default values are from XEP-71 Recommended Profile
    *   but may be modified at anytime.
    *    For example some clients may want to ignore font size property  would  add <br/>
    *       <pre class='code'>
    *           delete jabberwerx.xhtmlim.allowedStyles[jabberwerx.xhtmlim.allowedStyles.indexOf("font-size")] ;
    *       </pre><br/>
    *   to their initialization code.
    *   @property {Array} jabbewerx.xhtmlim.allowedStyles
    */
    jabberwerx.xhtmlim.allowedStyles = [
        "background-color",
        "color",
        "font-family",
        "font-size",
        "font-style",
        "font-weight",
        "margin-left",
        "margin-right",
        "text-align",
        "text-decoration"
    ];


    /**
    *   @minimal
    *   A map of tags that may be included in
    *   xhtml-im.  The defaults are defined in XEP-71 Recommended Profile.
    *   The map is indexed by tag and provides an array of allowed attributes for
    *   that tag. Clients may modify this map at any time to change behavior.
    *   For example a client that wanted to include table tags  would  add<br/>
    *   <pre class="code">
    *        jabberwerx.$.extend(jabberwerx.xhtmlim.allowedTags,{
    *           table:      ["style", "border", "cellpadding", "cellspacing", "frame", "summary", "width"]
    *        })
    *   </pre><br/>
    *   to their initialization code.
    *
    *   @property {Map} jabbewerx.xhtmlim.allowedTags
    */
    jabberwerx.xhtmlim.allowedTags = {
        br:         [],
        em:         [],
        strong:     [],
        a:          ["style","href","type"],
        blockquote: ["style"],
        cite:       ["style"],
        img:        ["style", "alt", "height", "src", "width"],
        li:         ["style"],
        ol:         ["style"],
        p:          ["style"],
        span:       ["style"],
        ul:         ["style"],
        body:       ["style", "xmlns", "xml:lang"]
    }


    /**
    *   @minimal
    *   Sanitize xhtmlNode by applying XEP-71 recommended profile.
    *
    *   Each node in the given DOM is checked to make sure it is an
    *   {@link jabberwerx.xhtmlim.allowedTags}, with allowed attributes and style values. If a
    *   node is not allowed it is removed from the DOM
    *   and its children reparented to its own parent. If an attribute
    *   is not allowed it is removed. If the attribute's name is "href" or "src"
    *   and its value starts with 'javascript:', its element is removed and the children
    *   reparented. Finally any css values not in the
    *   {@link jabberwerx.xhtmlim.allowedStyles} array is removed from the style attribute.
    *
    *   xhtmlNode  must be a <body/> or one of the other allowed tags.  Typical usage of this
    *   function would be to clean an html fragment (an entire <p/> for instance) or
    *   in preperation for a message stanza by passing a <body/> element.
    *
    *   @param DOM xhtmlNode <body xmlns='http://www.w3.org/1999/xhtml'/>
    *   @returns DOM A reference to xhtmlNode
    *   @throws TYPE_ERROR if xhtmlNode is not a DOM or not an allowed tag
    */
    jabberwerx.xhtmlim.sanitize = function(xhtmlNode) {
        //private filter function, expects a jq
        var filterNodes = function(fNode) {
            //keep element children to recurse later
            var myKids = fNode.children();
            var fDOM = fNode.get(0);
            if (jabberwerx.xhtmlim.allowedTags[fDOM.nodeName] === undefined) {
                fNode.replaceWith(fDOM.childNodes);
                fNode.remove();
            } else { //filter attributes
                var i = 0;
                while (i < fDOM.attributes.length) {
                    var aName = fDOM.attributes[i].nodeName;
                    if (jabberwerx.$.inArray(aName, jabberwerx.xhtmlim.allowedTags[fDOM.nodeName]) == -1) {
                        fNode.removeAttr(aName); //removes from attributes
                    } else {
                        if (aName == "href" || aName == "src") {
                            // filter bad href/src values
                            var aValue = fDOM.attributes[i].nodeValue;
                            if (aValue.indexOf("javascript:") == 0) {
                                fNode.replaceWith(fDOM.childNodes);
                                fNode.remove();
                            }
                        } else if (aName == "style") {
                            // filter unknown css  properties
                            var rProps = jabberwerx.$.map(
                                fDOM.attributes[i].value.split(';'),
                                function(oneStyle, idx) {
                                    return jabberwerx.$.inArray(oneStyle.split(':')[0], jabberwerx.xhtmlim.allowedStyles) != -1 ? oneStyle : null;
                                });
                            fNode.attr("style", rProps.join(';'));
                        }
                        ++i;
                    }
                }
            }

            for (var i = 0; i < myKids.length; ++i) {
                if (jabberwerx.isElement(myKids[i])) {
                    filterNodes(jabberwerx.$(myKids[i]));
                }
            }
        } //filterNodes

        if (!jabberwerx.isElement(xhtmlNode)) {
            throw new TypeError("xhtmlNode must be a DOM");
        }
        if (jabberwerx.xhtmlim.allowedTags[xhtmlNode.nodeName] === undefined) {
            throw new TypeError("xhtmlNode must be an allowed tag")
        }

        filterNodes(jabberwerx.$(xhtmlNode));
        return xhtmlNode;
    }

    // reset jQuery and $ if not present
    jabberwerx.$ = jQuery.noConflict(true);
    if (typeof(window.jQuery) == "undefined") {
        window.jQuery = jabberwerx.$;
    }
    if (typeof(window.$) == "undefined") {
        window.$ = jabberwerx.$;
    }

    window.jabberwerx = jabberwerx;
    jabberwerx.$(document).ready(function() {
        jabberwerx._init();
    });
})();
