/**
 * filename:        jabbwerwerx.ui.js
 * created at:      2009/10/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
//UI extensions to jabberwerx
;(function() {
    var jabberwerx = window.jabberwerx;

    //if jw is not loaded yet or ui extensions have already been added, bail
    if (!jabberwerx || ('ui' in jabberwerx)) {
        throw new Error("jabberwerx not defined!");
            return;
    }


    /**
     * @namespace
     * @description
     * Cisco AJAX XMPP Library is an easy to use, AJAX-based XMPP client. This namespace
     * contains UI-oriented views and controls.
     *
     * - Control to view/change your own presence
     * - Presence View for a specific Contact
     * - Roster View
     * - Chat 1:1 View
     * - MUC Room View
     * - Tabbed View (to contain 1:1 and MUC views)
     *
     * To use these features you must have an account on a jabber server.
     *
     *
     * ## Configuration
     * In addition to those options available by {@link jabberwerx}, the
     * following configuration options are available:
     * <table>
     * <tr>
     *  <th>Name</th>
     *  <th>Default</th>
     *  <th>Description</th>
     * </tr>
     * <tr>
     *  <td>themeName</td>
     *  <td>"default"</td>
     *  <td>The name of the theme to use.</td>
     * </tr>
     * </table>
     *
     * To set any of these options, create an object called
     * `jabberwerx_ui_config` in the global namespace, like this:
     *
     *      jabberwerx_ui_config = {
     *          themeName: "default"
     *      };
     *
     * This code must be evaluated **before** including this file,
     * jabberwerx.ui.js.
     *
     * ## Themeing
     * The images and css used to generate the tabbed chat and group chat UI are
     * found `themes/default/images` and `themes/default/css`. You may edit
     * these files directly, or make a copy of the `default` directory, and make
     * your edits there. To use your theme, set the `themeName` property of
     * `jabberwerx_ui_config` to the name of the copied directory.
    */
    jabberwerx.ui = {
        /**
        * JabberWerx.UI Version
        * @property {String} version
        */
        version: '8.5.0.22677',

        /**
         * Internal config settings. These may be overwritten by the user at init time
         * by setting properties on a global object named jabberwerx_ui_config.
         *
         * @property _config
         */
        _config: {
                /** The name of the theme directory. */
                themeName: 'default',
                /** Show status connection status at the bottom of the chat pane? Set to 'string' to enable. */
                showStatus: 'light'
        },

        /**
         * Returns the url for a passed image, given the currently configured theme.
         *
         * @param {String} fileName The name of an image file living inside the current theme's images folder.
         * @type String
         * @returns The url for a passed image, given the currently configured theme.
         */
        getThemeImageURL: function(fileName){
               return this.getThemeURL() + 'images/' + fileName;
        },

        /**
         * Returns the url for the currently configured theme folder.
         *
         * @type String
         * @returns The url for the currently configured theme folder.
         */
        getThemeURL: function(){
               return jabberwerx._getInstallURL() + 'resources/themes/' + this._config.themeName + '/';
        },

        /**
         * Internal JabberWerx init method. Clients do not need to call this.
         *
         * @private
         */
        _init: function(){
            this._inited = true;
            // copy user config into jabberwerx internal config

            var config = {};
            config = jabberwerx.$.extend(true, config, this._config);
            if (typeof(jabberwerx_config) != "undefined") {
                config = jabberwerx.$.extend(true, config, (jabberwerx_config || {}));
            }

            jabberwerx._config.ui = config;

            this.emoticons = new jabberwerx.ui.Emoticons();
        },

        /**
         * @private
         * Internally used to generate a nicely-formatted date string, relative to now.
         *
         * @param {Date} date A Javascript Date object.
         * @type String
         * @returns A string formatted to show a date relative to now.
         */
        months: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
        /**
         * <p>Generates a quick human-readable form of a Date. The generated
         * string does not include the date unless {date}'s year/month/day are
         * different from the current date's year/month/day.</p>
         *
         * @param   {Date} date The date to display
         * @returns  {String} The displayable string for {date}
         */
        getDateDisplayString: function(date) {
            var now = new Date();
            var year = date.getFullYear();
            var month = date.getMonth();
            var day = date.getDate();
            var min = date.getMinutes().toString();
            var hours = date.getHours();
            var meridian = 'am';
            if (hours > 12) {
                meridian = 'pm';
                hours -= 12;
            }

            var dayString = '';

            if (now.getFullYear() != year || now.getMonth() != month || now.getDate() != day) {
                dayString += this.months[month] + ' ' + day.toString();
            }
            if (now.getFullYear() != year) {
                dayString += ', ' + year.toString();
            }
            if (dayString.length) {
                dayString += ' ';
            }

            return dayString + hours.toString() + ':' + ((min.length == 1) ? ('0' + min) : min) + meridian;
        }
    };

    //theme, override if global config is defined
    if (typeof jabberwerx_ui_config != 'undefined'
        && typeof jabberwerx_ui_config.themeName != 'undefined') {
        jabberwerx.ui._config.themeName = jabberwerx_ui_config.themeName;
    }

    window.jabberwerx.ui = jabberwerx.ui;

    jabberwerx.$(document).ready(function() {
        if (!jabberwerx._config.installPath) {
            jabberwerx._config.installPath = jabberwerx._getInstallPath("jabberwerx.ui");
        }
        jabberwerx._init();
        jabberwerx.ui._init();

        var url = jabberwerx.ui.getThemeURL() + "css/styles.css";
        var styles = jabberwerx.$("head link[rel=stylesheet][href='" + url + "']");
        if (!styles.length) {
            jabberwerx.$("head").prepend('<link rel="stylesheet" href="' + url + '" type="text/css" media="screen" />');
        }
    });
})();
