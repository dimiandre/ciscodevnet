/**
 * filename:        ChatController.js
 * created at:      2009/01/01T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

;(function(){
    if (jabberwerx) {
        jabberwerx.ChatController = jabberwerx.Controller.extend(/** @lends jabberwerx.ChatController.prototype */{
            /**
             * @class
             * Controller class for chat functionality. Controls the creation and deletion of
             * ChatSession objects. Upon a message stanza been recieved, for which there is no
             * ChatSession object already created, a new ChatSession object is created and the
             * chatSessionOpened event fired. ChatSession's can be created directly using the
             * openChatSession method. The chatSessionClosed event is fired upon the closing of a
             * chat session.
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ChatController">jabberwerx.ChatController Events</a></li>
             * </ul>
             *
             * @extends jabberwerx.Controller
             *
             * @description
             * Creates a new ChatController with the given client.
             * 
             * @param {jabberwerx.Client} client The client object to use for
             *        communicating to the server
             * @throws {TypeError} If {client} is not an instance of
             *         jabberwerx.Client
             * @constructs jabberwerx.ChatController
             */
            init: function(client) {
                this._super(client, 'chat');
                
                var caps = client.controllers.capabilities ||
                           new jabberwerx.CapabilitiesController(client);
                caps.addFeature('http://jabber.org/protocol/chatstates');
                caps.addFeature('http://jabber.org/protocol/xhtml-im');
                
                // Setup events
                this.applyEvent('chatSessionOpened');
                this.applyEvent('chatSessionClosed');
                
                // Setup handler
                // Will filter on type != "groupchat" or "error" and "gone" chatstate not present
                this.client.event('afterMessageReceived').bindWhen('message[type!="groupchat"]'+
                    '[type!="error"] body'
                    , this.invocation('_messageHandler'));
            },
            
            /**
             * Creates a new ChatSession object, or returns an existing ChatSession object,
             * to be used to communicate with the jid specified.
             * @param {jabberwerx.JID|String} jid The jid of the user with whom the ChatSession
             *                                   should be set up with.
             * @param {String} [thread] The thread to use in the outgoing messages. A random 
             *                        thread value will be generated if one is not provided. Note:
             *                        the thread value can change during the duration of a chat
             *                        session.
             * @returns {jabberwerx.ChatSesison} A new ChatSession object if an object
             *                                  for this jid does not already exist,
             *                                  otherwise returns the existing ChatSession object
             *                                  for this jid.
             */
            openSession: function(jid, thread) {
                var chatSession = null;
                if (!(jid instanceof jabberwerx.JID)) {
                    jid = new jabberwerx.JID(jid);
                }
                chatSession = this.getSession(jid);
                if (!chatSession) {
                    // Create a new session and trigger event
                    chatSession = new jabberwerx.ChatSession(this.client, jid, thread);
                    this._chatSessions.push(chatSession);
                    this.event('chatSessionOpened').trigger({chatSession: chatSession,
                                                              userCreated: true});
                }
                return chatSession;
            },
            
            /**
             * Removes the ChatSession object from the active list. Fire the chatSessionClosed
             * event.
             * @param {jabberwerx.JID|String|jabberwerx.ChatSession} session The session to close
             * @returns {Boolean} <tt>true</tt> if successfully removed session, false otherwise
             */
            closeSession: function(session) {
                var index = this._getChatSessionIndex(session);
                if (index >= 0) {
                    // Remove session from list and fire event.
                    var closedChatSession = this._chatSessions[index];
                    this._chatSessions.splice(index, 1);
                    this.event('chatSessionClosed').trigger(closedChatSession);
                    closedChatSession.destroy();
                    delete closedChatSession;
                    
                    return true;
                }
                return false;
            },
            
            /**
             * Get the ChatSession object for this jid if it exists.
             * @param {jabberwerx.JID|String} jid The resource component of the jid is ignored
             * @returns {jabberwerx.ChatSession} object or null if the ChatSession does not exist
             */
            getSession: function(jid) {
                var index = this._getChatSessionIndex(jid);
                return (index >= 0) ? this._chatSessions[index] : null;
            },
            
            /**
             * @private
             * Handles message stanza event firings where the type is neither groupchat or error.
             * This method should not be called by client code.
             * @param {jabberwerx.EventObject} eventObj
             */
            _messageHandler: function(eventObj) {
                var msg = eventObj.data;
                var from = msg.getFromJID();
                if (this._getChatSessionIndex(from) < 0) {
                    // Create a new session and trigger event
                    var chatSession = new jabberwerx.ChatSession(this.client, from,
                                                                 msg.getThread());
                    this._chatSessions.push(chatSession);
                    this.event('chatSessionOpened').trigger({chatSession: chatSession,
                                                              userCreated: false});
                    // Trigger chat received event on the newly created chat session
                    chatSession._chatReceivedHandler(eventObj);
                    return true;
                }
                return false;
            },
            
            /**
             * Gets the index in _chatSessions of the session param.
             * @private
             * @param (jabberwerx.JID|String|jabberwerx.ChatSession) session The session to find
             * @returns (Integer) The position in _chatSessions of the session param, or -1 if not
             * found.
             */
            _getChatSessionIndex: function(session) {
                var index = -1;
                var jid;
                if (session instanceof jabberwerx.ChatSession) {
                    jid = session.jid;
                } else if (session instanceof jabberwerx.JID) {
                    jid = session;
                } else {
                    try {
                        jid = new jabberwerx.JID(session);
                    } catch(e) {
                        return index;
                    }
                }

                if (!jid) {
                    return index;
                }

                var privateMessage = this.isPrivateMessage(jid);
                var jidStr = (privateMessage) ?
                             jid.toString() :
                             jid.getBareJIDString();

                for (var i=0; i<this._chatSessions.length; i++) {
                    var chatJidStr = (privateMessage) ?
                                     this._chatSessions[i].jid.toString() :
                                     this._chatSessions[i].jid.getBareJIDString();
                    if (chatJidStr == jidStr) {
                        index = i;
                        break;
                    }
                }

                return index;
            },

            /**
             * <p>Checks to see if the bare version of the jid passed in
             * is a MUCRoom.</p>
             *
             * @param {jabberwerx.JID} jid The JID to check.
             * @returns {Boolean} <tt>true</tt> if the bare JID is a MUCRoom.
             * @throws {TypeError} if jid is not of type jabberwerx.JID
             */
            isPrivateMessage: function(jid) {
                if (!(jid instanceof jabberwerx.JID)) {
                    throw new TypeError("jid must be of type jabberwerx.JID");
                }

                var mucJid = jid.getBareJID();
                var entity = this.client.entitySet.entity(mucJid);
                return (entity && entity instanceof jabberwerx.MUCRoom);
            },
           
            /**
             * Used to determine if XEP-085 chat states will be sent upon chat state changes.
             * If <tt>true</tt>, then chat state changes are sent, if false, then they are not sent. Defaults
             * to <tt>true</tt>.
             * @type Boolean
             */
            sendChatStates: true,

            /**
             * A list of currently active sessions.
             * @private
             * @type jabberwerx.ChatSession[]
             */
            _chatSessions : []
        }, 'jabberwerx.ChatController');
    }
})();
