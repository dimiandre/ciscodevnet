/**
 * filename:        RosterController.js
 * created at:      2009/05/15T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

;(function(){
    if (jabberwerx) {
        
        //TODO: test this in IE (6,7,8?)
        //TODO: DOM creation only for W3C DOM
        //TODO: remove group
        
        jabberwerx.RosterController = jabberwerx.Controller.extend(/** @lends jabberwerx.RosterController.prototype */{
            /**
             * @class
             * <p>Controller class for roster functionality.</p>
             * 
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.RosterController">jabberwerx.RosterController Events</a></li>
             * </ul>
             *
             * @extends jabberwerx.Controller
             *
             * @description
             * Creates a new RosterController with the given client.
             * 
             * @param {jabberwerx.Client} client The client object to use for
             *        communicating to the server
             * @throws TypeError If {client} is not an instance of
             *         jabberwerx.Client
             * @constructs jabberwerx.RosterController
             */
            init: function(client) {
                this._super(client, "roster");

                // setup events
                this.applyEvent("errorEncountered");
                this.applyEvent("subscriptionReceived");
                this.applyEvent("unsubscriptionReceived");
                
                
                // setup auto-accept/-remove policy from global config
                var that = this;
                var policyKeys = [  "autoaccept",
                                    "autoaccept_in_domain",
                                    "autoremove"];
                var policyDefaults = jabberwerx._config.subscriptions;
                jabberwerx.$.each(policyKeys, function() {
                    var key = this;
                    
                    if (    policyDefaults !== undefined &&
                            policyDefaults[key] !== undefined) {
                        that[key] = policyDefaults[key];
                    }
                    
                    return true;
                });

                // setup handlers
                client.event("clientStatusChanged").bind(
                        this.invocation("_handleStatusChange"));
                client.event("iqReceived").bindWhen(
                        "[type='set'] query[xmlns='jabber:iq:roster']",
                        this.invocation("_handleRosterUpdate"));
                client.event("presenceReceived").bindWhen(
                        "[type='subscribe'], [type='subscribed'], [type='unsubscribe'], [type='unsubscribed']",
                        this.invocation("_handleSubscription"));
            },
            
            /**
             * <p>Destroys this RosterController and removes any event callbacks it registered.</p>
             */
            destroy: function() {
                // teardown handlers
                var client = this.client;
                
                client.event("clientStatusChanged").unbind(
                        this.invocation("_handleStatusChange"));
                client.event("iqReceived").unbind(
                        this.invocation("_handleRosterUpdate"));
                client.event("presenceReceived").unbind(
                        this.invocation("_handleSubscription"));

                this._super();
            },
            
            /**
             * Updates the given entity. This method calls
             * {@link #updateContact} with the displayName and groups from
             * the given entity.
             *
             * @param {jabberwerx.RosterContact} entity The entity to update
             * @throws {TypeError} if {entity} is not a Contact
             */
            updateEntity: function(entity) {
                if (!(entity && entity instanceof jabberwerx.RosterContact)) {
                    throw new TypeError("entity must be a contact");
                }
                
                var nick = entity.getDisplayName();
                if (nick == entity.jid.toString()) {
                    nick = "";
                }
                var groups = entity.getGroups();
                this.updateContact(entity.jid, nick, groups);
            },
            /**
             * Deletes the given entity. This method calls
             * {@link #deleteContact} with the displayName and groups from
             * the given entity.
             *
             * @param {jabberwerx.RosterContact} entity The entity to delete
             * @throws {TypeError} if {entity} is not a Contact
             */
            removeEntity: function(entity) {
                if (!(entity && entity instanceof jabberwerx.RosterContact)) {
                    throw new TypeError("entity must be a contact");
                }
                
                this.deleteContact(entity.jid);
            },
            
            /**
             * Fetch a user's roster and adds the contacts to the {@link jabberwerx.Client#entitySet}
             * 
             * @param {function} [callback] The function called after the server response is processed. If a server error
             * occurs, the function will be passed the error stanza response.
             * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
             * <p>The callback will be invoked in the context of the RosterController
             * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will
             * refer to the instance of RosterController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
             * 
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             *  
             */
            fetch: function(callback) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
                if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                    throw new TypeError('The callback param must be a function');
                }
                
                //The variable 'that' will be visible to the senqIq callback
                var that = this;
                
                //Remove the existing contacts in the entity set
                this._removeContactsFromEntitySet();
                
                //Send roster query
                this.client.sendIq('get', null, "<query xmlns='jabber:iq:roster'/>", 
                    function (stanza) {     
                        var error = that._checkError(stanza);
                        var items = jabberwerx.$('item', stanza);
                        
                        //Add the contacts to the entity set
                        //TODO: push eachentitySet call onto a timer?
                        that.client.entitySet.startBatch();
                        jabberwerx.$(items).each(function() {                                 
                            var jid = jabberwerx.$(this).attr('jid');
                            var contact = new jabberwerx.RosterContact(this, that);
                            var ent = that.client.entitySet.entity(jid);
                            if (ent) {
                                contact.apply(ent, false);
                                ent.remove();
                            }
                            that.client.entitySet.register(contact);
                            
                            /*DEBUG-BEGIN*/
                            jabberwerx.util.debug.log('Contact with JID ' + jid + ' is registered with the entity set');
                            /*DEBUG-END*/
                        });
                        that.client.entitySet.endBatch();
                        
                        if (error) {
                            that.event("errorEncountered").trigger({
                                operation: "fetch",
                                error: error
                            });
                        }
                        
                        if (callback) {
                            callback.call(that, error);
                        }
                    }
                );
                
            },
            
            /** 
             * Add a contact to a user's roster. Invoking this method is the same as invoking {@link jabberwerx.RosterController#updateContact}
             *
             * @param {jabberwerx.JID|String} jid The JID of the contact to be added
             * @param {String} [nickname] The nickname to associate with the contact
             * @param {String|String[]} [groups] The name of the group or array of groups names to associate with the contact
             * @param {function} [callback] The function called after the server response is processed. If a server error
             * occurs, the function will be passed the error stanza response. 
             * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
             * <p>The callback will be invoked in the context of the RosterController
             * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will 
             * refer to the instance of RosterController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
             * 
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             * @throws {jabberwerx.JID.InvalidJIDError} If the JID argument is undefined, null or an empty string
             */
            addContact: function(jid, nickname, groups, callback) {
                this._updateContact(jid, nickname, groups, callback, true);
            },
            
            /**
             * Update a contact on a user's roster.
             *
             * @param {jabberwerx.JID|String} jid The JID of the contact to be updated
             * @param {String} [nickname] The nickname to associate with the contact
             * @param {String|String[]} [groups] The name of the group or array of groups names to associate with the contact
             * @param {function} [callback] The function called after server response is processed. If a server error
             * occurs, the function will be passed the error stanza response.
             * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
             * <p>The callback will be invoked in the context of the RosterController
             * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will
             * refer to the instance of RosterController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
             * 
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             * @throws {jabberwerx.JID.InvalidJIDError} If the JID argument is undefined, null or on empty string
             */
            updateContact: function(jid, nickname, groups, callback) {
                // TODO: Cannot get server to produce any errors
                // TODO: After contact add, will roster in entity set be stale
                this._updateContact(jid, nickname, groups, callback, false);
            },

            /**
             * @private
             */
            _updateContact: function(jid, nickname, groups, callback, addContact) {
                if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                    throw new TypeError('The callback param must be a function');
                }
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
                
                var that = this;
                
                //Normalize JID to a bare JID object
                jid = jabberwerx.JID.asJID(jid).getBareJID();

                // remember existing contact (if any)
                var entity = this.client.entitySet.entity(jid);
                
                //Normalize groups array
                if (groups && (typeof groups == 'string' || groups instanceof String)) {
                    groups = [groups.toString()];
                } else if (!(groups instanceof Array) || !groups.length || 
                           (groups.length == 1 && groups[0] == "")) {
                    groups = (this.defaultGroup && [this.defaultGroup.toString()]) || [];
                }
                
                //Create query DOM
                var builder = new jabberwerx.NodeBuilder('{jabber:iq:roster}query');
                with (builder.element('item')) {
                    attribute('jid', jid.toString());
                    if (nickname) {
                        attribute('name', nickname);
                    }
                    for (var i = 0; i < groups.length; i++) {
                        element('group').text(groups[i]);
                    }
                }
                
                //Send roster set
                if (callback) {
                    this.client.sendIq("set", null, builder.data, function(stanza) {
                        if (stanza) {
                            var err = that._checkError(stanza);
                            callback.call(that, err);
                        }
                    });
                } else {
                    this.client.sendIq("set", null, builder.data);
                }
                
                if (addContact || this.autoSubscription) {
                    this.subscribe(jid);
                }
            },

            /**
             * <p>Sends out a subscribe request unless the current user is
             * already subscribed to the passed in JID.</p>
             *
             * @param {jabberwerx.JID|String} jid The JID of the contact to be updated
             *
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} is an invalid JID.
             */
            subscribe: function(jid) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }

                jid = jabberwerx.JID.asJID(jid).getBareJID();
                var entity = this.client.entitySet.entity(jid);

                if (entity && entity instanceof jabberwerx.RosterContact) {
                    if (entity.properties.subscription == "to" ||
                        entity.properties.subscription == "both") {
                        return;
                    }
                }
                this.client.sendStanza("presence", "subscribe", jid);
            },

            /**
             * <p>Sends out an unsubscribe request when the current user is
             * subscribed to the passed in JID.</p>
             *
             * @param {jabberwerx.JID|String} jid The JID of the contact to be updated
             *
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} is an invalid JID.
             */
            unsubscribe: function(jid) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }

                jid = jabberwerx.JID.asJID(jid).getBareJID();
                //remove previous subscription state as needed
                this._clearAckAck(jid);
                var entity = this.client.entitySet.entity(jid);

                if (entity && entity instanceof jabberwerx.RosterContact) {
                    if (entity.properties.subscription == "both") {
                        this.client.sendStanza("presence", "unsubscribe", jid);
                    } else if (entity.properties.subscription == "to" ||
                               entity.properties.subscription == "none") {
                        // We don't want a contact in a 'none' state.
                        // Delete the contact if that is what an unsubscribe would do.
                        this.deleteContact(jid);
                    }
                }
            },

            /**
             * Delete a contact from a user's roster
             * 
             * @param {jabberwerx.JID|String} jid The JID of the contact to be deleted
             * @param {function} [callback] The function called after the server response is processed. If a server error
             * occurs, the function will be passed the error stanza response.
             * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
             * <p>The callback will be invoked in the context of the RosterController
             * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will 
             * refer to the instance of RosterController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
             * 
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} is undefined, null or an empty string
             */
            deleteContact: function(jid, callback) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
                if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                    throw new TypeError('The callback param must be a function');
                }
                var that = this;
                
                //Normalize JID to a bare JID object
                var jid = new jabberwerx.JID.asJID(jid).getBareJID();
                
                //Create query DOM
                var builder = new jabberwerx.NodeBuilder('{jabber:iq:roster}query');
                builder = builder.element('item');
                builder = builder.attribute('jid', jid.toString());
                builder = builder.attribute('subscription', 'remove');             
                
                //Send roster set
                this.client.sendIq('set', null, builder.document.xml, 
                    function (stanza) {
                        var error = that._checkError(stanza);
                        if (callback) {
                            callback.call(that, error);
                        }
                    }
                ); 
            },
            
            /**
             * <p>Accepts a subscription request from a contact</p>
             * @param {String|JID} contact The contact to accept the subscription from
             * @param {String} [nickname] The nickname to apply to the contact in our roster
             * @param {String} [groups] The groups to apply to the contact in our roster
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not
             * connected
             */
            acceptSubscription: function(contact, nickname, groups) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
                // Ensure contact is a jid and strip the resource
                contact = jabberwerx.JID.asJID(contact).getBareJID();
                //remove previous subscription state as needed
                this._clearAckAck(contact);
				// Send subscribed
                this.client.sendStanza("presence", "subscribed", contact);
                this.updateContact(contact, nickname, groups);
            },
            
            /**
             * <p>Denies (or cancels an existing) subscription from a contact.</p>
             * @param {String|jabberwerx.JID} contact The contact to deny the subscription from
             * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not
             * connected
             */
            denySubscription: function(contact) {
                if (!this.client.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
                // Ensure contact is a jid and strip the resource
                contact = jabberwerx.JID.asJID(contact).getBareJID();
                //remove previous subscription state as needed
                this._clearAckAck(contact);
				// Send unsubscribed
                this.client.sendStanza("presence", "unsubscribed", contact);

                // Delete contact if present in entity set
                if (this.autoSubscription) {
                    var entity = this.client.entitySet.entity(contact);
                    if (entity && entity instanceof jabberwerx.RosterContact) {
                        this.deleteContact(contact);
                    }
                }
            },
            
            /**
             * Remove the already acknowledged "subscribed" flag.
             * Called anytime presence state may change (before we send sub pres)
             *
             * @private
             */
            _clearAckAck: function(jid) {
                var entity = this.client.entitySet.entity(jabberwerx.JID.asJID(jid).getBareJID());
                if (entity) {
                    delete entity.properties.ackack;
                }
            },

            /**
             * Remove existing contacts from the entity set
             * 
             * @private
             */
            _removeContactsFromEntitySet: function() {
                var entities = this.client.entitySet.toArray();
                var that = this;
                jabberwerx.$.each(entities, function() {
                    var entity = this;
                    if (entity instanceof jabberwerx.RosterContact) {
                        entity.destroy();
                    } 
                });
            },
            
            /**
             * Check a response element to see if it contains an error.
             * 
             * @private
             * @param {Element} parentElement The element to check to see if it contains a child &lt;error&gt; 
             * @returns {Element|undefined} The error element if it exists or else undefined 
             */
            _checkError: function(parentElement) {
                var error = undefined;
                var child = jabberwerx.$(parentElement).children("error")[0];
                if (child && child.nodeName == 'error') {
                    error = child;
                }
                return error;
            },
            
            /**
             * @private
             */
            _autoAccept: function(prs) {
                var from = prs.getFromJID();
                //remove last subscription state flag as needed
                this._clearAckAck(from);
				var entity = this.client.entitySet.entity(from);

                var handled = 
                    this.autoaccept == jabberwerx.RosterController.AUTOACCEPT_ALWAYS;

                if (    !handled &&
                        this.autoaccept_in_domain &&
                        from.getDomain() == this.client.connectedUser.jid.getDomain()) {
                    handled = true;
                }
                if (    !handled &&
                        this.autoaccept == jabberwerx.RosterController.AUTOACCEPT_IN_ROSTER &&
                        entity) {
                    var props = entity.properties;
                    handled =   props["subscription"] == "to" ||
                                props["subscription"] == "none" ||
                                props["ask"] == "subscribe";
                }
                
                if (handled) {
                    if (entity) {
                        var nick = entity.getDisplayName();
                        if (nick == entity.jid.toString()) {
                            nick = "";
                        }
                        var groups = entity.getGroups();

                        this.acceptSubscription(from, nick, groups);
                    } else {
                        this.acceptSubscription(from);
                    }
                }
                
                return handled;
            },
            /**
             * @private
             */
            _autoRemove: function(prs) {
                var from = prs.getFromJID();
                //remove previous subscription state as needed
                this._clearAckAck(from);
				this.client.sendStanza("presence",
                                       "unsubscribed",
                                       from);

                var entity = this.client.entitySet.entity(from.getBareJID());
                var handled = (this.autoSubscription && this.autoremove) ||
                               entity.properties.subscription == "from" ||
                               entity.properties.subscription == "none";

                if (handled) {
                    this.deleteContact(from);
                }
                
                return handled;
            },
            
            /**
             * @private
             */
            _handleSubscription: function(evt) {
                var prs = evt.data;
                switch (prs.getType()) {
                    case "subscribe":
                        // handle auto-accept
                        var handled = this._autoAccept(prs);
                        
                        // fire event
                        this.event("subscriptionReceived").trigger({
                            stanza: prs,
                            handled: handled
                        });
                        break;
                    case "subscribed":
                        // acknowledge the acknowledgement unless already acked
                        var e = this.client.entitySet.entity(prs.getFromJID().getBareJID());
                        if (!e || e.properties.ackack === undefined) {
                            this.client.sendStanza("presence",
                                                   "subscribe",
                                                   prs.getFrom());
                            if (e) {
                                e.properties.ackack = true;
                            }
                        }
                        break;
                    case "unsubscribe":
                        // handle auto-remove
                        var handled = this._autoRemove(prs);
                        
                        // fire event
                        this.event("unsubscriptionReceived").trigger({
                            stanza: prs,
                            handled: handled
                        });
                        break;
                    case "unsubscribed":
                        //remove previous subscription state as needed
                        this._clearAckAck(prs.getFromJID());
						// acknowledge the acknowledgement
                        this.client.sendStanza("presence",
                                               "unsubscribe",
                                               prs.getFrom());
                        break;
                }
                // Event handled
                return true;
            },
            /**
             * TODO: there is a timing issue here that can occur. In order to establish roster subscriptions
             * fetch() has to be called. However, at the moment there's no guarantee that the fetch() response
             * callback has updated the entity set prior to this event coming from the server. So this method and
             * the fetch roster response method might end up trying to add the same entity to the set.
             * Maybe _removeContactsFromEntitySet() should be called in the fetch() callback rather than directly
             * in fetch()?
             * 
             * @private
             */
            _handleRosterUpdate: function(evt) {
                var node = jabberwerx.$(evt.selected);
                var item = node.children('item');                         
                var jid = item.attr('jid');
                var subscr = item.attr("subscription");
                
                var entity = this.client.entitySet.entity(jid);
                if (subscr != "remove") {
                    item = item.get()[0]; 
                    if (entity && entity instanceof jabberwerx.RosterContact) {
                        // update the existing contact
                        entity.setItemNode(item);
                    } else {
                        if (entity && entity instanceof jabberwerx.TemporaryEntity) {
                            entity.remove();
                            entity = null;
                        }
                        // add the new contact
                        var contact = new jabberwerx.RosterContact(item, this);
                        if (entity) {
                            contact.apply(entity, false);
                            entity.remove();
                        }
                        this.client.entitySet.register(contact);
                    }
                } else if (entity && entity instanceof jabberwerx.RosterContact) {
                    delete entity.properties.subscription;
                    delete entity.properties.ask;
                    entity.destroy();
                }
                // Event handled
                return true;
            },
            
            /**
             * @private
             */
            _handleStatusChange: function(evt) {
                switch (evt.data.next) {
                    case jabberwerx.Client.status_connected:
                        this.fetch();
                        break;
                    case jabberwerx.Client.status_disconnected:
                        this._removeContactsFromEntitySet();
                        break;
                }
            },

            /**
             * <p>Indicates the behavior when any subscription action is necessary.
             * When set to false, the RosterController will do nothing in the way of
             * automatically sending subscribes or unsubscribes</p>
             *
             * <p>The default value is {true} which allows
             * {@link jabberwerx.RosterController#autoaccept},
             * {@link jabberwerx.RosterController#autoaccept_in_domain}
             * and {@link jabberwerx.RosterController#autoremove} to determine
             * the behavior of the RosterController when dealing with subscriptions.</p>
             * @type Boolean
             */
            autoSubscription: true,
            /**
             * <p>Indicates the behavior when receving presence subscriptions.
             * The possible values are:</p>
             * <ol>
             * <li>{@link jabberwerx.RosterController.AUTOACCEPT_NEVER}: never
             * automatically accept subscription requests.</li>
             * <li>{@link jabberwerx.RosterController.AUTOACCEPT_IN_ROSTER}:
             * only automatically accept if the sender is in the roster, with
             * either [subscription=to] or [ask=subscribe]. <b>DEFAULT</b></li>
             * <li>{@link jabberwerx.RosterController.AUTOACCEPT_ALWAYS}:
             * always automatically accept subscription requests.</li>
             * </ol>
             *
             * <p>The default value is
             * {@link jabberwerx.RosterController.AUTOACCEPT_IN_ROSTER}.</p>
             *
             * @type String
             */
            autoaccept: 'in-roster', //using a string, since constants not yet defined
            /**
             * <p>Indicates if subscription requests will be automatically
             * accepted if the sender's domain exactly matches the client's
             * domain. This behavior supercedes that indicated by
             * {@link jabberwerx.RosterController#autoaccept}. In order to
             * disable all possible automatic subscription acceptance, both
             * this property and {autoaccept} must be set appropriately.</p>
             *
             * <p>The default value is {true}, which is to accept in-domain
             * subscription requests.</p>
             * 
             * @type Boolean
             */
            autoaccept_in_domain: true,
            /**
             * <p>Indicates if unsubscription requests will be automatically
             * processed (removing the sending contact from this roster).</p>
             *
             * <p>The default value if {true}, which is to automatically remove
             * contacts when receiving unsubscribe requests.</p>
             *
             * @type Boolean
             */
            autoremove: true,
            /**
             * <p>Specifies the default group to which contacts are added or updated to. This
             * property is only used when a groups parameter is not specified (or empty) in the
             * add, update or accept subscription method call. It defaults to an empty string
             * (i.e. no group). Modify this variable directly to set the default group.</p>
             *
             * <p>This is not linked to
             * {@link jabberwerx.ui.RosterView#getDefaultGroupingName}.</p>
             *
             * @type    String
             */
            defaultGroup: "" 
        }, 'jabberwerx.RosterController');
        
        /**
         * <p>One of the possible values which {@link jabberwerx.RosterController#autoaccept} may be set to.</p>
         * @constant
         * @type {String}
         */
        jabberwerx.RosterController.AUTOACCEPT_NEVER = "never";
        /**
         * <p>One of the possible values which {@link jabberwerx.RosterController#autoaccept} may be set to.</p>
         * @constant
         * @type {String}
         */
        jabberwerx.RosterController.AUTOACCEPT_IN_ROSTER = "in-roster";
        /**
         * <p>One of the possible values which {@link jabberwerx.RosterController#autoaccept} may be set to.</p>
         * @constant
         * @type {String}
         */
        jabberwerx.RosterController.AUTOACCEPT_ALWAYS = "always";
    }
})();
