/**
 * filename:        MUCController.js
 * created at:      2009/06/04T10:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.MUCController = jabberwerx.Controller.extend(/** @lends jabberwerx.MUCController.prototype */ {
        /**
         * @class
         * Controller for working with Multi-User Chat (MUC).
         *
         * @description
         * <p>Creates a new instance of MUCController with the given client.
         * This method registers the newly created MUCController under the
         * client's controllers as "muc" (e.g. access via
         * client.controllers["muc"])</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.MUCController">jabberwerx.MUCContoller Events</a></li>
         * </ul>
         *
         * @param {jabberwerx.Client} client The client object to use for communicating to the server
         * @throws {TypeError} If {client} is not an instance of jabberwerx.Client
         * @constructs jabberwerx.MUCController
         * @extends jabberwerx.Controller
         */
        init: function(client) {
            this._super(client, "muc");
            
            var caps = client.controllers.capabilities || jabberwerx.CapabilitiesController(client);
            caps.addFeature('http://jabber.org/protocol/muc');
            caps.addFeature('http://jabber.org/protocol/muc#user');
            caps.addFeature('jabber:x:conference');
            caps.addFeature('http://jabber.org/protocol/xhtml-im');
            
	        // Apply mucInviteReceived event
	        this.applyEvent('mucInviteReceived');
	    
            this.client.event("clientStatusChanged").
                    bind(this.invocation("_handleStatusChange"));
            // Bind for direct invite messages (XEP-0249)
            this.client.event("messageReceived").
                    bindWhen("message:not([type]) x[xmlns='jabber:x:conference']",
                             this.invocation("_handleDirectInvite"));
            // Bind for mediated invite messages (XEP-045 ~ Section 7.5.2)
            this.client.event("messageReceived").
                    bindWhen("message[type=normal] x[xmlns='http://jabber.org/protocol/muc#user']>invite, message:not([type]) x[xmlns='http://jabber.org/protocol/muc#user']>invite",
                             this.invocation("_handleMediatedInvite"));
        },
        
        /**
         * <p>Destroys this instance of MUCController.</p>
         */
        destroy: function() {
            this._removeRoomsFromEntitySet();
            this._super();
        },
        
        /**
         * <p>Updates the given entity. This method is not called directly;
         * instead call {@link jabberwerx.Entity#update}.</p>
         *
         * <p>This implementation simply triggers an "entityUpdated" event
         * with the given entity.</p>
         *
         * @param {jabberwerx.MUCRoom} entity The entity to update
         * @throws {TypeError} if {entity} is not an instance of jabberwerx.MUCRoom
         */
        updateEntity: function(entity) {
            if (!(entity && entity instanceof jabberwerx.MUCRoom)) {
                throw new TypeError("entity must be a MUCRoom");
            }
            
            this.client.entitySet.event("entityUpdated").trigger(entity);
        },
        /**
         * <p>Removes the given entity. This method is not called directly;
         * instead call {@link jabberwerx.Entity#remove}.</p>
         *
         * <p>This implementation uses the following algorithm:</p>
         * <ol>
         * <li>If the room is currently active and the client is currently
         * connected, exit the room (via {@link jabberwerx.MUCRoom#exit}.</li>
         * <li>Once the room is not active and/or the client is not connected,
         * it is destroyed (via {@link jabberwerx.Entity#destroy}); this will
         * ultimately trigger an "entityDestroyed" event for {entity}.</li>
         * </ol>
         *
         * @param {jabberwerx.MUCRoom} entity The entity to remove
         * @throws {TypeError} if {entity} is not an instance of jabberwerx.MUCRoom
         */
        removeEntity: function(entity) {
            if (!(entity && entity instanceof jabberwerx.MUCRoom)) {
                throw new TypeError("entity must be a MUCRoom");
            }
            
            if (entity.isActive() && this.client.isConnected()) {
                entity.event("roomExited").bind(function(evt) {
                    entity.event("roomExited").unbind(arguments.callee);
                    entity.destroy();
                });
                entity.exit();
            } else {
                entity.destroy();
            }
        },
        
        /**
         * <p>Retreives or creates a MUCRoom for the given JID. This method
         * implements the following algorithm:</p>
         * <ol>
         * <li>If a MUCRoom already exists in the owning client for {jid}, that
         * instance is returned.</li>
         * <li>A new MUCRoom is created for {jid}.</li>
         * <li>If there is an entity for {jid} but it is not a MUCRoom,
         * the data from the original entity is applied to the new MUCRoom
         * (via {@link jabberwerx.Entity#apply}), and the original entity is
         * removed from the owning client.</li>
         * <li>The new MUCRoom is registered with the owning client (triggering
         * an "entityCreated" event) and returned.</li>
         * </ol>
         *
         * @param {jabberwerx.JID|String} jid The room JID
         * @returns {jabberwerx.MUCRoom} The room for {jid}
         * @throws {jabberwerx.JID.InvalidJIDError} if {jid}
         *         is not an instanceof jabberwerx.JID, and cannot be
         *         converted to one.
         */
        room: function(jid) {
            jid = jabberwerx.JID.asJID(jid).getBareJID();

            var room = this.client.entitySet.entity(jid);
            if (!(room && room instanceof jabberwerx.MUCRoom)) {
                var ent = room;
                room = new jabberwerx.MUCRoom(jid, this);
                if (ent) {
                    room.apply(ent);
                    ent.remove();
                }
                this.client.entitySet.register(room);
            }
            
            return room;
        },
        
        /**
         * @private
         */
        _removeRoomsFromEntitySet: function() {
            this.client.entitySet.each(function(room) {
                if (room instanceof jabberwerx.MUCRoom) {
                    room.remove();
                }
            });
        },
        
        /**
         * @private
         */
        _handleStatusChange: function(evt) {
            switch (evt.data.next) {
                case jabberwerx.Client.status_disconnected:
                    this._removeRoomsFromEntitySet();
                    break;
            }
        },
        
        /**
         * @private
         *
         * Handles directed invite messages (XEP-0249). Extracts the invitor, room and reason 
         * attributes from the message. Then passes these attributes on to the _handleInvite method.
         *
         * @param   {jabberwerx.EventObject} evtObj The event object containing the
         *          jabberwerx.Message object and the selected x element.
         */
        _handleDirectInvite: function(evtObj) {
            var msg = evtObj.data;
            // If the message also contains a mediated invite then ignore and let the mediated
            // invite handler handle it
            if (jabberwerx.$("message:not([type]) x[xmlns='http://jabber.org/protocol/muc#user']>invite",
                msg.getDoc()).length == 0) {
                var invitor = msg.getFromJID();
                var x = jabberwerx.$(evtObj.selected);
                var room = x.attr("jid");
                var reason = x.attr("reason");
                this._handleInvite(msg, room, invitor, reason);
                return true;
            }
            return false;
        },
        
        /**
         * @private
         *
         * Handles mediated invite messages (XEP-0045). Extracts the invitor, room, reason and
         * password attributes from the message. Then passes these attributes on to the
         * _handleInvite method.
         *
         * @param   {jabberwerx.EventObject} evtObj The event object containing the
         *          jabberwerx.Message object and the selected invite element.
         */
        _handleMediatedInvite: function(evtObj) {
            var msg = evtObj.data;
            var invite = evtObj.selected;
            var invitor = jabberwerx.$(invite).attr("from");
            var room = msg.getFromJID();
            var reason = jabberwerx.$("reason", invite).text() || null;
            var password = jabberwerx.$("password", evtObj.data.getDoc()).text() || null;
            this._handleInvite(msg, room, invitor, reason, password);
            return true;
        },
        
        /**
         * @private
         *
         * This method is invoked from one of the invite parsing methods. If the room parameter is
         * present, we convert the room and invitor parameteres to JID object and, provided a
         * MUCRoom for this jid is not already active, invoke the mucInivteReceived event.
         *
         * @param   {jabberwerx.Stanza} stanza The stanza containing the invite
         * @param   {jabberwerx.JID | String} room The room for which this client has received an
         *          invite to.
         * @param   {jabberwerx.JID | String} [invitor] The user that sent the invite
         * @param   {String} [reason] The reason attribute for this invite
         * @param   {String} [password] The password attribute for this invite
         */
        _handleInvite: function(stanza, room, invitor, reason, password) { 
            var mucInvite = new jabberwerx.MUCInvite(stanza, room, invitor, reason, password);
            var ent = this.client.entitySet.entity(mucInvite.getRoom());
            if (!(ent && ent instanceof jabberwerx.MUCRoom && ent.isActive())) {
                this.event('mucInviteReceived').trigger(mucInvite);
            }
        },

        /**
         * @private
         */
        _sendSearchIq: function(muc, callback, form) {
            var queryString = "{jabber:iq:search}query";
            var query = new jabberwerx.NodeBuilder(queryString);

            var type = "get";
            if (form) {
                if(this.escapeSearch) {
                    try {
                        /**
                         * trim any preceeding/trailing whitespace so that escapeNode
                         * doesn't throw an "unnecessary" exception in this case.
                         */
                        var roomName = jabberwerx.$.trim(form.getFieldValue("room-name"));
                        form.setFieldValue("room-name", jabberwerx.JID.escapeNode(roomName));
                    } catch(e) {
                        callback(null, e);
                        return;
                    }
                }
                
                query.node(form.getDOM().data);
                type = "set";
            }

            this.client.sendIQ(type, muc, query.data,
                function (stanza) {
                    if (!stanza) {
                        callback(null, jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT);
                        return;
                    } 

                    var iq = new jabberwerx.IQ(stanza);
                    if (iq.isError()) {
                        callback(null, iq.getErrorInfo());
                    } else {
                        var form = jabberwerx.$("x", iq.getNode()).get(0);
                        if (!form) {
                            callback(null, jabberwerx.Stanza.ERR_SERVICE_UNAVAILABLE);
                        } else {
                            callback(new jabberwerx.XDataForm(null, form));
                        }
                    }
                }
            );
        },

        /**
         * <p>Starts the process of searching for a room. This method will
         * return the search criteria from the MUC server through the
         * callback.</p>
         *
         * @param {jabberwerx.JID | String} muc The MUC server.
         * @param {Function} callback Callback fired when a response is received
         * or an error is encountered.
         *
         * <p>The callback has the following signature.
         * <pre class="code">
         * function callback(form, err) {
         *     form;    // The returned {@link jabberwerx.XDataForm}.
         *              // null if the method failed.
         *     err;     // Error explaining the failure,
         *              // undefined if the method succeeded.
         * }
         * </pre>
         * </p>
         *
         * @throws {jabberwerx.JID.InvalidJIDError} if {muc} is not an instance of
         *         jabberwerx.JID, and cannot be converted to one
         * @throws {TypeError} If callback is undefined or not a function.
         */
        startSearch: function(muc, callback) {
            muc = jabberwerx.JID.asJID(muc).getDomain();
            if (!jabberwerx.$.isFunction(callback)) {
                throw new TypeError("The variable 'callback' must be a function.");
            }

            this._sendSearchIq(muc, callback);
        },

        /**
         * <p>Submits the filled-out search criteria to the MUC server.
         * Returns the results through the callback.</p>
         *
         * @param {jabberwerx.JID | String} muc The MUC server.
         * @param {jabberwerx.XDataForm} form the filled-out search criteria.
         * @param {Function} callback Callback fired when a response is received
         * or an error is encountered.
         *
         * <p>The callback has the following signature.
         * <pre class="code">
         * function callback(form, err) {
         *     form;    // The returned {@link jabberwerx.XDataForm}.
         *              // null if the method failed.
         *     err;     // Error explaining the failure,
         *              // undefined if the method succeeded.
         * }
         * </pre>
         * </p>
         *
         * @throws {jabberwerx.JID.InvalidJIDError} if {muc} is not an instance of
         *         jabberwerx.JID, and cannot be converted to one
         * @throws {TypeError} If callback is undefined or not a function.
         * @throws {TypeError} If form is not a jabberwerx.XDataForm.
         */
        submitSearch: function(muc, form, callback) {
            muc = jabberwerx.JID.asJID(muc).getDomain();
            if (!jabberwerx.$.isFunction(callback)) {
                throw new TypeError("The variable 'callback' must be a function.");
            }
            if (!(form && form instanceof jabberwerx.XDataForm)) {
                throw new TypeError("form must be an XDataForm");
            }

            this._sendSearchIq(muc, callback, form);
        },

        /**
         * <p>Indicates if a muc room search value should be escaped before
         * passing it to the server as part of a muc room iq:search query.</p>
         * <p>The default value is {true}, which is to escape muc room search values</p>
         * @type Boolean
         */
        escapeSearch: true
    }, "jabberwerx.MUCController");
}
