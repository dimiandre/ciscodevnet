/**
 * filename:        CapabilitiesController.js
 * created at:      2010/02/25T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

;(function(){
    if (jabberwerx) {
        jabberwerx.CapabilitiesController = jabberwerx.Controller.extend(/** @lends jabberwerx.CapabilitiesController.prototype */{
            /**
             * @class
             * <p>Controller class for capability functionality. This class is responsible for attaching the capabilities element to all
             * outgoing presence stanzas. It also registers for disco info iq messages and responds with the identity and list of
             * supported capabilities of this client. The feature set for this client can be modified using the add and remove Feature
             * methods, or by modifying the jabberwerx_config option before loading the library. This is referred to as 
             * "Outgoing Caps."</p>
             * 
             * <p>Any incoming caps information from a presence packet will be stored in this controller. If the hash referred to in the
             * caps portion of the presence is not known, a disco#info iq will be sent out to the owner of the unknown hash. The caps
             * hash will have a list of associated JIDs and a list of the capabilities associated with the hash. This is referred to as
             * "Incoming Caps."</p>
             *
             * <p>All methods that are Incoming Caps will not wait for the
             * disco#info request before returning. They return the current
             * information for that particular JID at the time of the method
             * call. It is best to call the Incoming Caps methods in the
             * "entityUpdated" event callback.</p>
             *
             * @description
             * Creates a new CapabilitiesController with the given client.
             *
             * @param {jabberwerx.Client} client The owning client
             * @throws {TypeError} If {client} is not valid
             * @constructs jabberwerx.CapabilitiesController
             * @extends jabberwerx.Controller
             */
            init: function(client) {
                this._super(client, "capabilities");
                this.client.event('beforePresenceSent').bind(this.invocation('_beforePresenceHandler'));
                this.client.event('presenceSent').bind(this.invocation('_resendCaps'));
                this.client.event('iqReceived').bindWhen("*[type='get'] query[xmlns='http://jabber.org/protocol/disco#info']", this.invocation('_discoInfoHandler'));
                jabberwerx.globalEvents.bind("resourcePresenceChanged", this.invocation("_handleResourcePresenceUpdate"));
                this.addFeature('http://jabber.org/protocol/caps');
                this.addFeature('http://jabber.org/protocol/disco#info');
            },
            /**
             * Destroys this CapabilitiesController. This method unbinds all
             * registered callbacks.
             */
            destroy: function() {
                this.client.event('beforePresenceSent').unbind(this.invocation('_beforePresenceHandler'));
                this.client.event('presenceSent').unbind(this.invocation('_resendCaps'));
                this.client.event('iqReceived').unbind(this.invocation('_discoInfoHandler'));
                jabberwerx.globalEvents.unbind("resourcePresenceChanged", this.invocation("_handleResourcePresenceUpdate"));
                this._super();
            },
            
            /**
             * @private
             */
            _updatePresence: function(jid) {
                if (this.client.isConnected()) {
                    var p = this.client.getCurrentPresence().clone();
                    jabberwerx.$(p.getNode()).
                               find("c[xmlns=http://jabber.org/protocol/caps]").
                               remove();
                    if (jid) {
                        p.setTo(jid);
                    }
                    this.client.sendStanza(p);
                }
            },

            /**
             * <p>Add a feature to the feature set. If the feature already exists within the set then it will
             * not be added again and false will be returned.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String} feature Feature to add to feature set
             * @returns {Boolean} <tt>true</tt> if successfully added, false if unsuccessful
             */
            addFeature: function(feature) {
                var retVal = false;
                if (typeof feature == 'string') {
                    if (!this.containsFeature(feature)) {
                        // Insert item onto end of feature set and sort
                        this._featureSet.push(feature);
                        this._featureSet.sort();
                        retVal = true;
                    }
                }
                if (retVal) {
                    this._updatePresence();
                }
                
                return retVal;
            },

            /**
             * <p>Remove feature from the feature set.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String} feature The feature in the feature set to remove
             * @returns {Boolean} <tt>true</tt> if successfully removed feature, false if unsuccessful
             */
            removeFeature: function(feature) {
                var retVal = false;
                if (typeof feature == 'string' || feature instanceof String) {
                    var index = jabberwerx.$.inArray(feature, this._featureSet);
                    if (index >= 0) {
                        this._featureSet.splice(index, 1);
                        retVal = true;
                    }
                }
                if (retVal) {
                    this._updatePresence();
                }
                
                return retVal;
            },

             /**
             * <p>Add a feature to the feature set for the given jid.
             * If the feature already exists within the set then it will
             * not be added again and false will be returned.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String|jabberwerx.JID} jid The jid for which to add feature
             * @param {String} feature Feature to add to feature set
             * @throws {TypeError} If feature is not a non empty string.
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} could not be converted into a JID
             * @returns {Boolean} <tt>true</tt> if successfully added, <tt>false</tt> if unsuccessful
             */
            addFeatureToJid: function(jid, feature) {
                if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                }
                var validatedJid = jabberwerx.JID.asJID(jid).getBareJID();
                if (!this._featureSetPerJid[validatedJid]) {
                    this._featureSetPerJid[validatedJid] = new jabberwerx.CapabilitiesController.JidFeatureSet(this);
                }
                var retVal = this._featureSetPerJid[validatedJid].addFeature(feature);
                if (retVal) {
                    this._updatePresence(jid);
                }
                
                return retVal;
            },

            /**
             * <p>Checks to see if the given feature is in the list for the given jid.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String|jabberwerx.JID} jid The jid for which to lookup a feature
             * @param {String} feature Feature to look up
             * @throws {TypeError} If feature is not a non empty string.
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} could not be converted into a JID
             * @returns {Boolean} <tt>true</tt> if found, <tt>false</tt> otherwise
             */
            containsFeatureForJid: function(jid, feature) {
                if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                }
                var validatedJid = jabberwerx.JID.asJID(jid).getBareJID();
                if (this._featureSetPerJid[validatedJid]) {
                    return this._featureSetPerJid[validatedJid].containsFeature(feature);
                }
                else {
                    return false;
                }
            },


            /**
             * <p>Remove feature from the feature set for the given jid.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String|jabberwerx.JID} jid The jid for which to lookup a feature
             * @param {String} feature The feature in the feature set to remove
             * @throws {TypeError} If feature is not a non empty string.
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} could not be converted into a JID
             * @returns {Boolean} <tt>true</tt> if successfully removed feature, <tt>false</tt> otherwise
             */
            removeFeatureFromJid: function(jid, feature) {
                var retVal = false;
                if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                }
                var validatedJid = jabberwerx.JID.asJID(jid).getBareJID();
                if (this._featureSetPerJid[validatedJid]) {
                    retVal = this._featureSetPerJid[validatedJid].removeFeature(feature);
                    if (this._featureSetPerJid[validatedJid].extraFeatures.length == 0) {
                        delete this._featureSetPerJid[validatedJid];
                    }
                }
                if (retVal) {
                    this._updatePresence(jid);
                }
                return retVal;
            },

            /**
             * This function is invoked after normal presence stanza is sent.
             * It iterates through the set of jids and resends
             * corresponing stanzas to each one in the list.
             * @private
             */
            _resendCaps: function(eventObj) {

                var presence = eventObj.data;
                //Only resend presence stanzas if we are not sending typed presence or directed
                //presence or the user is still online.
                if (!(presence.getTo() || presence.getType() || presence.getShow() == 'unavailable')) {
                    for (var jid in this._featureSetPerJid) {
                        var entity = this.client.entitySet.entity(jid);
                        if (entity && entity instanceof(jabberwerx.RosterContact) &&
                            entity.properties.subscription != "both" &&
                            entity.properties.subscription != "to") {
                            var p = presence.clone();
                            jabberwerx.$(p.getNode()).find("c[xmlns=http://jabber.org/protocol/caps]").remove();
                            p.setTo(jid);
                            this.client.sendStanza(p);
                        }
                    }
                }
            },

            /**
             * <p>This function returns presence stanza with corresponding
             * caps node.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @private
             */
            attachCapabilitiesToPresence: function(presence) {
                if (!(presence && presence instanceof jabberwerx.Presence)) {
                    throw new TypeError("presence must be a jabberwerx.Presence");
                }

                // Check for the'c' node (capability node)
                var builder = jabberwerx.$(presence.getNode()).
                        find("c[xmlns=http://jabber.org/protocol/caps]").
                        map(function() { return new jabberwerx.NodeBuilder(this); })[0];
                // Check to see if ver attribute is set
                var ver_attr = jabberwerx.$(presence.getNode()).
                        find("c[ver]").
                        map(function() { return new jabberwerx.NodeBuilder(this); })[0];

                //If version is already attached no actions required.
                if (!ver_attr) {
                    var ver = null;
                    //If not found c node, create one
                    if (!builder) {
                        builder = new jabberwerx.NodeBuilder(presence.getNode()).
                        element("{http://jabber.org/protocol/caps}c");
                    }
                    //Check if it is direct presence
                    if (presence.getTo()) {
                        var jid = new jabberwerx.JID(presence.getTo());
                        ver = this.getVerificationStringForJid(jid);
                    }
                    if (!ver) {
                       //If not found extra features for the jid, set default ver.
                       ver = this.generateVerificationString();
                    }
                    builder.attribute("hash", "sha-1");
                    builder.attribute("node", this.node);
                    builder.attribute("ver", ver);
                }
            },

             /**
             * <p>Helper function used in unit tests which returns verification string for the given jid.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {jabberwerx.JID|String} jid The jid for which to return the verification string for.
             * @throws {jabberwerx.JID.InvalidJIDError} If {jid} could not be converted into a JID
             * @returns {String} Returns null if match is not found, otherwise returns verification string
             */
            getVerificationStringForJid: function(jid) {
                var ver = null;
                var validatedJid = jabberwerx.JID.asJID(jid).getBareJID();
                if (this._featureSetPerJid[validatedJid]) {
                    ver = this._featureSetPerJid[validatedJid].ver;
                }
                return ver;
            },

             /**
             * <p>This functions returns verification string based on node value passed.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String} nodeVal for which to return the verification string for
             * @returns {String} Returns null if match is not found, otherwise returns verification string
             */
            getVerificationString: function(nodeVal) {
                var ver = null;

                if (nodeVal == this.node + '#' + this.generateVerificationString()) {
                    ver = this.generateVerificationString();
                } else {
                    for (var jid in this._featureSetPerJid) {
                        if (nodeVal == this.node + '#' + this._featureSetPerJid[jid].ver) {
                            ver = this._featureSetPerJid[jid].ver;
                            break;
                        }
                    }

                }
                return ver;
            },

             /**
             * <p>This function returns set of features for the given verification string.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String} ver Verification string for which to return the set of features for.
             * @throws {TypeError} if ver is not a non empty String.
             * @returns {String[]} Alphabetically sorted set of features
             */
            getFeaturesForVerificationString: function(ver) {
                var features = [];
                if (!(ver && typeof ver == 'string')) {
                   throw new TypeError("version must be non empty string");
                }

                if (ver == this.generateVerificationString()) {
                    features = this.getFeatureSet();
                } else {
                     for (var jid in this._featureSetPerJid) {
                        if (ver == this._featureSetPerJid[jid].ver) {
                            features =  jabberwerx.unique(this.getFeatureSet().concat(this._featureSetPerJid[jid].extraFeatures));
                            features.sort();
                         }
                    }
                }

                return features;
            },

            /**
             * <p>Get the alphabetically sorted array of features in the feature set</p>
             * <p>This is an Outgoing Caps method.</p>
             * @returns {String[]} Alphabetically sorted set of features
             */
            getFeatureSet: function() {
                return this._featureSet;
            },

            /**
             * <p>Returns true if the feature is in the feature set, or false if not present.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {String} feature for which to check for
             * @returns {Boolean} <tt>true</tt> if present, otherwise false
             */
            containsFeature: function(feature) {
                var retVal = false;
                for (var i=0; i<this._featureSet.length; i++) {
                    if (this._featureSet[i] == feature) {
                        retVal = true;
                        break;
                    }
                }
                return retVal;
            },

            /**
             * <p>Generate the verification string according to XEP-0115 Section 5. Uses the SHA-1 algorithm.
             * Uses the feature set and identity to determine the verification string.</p>
             * <p>This is an Outgoing Caps method.</p>
             * @param {Array} [features] Array of features
             * @returns {String} the verification string
             */
            generateVerificationString: function(features) {
                var feats = (features ? 
                    jabberwerx.unique(this._featureSet.concat(features)) :
                    this._featureSet);
                    
                return jabberwerx.CapabilitiesController._generateVerificationString(
                    [this._getIdentity()],
                    feats,
                    []);
            },

            /**
             * Builds up the identity from the individual parts of the identity member variable
             * @private
             * @returns {String} full identity
             */
            _getIdentity: function() {
                return (this.identity.category ? this.identity.category : '') + '/' +
                       (this.identity.type ? this.identity.type : '') + '//' +
                       (this.identity.name ? this.identity.name : '');
            },

            /**
             * Triggered before a presence stanza is sent. Attaches capability node to presence object.
             * @private
             * @param {jabberwerx.EventObject} eventObj
             * @returns {Boolean} false This allows other observers (if binded) to deal with the event also
             */
            _beforePresenceHandler: function(eventObj) {
                var presence = eventObj.data;
                //Only attach caps to presence stanzas without type and if the user
                //is online (not unavailable).
                if (!(presence.getType() || presence.getShow() == 'unavailable')) {
                    this.attachCapabilitiesToPresence(presence);
                }
                return false;
            },

            /**
             * Triggered on receiving an IQ stanza where the query has an xmlns='http://jabber.org/protocol/disco#info' attribute.
             * Replies with an IQ result to the requesting jid which contains the identity and feature set.
             * @private
             * @param {jabberwerx.EventObj} eventObj
             */
            _discoInfoHandler: function(eventObj) {

                jabberwerx.util.debug.log("received disco#info request...");
                var iq = eventObj.data;
                var iqResult = this._createDiscoInfoResponse(iq);
                this.client.sendStanza(iqResult);
                return true;
            },

            /**
             * Creates the iq result or error stanza to a disco info request.
             * @private
             * @param {jabberwerx.IQ} iq The IQ stanza of type 'get' to create the response for
             */
            _createDiscoInfoResponse: function(iq) {
                var iqResult = null;
                var nodeValue = jabberwerx.$(iq.getQuery()).attr('node');
                var ver = null;

                if (nodeValue) {
                   ver = this.getVerificationString(nodeValue);
                }

                if (!nodeValue || ver) {
                    // Creates a iq stanza of type result to reply to the incoming iq
                    var builder = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/disco#info}query");
                    if (ver) {
                        builder.attribute('node', this.node + '#' + ver);
                    }

                    var identity = {
                        "category": this.identity.category,
                        "type": this.identity.type};
                    if (this.identity.name) {
                        identity.name = this.identity.name;
                    }

                    builder.element('identity', identity);
                    var features = null;
                    if (!nodeValue) {
                        features = this._featureSet;
                    } else {
                        features = this.getFeaturesForVerificationString(ver);
                    }

                    jabberwerx.$.each(features, function() {
                                builder.element('feature', {"var": this});
                                return true;
                    });

                    iqResult = iq.reply(builder.data);
                } else {
                    iqResult = iq.errorReply(jabberwerx.Stanza.ERR_ITEM_NOT_FOUND);
                }

                return iqResult;
            },
            
            
            /**
            * @private
            * return all capsinfo that have the given jid as a reference.
            * fulljid is matched exactly, bare jid matches every resource
            */
            _findCapsInfo: function(jid) {
                var result = [];
                //for each caps info, see if there is at least one reference from this jid
                for (var ver in this._capsCache) {
                    var ci = this._capsCache[ver];
                    if (ci.getReferences(jid).length > 0) {
                        result.push(ci);
                    }
                }                    
                return result;
            },


            /**
             * @private
             * check caps cache for the first occurance of this jid. may be bare or full
             * returns null if not found
             */
            _firstCapsInfo: function(jid) {
                var ret = this._findCapsInfo(jid);
                if (ret.length > 0) {
                    return ret[0];
                }
                return null;
            },
            
            /**
             * <p>Checks capablility information of given JID for requested feature.
             * All resources of a given bare jid are checked until one supports the feature.
             * A full jid selects at most one caps info.</p>
             * <p>This is an Incoming Caps method.</p>
             * @param {String|jabberwerx.JID} jid entity to check for features
             * @param {String} feature requested feature to search jid's capsinfo for
             * @throws {TypeError} if feature is not a non empty string.
             * @returns {Boolean} <tt>true</tt> if jid's capsinfo has the given feature
             */
            isSupportedFeature: function(jid, feature) {
                if (!jabberwerx.util.isString(feature)) {
                    throw new TypeError("Feature must be a non empty string");
                }
                jid = jabberwerx.JID.asJID(jid);
                
                var caps = _findCapsInfo(jid);
                //does feature exist in any matching caps?
                for (var ci in caps) {
                    if (jabberwerx.$.inArray(feature, ci.features) != -1) {
                        return true;
                    }
                }                    
                return false;
            },
            
            /**
             * <p>Fetch all resources of the given jid that support the requested feature.
             * Given JID is coerced to bare.</p>
             * <p>This is an Incoming Caps method.</p>
             * @param {String|jabberwerx.JID} jid base jid for resource search
             * @param {String} feature Supported feature
             * @returns {JID[]} of supporting resources. may be empty
             * @throws {TypeError} if given feature is not a non-empty string
             * @throws {TypeError} if given jid is not valid
             */
            getSupportedResources: function(jid, feature) {
                if (!jabberwerx.util.isString(feature) || feature == "") {
                    throw new TypeError("Feature must be a non empty string");
                }
                jid = jabberwerx.JID.asJID(jid).getBareJID(); 
                
                var caps = this._findCapsInfo(jid); //barejid will match all resources
                var resmap = {};
                //caps is list of  capinfo with at least one matching resource
                for (var j = 0; j < caps.length; ++j) {
                    var ci = caps[j];
                    if (jabberwerx.$.inArray(feature, ci.features) != -1) {
                        var refs = ci.getReferences(jid);
                        for (var i = 0; i < refs.length; ++i) {
                            resmap[refs[i].toString()] = refs[i]; //dedup by overwriting any existing
                        }
                    }
                }
                //return the map of actual jids
                var result = [];
                jabberwerx.$.each(resmap, function(idx, jidstr) {
                    result.push(resmap[idx]);
                });
                return result;
            },

            /**
             * Get Features for the given JID.
             *
             * Returns an array of feature Strings for the given JID.
             * A bare JID will return a union of features for all resources
             * while a full JID will return only features that match exactly.
             * <p>This is an Incomming Caps method.</p>
             * 
             * @param {jabberwerx.JID|String} jid the bare or full JID to query
             * @returns {Array} unique feature strings, may be empty if no matches or features were found.
             * @throws {TypeError} if jid is not a valid {@link jabberwerx.JID}
             * @see http://xmpp.org/extensions/xep-0030.html for complete discussion of entity features.
             */
            getFeatures: function(jid) {
                jid = jabberwerx.JID.asJID(jid); //throws TypeError as needed
                var result = [];
                var caps = this._findCapsInfo(jid);
                for (var i = 0; i < caps.length; ++i) {
                    result = result.concat(caps[i].features);
                }
                return jabberwerx.unique(result);
            },
            
            /**
             * Get Identities for the given JID.
             *
             * Returns an array of Identity objects for the given JID.
             * A bare JID will return a union of entity identities for all resources
             * while a full JID will return only identities that match exactly.
             * <p>The returned identity object is defined
             * <pre class="code">
             *  {
             *      category {String}   The identity category.
             *      type     {String}   The category's type
             *      name     {String}   The human readable name for this identity
             *      xmlLang  {String}   The language code for the name property.
             *  }
             *  </pre>
             *  All properties will exist but their value may be an empty String.</p>
             * <p>This is an Incomming Caps method.</p>
             * 
             * @param {jabberwerx.JID|String} jid the bare or full JID to query
             * @returns {Array} Identity array, may be empty if no matches were found.
             * @throws {TypeError} if jid is not a valid {@link jabberwerx.JID}
             * @see http://xmpp.org/extensions/xep-0030.html for complete discussion of entity identities.
             * @see http://xmpp.org/registrar/disco-categories.html for registered categories and
             *      corresponding types.
             */
            getIdentities: function(jid) {
                jid = jabberwerx.JID.asJID(jid); //throws TypeError as needed
                var ids = [];
                var caps = this._findCapsInfo(jid);
                for (var i = 0; i < caps.length; ++i) {
                    //ci.identities is an array of parsed identity strings
                    ids = ids.concat(caps[i].identities);                                            
                }
                //map into an array of identity objects
                return jabberwerx.$.map(
                    jabberwerx.unique(ids),
                    function (id, idx) {
                        var idParts = id.split("/");
                        return {
                            category: idParts[0],
                            type: idParts[1],
                            xmlLang: idParts[2],
                            name: idParts[3]
                        };
                    });       
            },
            
            /**
             * @private
             * create a CapsINfo and disco#info as needed
             */
            _newCapsInfo: function(jid, ver, node) {
                var ci = new jabberwerx.CapabilitiesController.CapsInfo(this, node, ver);
                ci.addReference(jid);
                this._capsCache[ver] = ci;
                //fetch disco#info for this entity
                this.client.controllers.disco.fetchInfo(
                    jid, 
                    ci.id, 
                    function (identities, features, forms, err) {
                        if (err) {
                            //invalidate capsinfo by validating an empty instance
                            ci.validate();
                        } else {
                            ci._populate(identities, features, forms);
                        }
                        //if we have a valid caps configuration (ids,feats and forms match hash)
                        //event that all referenced entities have been updated.
                        ci._capsController._updateRefs(ci.ver);
                    });
            },
            /**
            * @private
            * fire the entityupdated event for all referenced jids for one particular CapsInfo
            */
            _updateRefs: function(ver) {                
                var ujids = [];
                var ci = this._capsCache[ver];
                //dedup resources
                for (var i = 0; i < ci.items.length; ++i) {
                    //work with bare jids
                    var bstr = jabberwerx.JID.asJID(ci.items[i]).getBareJIDString();
                    if (jabberwerx.$.inArray(bstr, ujids) == -1) {
                        ujids.push(bstr);
                    }
                }
                for (var i = 0; i < ujids.length; ++i) {
                    var entity = this.client.entitySet.entity(ujids[i]);//, this._capsCache[ver]._node);
                    if (entity) {
                        this.client.entitySet.event("entityUpdated").trigger(entity); //fire and updated event
                    }
                }
            },


            /**
             * @private
             * clear the items array from all cached CapsInfo (clear info references)
             */            
            _clearRefs: function() {
                for (var ver in this._capsCache) {
                    this._capsCache[ver].clearReferences();
                }                
            },

            /**
             * @private
             * walk caps cache and remove any invalid capsinfo
             */            
           _removeInvalid: function() {
                for (var ver in this._capsCache) {
                    var ci = this._capsCache[ver];
                    if (ci.status != "valid") {
                        delete this._capsCache[ver];
                    }
                }
            },
            
            /**
             * @private
             */
            _handleResourcePresenceUpdate: function(evt) {
                if (!this._handle115Receiving) {
                    return false; //bail if debugging
                }
                if (!this.client.connectedUser) {
                    return false; //
                }
                var _myjid = jabberwerx.JID.asJID(this.client.connectedUser.jid + '/' + this.client.resourceName);

                var presence = evt.data.presence;
                //bail if no caps info
                var ce = presence ? jabberwerx.$(presence.getNode()).find("c[xmlns=http://jabber.org/protocol/caps]") : null;
                if (!ce || ce.length == 0) {
                    return false;
                }
                var jid =  evt.data.fullJid;
                var ver = ce.attr("ver");
                var node = ce.attr("node");
                //if already in cache, fulljid compare
                if (jid.equals(_myjid) && !this._capsCache[ver]) {
                    //create and populate a capsinfo for our config
                    var ci = new jabberwerx.CapabilitiesController.CapsInfo(this, node, ver);
                    ci._populate(this._getIdentity(), this._featureSet, []);
                    ci.addReference(_myjid);
                    this._capsCache[ver] = ci;
                    return false;
                }
                //if we have no entity yet bail and wait for entityset to add
                var entity = this.client.entitySet.entity(jid.getBareJID());//, this._capsCache[ver]._node);
                if (!entity) {
                    return false;
                }
                //full jid should match exactly one capsinfo. remove from existing
                var oldci = this._firstCapsInfo(jid);
                //done if resource is unavailable
                if (presence && presence.getType() != 'unavailable') {                        
                    //add reference to exisiting capsinfo, create new as needed
                    ci = this._capsCache[ver];
                    //remove ref from existing id changing caps info
                    if (oldci && (oldci === ci)) {
                        return false;
                    } else if (oldci) {
                        oldci.removeReference(jid);
                    }
                    if (ci) {
                       ci.addReference(jid);
                        //update the entity
                    } else {
                        this._newCapsInfo(jid, ver, node);
                        return false;
                    }
                //if unavailble cleanup any old references
                } else if (oldci) {
                
                    oldci.removeReference(jid);
                }
                this.client.entitySet.event("entityUpdated").trigger(entity); //fire and updated event
                return false;
            },
            
            /**
             * This simple object defines the identity to use in disco#info messages. It defaults to:
             * <i>&lt;identity category='client' type='pc' name='Cisco AJAX XMPP Library'/&gt;</i>
             * and can be changed using CapabilitiesController.identity.<i>property</i> = <i>value</i>
             * Do not replace this identity property directy. Example:
             * <i>CapabilitiesController.identity = {category: 'cat', name: 'name', type: 'type'};</i>
             * as this will cause undefined behaviour of this class.
             * Calling identity.toString() returns a string in the format:
             * <i>category/type/xmlLang/name</i>
             * <b>Note</b>: No support for the xml:lang attribute in this release
             * <p>This is an Outgoing Caps property.</p>
             */
            identity : {
                category: 'client',
                name: 'Cisco AJAX XMPP Library',
                type: 'pc',
                toString: function() {
                    return (this.category ? this.category : '') + '/' +
                           (this.type ? this.type : '') + '//' +
                           (this.name ? this.name : '');
                }
            },

            /**
             * <p>This is the node value sent out as part of the capabilities of this client. It defaults
             * to 'http://jabber.cisco.com/caxl'. Most clients will want to change this value.
             * It can be modified directly via jabberwerx.client.controller['capabilities'].node, or
             * through the jabberwerx_config object.</p>
             * <p>This is an Outgoing Caps property.</p>
             * @type String
             */
            node : 'http://jabber.cisco.com/caxl',

            /**
             * Holds a sorted list of features supported by this client. Do not modify directy but instead use the
             * CapabilitiesController.*Feature*() methods. 'http://jabber.org/protocol/disco#info' is included by
             * default as all entities must support this.
             * @private
             */
            _featureSet : ['http://jabber.org/protocol/disco#info'],
             /**
              * Holds a list of objects that track jid --> features mappings.
              * @private
              */
            _featureSetPerJid : {},

             /**
              * cache of CapsInfo objects
              * @private
              */
            _capsCache : {},

             /**
              * debugging flag used in demo to prevent 115 handling. See capsdemo.html for useage
              * @private
              */
            _handle115Receiving : true,
            
            /**
             * Flag indicating  that caps resend/resubscription is in progress,
             * set to true after normal presence packet is sent out, and set to false
             * after all direct presences/subscriptions have been resent.
             * @private
             */
            _updateCaps: false
        }, 'jabberwerx.CapabilitiesController');

        jabberwerx.CapabilitiesController.JidFeatureSet = jabberwerx.JWModel.extend ({
             /**
              * @class
              * This class is responsible for tracking jid/features mappings for
              * incoming caps packets and generating verification strings for
              * the feature sets. It is used to stored additional features for
              * the jids. The base set is stored in the CapabilitiesController
              * class.
              * @description
              * <p> This class is embedded into CapabilitiesController since it is only
              * used withing it's scope.
              *
              * @param {jabberwerx.CapabilitiesController} capsController owning this class
              * @constructs jabberwerx.CapabilitiesController.JidFeatureSet
              * @extends jabberwerx.JWModel
              */
             init: function(capsController) {
                 this._super();
                 this._capsController = capsController;
             },
             /**
              * CapabilitiesController reference.
              * @private
              */
             _capsController : {},
             /**
              * Additional features stored for this jid.
              * @type {Array}
              */
             extraFeatures : [],
             /**
              * Verification string based on the set of all features including the base set.
              * It gets recalculated every time the set is altered.
              */
             ver: null,
             /**
              * Add a feature to the feature set. If the feature already exists within the set
              * then it will not be added again and false will be returned.
              * @param {String} feature to add to feature set
              * @throws {TypeError} if feature is not a non empty string.
              * @returns {Boolean} <tt>true</tt> if successfully added, false if unsuccessful
              */
             addFeature: function(feature) {
                 var retVal = false;
                 if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                 }

                 if (!this.containsFeature(feature)) {
                     this.extraFeatures.push(feature);
                     this.extraFeatures.sort();
                     this.ver =  this._capsController.generateVerificationString(this.extraFeatures);
                     retVal = true;
                 }

                 return retVal;
             },

             /**
              * Checks to see if the given feature is in the list for the given jid.
              * @param {String} feature The feature to look up
              * @throws {TypeError} if feature is not a non empty string
              * @returns {Boolean} <tt>true</tt> if found, false otherwise
              */
             containsFeature: function(feature) {
                 var retVal = false;
                 if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                 }

                 var index = jabberwerx.$.inArray(feature, this.extraFeatures);
                 if (index >= 0) {
                       retVal = true;
                 }
                 return retVal;
             },
             /**
              * Remove feature from the feature set.
              * @param {String} feature The feature in the feature set to remove
              * @throws {TypeError} if feature is not a non empty string.
              * @returns {Boolean} <tt>true</tt> if successfully removed feature, false if unsuccessful
              */
             removeFeature: function(feature) {
                 var retVal = false;
                 if (!(feature && typeof feature == 'string')) {
                    throw new TypeError("feature must be non empty string");
                 }
                 var index = jabberwerx.$.inArray(feature, this.extraFeatures);
                 if (index >= 0) {
                     this.extraFeatures.splice(index, 1);
                     this.ver =  this._capsController.generateVerificationString(this.extraFeatures);
                     retVal = true;
                 }

                 return retVal;
             }


        }, "jabberwerx.CapabilitiesController.JidFeatureSet");
    }
    
    jabberwerx.CapabilitiesController.CapsInfo = jabberwerx.JWModel.extend(/** @lends jabberwerx.CapabilitiesController.CapsInfo.prototype */{
            /**
             * @private
             * @class
             * An encapsulation of one unique capabilities set. Private, used
             * only internally by capability controller for incoming caps
             * presence packets.
             *
             * @description
             * <p>Creates a new CapabilitiesController.CapsInfo with the given CapabilitiesController.
             *  If fetching JID is defined it will be disco#info queried and validated on result.
             * @param {jabberwerx.CapabilitiesController} capsController owning controller
             * @param {string} node identity of this client cap
             * @param {string} [ver] verification hash we will validate against
             *
             * @throws {TypeError} If {capsController} is not valid
             * @constructs jabberwerx.CapabilitiesController.CapsInfo
             * @extends jabberwerx.JWModel
             */
            init: function(capsController, node, ver) {
                this._super();

                if (!capsController || !(capsController instanceof jabberwerx.CapabilitiesController)) {
                    throw new TypeError("CapsInfo must be created with a valid CapabilitiesController");
                }

                this._capsController = capsController;
                this._node = node;
                this.ver = (ver ? ver : "");
                this._lockedVer = this.ver != "";
                this.id = this._node + "#" + this.ver;
            },

            /**
             * <p>Verify our "locked" ver is the same as a newly generated one,
             * validates the locked ver is valid for current idents/features/forms.
             * Also insures identities, features and forms are legal. Duplicate
             * identities are specifically called out in xep 115. Finally sets status
             * to "valid" or "invalid"
             * status = "valid"  ==> features et all match locked verify string
             * status = "invalid" ==> features do not match, bad data during a populate
             * @returns {Boolean} <tt>true</tt> if status=="valid"
             */
            validate: function() {
                this.status = "invalid";
                this.id = this._node + '#' + this.ver;
                //make sure all identities are unique, dups are expressly forbidden
                var uniqueIdentities = [].concat(this.identities); 
                if (jabberwerx.unique(uniqueIdentities).length != this.identities.length) {
                    jabberwerx.util.debug.log("Duplicate identifier found validating caps for " + this.id);
                    return false;
                }
                var uniqueFeatures = [].concat(this.features); 
                //dup features
                if (jabberwerx.unique(uniqueFeatures).length != this.features.length) {
                    jabberwerx.util.debug.log("Duplicate feature found validating caps for " + this.id);
                    return false;
                }
                //check for duplicate, malformed form types
                for (var i = 0; i < this._rawforms.length; ++i) {
                    var ftfld = this._rawforms[i].getFieldByVar("FORM_TYPE");
                    var ft = this._rawforms[i].getFORM_TYPE();
                    ftvals = ftfld.getValues();
                    for (var j = 0; j < ftvals.length; ++j) {
                        if (ftvals[j] != ft) {
                            return false; //multiple values with differnt cdata
                        }
                    }
                    if (this.forms[ft] != null) {
                        return false; //duplicate formtype
                    }
                    this.forms[ft] = this._rawforms[i];
                }
                var newVer = this._generateVerString();
                //floating ver, using this capinfo as a verify string generator
                if (!this._lockedVer) {
                    this.ver = newVer;
                }
                if (newVer == this.ver) {
                    this.status = "valid";
                    this.id = this.ver;
                }
                return this.status == "valid";
            },

            /**
             * @private
             * convenience method to call static generator
             */
            _generateVerString: function() {
                var frms = [];
                for (var onefrm in this.forms) {
                    frms.push(this.forms[onefrm]);
                }
                return jabberwerx.CapabilitiesController._generateVerificationString(this.identities, this.features, frms);
            },

            /**
             * <p>populate this caps from given disco info result packet or preparsed arrays
             * of identities, features and forms.
             * validates after populating and returns valid state. </p>
             *
             * <p>arguments may be
             *   an iq/query disco#info result node
             *   an object containing
             *       .identities {array of string}, optional may be empty, undefined, single string or array of string
             *       .features {array of string}, optional may be empty , undefined,single string or array of string
             *       .forms {map |DATA-FORM] map is [FORM_TYPE] ==> DATA-FORM}, optional may be empty/undefiend
             *   An error node (<element of type="error")
             *   This allows controller to populate as a pass through disco#info result or
             *   supply exact cap info when constructing our own</p>
             *
             * @param {String|String[]} identities list of identites to populate
             * @param {String|String[]} features list of features to populate
             * @param {XDataForm|XDataForm[]} forms list of xdata form strings to populate
             * @returns {Boolean} <tt>true</tt> if populated info matches our verification string
             * @throws {TypeError} If a  parameter is not valid (not an array of strings/xdataforms)
             */
            _populate: function(identities, features, forms) {
                //reset state
                this.identities = [];
                this.features = [];
                this.forms = {};
                this._rawforms = [];
                
                this.status = "invalid";
                this.id = this._node + '#' + this.ver;

                //preparsed lists provided
                if (identities) {
                    this.identities = this.identities.concat(identities);
                }
                if (features) {
                    this.features = this.features.concat(features);
                }
                if (forms) {
                    for (var i = 0; i < forms.length; ++i) {
                        //XEP 115 5.4.3.6, ignore forms with not hidden or missing FORM_TYPE fields
                        var ftvar = forms[i].getFieldByVar("FORM_TYPE");
                        if (ftvar && (ftvar.getType() == "hidden")) {
                            this._rawforms.push(forms[i]);
                        }
                    }
                }
                
                //may not validate if no initial verification string provided but lists should still be sorted
                this.identities.sort(); this.features.sort();
                
                return this.validate();
            },

            /**
             *@private
             *@param {String|JID} jid to add to this capsinfo
             *add a jid to item array.
             */
            addReference: function(jid) {
                if (this.indexOfReference(jid, true) == -1) {
                    this.items.push(jid.toString());
                }
            },
            
            /**
             *@private
             *@param {String|JID} jid to remove from this capsinfo
             */
            removeReference: function(jid) {
                var idx = this.indexOfReference(jid, true);
                if (idx != -1) {                
                    this.items.splice(idx, 1);
                }
            },

            /**
             *@private
             *true if given jid is in item array
             * if jid is bare any resource will count as a ref, exact match on full
             */
            hasReference: function(jid) {
                return this.indexOfReference(jid) != -1;
            },
            
            /**
             *@private
             *index of the given ref in the item array
             * if jid is bare first resource encountered is used, exact match on full
             * returns -1 if no match could be found
             *exact override bare/full behavior and forces an exact match
             */
            indexOfReference: function(jid, exact) {
                var tjid = jabberwerx.JID.asJID(jid); 
                exact = exact || (tjid.getResource() != '');
                if (!exact) {
                    tjid = tjid.getBareJID(); //save a few cycles I guess
                }
                for (var i = 0; i < this.items.length; ++i) {
                    var ijid = jabberwerx.JID.asJID(this.items[i]);
                    if ((exact && tjid.equals(ijid)) ||
                        (!exact && tjid.equals(ijid.getBareJID()))) {
                        return i;
                    }
                }
                return -1;
            },

            /**
             *@private
             * get a list of matching jids. If bare matches all resources, otherwise exact match only (one elelemnt array returned)
             */
            getReferences: function(jid) {
                var result = [];
                var tjid = jabberwerx.JID.asJID(jid);
                var exact = tjid.getResource() != '';
                for (var i = 0; i < this.items.length; ++i) {
                    var ijid = jabberwerx.JID.asJID(this.items[i]);
                    if (exact && ijid.equals(tjid)) {
                        return [ijid];
                    }
                    if (!exact && ijid.getBareJID().equals(tjid)) {
                        result.push(ijid);
                    }
                }
                return result;            
            },

            /**
             * @private
             * clear references
             */
            _clearReferences: function() {
                this.items = [];
            },
            
            /**
             * CapabilitiesController reference.
             * @private
             * todo - not sure this is needed
             */
            _capsController : {},

            /**
             *@private
             * uri of this client cap
             */
            _node: '',

            /**
             *@private
             * XDataform list before validation and copy to forms
             */
            _rawforms: [],
            
            /**
             *@private
             *  "locked" Verification string passed to constructor and used for
             *  validation.
             */
            ver: '',

            /**
             * status of this cc info, "pending" "valid" "invalid"
             * @type String
             */
            status: "invalid",

            /**
             * List of associated identity strings "catagory/type/lang/name"
             * @type Array
             */
            identities: [],

            /**
             * List of associated features
             * @type Array
             */
            features: [],

            /**
             * extra forms map of [FORM-TYPE] [data form]
             * @type FORM-TYPE
             */
            forms : {},

            /**
             * list of full jids that are currently tracked (sent presence) and support this client cap
             * @type {Array}
             */
            items: []
    }, "jabberwerx.CapabilitiesController.CapsInfo");

    /**
     * @private
     * Generate the verification string according to XEP-0115 Section 5. Uses the SHA-1 algorithm.
     * Uses the feature set, identity and attached forms to determine the verification string.
     * @param {String[]} identities  string array of catagory/type/lang/name, may be empty
     * @param {String[]} features string array of feature var values, may be empty
     * @param {XDataForm[]] optional array of XDataForms  may be empty
     * @param {boolean} [noEncode] optional flag to return raw verify string, used by unit-tests
     * @returns {String} the verification string
     */
    jabberwerx.CapabilitiesController._generateVerificationString = function(identities, features, forms, noEncode) {
        //helper func to walk fields and values, producing a xep115 compliant serialization
        //FORM_TYPE<field1<val1<...<valn<...<fieldn fields and values alpha sorted
        var __form115rep = function(form) {
            var result =  form.getFORM_TYPE() + "<";
            var ta = jabberwerx.$.map(form.fields,
                        function(field) {
                            var fstr = field.getVar();
                            if (fstr == "FORM_TYPE") {
                                return null;
                            }
                            fa = [].concat(field.getValues());
                            fa.sort();
                            return fstr + "<" + fa.join("<"); 
                        });            
            ta.sort();
            result += ta.join("<") + "<";
            return result;
        };

        var baseStr = ""
        if (identities && identities.length > 0) {
            identities.sort();
            baseStr += identities.join('<') + '<';
        }
        if (features && features.length > 0) {
            features.sort();
            baseStr += features.join('<') + '<';
        }
        //make an array of 115 ver comparitor strings for the forms, sort it and join
        if (forms && forms.length){
            var verArr = [];
            for (var i = 0; i < forms.length; ++i) {
                verArr.push(__form115rep(forms[i]));
            }
            verArr.sort();
            baseStr += verArr.join(''); //joining with an empty string, form reps already include final "< "
        }
        if (!noEncode) {
            return jabberwerx.util.crypto.b64_sha1(jabberwerx.util.crypto.utf8Encode(baseStr));
        }
        return baseStr;
    }
    
    jabberwerx.Client.intercept({
        //intercept client's init and add CapsController
        init: function() {
            this._super.apply(this, arguments); //pass all args onto base class        
            //capabilitiesController and add features and identity, if specified in jabberwerx._config
            var capController = new jabberwerx.CapabilitiesController(this);
            if (jabberwerx._config.capabilityFeatures) {
                for (var i=0; i<jabberwerx._config.capabilityFeatures.length; i++) {
                    capController.addFeature(jabberwerx._config.capabilityFeatures[i]);
                }
            }
            if (jabberwerx._config.capabilityIdentity) {
                capController.identity.category = jabberwerx._config.capabilityIdentity.category;
                capController.identity.type = jabberwerx._config.capabilityIdentity.type;
                capController.identity.name = jabberwerx._config.capabilityIdentity.name;
                capController.node = jabberwerx._config.capabilityIdentity.node;
            }
        },
        //override _connected to send out initial presence packet.
        //JJF I can't imagine this will stay here but wanted to move it out of client
        _connected: function() {
            this._super.apply(this, arguments);
            if (!this.getCurrentPresence()) {
                this.sendPresence();
            }
        }
    });
    
    

})();
