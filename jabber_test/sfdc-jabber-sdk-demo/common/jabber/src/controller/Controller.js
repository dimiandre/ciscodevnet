/**
 * filename:        Controller.js
 * created at:      2009/05/11T07:48:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.Controller = jabberwerx.JWModel.extend(/** @lends jabberwerx.Controller.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Abstract base class for all controller types.</p>
         *
         * <p>All controller types have a unique simple name (e.g. "roster" for
         * the RosterController, or "capabilities" for the
         * CapabilitiesController).</p>
         *
         * @description
         * Creates a new jabberwerx.Controller with the given owning client.
         * This method sets the name and client for this Controller, and
         * stores it in the {@link jabberwerx.Client#controllers} hashtable.
         *
         * @param {jabberwerx.Client} client The owning client
         * @param {String} name The common name for this controller
         * @throws {TypeError} if {client} is not the correct type, or if
         *                   {name} is not a non-empty string.
         * @constructs jabberwerx.Client
         * @extends jabberwerx.JWModel
         */
        init: function(client, name) {
            this._super();
            
            if (!name || typeof name != 'string') {
                throw new TypeError("name must be a non-empty string");
            }
            if (!(client instanceof jabberwerx.Client)) {
                throw new TypeError("client must be a jabberwerx.Client");
            }
            
            this.client = client;
            this.name = name;
            
            // overwrites any other controller with this name
            var orig = client.controllers[name];
            if (orig) {
                orig.destroy();
            }
            client.controllers[name] = this;
        },
        /**
         * <p>Destroys this Controller. This method deletes this controller
         * from its owning client.</p>
         *
         * <p>Subclasses SHOULD override this method to perform any additional
         * cleanup (e.g. removing event callbacks), but MUST call the
         * superclass' implementation (this._super()).</p>
         */
        destroy: function() {
            if (    this.client &&
                    this.client.controllers &&
                    this.client.controllers[this.name]) {
                delete this.client.controllers[this.name];
                delete this.client;
            }
            
            this._super();
        },
        
        /**
         * <p>Users should not call this method directly. Instead, call
         * {@link jabberwerx.Entity#update}.</p>
         *
         * @param {jabberwerx.Entity} entity The entity to update
         * @throws {TypeError} if entity is not an instance of Jabberwerx.Entity
         * @returns {jabberwerx.Entity} updated entity
         */
        updateEntity: function(entity) {
            if (!(entity && entity instanceof jabberwerx.Entity && entity.controller === this)) {
                throw new TypeError("invalid entity to update");
            }
            this.client.entitySet.event("entityUpdated").trigger(entity);

            return entity;
        },
        
        /**
         * <p>Users should not call this method directly. Instead, call
         * {@link jabberwerx.Entity#remove}.</p>
         *
         * @param {jabberwerx.Entity} entity The entity to remove
         * @throws {TypeError} if entity is not an instance of Jabberwerx.Entity
         * @returns {jabberwerx.Entity} deleted entity
         */
        removeEntity: function(entity) {
            if (!(  entity &&
                    entity instanceof jabberwerx.Entity &&
                    entity.controller === this)) {
                throw new TypeError("invalid entity to delete");
            }
            entity.destroy();
            
            return entity;
        },
        
        /**
         * The client object that is used to manage roster API calls
         * 
         * @type jabberwerx.Client
         */
        client: null,
        /**
         * The name of this controller.
         *
         * @type String
         */
        name: ''
    }, 'jabberwerx.Controller');
}