/**
 * filename:        PEPController.js
 * created at:      2010/04/08T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.PEPController = jabberwerx.PubSubController.extend(/** @lends jabberwerx.PEPController.prototype */{
        /**
         * @class
         * <p>Controller to manage PEP nodes.</p>
         *
         * @description
         * <p>Creates a new PEPController with the given client.</p>
         *
         * @param   {jabberwerx.Client} client The client object to use for communicating to the server
         * @throws  {TypeError} If {client} is not a jabberwerx.Client
         * @constructs  jabberwerx.PEPController
         * @extends     jabberwerx.PubSubController
         */
        init: function(client) {
            this._super(client, "pep");
            this._cleanupMode = jabberwerx.PubSubController.CLEANUP_DELEGATES;
        },
        /**
         * <p>Destroys this PEPController. This method removes all PEPNodes
         * from the owning client.</p>
         */
        destroy: function() {
            this._super();
        },
        
        /**
         * <p>Return the specified PEPNode. Creates the node
         * if one doesn't already exist. This method overrides
         * the superclass to return PEPNode instances, and to
         * make {jid} an optional argument.</p>
         *
         * @param   {String} node the node
         * @param   {jabberwerx.JID|String} [jid] The JID.
         * @returns {jabberwerx.PEPNode} The requested PEPNode.
         * @throws  {TypeError} if {node} is not a string.
         * @throws  {jabberwerx.JID.InvalidJIDError} if {jid} defined and
         *          is invalid.
         */
        node: function(node, jid) {
            return this._super(node, jid);
        },
        
        /**
         * @private
         * Override to allow {jid} to be optional.
         */
        _prepareJid: function(jid) {
            return (jid) ? this._super(jid) : undefined;
        },
        /**
         * @private
         */
        _getNodeClass: function() {
            return jabberwerx.PEPNode;
        }
    }, "jabberwerx.PEPController");
}
