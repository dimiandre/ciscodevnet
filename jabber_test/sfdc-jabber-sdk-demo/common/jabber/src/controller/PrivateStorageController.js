/**
 * filename:        PrivateStorageController.js
 * created at:      2009/11/04T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */


if (jabberwerx) {    
    jabberwerx.PrivateStorageController = jabberwerx.Controller.extend(/** @lends jabberwerx.PrivateStorageController.prototype */{
        /**
         * @class
         * <p>Controller class for dealing with private storage functionality.</p>
         * 
         * @description
         * <p>Creates a new PrivateStorageController with the given client.</p>
         * 
         * @param       {jabberwerx.Client} client The client object to use for communicating to the server
         * @throws      {TypeError} If {client} is not an instance of jabberwerx.Client
         * @constructs  jabberwerx.PrivateStorageController
         * @extends     jabberwerx.Controller
         */        
        init: function(client) {
            this._super(client, "privateStorage");
           
        },
        
        /**
         * <p>Destroys private storage controller.</p>
         */        
        destroy: function() {
            this._super();
        },
        
        /**
         * <p>Fetches a private storage data and invokes the callback function passing it received xml element or error encountered.</p>
         *
         * @param {String} ename Ename string for the privacy storage element.
         * @param {function} callback The function called after the server response is processed. If a server error
         * occurs, the function will be passed the error stanza response.
         * <p>The signature of the function is <code>callback(private_data, errorStanza)</code>.</p>
         * <p>The callback will be invoked in the context of the PrivateStorageController
         * e.g. <code>callback.call(this, private_data, errorStanza)</code>. Therefore in the callback method <code>this</code> will 
         * refer to the instance of PrivateStorageController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
         * @throws {TypeError} If {ename} is not a properly formatted ename element.          
         * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
         */
         fetch: function(ename, callback) {
            if (!this.client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (!jabberwerx.$.isFunction(callback)) {
                throw new TypeError('The callback param must be a function');
            }  
            
            if (!ename) {
                throw new TypeError('Private storage element name should not be empty.');
            }
                      
            var query = new jabberwerx.NodeBuilder("{jabber:iq:private}query");
            var prv_elem = query.element(ename);
  
            var that = this;
               
            this.client.sendIq("get", null, query.data,
                function (stanza) {
                    var error = that._checkError(stanza);
                    var private_data = jabberwerx.$(stanza).find(prv_elem.data.tagName + "[xmlns=" + prv_elem.data.getAttribute("xmlns") + "]").get(0);    
                    if (callback) {
                        callback.call(that, private_data, error);                    
                    }                                                         
  
                }            
            );   
            
         },

       /**
         * <p>Updates the server with new private storage data.</p>
         *
         * @param   {Element} private_elm Document element.
         * @throws  {TypeError} If {private_elm} is undefined or not a document element.
         */         
         update: function(private_elm) {
             if (!jabberwerx.isElement(private_elm)) {
                  throw new TypeError('Private storage should be an element.');
             }
             var query = new jabberwerx.NodeBuilder("{jabber:iq:private}query").
                         node(private_elm).parent; 
             this.client.sendIq("set", null, query.data);            
         },
         
       /**
         * <p>Clears private storage data.</p>
         *
         * @param {String} ename Ename string for the privacy storage element.
         * @throws {TypeError} If {ename} is not a properly formatted ename element.
         */         
         remove: function(ename) {
            if (!ename) {
                throw new TypeError('Private storage element name should not be empty.');
            }
                
            var query = new jabberwerx.NodeBuilder("{jabber:iq:private}query").
                            element(ename).parent; 
            this.client.sendIq("set", null, query.data);            
         },                 
           
        /**
         * Check a response element to see if it contains an error.
         * 
         * @private
         * @param {Element} parentElement The element to check to see if it contains a child &lt;error&gt; 
         * @returns {Element|undefined} The error element if it exists or else undefined 
         */
        _checkError: function(parentElement) {
            return jabberwerx.$(parentElement).children("error")[0];
        }

 
    }, 'jabberwerx.PrivateStorageController');
}
