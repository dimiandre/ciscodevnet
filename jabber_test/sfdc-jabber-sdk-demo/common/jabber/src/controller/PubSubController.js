/**
 * filename:        PubSubController.js
 * created at:      2009/11/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.PubSubController = jabberwerx.Controller.extend(/** @lends jabberwerx.PubSubController.prototype */{
        /**
         * @class
         * <p>Controller that provides support for XEP-0060 Publish-Subscribe</p>
         *
         * @description
         * <p>Creates a new PubSubController for the given client.</p>
         *
         * <p><b>NOTE:</b> Only subclasses should specify a value for {name}.
         * Otherwise, only {client} should be passed into this constructor.</p>
         *
         * @param   {jabberwerx.Client} client The client object to use for communicating to the server
         * @param   {String} [name] The name of this controller (default is "pubsub")
         * @throws  {TypeError} If {client} is not a jabberwerx.Client
         * @constructs  jabberwerx.PubSubController
         * @extends     jabberwerx.Controller
         */
        init: function(client, name) {
            this._super(client, name || "pubsub");
            
            this._cleanupMode = jabberwerx.PubSubController.CLEANUP_ALL;
            this.client.event("clientStatusChanged").
                 bind(this.invocation("_handleStatusChange"));
        },
        /**
         * <p>Destroys this PubSubController and removes any event callbacks it registered.</p>
         */
        destroy: function() {
            // make sure all nodes are removed
            this._removeNodesFromEntitySet(jabberwerx.PubSubController.CLEANUP_ALL);

            this.client.event("clientStatusChanged").
                 unbind(this.invocation("_handleStatusChange"));

            this._super();
        },

        /**
         * <p>Return the specified PubSubNode. Creates the node
         * if one doesn't already exist.</p>
         *
         * @param {String} node the node
         * @param {jabberwerx.JID|String} jid The JID.
         * @returns {jabberwerx.PubSubNode} The requested PubSubNode.
         * @throws {TypeError} if {node} is not a string.
         * @throws {jabberwerx.JID.InvalidJIDError} if {jid} is an invalid JID.
         */
        node: function(node, jid) {
            if (!(node && typeof(node) == "string")) {
                throw new TypeError("node must be a non-empty string");
            }
            jid = this._prepareJid(jid);

            var pstype = this._getNodeClass();
            var pubsub = this.client.entitySet.entity(jid, node);
            if (!(pubsub && pubsub instanceof pstype)) {
                var tmp = pubsub;
                pubsub = new pstype(jid, node, this);
                if (tmp) {
                    pubsub.apply(tmp);
                    tmp.remove();
                }
                this.client.entitySet.register(pubsub);
            }

            return pubsub;
        },
        /**
         * @private
         * Prepares the JID by ensuring {jid} is a {@link jabberwerx.JID}
         * and getting the bare JID version of that.
         *
         * @throws  {jabberwerx.JID.InvalidJIDError} If {jid} is invalid
         */
        _prepareJid: function(jid) {
            return jabberwerx.JID.asJID(jid).getBareJID();
        },
        /**
         * @private
         */
        _getNodeClass: function() {
            return jabberwerx.PubSubNode;
        },

        /**
         * @private
         */
        _removeNodesFromEntitySet: function(mode) {
            if (!mode) {
                mode = this._cleanupMode;
            }
            
            var that = this;
            var cleanupFn;
            if (mode == jabberwerx.PubSubController.CLEANUP_ALL) {
                cleanupFn = function(node) {
                    node.remove();
                }
            } else if (mode == jabberwerx.PubSubController.CLEANUP_DELEGATES) {
                cleanupFn = function(node) {
                    if (node.delegate) {
                        node.remove();
                    } else {
                        // clear delegate list
                        node.properties.delegates = {};
                    }
                }
            } else {
                // noop
                cleanupFn = function(node) {}
            }
            
            this.client.entitySet.each(function(node) {
                if (node.controller === that) {
                    cleanupFn(node);
                }
            });
        },
        
        /**
         * @private
         */
        _handleStatusChange: function(evt) {
            switch (evt.data.next) {
                case jabberwerx.Client.status_disconnected:
                    this._removeNodesFromEntitySet();
                    break;
            }
        },
        
        /**
         * @private
         * Property to indicate what clean up happens upon disconnect
         */
        _cleanupMode: null
    }, "jabberwerx.PubSubController");
    /**
     * @private
     * @constant
     * Indicates all nodes should be removed upon disconnect
     */
    jabberwerx.PubSubController.CLEANUP_ALL = "all";
    /**
     * @private
     * @constant
     * Indicates only nodes with a delegate should be removed upon disconnect
     */
    jabberwerx.PubSubController.CLEANUP_DELEGATES = "delegates";
}
