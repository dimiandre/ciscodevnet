/**
 * filename:        PrivacyListController.js
 * created at:      2009/05/15T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */


if (jabberwerx) {    
    jabberwerx.PrivacyListController = jabberwerx.Controller.extend(/** @lends jabberwerx.PrivacyListController.prototype */{
        /**
         * @class
         * <p>Controller class for privacy lists functionality.</p>
         * 
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.PrivacyListController">jabberwerx.PrivacyListController Events</a></li>
         * </ul>
         * </p>
         * @description
         * <p>Creates a new PrivacyListController with the given client.</p>
         * 
         * @param       {jabberwerx.Client} client The client object to use for communicating to the server
         * @throws      {TypeError} If {client} is not an instance of jabberwerx.Client
         * @constructs  jabberwerx.PrivacyListController
         * @extends     jabberwerx.Controller
         */        
        init: function(client) {
            this._super(client, "privacyList");

            // setup events
            this.applyEvent("errorEncountered");
            this.applyEvent("privacyListApplied");
            this.applyEvent("privacyListUpdated");
            this.applyEvent("privacyListRemoved");
           
            // setup handlers
            this.client.event('afterIqReceived').bindWhen("[type=set] query[xmlns=jabber:iq:privacy] list", this.invocation('_handlePrivacyListUpdate'));
 
        },
        
        /**
         * <p>Destroys privacy list controller and removes any event callbacks it registered.</p>
         */        
        destroy: function() {
            // teardown handlers
            this.client.event('afterIqReceived').unbind(this.invocation('_handlePrivacyListUpdate'));
            this._super();
        },
        
        /**
         * <p>Fetch a privacy list by name, creates {@link jabberwerx.PrivacyList} object and adds it to the map.</p>
         *
         * @param {String} list Name of the privacy list.
         * @param {function} [callback] The function called after the server response is processed. If a server error
         * occurs, the function will be passed the error stanza response.
         * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
         * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will
         * refer to the instance of PrivacyListController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object</p>
         * @throws {TypeError} If {list} is not a &lt;string/&gt;&lt;PrivacyList/&gt;          
         * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
         */
         fetch: function(list, callback) {
            if (!this.client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                throw new TypeError('The callback param must be a function');
            }
            
            if (!list || typeof list != 'string') {
                throw new TypeError('Privacy list name should be non empty string.'); 
            }  
            this._doFetch(list, true, callback);      
         },
         
        /**
         * 
         * Performs actual fetch, provides code reuse for refreshing after list update/remove.
         * @private
         */         
        _doFetch: function(list, create, callback) {   
            //The variable 'that' will be visible to the senqIq callback
            var that = this;
            var errorOccured = false;
            var removed = false;                               
            //Send privacy list fetch query
            this.client.sendIq('get', null, "<query xmlns='jabber:iq:privacy'><list name='" + list + "'/></query>", 
                function (stanza) {     
                    var error = that._checkError(stanza);
                    var prvListNode = jabberwerx.$(stanza).find("list").get(0);
                    var list_name = prvListNode.getAttribute("name");
                    var privListObj = that._privacyLists[list_name];
                    if (error) {
                        //If item-not-found error, we just create a list
                        var item_not_found = jabberwerx.$(error).find("item-not-found").get(0);
                        if (item_not_found) {
                            if (!create) {
                                if (that._privacyLists[list_name]) {
                                    // fire remove event
                                    that.event("privacyListRemoved").trigger(that._privacyLists[list_name]);
                                    that._privacyLists[list_name].destroy();
                                    removed = true;        
                                }                                
                            } else {
                                error = null;
                            }
                         } 
                         else {
                             errorOccured = true;
                         }    
                    }
                    if (errorOccured) {
                       that.event("errorEncountered").trigger({
                            operation: "fetch",
                            error: error,
                            target: that._privacyLists[list_name]
                        });                    
                    } else {
                          if (!removed) {
                              if (that._privacyLists[list_name]) {
                                  that._privacyLists[list_name]._update(prvListNode);
                              } else {                                
                                  //Add privacy list to the map
                                  privListObj = that._privacyLists[list_name] = new jabberwerx.PrivacyList(prvListNode, that);                                                        
                              }
                              // fire update event
                              that.event("privacyListUpdated").trigger(that._privacyLists[list_name]);                               
                          }                   
                    }
                    if (callback) {
                       callback.call(that, privListObj, error);
                    }
                }  
            );
            
        },

        /**
         * Removes privacy list from the map.
         * 
         * @private
         */   
         _remove: function(list) {
              if (this._privacyLists[list]) {
                  delete this._privacyLists[list];    
              }
             
         },        
        
        
        /**
         * Check a response element to see if it contains an error.
         * 
         * @private
         * @param {Element} parentElement The element to check to see if it contains a child &lt;error&gt; 
         * @returns {Element|undefined} The error element if it exists or else undefined 
         */
        _checkError: function(parentElement) {
            return jabberwerx.$(parentElement).children("error")[0];
        },

        /**
         * <p>Sets the active privacy list (or null/undefined to clear active).</p>
         * @param {String} list Name of the privacy list.
         * @param {function} [callback] The function called after the server response is processed. If a server error
         * occurs, the function will be passed the error stanza response.
         * <p>The signature of the function is <code>callback(errorStanza)</code>.</p>
         * <p>The callback will be invoked in the context of the PrivacyListController
         * e.g. <code>callback.call(this, errorStanza)</code>. Therefore in the callback method <code>this</code> will
         * refer to the instance of PrivacyListController and so <code>this.client</code> will retrieve the {@link jabberwerx.Client} object.</p>
         * @throws {jabberwerx.Client.NotConnectedError} If the {@link jabberwerx.Client} is not connected
         * @throws {TypeError} If {list} is not a &lt;string/&gt;&lt;PrivacyList/&gt;
         */
        
        apply: function(list, callback) {
            if (!this.client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }

            if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                throw new TypeError('The callback param must be a function');
            }
            var list_name;
            if (list) {
                if (typeof list == 'string') {
                    list_name = list;
                }  else if (list instanceof jabberwerx.PrivacyList) {
                       list_name = list.getName();
                } else {
                    throw new TypeError('Privacy list name should be string or PrivacyList or empty.');
                }          
            }            
  
            
            var queryNode = new jabberwerx.NodeBuilder("{jabber:iq:privacy}query").
                                attribute("type", "set");
                               
            var activeNode = queryNode.element("active");
                                
            if (list) {
                activeNode.attribute("name", list_name);
            } 
                           
            //The variable 'that' will be visible to the senqIq callback
            var that = this;
                                             
            //Send privacy list fetch query
            this.client.sendIq('set', null, queryNode.data, 
                function (stanza) {     
                    var error = that._checkError(stanza);
                    if (error) {                  
                        that.event("errorEncountered").trigger({
                            operation: "apply",
                            error: error
                        });
                    } else {           
                          // fire fetch event
                          that.event("privacyListApplied").trigger({
                              list: list_name
                          });
                    }
                    if (callback) {
                        callback.call(that, error);                    
                    }
                }  
            );
            
       },
       
        /**
         * Watches for privacy list push from the server, 
         * each connected resource must return iq result.
         * @private
         */
        _handlePrivacyListUpdate: function(evt) {
            var list_name = jabberwerx.$(evt.selected).attr("name")
            // respond and note that we handled it
            var iq = new jabberwerx.IQ();
            iq.setTo(evt.data.getFrom());
            iq.setFrom();
            iq.setType("result");
            iq.setID(evt.data.getID());
            this.client.sendStanza(iq);
            
            this._doFetch(list_name);
            
            return true;             
          },
        /**
         * Holds a map of privacy lists objects and their names
         * @private
         */
        _privacyLists : {}
        
 
    }, 'jabberwerx.PrivacyListController');
}
