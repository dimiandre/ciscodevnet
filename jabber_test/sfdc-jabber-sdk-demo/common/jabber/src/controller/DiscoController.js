/**
 * filename:        DiscoController.js
 * created at:      2009/05/15T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */


if (jabberwerx) {    
    jabberwerx.DiscoController = jabberwerx.Controller.extend(/** @lends jabberwerx.DiscoController.prototype */{
        /**
         * @class
         * <p>Controller class for collecting disco info on the nodes.</p>
         * 
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.DiscoController">jabberwerx.DiscoController Events</a></li>
         * </ul>
         * </p>
         * @description
         * <p>Creates a new DiscoController with the given client.</p>
         * 
         * @param       {jabberwerx.Client} client The client object to use for communicating to the server
         * @throws      {TypeError} If {client} is not an instance of jabberwerx.Client
         * @constructs  jabberwerx.DiscoController
         * @extends     jabberwerx.Controller
         */        
        init: function(client) {
            this._super(client, "disco");

            // setup events
            this.applyEvent("discoInitialized");
           
            // setup handlers
            this.client.event("clientStatusChanged").bind(this.invocation("_handleStatusChange")); 
        },
        
        /**
         * <p>Destroys disco controller and removes any event callbacks it registered.</p>
         */        
        destroy: function() {
            // teardown handlers
            this.client.event('clientStatusChanged').unbind(this.invocation('_handleStatusChange'));
            this._super();
        },
        
        /**
         * Watches for client status events. Kicks off disco walk on connect, 
         * cleans up created entities on disconnect.
         * @private
         * @param {jabberwerx.EventObject} evt The event object.
         */            
        _handleStatusChange: function(evt) {
            switch (evt.data.next) {
                case jabberwerx.Client.status_connected: 
                    this._walkDisco();
                    break;
                case jabberwerx.Client.status_disconnected:
                    this._cleanUp();
                    this._initialized = false;
                    break;                
            }
        },
         
        /**
         * Sets up iqs for the server disco#info and disco#items.
         * @private
         */            
        _walkDisco: function() {
            //Send disco info iq for the server
            var info = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/disco#info}query");       
            this.client.sendIq("get", this.client.connectedServer.jid, info.data, this.invocation('_handleDiscoInfo'));
            //Send disco items iq for the server
            var items = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/disco#items}query");    
            this.client.sendIq("get", this.client.connectedServer.jid, items.data, this.invocation('_handleDiscoItems'));     
         },
       
        /**
         * Get's invoked when disco#info iq received.
         * @private
         */             
        _handleDiscoInfo: function (stanza) {
            var jid = stanza.getAttribute("from");
            //Check if entity is in the entity cache                      
            var server = this.client.entitySet.entity(jid);
            if (!server) {
                server = new jabberwerx.Server(jid, this);  
                this.client.entitySet.register(server);
            } 
            
            //Check for errors in the stanza
            var error = jabberwerx.$(stanza).find("error");
            if (error.length == 0) {
                //Add identities from disco#info stanza
                var identities = jabberwerx.$(stanza).find("identity");
             
                server.identities = [];
                jabberwerx.$.each(identities, function() {
                      server.identities.push(this.getAttribute("category") + "/" + this.getAttribute("type"));
                });

                //Add features from disco#info stanza
                var features = jabberwerx.$(stanza).find("feature");
                server.features = [];
                jabberwerx.$.each(features, function() {
                      server.features.push(this.getAttribute("var"));
                });
                //We only update display name if we are the owner
                if (server.controller === this && identities.get(0).getAttribute("name")) {
                    //Display name will fire an update event
                    server.setDisplayName(identities.get(0).getAttribute("name"));
                }  else {            
                    //Fire an update event even if disco controller does not own this entity
                    server.update();
                }
            }
            
            if (jid != this.client.connectedServer.jid) {
                //Remove jid from the list if found
                var idx = -1;
                for (var i = 0; i < this._pendingJids.length; i++) {
                    if (this._pendingJids[i] == jid) {
                        idx = i;
                        break;
                    }
                }      
                if (idx >=0) {      
                    this._pendingJids.splice(idx, 1);
                }
                //Check to see if we are the last disco#info expected 
                //to trigger "initilized" event.
                if (this._pendingJids.length == 0) {
                    this._initialized = true;
                    this.event("discoInitialized").trigger();             
                }                 
            }            

        },

        /**
         * Get's invoked when disco#items iq received.
         * @private
         */            
        _handleDiscoItems: function (stanza) {            
            //Add identities from disco#items stanza
            var items = jabberwerx.$(stanza).find("item");
            if (items.length == 0 || jabberwerx.$(stanza).children("error")[0]) {
                this._initialized = true;
                this.event("discoInitialized").trigger();             
                return;
            }
                        
            var info = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/disco#info}query");                           
            var that = this;

            jabberwerx.$.each(items, function() {
                  var item = that.client.entitySet.entity(this.getAttribute("jid"));
                  if (!item) {
                      item = new jabberwerx.Server(this.getAttribute("jid"), that);
                      that.client.entitySet.register(item);
                  }
            
                  that._pendingJids.push(this.getAttribute("jid"));
                  that.client.sendIq("get", this.getAttribute("jid"), info.data, that.invocation('_handleDiscoInfo'));   
            });
      
         
        },        
        
        /**
         * Cleans up all the entities with DiscoController owner.
         * 
         * @private
         */
        _cleanUp: function() {
            var that = this;
            this._pendingJids = [];
            this.client.entitySet.each(function() {
                    if (this.controller === that) {
                        this.remove();
                    }   
                });           
        },
        
        /**
         * <p>Finds all entities with given identity.</p>
         * @param {String} identity Identity for which to find entities.
         * @throws {TypeError} If DiscoController has not finished initializing.
         * @returns {jabberwerx.Entity[]} which contain the given identity.
         */       
        findByIdentity: function(identity) {
            if (!this._initialized) {
                throw new TypeError("Disco controller has not been initialized.");
            }
            var entities = [];
            
            this.client.entitySet.each(function(entity) {
                if (entity.hasIdentity(identity)) {
                    entities.push(entity);
                }   
            }); 
            return entities;
        },

        /**
         * <p>Sends out a disco#info request to the given jid/node combination
         * and returns the results to the provided callback.</p>
         *
         * @param {jabberwerx.JID|String} jid The target JID for the disco#info
         * request.
         * @param {String} [node] The optional node associated with the JID.
         * @param {Function} cb The callback executed when the disco#info
         * request returns or times out.
         * @throws {TypeError} if cb is not a function
         * @throws {TypeError} if node is not a String
         */
        fetchInfo: function(jid, node, cb) {
            jid = jabberwerx.JID.asJID(jid);

            if (!jabberwerx.$.isFunction(cb)) {
                throw new TypeError("cb must be a function.");
            }

            var info = new jabberwerx.NodeBuilder(this._DISCO_INFO);
            if (node) {
                if (typeof(node) == "string") {
                    info.attribute("node", node);
                } else {
                    throw new TypeError("If node is defined, it must be a string.");
                }
            }

            var callbacks = this._fetchInfoMap[this._jwesAsKey(jid, node)];
            if (!callbacks) {
                this._fetchInfoMap[this._jwesAsKey(jid, node)] = [cb];
            } else {
                callbacks.push(cb);
                return;
            }

            var that = this;
            this.client.sendIq("get", jid, info.data,
                function(stanza) {
                    var identities = [];
                    var features = [];
                    var extras = [];
                    var err = null;

                    if (stanza) {
                        var iq = new jabberwerx.IQ(stanza);

                        if (iq.isError()) {
                            err = iq.getErrorInfo();
                        } else {
                            var contents = jabberwerx.$(iq.getQuery()).contents();
                            for (index = 0; index < contents.length; index++) {
                                var content = contents[index];
                                switch (content.nodeName) {
                                    case ("identity"):
                                        content = jabberwerx.$(content);
                                        var identity = content.attr("category") + "/";
                                        identity = identity + (content.attr("type") ? content.attr("type") : "") + "/";
                                        identity = identity + (content.attr("xml:lang") ? content.attr("xml:lang") : "") + "/";
                                        identity = identity + (content.attr("name") ? content.attr("name") : "");
                                        identities.push(identity);
                                        break;
                                    case ("feature"):
                                        features.push(jabberwerx.$(content).attr("var"));
                                        break;
                                    case ("x"):
                                        if (content.getAttribute("xmlns") == "jabber:x:data") {
                                            extras.push(new jabberwerx.XDataForm(null, content));
                                        }
                                        break;
                                }
                            }
                        }
                    } else {
                        err = jabberwerx.Stanza.ERR_INTERNAL_SERVER_ERROR
                    }

                    var callbacks = that._fetchInfoMap[that._jwesAsKey(jid, node)];
                    for (i = 0; i < callbacks.length; i++) {
                        callbacks[i](identities, features, extras, err);
                    }
                    delete that._fetchInfoMap[that._jwesAsKey(jid, node)];
                }, this.timeout);
        },

        /**
         * @private
         */
        _jwesAsKey: function(jid, node) {
            return "[" + (jid || "") + "]:[" + (node || "") + "]";
        },

        /**
         * @private
         */
        _fetchInfoMap: {},

        /**
         * @private
         */
        _DISCO_INFO: "{http://jabber.org/protocol/disco#info}query",

        /**
         * Number of seconds before iq requests time out.
         * @type Number
         */
        timeout: 30,

        /**
         * <p>Finds all entities with given feature.</p>
         * @param {String} feature Feature for which to find entities.
         * @throws {TypeError} If DiscoController has not finished initializing.
         * @returns {jabberwerx.Entity[]} which contain the given feature.
         */
        findByFeature: function(feature) {
            if (!this._initialized) {
                throw new TypeError("Disco controller has not been initialized.");
            }
            var entities = [];
           
            this.client.entitySet.each(function(entity) {
                if (entity.hasFeature(feature)) {
                    entities.push(entity);
                }   
            }); 
            return entities;
        },         
               
 
         /**
         * Flag indicating if disco walk has finished
         * @private
         */  
        _initialized: false,
        
         /**
         * Array containing jids of all pending disco#info items.
         * @private
         */  
        _pendingJids: []                              
 
    }, 'jabberwerx.DiscoController');
    
    //intercept client's init and add disco controller
    jabberwerx.Client.intercept({
        init: function() {
            this._super.apply(this, arguments); //pass all args onto base class
            //client owns disco controller
            var discoController = new jabberwerx.DiscoController(this);
        }
    });
}
