/**
 * filename:        ErrorReporter.js
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.ErrorReporter = jabberwerx.JWBase.extend(/** @lends jabberwerx.ErrorReporter.prototype */{
        /**
         * @class
         * <p>This class manages user-friendly strings for error cases.</p>
         *
         * <p><b>NOTE:</b> This class should not be created directly. Use the
         * singleton {@link jabberwerx.errorReporter} instead.</p>
         *
         * @description
         * <p>Creates a new ErrorReporter.</p>
         *
         * <p><b>NOTE:</b> This class should not be created directly. Use the
         * singleton {@link jabberwerx.errorReporter} instead.</p>
         *
         * @constructs jabberwerx.ErrorReporter
         * @extends jabberwerx.JWBase
         */
         init: function() {
            this._super();
         },


        /**
         * <p>Returns a user-friendly string associated with
         * the error object passed in.</p>
         *
         * @param {string|Element} err Error object.
         * @returns {string} Error message associated with err.
         */
        getMessage: function(err) {
            if (err) {
                if (jabberwerx.isElement(err)) {
                    var msg;
                    var textMsg;
                    for (var i = 0; i < err.childNodes.length; i++) {
                        var node = err.childNodes[i];

                        if (node.nodeName == 'text') {
                            textMsg = jabberwerx.$(node).text();
                        } else {
                            var error = '{'+node.getAttribute('xmlns')+'}'+node.nodeName;
                            msg = this._errorMap[error];
                            if (msg) {
                                return msg;
                            }
                        }
                    }

                    if (textMsg) {
                        return textMsg;
                    }
                } else if (err.message) {
                    var msg = this._errorMap[err.message];
                    if (msg) {
                        return msg;
                    }
                }
            }
            return this._errorMap[""];
        },

        /**
         * <p>Adds an entry to the error map to associate an error
         * with user-friendly message.</p>
         *
         * @param {string} key The string representation of the error.
         * @param {string} value The user-friendly message to associate with the key.
         * @throws {TypeError} if {value} is not a string.
         */
        addMessage: function(key, value) {
            if (!value || typeof(value) != "string") {
                throw new TypeError("value must be a string.");
            }

            if (key) {
                this._errorMap[key] = value;
            }
        },

        /**
         * @private
         */
        _errorMap: {"" : "Operation failed",
            "{urn:ietf:params:xml:ns:xmpp-streams}conflict" :
                "This resource is logged in elsewhere.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}bad-request" :
                "The request was not valid.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}conflict" :
                "Conflicting names were encountered.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}feature-not-implemented" :
                "This feature is not yet implemented. Sorry for the inconvenience.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}forbidden" :
                "You are not authorized to perform this action.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}internal-server-error" :
                "An unknown server error occurred. Contact your administrator.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}item-not-found" :
                "The requested item could not be found.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}jid-malformed" :
                "The JID is not valid.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-acceptable" :
                "The given information was not acceptable.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-allowed" :
                "You are not allowed to perform this action.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-authorized" :
                "You are not authorized to perform this action.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}undefined-condition" :
                "An unknown error occurred. Contact your administrator.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}unexpected-request" :
                "Did not expect the request at this time.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}registration-required" :
                "You must register with the service before continuing.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}remote-server-not-found" :
                "Could not find the requested server.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}remote-server-timeout" :
                "Unable to contact the server.",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}service-unavailable" :
                "This service is not available. Try again later.",
            "{urn:ietf:params:xml:ns:xmpp-sasl}temporary-auth-failure" :
                "Unable to login. Check username and password.",
            "{urn:ietf:params:xml:ns:xmpp-sasl}mechanism-too-weak" :
                "You are not authorized to perform this action.",
            "{urn:ietf:params:xml:ns:xmpp-sasl}not-authorized" :
                "Unable to login. Check username and password."
        }

    }, "jabberwerx.ErrorReporter");

    /**
     * <p>The singleton object of ErrorReporter. Use this instead of creating
     * a new ErrorReporter object.</p>
     */
    jabberwerx.errorReporter = new jabberwerx.ErrorReporter();
}
