/**
 * filename:        User.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.User = jabberwerx.Entity.extend(/** @lends jabberwerx.User.prototype */ {
        /**
         * @class
         * @minimal
         * <p>The general User representation. Do not construct users by hand
         * with the new operator. See {@link jabberwerx.ClientEntityCache#localUser}.<p>
         *
         * @description
         * Creates a new User with the given JID and cache location
         * 
         * @param   {String|jabberwerx.JID} jid The user's JID
         * @param   {jabberwerx.ClientEntityCache} [cache] The owning cache
         * @abstract
         * @constructs jabberwerx.User
         * @extends jabberwerx.Entity
         */     
        init: function(jid, cache) {
            this._super({jid: jid}, cache);
        }
    }, 'jabberwerx.User');

           /**
         * @class
         * @minimal
         * <p>The LocalUser representation.
         *
         * @description
         * Creates a new LocalUser with the given JID and cache location
         * <p><b>NOTE:</b> This type should not be created directly </p>
         * <p>Use {@link jabberwerx.ClientEntityCache#localUser} instead.</p></p>
         * @param   {String|jabberwerx.JID} jid The user's JID
         * @param   {jabberwerx.ClientEntityCache} [cache] The owning cache
         * @constructs jabberwerx.User
         * @extends jabberwerx.Entity
         */   
    jabberwerx.LocalUser = jabberwerx.User.extend(/** @lends jabberwerx.LocalUser.prototype */ {            

        /**
         * Retrieves the user's display name. This method simply returns the
         * node portion of the user's JID.
         *
         * @return  {String} The display name
         */
        getDisplayName: function() {
            return this._displayName || jabberwerx.JID.unescapeNode(this.jid.getNode());
        }

    }, 'jabberwerx.LocalUser');
}