/**
 * filename:        Emoticons.js
 * created at:      2009/07/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.Emoticons = jabberwerx.JWModel.extend(/** @lends jabberwerx.ui.Emoticons.prototype */{
        /**
         * @class
         * <p>Class for working with supported emoticons</p>
         *
         * @description
         * <p>
         * Holds a list of supported emoticons and their corresponding image files, also provides a
         * translate function for substituting in html img tags in place of raw emoticon text.
         * </p>
         *
         * @constructs jabberwerx.ui.Emoticons
         * @extends jabberwerx.JWModel
         */
        init: function() {
            this._super();
            // Init _emoticonRegex
            var wordstart = /^\w/;
            var wordend = /\w$/;
            var str = "";
            var chunks = [];
            for (var i in this.emoticons) {
                str = wordstart.test(i) ? "\\b" : "\\B";
                str += i.replace(/[)(]/g, "\\$&");
                str += wordend.test(i) ? "\\b" : "\\B";
                chunks.push(str);
            }
            this._emoticonRegex = new RegExp(chunks.join("|"), "g");
        },
        
        /**
         * Takes a string and substitutes emoticon image links in where matches are found.<br>
         * Example: <p><pre class="code" >translate('hi ;)')</pre></p>
         * <p> returns </p>
         * <p><pre class="code">
         * ['hi ', '&lt;img src="<i>img_file_location</i>" title=":)" alt=":)" /&gt;']</pre></p>
         * @param {String|jQuery} rawText Text or xml to translate
         * @returns {jQueryCollection}
         * @throws {InvalidRawTextFormat} If rawText is not a string or jQuery object.
         */
        translate: function(rawText) {
            var that = this;
            var findTextNodes = function(element) {
                if (jabberwerx.isText(element)) {
                    var replace = that._translate(jabberwerx.$(element).text());
                    jabberwerx.$(element).replaceWith(replace);
                } else if (element.hasChildNodes()) {
                    for (var i = 0; i < element.childNodes.length; i++) {
                        findTextNodes(element.childNodes[i]);
                    }
                }
            };

            if (typeof rawText == 'string') {
                return this._translate(rawText);
            } else if (rawText instanceof jabberwerx.$) {
                // traverse nodes looking for text nodes
                for (var i = 0; i < rawText.length; i++) {
                    findTextNodes(rawText[i]);
                }

                return rawText;
            } else {
               throw new jabberwerx.ui.Emoticons.InvalidRawTextFormat();
            }
        },

        /**
         * @private
         */
        _translate: function(rawText) {
            var translationContainer = jabberwerx.$('<translationContainer/>');
            // Don't substitute in emoticons if rawText is > 1024 chars in length
            if (rawText.length > 1024) {
                translationContainer.append(rawText);
            } else if (rawText) {
                if (!this._imageFolder) {
                    this._imageFolder = jabberwerx.ui.getThemeImageURL("emoticons/");
                }
                // Get all matches and intervening text from the rawText
                var splitText = rawText.split(this._emoticonRegex);
                var matches = rawText.match(this._emoticonRegex) || [];
                
                var j = 0;
                if (!splitText[0]) {
                    // FF will have an empty string as the first element of splitText if there is an
                    // emoticon match at the start of rawText
                    j = 1;
                } else if (rawText.substr(0, splitText[0].length) === splitText[0]) {
                    // If the rawText doesn't start with an emoticon then add the first element of
                    // splitText
                    translationContainer.append(splitText[0]);
                    j = 1;
                }
                
                // Loop through the rest of the text
                for (var i =0; i < matches.length; i++, j++) {
                    translationContainer.append("<img src='" + this._imageFolder +
                                                    this.emoticons[matches[i]] +
                                                    "' title='" + matches[i] +
                                                    "' alt='" + matches[i] + "' />");
                    if (splitText[j]) {
                        translationContainer.append(splitText[j]);
                    }
                }
            }
            return translationContainer.contents();
        },
        
        /**
         * Map of supported emoticons and their corresponding image files
         * @type String:String
         */
        emoticons: {
            '(A)' : 'angelic.gif',
            ':saint' : 'angelic.gif',
            ':-@' : 'angry.gif',
            ':@' : 'angry.gif',
            ':grrr' : 'angry.gif',
            ':mad' : 'angry.gif',
            'X(' : 'angry.gif',
            'X-(' : 'angry.gif',
            'x(' : 'angry.gif',
            'x-(' : 'angry.gif',
            ':D' : 'big_smile.gif',
            ':-D' : 'big_smile.gif',
            ':blah' : 'bored.gif',
            ':sleep' : 'bored.gif',
            ':-x' : 'closed.gif',
            ':coffee' : 'coffee.gif',
            ':conf' : 'confused.gif',
            ':cool' : 'cool.gif',
            ':crazy' : 'crazy.gif',
            ':evil' : 'evil.gif',
            ':(' : 'frown.gif',
            ':-(' : 'frown.gif',
            'B)' : 'geek.gif',
            'B-)' : 'geek.gif',
            ':go' : 'greenlight.gif',
            ':heart' : 'heart.gif',
            ':brokenheart' : 'heart_broken.gif',
            ':joker' : 'joker.gif',
            ':LOL' : 'laugh.gif',
            ':lol' : 'laugh.gif',
            ':idea' : 'lightbulb.gif',
            ':oops' : 'mistake.gif',
            ':roll' : 'roll_eyes.gif',
            ':paranoid' : 'scared.gif',
            ':ill' : 'sick.gif',
            ':sigh' : 'sigh.gif',
            ':)' : 'smiley.gif',
            '(:' : 'smiley.gif',
            '(=:' : 'smiley.gif',
            ':-)' : 'smiley.gif',
            '=)' : 'smiley.gif',
            ':stop' : 'stop.gif',
            ':O' : 'surprise.gif',
            ':-O' : 'surprise.gif',
            ':o' : 'surprise.gif',
            ':-o' : 'surprise.gif',
            ':-P' : 'tongue.gif',
            ':-p' : 'tongue.gif',
            ':P' : 'tongue.gif',
            ':p' : 'tongue.gif',
            ';)' : 'wink.gif',
            ';-)' : 'wink.gif'
        },
        
        /**
         * @private
         * Regular expression to match all emoticions. Initialized during object initialization.
         * @type {RegExp}
         */
        _emoticonRegex: null,
        
        /**
         * @private
         * Points to location of emoticon images. Initialized upon first call to Emoticons.translate
         * @type {String}
         */
        _imageFolder: null
    });
    /**
     * @class
     * InvalidRawTextFormat exception
     * @extends jabberwerx.util.Error
     */
    jabberwerx.ui.Emoticons.InvalidRawTextFormat = jabberwerx.util.Error.extend('The rawText parameter must be of type string.');
}
