;(function(){
	if (jabberwerx) {
        jabberwerx.TemporaryEntity = jabberwerx.Entity.extend(/** @lends jabberwerx.TemporaryEntity.prototype */ {
            /**
             * @class
             * @minimal
             * <p>An empty extension of jabberwerx.Entity. Used when storing presence status for user's who
             * would otherwise not be in the entity set..</p>
             *
             *
             * @description
             * <p>An empty extension of jabberwerx.Entity</p>
             * @param   {String|jabberwerx.JID} [jid] The identifying JID
             * @param   {jabberwerx.Controller|jabberwerx.EntitySet} [cache] The
             *          controller or cache for this entity
             * @extends jabberwerx.Entity
             * @constructs jabberwerx.TemporaryEntity
             */
            init: function(jid, cache) {
                this._super({jid: jid}, cache);
            }
        }, 'jabberwerx.TemporaryEntity');
    }
})();