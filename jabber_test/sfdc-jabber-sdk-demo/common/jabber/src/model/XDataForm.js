/**
 * filename:        XDataForm.js
 * created at:      2009/11/10T11:20:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.XDataForm = jabberwerx.JWModel.extend(/** @lends jabberwerx.XDataForm.prototype */ {
        /**
         * @class
         * <p>Holds data form fields and properties.</p>
         *
         * @description
         * <p>Creates a new XDataForm with the given DOM element or empty form of the given type.</p>
         * 
         * @param   {String} [form] The form type
         * @param   {Element} [payload] The DOM representing XDataForm
         * @throws  {TypeError} If any of the parameters are not valid
         * @constructs jabberwerx.XDataForm
         * @extends JWModel
         */
        init: function(form, payload) {
            this._super();
            if (payload) {
                if (jabberwerx.isElement(payload)) {
                    payload = jabberwerx.$(payload);
                    
                    //Set title and istructions
                    this._type = payload.attr("type");
                    this._title = payload.children("title").text();
                    this._instructions = payload.children("instructions").text();
                    //If we are passed DOM element, search for all
                    //field nodes, create XDataFormField object for 
                    //each one and append it to the fields array
                    var that = this;
                    
                    var fields = payload.children("field");
                    var field;
                    jabberwerx.$.each(fields, function() {
                        field = new jabberwerx.XDataFormField(null, null, this);
                        that.addField(field);
                    }); 
                    var reported_fields = payload.find("reported:first  > field");
                    var reported_field; 
                    
                    jabberwerx.$.each(reported_fields, function() {
                        reported_field = new jabberwerx.XDataFormField(null, null, this);
                        that.reported.push(reported_field);
                    }); 
                    var items = payload.find("item");
                    var item;
                    jabberwerx.$.each(items, function() {
                        item = new jabberwerx.XDataFormItem(this);
                        //We need to copy corresponding field types, options and descriptions 
                        //from reported fields into item fields.
                        jabberwerx.$.each(that.reported, function() {
                            var field = item.getFieldByVar(this.getVar());
                            if (field) {
                                field.setType(this.getType());
                                field.setOptions(this.getOptions());
                                field.setDesc(this.getDesc());
                            }
                        });
                        that.items.push(item);
                    });                      
                    
                }  
                else {
                    throw new TypeError("payload needs to be an Element");    
                }         
            } else {  
                if (form) {
                    if (typeof form == 'string') {
                        this._type = form;                   
                    }
                    else {
                        throw new TypeError("string form type is required");    
                    }                 
                }     
                else {
                    this._type = 'form';
                }                    
            }
  

        },
        
        /**
         * <p>Destroys this data form.</p>
         */
        destroy: function() {
            this._super();
        },
        
        
        /**
         * <p>Returns form type.</p>
         * @returns {String} Form type
         */
        getType: function() {
           return this._type;   
        },
            
        /**
         * <p>Returns form title.</p>
         * @returns {String} Form title
         */
        getTitle: function() {
           return this._title;   
        },
        
        /**
         * <p>Sets form title.</p>
         * @param {String} [title] Form title
         */
        setTitle: function(title) {
            this._title = title;  
        },
        
        /**
         * <p>Returns form instructions.</p>
         * @returns {String} Form instructions
         */
        getInstructions: function() {
           return this._instructions;   
        },
        
        /**
         * <p>Sets form instructions.</p>
         * @param {String} [instructions] Form instructions
         */
        setInstructions: function(instructions) {
            this._instructions = instructions;   
        }, 
      
        /**
         * <p>Returns XDataForm DOM element.</p>
         * @returns {Element} DOM element representing XDataForm
         */
        getDOM: function () {
             var form = new jabberwerx.NodeBuilder("{jabber:x:data}x");
             if (this._type) {
                 form.attribute("type",this._type);
             }
             if (this._title) {               
                 form.element("title").text(this._title);
             }
             
             if (this._instructions) {               
                 form.element("instructions").text(this._instructions);
             }             
                           
             var that = this;
             
             jabberwerx.$.each(that.fields, function() {
                 this.getDOM(form);   
             });
             
             if (that.reported.length) {
                 var reported = form.element("reported");
                 jabberwerx.$.each(that.reported, function() {
                     this.getDOM(reported);   
                 });
             }
             
             if (that.items.length) {
                 var items = form.element("items");
                 jabberwerx.$.each(that.items, function() {
                     this.getDOM(items);   
                 });
             }
             
             
             return form;
         },      
         
        
        /**
         * <p>Adds field to the data form.</p>
         * @param {jabberwerx.XDataFormField} [field] Field to add to the form        
         * 
         */
        addField: function(field) {
            if (!field instanceof jabberwerx.XDataFormField) {
                throw new TypeError("field must be type jabberwerx.XDataFormField");
            } 
            
            //Remove if field already exists
            var idx = this._indexOf(this.fields, field.getVar());
            if (idx != -1) {
                this.fields.splice(idx,1);
            }              
            this.fields.push(field);               
                    
        },    
                      
        /**
         * <p>Returns field based on var name or null if not found or name is null.</p>
         * @param {String} [name] Var value for the field  
         * @returns {jabberwerx.XDataFormField}
         */
        getFieldByVar: function(name) {
            if (!name) {
                return null;
            }
            var idx = this._indexOf(this.fields, name);
            if (idx != -1) {
                return this.fields[idx];
            }          
            return null;          
        },

        /**
         * <p>Sets form type field value for the form.</p>
         * @param {String} [type] 'FORM_TYPE' value for the form
         */
        setFORM_TYPE: function(type) {
            this.getFieldByVar('FORM_TYPE').setValues(type);     
        },
        
        /**
         * <p>Returns field based on var name or null if not found or name is null.</p>
         * @returns {String} 'FORM_TYPE' value for the form
         */
        getFORM_TYPE: function() {
            return this.getFieldByVar('FORM_TYPE').getValues()[0];          
        },
                
        /**
         * <p>Creates new jabberwerx.XDataForm and sets values for the passed var(s).</p>
         * @param {Object} [fieldsAndValues] Object containing id/value pairs for data fields.
         * @returns {jabberwerx.XDataForm} Newly created form of type "submit".
         */
        submit: function(fieldsAndValues) {
            var submitForm = new jabberwerx.XDataForm("submit");
            
            var property, field;
            var values;
            var idx;
            //First populate values for the submitted fields.
            if (fieldsAndValues) {
                var that = this;
	  	        jabberwerx.$.each(fieldsAndValues, function(property) {
                    if (typeof property == "string") {
                        //Look up if field is already there
                        //values could be array
                        values = [].concat(fieldsAndValues[property]);
                        field = new jabberwerx.XDataFormField(property, values);
                        idx = that._indexOf(that.fields, field.getVar());
                        if (idx != -1) {
                            field.setType(that.fields[idx].getType());
                            field.setRequired(that.fields[idx].getRequired());
                        }                           
                        submitForm.addField(field);
                    }
                });
            }
           
            //If the value was not submitted, populate default
            for(var i=0; i<this.fields.length; i++) {
                //Check if submit has this field
                idx = this._indexOf(submitForm.fields, this.fields[i].getVar());
                if (idx == -1 && (this.fields[i].getValues().length || this.fields[i].getRequired())) {
                    if (this.fields[i].getType() == "fixed" ||
                        this.fields[i].getType() == "hidden") continue;
                    field = new jabberwerx.XDataFormField(this.fields[i].getVar(), this.fields[i].getValues());
                    field.setType(this.fields[i].getType());
                    field.setRequired(this.fields[i].getRequired());
                    submitForm.addField(field);
                }
            }             
            //Will throw exception here if validation issues have been encountered
            submitForm.validate();
            
            return submitForm;          
        },        

         /**
         * <p>Returns XDataForm type "cancel".</p>
         * @returns {jabberwerx.XDataForm} Newly created form of type "cancel".
         */
        cancel: function() {
            var cancelForm = new jabberwerx.XDataForm("cancel");
            return cancelForm;          
        }, 
        
       /**
         * <p>Returns index of the field for the given var name or -1 if not found or the var is null.</p>
         * @private
         * @param {jabberwerx.XDataFormField[]} [fields] Array of fields to search.
         * @param {String} [name] Array of fields to search.
         * @returns {Integer} Index of the field
         */        
        _indexOf: function(fields, name) {
            if (!name) {
                return -1;
            }
            for(var i=0; i<fields.length; i++) {
                if (fields[i].getVar() == name) {
                    return i;
                }
            }
            return -1;
        },                                 
      

       /**
         * <p>Iterates through all the form fields and performs the validation.</p>
         * @throws  {TypeError} If any of the rules for field's values have been violated
         */        
        validate: function() {
            for(var i=0; i<this.fields.length; i++) {
                var field = this.fields[i];
                field.validate();
            }
    
            //Validate result set
            this._validateReported();
                        
            for(var i=0; i<this.items.length; i++) {
                this._validateItem(this.items[i]);
            }            
        },
        
        /**
         * @private
         * Validates info for reported fields
         */        
        _validateReported: function() {
            for(var i=0; i<this.reported.length; i++) {
                if (!this.reported[i].getVar()) {
                     throw new TypeError("reported should have var");           
                }
                if (!this.reported[i].getType()) {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                        "reported field should have type",
                        {field:this.reported[i].getVar()});                
                }
                if (!this.reported[i].getLabel()) {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                        "reported field should have label",
                        {field:this.reported[i].getVar()});                
                }

                if (this.reported[i].getValues().length > 0) {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                        "reported field should not have values",
                        {field:this.reported[i].getVar()});                
                }                                               
                this.reported[i].validate();
            }           
        },  
        
       /**
         * @private
         * Validates item against reported fields
         */        
        _validateItem: function(item) {
            var found;
            var fields = item.fields;
            for(var i=0; i<this.reported.length; i++) {      
                found = false;
                for(var j=0; j<fields.length || found; j++) {
                    if (fields[j].getVar() == this.reported[i].getVar()) {
                        found = true;
                        fields[j].setType(this.reported[i].getType());
                        fields[j].validate();
                    }
                }
                if (!found) {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                    "reported field is not found in one of the items",
                    {field:this.reported[i].getVar()});                       
                }
            }
        },             
                
        /**
         * <p>Returns first value for the field based on var name or null if field is not found or no values exist for the field.</p>
         * @param {String} [name] Var value for the field  
         * @returns {String} First value for the field
         */
        getFieldValue: function(name) {
            if (!name) {
                return null;
            }
            var idx = this._indexOf(this.fields, name);
            if (idx == -1) {
                return null;
            }          
            if (this.fields[idx].getValues().length > 0) {
                return this.fields[idx].getValues()[0];
            }
            return null;          
        },
        
         /**
         * <p>Sets/clears value for the field based on var name.</p>
         * @param {String} [name] Var name for the field  
         * @param {String} [value] Value for the field  
         */
        setFieldValue: function(name, value) {
            if (!name) {
                return;
            }
            var idx = this._indexOf(this.fields, name);
            if (idx == -1) {
                return;
            }          
            this.fields[idx].setValues(value);
        },
                              
        /**
         * @private
         */
        _type: null,
        /**
         * @private
         */
        _title: null,
        /**
         * @private
         */
        _instructions: null,
        /**
         * Array of XDataFormFields objects for the forms of type "form" and "submit"
         */
        fields: [],
        /**
         * Array of XDataFormFields objects for the forms of type "result"
         */
        reported: [],
        /**
         * Array of XDataFormFields objects for the forms of type "result"
         */
        items: []                                
    }, "jabberwerx.XDataForm");
}
