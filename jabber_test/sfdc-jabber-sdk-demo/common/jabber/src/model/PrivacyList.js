/**
 * filename:        PrivacyList.js
 * created at:      2009/10/26T00:00:00-07:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.PrivacyList = jabberwerx.JWModel.extend(/** @lends jabberwerx.PrivacyList.prototype */{
        /**
         * @class
         * <p>Representation of a XEP-0016 privacy list. This class manages
         * a privacy list to block (or removing blocking of) specific JIDs.
         * It does not provide for any other allow/deny logic from XEP-0016.</p>
         *
         * @description
         * <p>Creates a new PrivacyList with the given &lt;list/&gt; and
         * PrivacyListController.</p>
         *
         * <p><b>NOTE:</b> This type should not be created directly. Instead use
         * {@link jabberwerx.PrivacyListController#fetch}.</p>
         *
         * @param       {Element} listNode The element representing the list
         * @param       {jabberwerx.PrivacyListController} ctrl The owning
         *              controller
         * @throws      {TypeError} If {listNode} is not a &lt;list/&gt;
         *              element; or if {ctrl} is not a PrivacyListController
         * @constructs  jabberwerx.PrivacyList
         * @extends     jabberwerx.JWModel
         * @see         <a href='http://xmpp.org/extensions/xep-0016.html'>XEP-0016: Privacy Lists</a>
         */
        init: function(listNode, ctrl) {
            this._super();
            
            if (!jabberwerx.isElement(listNode)) {
                throw new TypeError("listNode must be an Element");
            }
            this._DOM = jabberwerx.$(listNode);
            
            if (!(ctrl && ctrl instanceof jabberwerx.PrivacyListController)) {
                throw new TypeError("controller must be a PrivacyListController");
            }
            this.controller = ctrl;
        },
        /**
         * <p>Destroys this privacy list. This method removes any event callbacks
         * it registered.</p>
         */
        destroy: function() {
            this.controller._remove(this.getName());
            this._super();
        },

        /**
         * <p>Retreives the name of this privacy list.</p>
         *
         * @returns  {String} The list name
         */
        getName: function() {
            return this._DOM.attr("name");
        },
        /**
         * <p>Retrieves the list of blocked JIDs. The returned list
         * is a snapshot of the JIDs marked as blocked at the time this
         * method is called.</p>
         *
         * @returns  {jabberwerx.JID[]} The list of blocked JIDs
         */
        getBlockedJids: function() {
            var fn = function() {
                var item = jabberwerx.$(this);
                if (item.attr("type") != "jid") {
                    return null;
                }
                if (item.attr("action") != "deny") {
                    return null;
                }
                
                return jabberwerx.JID.asJID(item.attr("value"));
            };
            
            return this._DOM.children().map(fn).get();
        },
        
        /**
         * <p>Block the given JID. This method adds an item to the
         * privacy list with type is "jid", action is "deny", and
         * value is {jid}.</p>
         *
         * <p><b>NOTE:</b> this method does not update the server.
         * Call {@link #update} to commit changes to this privacy list.</p>
         *
         * @param   {String|jabberwerx.JID} jid The JID to block
         * @throws  {jabberwerx.JID.InvalidJIDError} If {jid} is not valid
         */
        blockJid: function(jid) {
            jid = jabberwerx.JID.asJID(jid).getBareJIDString();
            
            var item = this._DOM.find("item[type=jid][value=" + jid + "]");
            if (!this._cleanDOM) {
                this._cleanDOM = jabberwerx.$(this._DOM.get(0).cloneNode(true));
            }
            if (!item.length) {
                item = new jabberwerx.NodeBuilder(this._DOM.get(0)).
                        element("item").
                        attribute("action", "deny").
                        attribute("type", "jid").
                        attribute("value", jid);
                item = jabberwerx.$(item.data);
            } else {
                item.attr("action", "deny");
                // make sure we block everything!!
                item.empty();
                item.remove();
            }

            this._DOM.prepend(item);
            
            // note we blocked
            this._updateDirty(jid, this._blocked, this._unblocked);
        },
        /**
         * <p>Unblock the given JID. This method removes an item from the
         * privacy list where the type is "jid", action is "deny", and value
         * is equal to {jid}.</p>
         *
         * <p><b>NOTE:</b> this method does not update the server.
         * Call {@link #update} to commit changes to this privacy list.</p>
         *
         * @param   {String|jabberwerx.JID} jid The JID to unblock
         * @throws  {jabberwerx.JID.InvalidJIDError} If {jid} is not valid
         */
        unblockJid: function(jid) {
            jid = jabberwerx.JID.asJID(jid).getBareJIDString();
            
            var item = this._DOM.find("item[type=jid][value=" + jid + "]");
            if (item.length) {
                if (!this._cleanDOM) {
                    this._cleanDOM = jabberwerx.$(this._DOM.get(0).cloneNode(true));
                }
                item.remove();
                // note we unblocked
                this._updateDirty(jid, this._unblocked, this._blocked);
            }
            
        },
         /**
         * @private
         */
        _update: function(listNode) {
            this._DOM = jabberwerx.$(listNode);
            this._blocked = [];
            this._unblocked = [];
        },        
        
        /**
         * @private
         */
        _updateDirty: function(jid, added, remed) {
            var idxOf = function(arr) {
                for (var idx = 0; idx < arr.length; idx++) {
                    if (arr[idx] == jid) {
                        return idx;
                    }
                }
                
                return -1;
            };
            var idx;
            idx = idxOf(remed);
            if (idx != -1) {
                remed.splice(idx, 0);
            }
            idx = idxOf(added);
            if (idx == -1) {
                added.push(jid);
            }
        },
        
        /**
         * <p>Updates the server with this privacy list's current state. This
         * method sends the current list to the server. It first sends
         * &lt;presence type='unavailable'/&gt; to each newly blocked JID, and
         * resends the last &lt;presence/&gt; update if any JIDs were
         * unblocked.</p>
         *
         * <p>This method will ultimately trigger the "privacyListUpdated" event
         * if successful, or "errorEncountered" event if the update failed.</p>
         *
         * <p>The value of {cb} is either "undefined" or a function matching the
         * following:</p>
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this jabberwerx.PrivacyList
         *      err;    // undefined if the update succeeded, or
         *              // the &lt;error/&gt; element if update failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback to execute when updated
         * @throws  {TypeError} If {cb} is not undefined and not a function
         */
        update: function(cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            // recalculate 'order' of list items
            var items = this._DOM.children();
            if (!items.length) {
                // all JIDs removed; add a "allow all" default
                new jabberwerx.NodeBuilder(this._DOM.get(0)).
                        element("item").
                        attribute("action", "allow");
                items = this._DOM.children();
            }
            items.each(function(idx) {
                //we are going to use the item's index as it's new order
                jabberwerx.$(this).attr("order", idx);
            });
            
            var ctrl = this.controller;
            var client = ctrl.client;
            
            // send presence[type=unavailable] to blocked JIDs
            var prs = new jabberwerx.Presence();
            prs.setType("unavailable");
            for (var idx = 0; idx < this._blocked.length; idx++) {
                var jid = this._blocked[idx];
                var ent = client.entitySet.entity(jid);
                if (!ent) {
                    continue;
                }
                if (!ent.getPrimaryPresence()) {
                    continue;
                }
                prs.setTo(jid);
                client.sendStanza(prs);
            }
            
            var blocked = this._blocked;
            var unblocked = this._unblocked;
            var that = this;
            var fn = function(evt) {
                // prepare current presence
                var prs = client.getCurrentPresence().clone();
                prs.setFrom();
                prs.setTo();
                
                switch (evt.name) {
                    case "errorencountered":
                        if (blocked.length) {
                            client.sendStanza(prs);
                        }
                        if (cb) {
                            cb.call(that, evt.data.error);
                        }
                        break;
                    case "privacylistupdated":
                        if (unblocked.length) {
                            client.sendStanza(prs);
                        }
                        if (cb) {
                            cb.call(that);
                        }
                        break;
                }
                
                ctrl.event("errorEncountered").unbind(arguments.callee);
                ctrl.event("privacyListUpdated").unbind(arguments.callee);
            };
            
            
            ctrl.event("errorEncountered").bindWhen(
                    function(data) {
                        return (data.target === that) ? that : null },
                    fn);
            ctrl.event("privacyListUpdated").bindWhen(
                    function(data) { return (data === that) ? that : null },
                    fn);
            var query = new jabberwerx.NodeBuilder("{jabber:iq:privacy}query");
            query = jabberwerx.$(query.data);
            query.append(this._DOM.get(0).cloneNode(true));
            
            client.sendIq("set", null, query.get(0), function(stanza) {
                var err;
                
                if (!stanza) {
                    err = jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT;
                } else {
                    err = new jabberwerx.IQ(stanza).getErrorInfo();
                }
                
                if (err && that._cleanDOM) {
                    // revert to original and trigger event
                    that._DOM = that._cleanDOM;
                    ctrl.event("errorEncountered").trigger({
                        operation: "update",
                        target: that,
                        error: err.getNode()
                    });
                }
                delete that._cleanDOM;
            });
        },
        /**
         * <p>Removes the list from the server. This method also calls
         * {@link #destroy} on this PrivacyList object.</p>
         *
         * <p>If a callback is provided, it's signature is expected to match
         * following:</p>
         * <pre class="code">
         *  function callback(err) {
         *      this;       // this PrivacyList object
         *      err;        // the &lt;error/&gt; element, or undefined if
         *                  // successful
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is not undefined and not a function
         */
        remove: function(cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            if (cb) {
                var ctrl = this.controller;
                var that = this;
                var fn = function(evt) {
                    var err = (evt.name == "errorencountered") ?
                            evt.data.error :
                            undefined;
                    cb.call(that, err);
                    
                    ctrl.event("errorEncountered").unbind(arguments.callee);
                    ctrl.event("privacyListRemoved").unbind(arguments.callee);
                };
                
                ctrl.event("errorEncountered").bindWhen(
                        function(data) { return (data.target === that) ? that : null; },
                        fn);
                ctrl.event("privacyListRemoved").bindWhen(
                        function(data) { return (data === that) ? that : null; },
                        fn);
            }
            
            var query = new jabberwerx.NodeBuilder("{jabber:iq:privacy}query").
                    element("list").
                    attribute("name", this.getName()).
                    parent;
            this.controller.client.sendIq("set", null, query.data);
        },

        /**
         * Called just prior to this object being serialized. This method
         * serializes the underlying &lt;list/&gt; element into its
         * XML representation.
         */
        willBeSerialized: function() {
            if (this._DOM) {
                this._serializedXML = this._DOM.get(0).xml;
                delete this._DOM;
            }
        },
        /**
         * Called just after this object is unserialized. This method
         * unserializes the underlying &lt;list/&gt; element into its
         * DOM representation.
         */
        wasUnserialized: function() {
            if (this._serializedXML) {
                this._DOM = jabberwerx.$(jabberwerx.util.unserializeXML(this._serializedXML));
                delete this._serializedXML;
            }
        },
        
        /**
         * @private
         */
        _blocked: [],
        /**
         * @private
         */
        _unblocked: [],
        /**
         * @private
         */
        _DOM: null
    }, "jabberwerx.PrivacyList");
}
