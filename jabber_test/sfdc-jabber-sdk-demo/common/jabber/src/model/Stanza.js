/**
* filename:        Stanza.js
* created at:      2009/05/11T00:00:00-06:00
*
* Copyrights
*
* Portions created or assigned to Cisco Systems, Inc. are
* Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
*/

if (jabberwerx) {
    jabberwerx.Stanza = jabberwerx.JWModel.extend( /** @lends jabberwerx.Stanza.prototype */ {
        /**
         * @class
         * @minimal
         * <p>A representation of an XMPP stanza.</p>
         *
         * @description
         * Creates a new Stanza with the given root element or root name.
         * 
         * @param   {Element|String} root The element name of the stanza
         * @throws  {TypeError} If {root} is not a valid Element or String
         * @constructs jabberwerx.Stanza
         * @extends JWModel
         */                 
        init: function(root) {
            this._super();
            
            var builder = new jabberwerx.NodeBuilder();
            if (jabberwerx.isElement(root)) {
                builder = builder.node(root);
            } else if (typeof(root) == "string") {
                // If root does not contain a namespace, assume "{jabber:client}"
                if (!(/^\{[^\}]*\}/.test(root))) {
                    root = "{jabber:client}" + root;
                }
                builder = builder.element(root);
            } else {
                throw new TypeError("root must be an element or expanded-name");
            }
            this._DOM = builder.data;
            
            // Check for timestamp and populate date variable accordinaly if present
            var date = new Date();
            var dateString = jabberwerx.$(builder.data).
                    find("x[xmlns='jabber:x:delay'][stamp]").
                    attr("stamp");
            if (!dateString) {
                dateString = jabberwerx.$(builder.data).
                      find("delay[xmlns='urn:xmpp:delay'][stamp]").
                      attr("stamp");            
            }                          
            if (dateString) {
                try {
                    date = jabberwerx.parseTimestamp(dateString);
                } catch (ex) {
                    //DEBUG-BEGIN
                    jabberwerx.util.debug.log("could not parse delay: " + ex);
                    //DEBUG-END
                }
            }
            this.timestamp = date;
        },
        
        /**
         * <p>Retrieves the XML element representing this Stanza.</p>
         *
         * @returns  {Element} The Stanza's element
         */
        getNode: function() {
            return this._DOM;
        },
        /**
         * <p>Retrieves the document that owns this Stanza's XML element.</p>
         *
         * @returns  {Document} The owning document for the Stanza's element
         */
        getDoc: function() {
            return this.getNode().ownerDocument;
        },
        /**
         * <p>Generates the XML string representation of this Stanza.</p>
         *
         * @returns {String} The XML
         */
        xml: function() {
            return jabberwerx.util.serializeXML(this._DOM);
        },
        /**
         * <p>Retrieves the stanza-type for this Stanza. This is equivalent to
         * retrieving the node name from {@link #getNode}.</p>
         *
         * @returns  {String} The type of stanza (e.g. "message")
         */
        pType: function() {
            return this.getNode().nodeName;
        },
        
        /**
         * @private
         */
        _getAttrValue: function(name) {
            return this.getNode().getAttribute(name);
        },
        /**
         * @private
         */
        _setAttrValue: function(name, val) {
            if (val === undefined || val === null) {
                this.getNode().removeAttribute(name);
            } else {
                // allows for an empty attribute (defined with no value)
                this.getNode().setAttribute(name, val);
            }
        },
        
        /**
         * @private
         * get text from matching child nodes. name may be expanded name
         */
        _getChildText: function(name) {
            var nnode = new jabberwerx.NodeBuilder(name).data;
            var nodens = (nnode.namespaceURI) ?
                            nnode.namespaceURI : this.getNode().namespaceURI;
            var matches = jabberwerx.$(this.getNode()).children(nnode.nodeName).
                            map(function (idx, child) {
                                return child.namespaceURI==nodens ? child:null;
                            });
            return matches.length ? jabberwerx.$(matches[0]).text() : null;
        },
        /**
         * @private
         * <p>Changes or removes the child lement text for the given name.</p>
         *
         * @param   {String} name The name of the child element
         * @param   {String} [val] The new value for {name}
         */
        _setChildText: function(name, val) {
            var n = jabberwerx.$(this.getNode()).children(name);
            if (val === undefined || val === null) {
                n.remove();
            } else {
                if (!n.length) {
                    n = jabberwerx.$(new jabberwerx.NodeBuilder(this.getNode()).
                        element(name).
                        data);
                }
                n.text(val);
            }
        },
        
        /**
         * <p>Retrieves the type for this Stanza. This is equivalent to
         * retrieving the "type" attribute from {@link #getNode}.</p>
         *
         * @returns  {String} The type
         */
        getType: function() {
            return this._getAttrValue("type");
        },
        /**
         * <p>Changes or removes the type for this Stanza.</p>
         *
         * <p>If {type} is "", undefined, or null, any current value is
         * removed.</p>
         *
         * @param   {String} [type] The new type
         */
        setType: function(type) {
            this._setAttrValue("type", type || undefined);
        },
        
        /**
         * <p>Retrieves the ID for this Stanza.</p>
         *
         * @return  {String} The ID
         */
        getID: function() {
            return this._getAttrValue("id");
        },
        /**
         * <p>Changes or removes the ID for this Stanza.</p>
         *
         * <p>If {id} is "", undefined, or null, any current value is
         * removed.</p>
         *
         * @param   {String} [id] The new ID
         */
        setID: function(id) {
            this._setAttrValue("id", id || undefined);
        },
        
        /**
         * <p>Retrieves the "from" address for this Stanza.</p>
         *
         * @returns  {String} The address this Stanza is from
         */
        getFrom: function() {
            return this._getAttrValue("from") || null;
        },
        /**
         * <p>Retrieves the "from" address for this Stanza as a JID.
         * This method will attempt to convert the "from" address
         * string into a JID, or return <tt>null</tt> if unable to.</p>
         *
         * @returns  {jabberwerx.JID} The JID this Stanza is from
         */
        getFromJID: function() {
            var addr = this.getFrom();
            
            if (addr) {
                try {
                    addr = jabberwerx.JID.asJID(addr);
                } catch (ex) {
                    //DEBUG-BEGIN
                    jabberwerx.util.debug.log("could not parse 'from' address: " + ex);
                    //DEBUG-END
                    addr = null;
                }
            }
            
            return addr;
        },
        /**
         * <p>Changes or removes the "from" address for this Stanza.</p>
         * 
         * @param   {String|jabberwerx.JID} [addr] The new from address
         */
        setFrom: function(addr) {
            addr = (addr) ?
                   jabberwerx.JID.asJID(addr) :
                   undefined;
                   
            this._setAttrValue("from", addr);
        },
        
        /**
         * <p>Retrieves the "to" address for this Stanza.</p>
         *
         * @returns  {String} The address this Stanza is to
         */
        getTo: function() {
            return this._getAttrValue("to") || null;
        },
        /**
         * <p>Retrieves the "to" address for this Stanza as a JID.
         * This method will attempt to convert the "to" address
         * string into a JID, or return <tt>null</tt> if unable to.</p>
         *
         * @returns  {jabberwerx.JID} The JID this Stanza is to
         */
        getToJID: function() {
            var addr = this.getTo();
            
            if (addr) {
                try {
                    addr = jabberwerx.JID.asJID(addr);
                } catch (ex) {
                    //DEBUG-BEGIN
                    jabberwerx.util.debug.log("could not parse 'to' address: " + ex);
                    //DEBUG-END
                    addr = null;
                }
            }
            
            return addr;
        },
        /**
         * <p>Changes or removes the "to" address for this Stanza.</p>
         * 
         * @param   {String|jabberwerx.JID} [addr] The new to address
         */
        setTo: function(addr) {
            addr = (addr) ?
                   jabberwerx.JID.asJID(addr) :
                   undefined;
                   
            this._setAttrValue("to", addr);
        },
        
        /**
         * <p>Determines if this Stanza is reporting an error.</p>
         *
         * @returns  {Boolean} <tt>true</tt> if this is a Stanza of type error
         */
        isError: function() {
            return this.getType() == "error";
        },
        /**
         * <p>Returns an ErrorInfo object containing the error information
         * of this stanza if there is any. Otherwise it returns null.</p>
         * @returns {jabberwerx.Stanza.ErrorInfo} The ErrorInfo object
         */
        getErrorInfo: function() {
            var err = jabberwerx.$(this.getNode()).children("error");
            
            if (this.isError() && err.length) {
                err = jabberwerx.Stanza.ErrorInfo.createWithNode(err.get(0));
            } else {
                err = null;
            }
            
            return err;
        },
        
        /**
         * Creates a duplicate of this stanza. This method performs a deep
         * copy of the DOM.
         * 
         * @returns  {jabberwerx.Stanza} The cloned stanza
         */
        clone: function() {
            var cpy = jabberwerx.Stanza.createWithNode(this.getNode());
            cpy.timestamp = this.timestamp;
            
            return cpy;
        },
        /**
         * Creates a stanza with the addresses reversed. This method
         * clones this Stanza, sets the "to" address to be the original
         * "from" address, then (optionally) sets the "from" address to be
         * the original "to" address. 
         *
         * @param   {Boolean} [include_from] <tt>true</tt> if the new
         *          stanza should include a "from" address (default is
         *          <tt>false</tt>)
         * @returns  {jabberwerx.Stanza} The cloned stanza, with addresses
         *          swapped.
         */
        swap: function(include_from) {
            var cpy = this.clone();
            cpy.setTo(this.getFromJID());
            cpy.setFrom(include_from ? this.getToJID() : null);
            
            return cpy;
        },
        /**
         * <p>Creates an error stanza based on this Stanza. This method
         * calls {@link #swap}, sets the type to "error", and appends an
         * &lt;error/&gt; element with the data from {err}.</p>
         *
         * @param   {jabberwerx.Stanza.ErrorInfo} err The error information
         * @return  {jabberwerx.Stanza} The error stanza
         * @throws  {TypeError} If {err} is not a ErrorInfo
         */
        errorReply: function(err) {
            if (!(err && err instanceof jabberwerx.Stanza.ErrorInfo)) {
                throw new TypeError("err must be an ErrorInfo");
            }
            
            var retval = this.swap();
            retval.setType("error");
            
            // TODO: better namespace handling for node??
            var builder = new jabberwerx.NodeBuilder(retval.getNode()).
                    xml(err.getNode().xml);
            
            return retval;
        },
        
        /**
         * Called before this Stanza is serialized for persistence. This
         * method saves a string representation of the XMPP stanza and
         * converts the timestamp from a Date object into a number.
         */
        willBeSerialized: function() {
            this.timestamp = this.timestamp.getTime();
            this._serializedXML = this._DOM.xml;
            delete this._DOM;
        },
        /**
         * Called after this Stanza is deserialized from persistence. This
         * method rebuilds the DOM structure from the saved XML string and
         * converts the timestamp from a number into a Date object.
         */
        wasUnserialized: function() {
            if (this._serializedXML && this._serializedXML.length) {
                this._DOM = jabberwerx.util.unserializeXML(this._serializedXML);
                delete this._serializedXML;
            }
            
            this.timestamp = this.timestamp ? new Date(this.timestamp) : new Date();
        },

        /**
         * The timestamp of this Stanza. If this stanza contains a
         * "{urn:xmpp:time}delay" or "{jabber:x:delay}x" child element, this
         * value reflects the date specified by that element. Otherwise, it is
         * the timestamp at which this stanza was created.
         *
         * @type    Date
         */
        timestamp: null,
        
        /**
         * @private
         */
        _DOM: null
    }, "jabberwerx.Stanza");
    
    jabberwerx.Stanza.ErrorInfo = jabberwerx.JWModel.extend(/** @lends jabberwerx.Stanza.ErrorInfo.prototype */{
        /**
         * @class
         * @minimal
         * <p>Representation of stanza error information.</p>
         *
         * @description
         * <p>Creates a new ErrorInfo with the given information.</p>
         *
         * @param   {String} [type] The error type ("cancel", "auth", etc)
         * @param   {String} [cond] The error condition
         * @param   {String} [text] The error text description
         * @constructs jabberwerx.Stanza.ErrorInfo
         * @extends JWModel
         */
        init: function(type, cond, text) {
            this._super();
            
            this.type = type || "wait";
            this.condition = cond || "{urn:ietf:params:xml:ns:xmpp-stanzas}internal-server-error";
            this.text = text || "";
            
            // IE work-around
            this.toString = this._toErrString;
        },
        
        /**
         * <p>Retrieves the element for this ErrorInfo. The returned element
         * is as follows:</p>
         *
         * <pre class="code">
         *  &lt;error type="{type}"&gt;
         *      &lt;{condition-local-name} xmlns="urn:ietf:params>xml:ns:xmpp-stanzas"/&gt;
         *      &lt;text xmlns="urn:ietf:params>xml:ns:xmpp-stanzas"&gt;{text}&lt;/text&gt;
         *  &lt;/error&gt;
         * </pre>
         *
         * @returns  {Element} The DOM representation
         */
        getNode: function() {
            var builder = new jabberwerx.NodeBuilder("error");
            
            builder.attribute("type", this.type);
            builder.element(this.condition);
            if (this.text) {
                builder.element("{urn:ietf:params:xml:ns:xmpp-stanzas}text").
                        text(this.text);
            }
            
            return builder.data;
        },
        
        /**
         * <p>Called after this object is rehydrated. This method sets the toString
         * method as expected.</p>
         */
        wasUnserialized: function() {
            // IE work-around
            this.toString = this._toErrString;
        },
        
        /**
         * @private
         */
        _toErrString: function() {
            return this.condition;
        },
        
        /**
         * <p>The type of error info.</p>
         *
         * @type    String
         */
        type: "",
        /**
         * <p>The condition of the error info. This is the expanded-name of
         * the predefined condition for the ErrorInfo.</p>
         *
         * @type    String
         */
        condition: "",
        /**
         * <p>The optional text description for the error info.</p>
         *
         * @type    String
         */
        text: ""
    }, "jabberwerx.Stanza.ErrorInfo");
    /**
     * <p>Creates an ErrorInfo based on the given node.</p>
     *
     * @param   {Element} node The XML &lt;error/&gt;
     * @return  {jabberwerx.Stanza.ErrorInfo} The ErrorInfo
     * @throws  {TypeError} If {node} is not an element
     */
    jabberwerx.Stanza.ErrorInfo.createWithNode = function(node) {
        if (!jabberwerx.isElement(node)) {
            throw new TypeError("node must be an Element");
        }
        node = jabberwerx.$(node);
        var type = node.attr("type");
        var cond = node.
                children("[xmlns=urn:ietf:params:xml:ns:xmpp-stanzas]:not(text)").
                map(function() {
                    return "{urn:ietf:params:xml:ns:xmpp-stanzas}" + this.nodeName;
                }).get(0);
        var text = node.
                children("text[xmlns=urn:ietf:params:xml:ns:xmpp-stanzas]").
                text();

        // TODO: search for known errors first?
        return new jabberwerx.Stanza.ErrorInfo(type, cond, text);
    };
    
    /**
     * <p>ErrorInfo for a bad request error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_BAD_REQUEST = new jabberwerx.Stanza.ErrorInfo(
            "modify",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}bad-request");
    /**
     * <p>ErrorInfo for a conflict error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_CONFLICT = new jabberwerx.Stanza.ErrorInfo(
            "modify",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}conflict");
    /**
     * <p>ErrorInfo for a feature not implemented error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_FEATURE_NOT_IMPLEMENTED = new jabberwerx.Stanza.ErrorInfo(
            "cancel",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}feature-not-implemented");
    /**
     * <p>ErrorInfo for a forbidden error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_FORBIDDEN = new jabberwerx.Stanza.ErrorInfo(
            "auth",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}forbidden");
    /**
     * <p>ErrorInfo for an internal server error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_INTERNAL_SERVER_ERROR = new jabberwerx.Stanza.ErrorInfo(
            "wait",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}internal-server-error");
    /**
     * <p>ErrorInfo for a non-existent item.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_ITEM_NOT_FOUND = new jabberwerx.Stanza.ErrorInfo(
            "cancel",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}item-not-found");
    /**
     * <p>ErrorInfo for a malformed JID error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_JID_MALFORMED = new jabberwerx.Stanza.ErrorInfo(
            "modify",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}jid-malformed");
    /**
     * <p>ErrorInfo for a not acceptable error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_NOT_ACCEPTABLE = new jabberwerx.Stanza.ErrorInfo(
            "modify",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-acceptable");
    /**
     * <p>ErrorInfo for a not allowed error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_NOT_ALLOWED = new jabberwerx.Stanza.ErrorInfo(
            "cancel",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-allowed");
    /**
     * <p>ErrorInfo for a not authorized error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_NOT_AUTHORIZED = new jabberwerx.Stanza.ErrorInfo(
            "auth",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}not-authorized");
    /**
     * <p>ErrorInfo for a service unavailable error.</p>
     *
     * @type    jabberwerx.Stanza.ErrorInfo
     */
    jabberwerx.Stanza.ERR_SERVICE_UNAVAILABLE = new jabberwerx.Stanza.ErrorInfo(
            "wait",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}service-unavailable");

    /**
     * <p>ErrorInfo for a remote server timeout error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT = new jabberwerx.Stanza.ErrorInfo(
            "wait",
            "{urn:ietf:params:xml:ns:xmpp-stanzas}remote-server-timeout");
            
    /**
     * Factory method for creating a stanza from an XML node.
     * 
     * @static
     * @param   {Element} node An XML node.
     * @return  {jabberwerx.Stanza} The Stanza object wrapping the given
     *          node
     */
    jabberwerx.Stanza.createWithNode = function(node) {
        if (!jabberwerx.isElement(node)) {
            throw new TypeError("node must be an element");
        }
        
        var stanza;
        switch(node.nodeName) {
            case "iq":
                stanza = new jabberwerx.IQ(node);
                break;
            case "message":
                stanza = new jabberwerx.Message(node);
                break;
            case "presence":
                stanza = new jabberwerx.Presence(node);
                break;
            default:
                stanza = new jabberwerx.Stanza(node);
                break;
        }
        
        return stanza;
    };

    jabberwerx.IQ = jabberwerx.Stanza.extend(/** @lends jabberwerx.IQ.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Represents an &lt;iq/&gt; stanza.</p>
         *
         * @description
         * <p>Creates a new IQ object.</p>
         * 
         * @param   {Element} [packet] The &lt;iq/&gt; element, or <tt>null</tt>
         *          for an empty IQ
         * @throws  {TypeError} If {packet} is not an &lt;iq/&gt; element
         * @constructs jabberwerx.IQ
         * @extends jabberwerx.Stanza
         */     
        init: function(packet) {
            if (packet) {
                if (!jabberwerx.isElement(packet)) {
                    throw new TypeError("packet must be an &lt;iq/&gt; Element");
                }
                if (packet.nodeName != "iq") {
                    throw new TypeError("packet must be an &lt;iq/&gt; Element");
                }
            }
            this._super(packet || "{jabber:client}iq");
        },
        
        /**
         * <p>Retrieves the payload for this IQ.</p>
         *
         * @returns  {Element} The payload for this IQ, or {null} if none
         */
        getQuery: function() {
            return jabberwerx.$(this.getNode()).children(":not(error)").get(0);
        },
        /**
         * <p>Changes or removes the payload for this IQ.</p>
         *
         * @returns  {Element} The payload for this IQ, or {null} if none
         * @throws {TypeError} If {payload} is not an Element
         */
        setQuery: function(payload) {
            if (payload && !jabberwerx.isElement(payload)) {
                throw new TypeError("Node must be an element");
            }
        
            var q = jabberwerx.$(this.getNode()).children(":not(error)");
            q.remove();
            
            if (payload) {
                new jabberwerx.NodeBuilder(this.getNode()).node(payload);
            }
        },
        /**
         * <p>Generates a reply IQ from this IQ. This method clones this
         * IQ and sets the type to "result". If {payload} is specified,
         * it is added, otherwise the IQ is empty.</p>
         *
         * @param   {Element|String} payload The payload for this IQ
         * @return  {jabberwerx.IQ} The reply for this IQ
         * @throws  {TypeError} If {payload} is not an XML Element
         */
        reply: function(payload) {
            var retval = this.swap();
            
            try {
                jabberwerx.$(retval.getNode()).empty();
            } catch (ex) {
                var n = retval.getNode();
                for (var idx = 0; idx < n.childNodes.length; idx++) {
                    n.removeChild(n.childNodes[idx]);
                }
            }
            if (payload) {
                var builder = new jabberwerx.NodeBuilder(retval.getNode());
                
                if (jabberwerx.isElement(payload)) {
                    builder.node(payload);
                } else if (typeof(payload) == "string") {
                    builder.xml(payload);
                } else {
                    throw new TypeError("payload must be an Element or XML representation of an Element");
                }
            }
            retval.setType("result");
            
            return retval;
        }
    }, "jabberwerx.IQ");

    jabberwerx.Message = jabberwerx.Stanza.extend(/** @lends jabberwerx.Message.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Represents a &lt;message/&gt; stanza.</p>
         * 
         * @description
         * <p>Creates a new Message object.</p>
         *
         * @param   {Element} [packet] The &lt;message/&gt; element, or
         *          <tt>null</tt> for an empty Message
         * @throws  {TypeError} If {packet} is not a &lt;message/&gt; element
         * @constructs jabberwerx.Message
         * @extends jabberwerx.Stanza
         */     
        init: function(packet) {
            if (packet) {
                if (!jabberwerx.isElement(packet)) {
                    throw new TypeError("Must be a <message/> element");
                }
                if (packet.nodeName != "message") {
                    throw new TypeError("Must be a <message/> element");
                }
            }
            
            this._super(packet || "{jabber:client}message");
        },
        
        /**
         * <p>Retrieves the plaintext body for this Message.</p>
         *
         * @returns  {String} The body
         */
        getBody: function() {
            return this._getChildText("body");
        },
        /**
         * <p>Changes or removes the body for this Message.</p>
         * <p>Changes to the plaintext body will automatically clear the 
         *  XHTML-IM body (XEP-71 8#2). In practice <tt>setBody</tt> and 
         *  {@link #setHTML} are mutually exclusively, using both 
         *  within the same message is not recommended.</p>
         * @param   {String} [body] The new message body
         */
        setBody: function(body) {
            this.setHTML();
            this._setChildText("body", body || undefined);
        },

        /**
         * <p>Retrieves the XHTML-IM body element for this Message.
         *  The first body contained within an html element (xep-71 namespaces) or null
         *   if the element does not exist. Returned element will be cleaned using xep-71
         *   Recommended  Profile. See {@link jabberwerx.xhtmlim#.sanitize}.
         *  NOTE the entire body element is returned, not just its contents. </p>
         * @returns  {DOM} The XHTML body element or null
         */
        getHTML: function() {
            var ret = jabberwerx.$(this.getNode()).find("html[xmlns=http://jabber.org/protocol/xhtml-im]>body[xmlns=http://www.w3.org/1999/xhtml]:first");
            if (ret.length && !this._isSanitized) { //most likely a received message
                this.setHTML(ret.children().get()); //setHTML will sanitize and set state as needed. 
                return this.getHTML();
            }
            return ret.length ? ret.get(0) : null;
        },
        /**
         * <p>Changes or removes both the HTML and plaintext bodies for this Message.</p>
         * <p>XHTML-IM and plaintext must have the same text value (xep-71 8#2). See {@link #setBody}.
         *  HTML is cleaned using xep-71 Recommended Profile (See {@link jabberwerx.xhtmlim#.sanitize}). 
         *  <tt>html</tt> may be a string that will parse into a root tag, a single root HTML tag or an array of 
         *  HTML tags (must not be body elements). <tt>setHTML</tt> adds a body wrapper as needed.
         *  If <tt>html</tt> is <tt>null</tt> the XHTML-IM html element is removed and the plaintext body is cleared.</p>
         * @param   {DOM|Array|String} [html] The new message
         * @throws {TypeError} If html is defined and not a parsable string,  DOM or a non empty Array of DOM
         */
        setHTML: function(html) {          
            if (html && !jabberwerx.util.isString(html) && !jabberwerx.isElement(html) && 
               (!jabberwerx.$.isArray(html) || !html.length)) {
                throw new TypeError("html must be a string, DOM or an array");
            }
            this._isSanitized = false;
            var htmlNode = jabberwerx.$(this.getNode()).find("html[xmlns=http://jabber.org/protocol/xhtml-im]");
            if (htmlNode) {
                htmlNode.remove();
            }
            this._setChildText("body"); 
            if (html) {
                if (jabberwerx.util.isString(html)) {
                    html = jabberwerx.util.unserializeXML(html);
                }
                if (jabberwerx.$.isArray(html)) {      
                    var newBodyBuilder = new jabberwerx.NodeBuilder("{http://www.w3.org/1999/xhtml}body");                
                    jabberwerx.$.each(html, function (index, item) {
                        newBodyBuilder.node(item);
                    });
                    html = newBodyBuilder.data;                    
                } else if (html.nodeName != "body") {
                    html = jabberwerx.util.unserializeXML("<body xmlns='http://www.w3.org/1999/xhtml'>" + html.xml + "</body>");
                }
                jabberwerx.xhtmlim.sanitize(html);
                htmlNode = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/xhtml-im}html").node(html).parent.data;
                jabberwerx.$(this.getNode()).append(htmlNode.cloneNode(true));
                this._setChildText("body", jabberwerx.$(htmlNode).text());
                this._isSanitized = true;
            }           
        },
       
        /**
         * <p>Retrieves the subject for this Message.</p>
         *
         * @returns  {String} The subject
         */
        getSubject: function() {
            return this._getChildText("subject");
        },
        /**
         * <p>Changes or removes the subject for this Message.</p>
         *
         * @param   {String} [subject] The new message subject
         */
        setSubject: function(subject) {
            this._setChildText("subject", subject || undefined);
        },
        
        /**
         * <p>Retrieves the thread for this Message.</p>
         *
         * @returns  {String} The thread
         */
        getThread: function() {
            return this._getChildText("thread") || null;
        },
        /**
         * <p>Changes or removes the thread for this Message.</p>
         *
         * @param   {String} [thread] The new message thread
         */
        setThread: function(thread) {
            this._setChildText("thread", thread || undefined);
        },
        
        /**
         * @private
         * xhtml has been santized flag 
         * @type  {Boolean}
         */
        _isSanitized: false
    }, "jabberwerx.Message");

    /**
     * Takes a String or an Element and substitutes in the user name, nickname or handle of the sender, where the string begins with '/me '.<br>
     * Example: translate('/me laughs', 'foo') returns ['* foo laughs']
     * @param   {String|Element} content The message content.
     * @param   {String} displayName The displayName of the sender
     * @return {jQueryCollection|null} The translated Message if content contained a XEP-0245 '/me ' command, otherwise null
     * @throws {jabberwerx.Message.InvalidContentFormat} if content is not a string or a Element.
     */
    jabberwerx.Message.translate = function(content, displayName) {
        var xep0245Found = false;
        var textNodeFound = false;
        var translatedContent = null;

        var findTextNodes = function(element, displayName) {
            if (!xep0245Found && !textNodeFound) {
                if (jabberwerx.isText(element)) {
                    var replace = translateSlashMe(jabberwerx.$(element).text(), displayName);
                    if (xep0245Found) {
                        jabberwerx.$(element).replaceWith(replace);
                    } else {
                        textNodeFound = true;
                    }
                } else if (element.hasChildNodes()) {
                    for (var i = 0; i < element.childNodes.length; i++) {
                        findTextNodes(element.childNodes[i], displayName);
                    }
                }
            }
        };


        var translateSlashMe = function(rawText, displayName) {
			var slashMe = "/me ";
            if (rawText.substring(0,slashMe.length).toLowerCase() == slashMe) {
                xep0245Found = true;
                return ("* " + displayName + " " + rawText.substring(slashMe.length));
            }
            return rawText;
		};

        if (typeof content == "string") {
            content = translateSlashMe(content, displayName);
        } else if (jabberwerx.isElement(content)) {
            // traverse nodes looking for text nodes
            for (var i = 0; i < content.childNodes.length; i++) {
                if (!xep0245Found && !textNodeFound) {
                    findTextNodes(content.childNodes[i], displayName);
                } else {
                    break;
                }
            }
        } else {
           throw new jabberwerx.Message.InvalidContentFormat();
        }
        
        if (xep0245Found) {
            translatedContent = content;
        }
        return translatedContent;
    };
	/**
     * @class jabberwerx.Message.InvalidContentFormat
     * @minimal
     * <p>Error to indicate the content is not type of string or a jQuery object.</p>
     * @description
     * <p>Creates a new InvalidContentFormat with the given message.</p>
     * @extends jabberwerx.util.Error
     */
	jabberwerx.Message.InvalidContentFormat = jabberwerx.util.Error.extend('The content parameter must be of type string or a jQuery object.');

    jabberwerx.Presence = jabberwerx.Stanza.extend(/** @lends jabberwerx.Presence.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Represents a &lt;presence/&gt; stanza.</p>
         * 
         * @description
         * <p>Creates a new Presence object.</p>
         *
         * @param   {Element} [packet] The &lt;presence/&gt; element, or
         *          <tt>null</tt> for an empty Presence
         * @throws  {TypeError} If {packet} is not an &lt;presence/&gt; element
         * @constructs jabberwerx.Presence
         * @extends jabberwerx.Stanza
         */     
        init: function(packet) {
            if (packet) {
                if (!jabberwerx.isElement(packet)) {
                    throw new TypeError("packet must be a &lt;presence/&gt; Element");
                }
                if (packet.nodeName != "presence") {
                    throw new TypeError("packet must be a &lt;presence/&gt; Element");
                }
            }
            this._super(packet || "{jabber:client}presence");
        },
        
        /**
         * <p>Retrieves the priority for this Presence. This method
         * returns the integer value for the &lt;priority/&gt; child
         * element's text content, or 0 if none is available.</p>
         *
         * @returns  {Number} The priority
         */
        getPriority: function() {
            var pri = this._getChildText("priority");
            pri = (pri) ? parseInt(pri) : 0;
            return !isNaN(pri) ? pri : 0;
        },
        /**
         * <p>Changes or removes the priority for this Presence.</p>
         *
         * @param   {Number} [pri] The new priority
         * @throws  {TypeError} If {pri} is defined and not a number.
         */
        setPriority: function(pri) {
            // NOTE: 0 evaluates to false, so need to check
            if (pri !== undefined && pri !== null && typeof(pri) != "number") {
                throw new TypeError("new priority must be a number or undefined");
            }
            this._setChildText("priority", pri);
        },
        
        /**
         * <p>Retrieves the show value for this Presence. This method
         * returns the text content of the &lt;show/&gt; child eleemnt,
         * or {@link #.SHOW_NORMAL} if none is available.</p>
         *
         * @returns  {String} The show value
         */
        getShow: function() {
            return this._getChildText("show") || jabberwerx.Presence.SHOW_NORMAL;
        },
        /**
         * <p>Changes or removes the show value for this Presence.</p>
         *
         * @param   {String} [show] The new show value
         * @throws  {TypeError} If {show} is defined and not one of
         *          "away", "chat", "dnd", or "xa".
         */
        setShow: function(show) {
            if (show && (show != jabberwerx.Presence.SHOW_AWAY &&
                        show != jabberwerx.Presence.SHOW_CHAT &&
                        show != jabberwerx.Presence.SHOW_DND  &&
                        show != jabberwerx.Presence.SHOW_XA)) {
                throw new TypeError("show must be undefined or one of 'away', 'chat', 'dnd', or 'xa'");
            }
            
            this._setChildText("show", show || undefined);
        },
        
        /**
         * <p>Retrieves the status value for this Presence. This method
         * returns the text content of the &lt;status/&gt; child element,
         * or <tt>null</tt> if none is available.</p>
         *
         * @returns  {String} The show value
         */
        getStatus: function() {
            return this._getChildText("status") || null;
        },
        /**
         * <p>Changes or removes the status value for this Presence.</p>
         *
         * @param   {String} [status] The new status value
         */
        setStatus: function(status) {
            this._setChildText("status", status || undefined);
        },
        
        /**
         * <p>Compares this Presence to the given presence object for
         * natural ordering. The order is determined via:</p>
         * <ol>
         * <li>The &lt;priority/&gt; values</li>
         * <li>The &lt;show/&gt; values</li>
         * <li>The timestamps</li>
         * </ol>
         *
         * <p>A missing &lt;priority/&gt; value is equal to "0".</p>
         *
         * <p>A missing &lt;show/&gt; value is equal to "" (normal).</p>
         *
         * @param   {jabberwerx.Presence} presence Object to compare against
         * @returns  {Integer} -1, 1, or 0 if this Presence is before, after,
         *          or in the same position as {presence}
         */
        compareTo: function(presence) {
            if (!(presence && presence instanceof jabberwerx.Presence)) {
                throw new TypeError("presence must be an instanceof jabberwerx.Presence");
            }
            
            var p1, p2;
            
            p1 = this.getPriority() || 0;
            p2 = presence.getPriority() || 0;
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            }
            
            p1 = this._showStrToInt(this.getType() || this.getShow());
            p2 = this._showStrToInt(presence.getType() || presence.getShow());
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            }
            
            p1 = this.timestamp;
            p2 = presence.timestamp;
            if (p1 > p2) {
                return 1;
            } else if (p1 < p2) {
                return -1;
            }
            
            return 0;
        },
        
        /**
         * @private
         * Assign an integer value depending on the string value passed through.
         * @param showStr String value to convert to an integer
         * @returns {Integer} Integer value as follows:
         *              'chat':         5
         *              '':             4
         *              'away':         3
         *              'xa':           2
         *              'dnd':          1
         *              'unavailable':  0
         *      return -1 in case of any other value
         */
        _showStrToInt: function(showStr) {
            showStr = (showStr ? showStr : "");
            switch (showStr) {
                case 'chat':
                    return 5;
                case '':
                    return 4;
                case 'dnd':
                    return 3;
                case 'away':
                    return 2;
                case 'xa':
                    return 1;
                case 'unavailable':
                    return 0;
                default:
                    return -1;
            }
        },
        
        /**
         * Sets show, status and priority via a single method call
         * @param {String} [show] A status message
         * @param {String} [status] A status indicator
         * @param {Integer} [priority] A priority for this resource
         * @returns {jabberwerx.Presence} This updated presence stanza
         */
        setPresence: function(show, status, priority) {
            if (show) {
                this.setShow(show);
            }
            if (status) {
                this.setStatus(status);
            }
            if (priority !== undefined && priority !== null) {
                this.setPriority(priority);
            }
            return this;
        }
    }, "jabberwerx.Presence");
    
    /**
     * The "away" status.
     * @constant 
     * @type String
     */
    jabberwerx.Presence.SHOW_AWAY = "away";
    /**
     * The "chat" status.
     * @constant 
     * @type String
     */
    jabberwerx.Presence.SHOW_CHAT = "chat";
    /**
     * The "dnd" status.
     * @constant 
     * @type String
     */    
    jabberwerx.Presence.SHOW_DND = "dnd";
    /**
     * The "normal" status.
     * @constant 
     * @type String
     */
    jabberwerx.Presence.SHOW_NORMAL = "";
    /**
     * The "xa" status.
     * @constant 
     * @type String
     */
    jabberwerx.Presence.SHOW_XA = "xa";
}
