/**
 * filename:        Contact.js
 * created at:      2009/05/15T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.Contact = jabberwerx.User.extend(/** @lends jabberwerx.Contact.prototype */{
        /**
         * @class
         * <p>Contact object which represents a user known to the current
         * client. This object SHOULD NOT be created directly.</p>
         *
         * @description
         * <p>Creates a new Contact with the given JID and controller.</p>
         *
         * @param   {jabberwerx.JID} jid The contact JID
         * @param   {jabberwerx.Controller} ctrl The creating controller
         * @throws  {TypeError} If {jid} is invalid; or if {ctrl} is
         *          invalid.
         * @constructs  jabberwerx.Contact
         * @extends     jabberwerx.User
         */
        init: function(jid, ctrl) {
            this._super(jid, ctrl);
        }
    }, "jabberwerx.Contact");

    jabberwerx.RosterContact = jabberwerx.Contact.extend(/** @lends jabberwerx.RosterContact.prototype */ {
        /**
         * @class
         * <p>RosterContact object which is part of a user's roster. This object
         * SHOULD NOT be created directly. Instead, it should be created via
         * {@link jabberwerx.RosterController#addContact}.</p>
         * 
         * <p>This entity has the following in its {@link #properties} map:</p>
         * <table>
         * <tr><th>Name</th><th>Type</th><th>Description</th></tr>
         * <tr><td>name</td><td>String</td><td>The nickname from the server (may be "")</td></tr>
         * <tr><td>subscription</td><td>String</td><td>The subscription state (one of "none", "to", "from", or "both")</td></tr>
         * <tr><td>ask</td><td>String</td><td>The pending subscription state (one of "subscribe", "unsubscribe", or "")</td></tr>
         * </table>
         *
         * @description
         * <p>Creates a new Contact with the given item node and creating
         * RosterController</p>
         *
         * @param   {Element} item A DOM node which represents an
         *          &lt;item&gt; element
         * @param   {jabberwerx.RosterController} roster The creating
         *          RosterController
         * @throws  {TypeError} If {item} is not a valid DOM element; or if
         *          {roster} is not a valid RosterController
         * @constructs jabberwerx.RosterContact
         * @extends jabberwerx.Contact
         */
        init: function(item, roster) {
            if (!(roster && roster instanceof jabberwerx.RosterController)) {
                throw new TypeError("roster must be provided");
            }
            this._super(
                    jabberwerx.$(item).attr("jid"),
                    roster);
            
            this._initializing = true;
            this.setItemNode(item);
            this._initializing = false;
        },
        
        /**
         * <p>Sets the contact node object. Calling this method triggers an
         * "entityUpdated" event.</p>
         *
         * <p>This method SHOULD NOT be called directly.</p>
         * 
         * @param   {Node} node A DOM node which represents an &lt;item&gt;
         *          element
         * @throws {TypeError} if {node} is not valid
         */
        setItemNode: function(node) {
            if (!(node && jabberwerx.isElement(node))) {
                throw new TypeError("node cannot be null");
            }
            
            if (this._node !== node) {
                this._node = node;
                node = jabberwerx.$(node);
                
                // setup properties
                var oldSub = this.properties.subscription;
                this.properties.subscription = node.attr("subscription") || "";
                this.properties.ask = node.attr("ask") || "";
                this.properties.name = node.attr("name") || "";
                
                var newSub = this.properties.subscription;
                if (    !(!oldSub || oldSub == "from" || oldSub == "none") &&
                        (!newSub || newSub == "from" || newSub == "none")) {
                    this.updatePresence();
                } else if ( !(!newSub || newSub == "from" || newSub == "none") &&
                            !this.getPrimaryPresence()) {
                    var prs = new jabberwerx.Presence();
                    prs.setType("unavailable");
                
                    var jid = jabberwerx.JID.asJID(node.attr("jid")).getBareJID();
                    prs.setFrom(jid);
                    this.updatePresence(prs);
                }
                                
                // setup displayName manually
                this._displayName = this.properties.name;
                
                // setup groups manually
                this._groups = node.children("group").
                        map(function() { return jabberwerx.$(this).text(); }).
                        get() || [];
                
                // trigger event manually
                if (!this._initializing && this._eventing["updated"]) {
                    this._eventing["updated"].trigger(this);
                }
            }
        },
        /**
         * Get the contact node object
         * 
         * @returns {Node} A DOM node which represents an &lt;item&gt; element
         */
        getItemNode: function() {
            return this._node;
        },
        
        /**
         * <p>Retrieves the display name of this contact. This method overrides
         * the base to use the properties name, or the default implementation if
         * no server-side name is present.</p>
         *
         * @returns {String} The display name
         */
        getDisplayName: function() {
            return this.properties.name || this._super();
        },
        /**
         * <p>Changes or removes the display name for this Contact. This
         * method overrides the base class to instead change the display
         * name via {@link jabberwerx.RosterController#updateContact}.</p>
         * @param {String}name The display name to be set
         */
        setDisplayName: function(name) {
            if (name != this._displayName) {
                this.controller.updateContact(
                        this.jid,
                        name,
                        this.getGroups());
            }
        },
        /**
         * <p>Changes or removes the groups for this Contact. This
         * method overrides the base class to instead operate via
         * {@link jabberwerx.RosteControllerr#updateContact}.</p>
         * @param {String] The name of groups
         */
        setGroups: function(groups) {
            this.controller.updateContact(
                    this.jid,
                    this.properties.name,
                    groups);
        },
        
        /**
         * <p>Deletes this Contact. This method overrides the base class to
         * instead operate via {@link jabberwerx.RosterController#deleteContact}.</p>
         */
        remove: function() {
            this.controller.deleteContact(this.jid);
        },

        /**
         * @private
         */
        _handleUnavailable: function(presence) {
            if (this.properties.subscription == "both" ||
                this.properties.subscription == "to" ||
                this.properties.temp_sub) {
                var pres = this.getPrimaryPresence();
                if (!pres) {
                    this._insertPresence(presence);
                } else if (pres.getType() == "unavailable") {
                    this._clearPresenceList();
                    this._insertPresence(presence);
                }
            }
        },
        
        /**
         * @private
         */
        _node: null
    }, 'jabberwerx.RosterContact');
}
