/**
 * filename:        PubSubNode.js
 * created at:      2009/11/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.PubSubItem = jabberwerx.JWModel.extend(/** @lends jabberwerx.PubSubItem.prototype */{
        /**
         * @class
         * <p>Represents a publish-subscribe item.</p>
         *
         * @description
         * <p>Creates a new PubSubItem with the given DOM element.</p>
         *
         * <p><b>NOTE:</b> This type should not be created directly. Instead,
         * the PubSubNode creates instances of PubSubItem as needed.</p>
         *
         * @param   {Element} dom The &lt;item/&gt; element
         * @constructs  jabberwerx.PubSubItem
         * @extends     jabberwerx.JWModel
         */
        init: function(dom) {
            this._super();
            
            this._update(dom);
        },
        /**
         * <p>Destroys this PubSubItem.</p>
         */
        destroy: function() {
            this._super();
        },
        
        /**
         * <p>Called when this object is about to be serialized. This method
         * converts the timestamp from a Date object to the number of seconds
         * since the Epoch.</p>
         */
        willBeSerialized: function() {
            this.timestamp = this.timestamp.getTime();
        },
        /**
         * <p>Called just after this object is unserialized. This method
         * converts the timestamp from the number of seconds since the Epoch
         * to a Date object.</p>
         */
        wasUnserialized: function() {
            this.timestamp = new Date(this.timestamp);
        },
        
        /**
         *
         */
        _update: function(dom) {
            dom = jabberwerx.$(dom);
            var val;
            
            val = dom.attr("publisher");
            this.publisher = (val) ? jabberwerx.JID.asJID(val) : null;
            
            val = dom.attr("timestamp");
            this.timestamp = (val) ? jabberwerx.parseTimestamp(val) : new Date();
            
            this.id = dom.attr("id");
            this.data = dom.children().get(0);
        },
        
        /**
         * <p>The identifier of this item.</p>
         *
         * @type String
         */
        id: null,
        
        /**
         * <p>The publisher of this item, or <tt>null</tt> if unknown.</p>
         *
         * @type jabberwerx.JID
         */
        publisher:  null,
        /**
         * <p>The timestamp for this item, or the current time at creation
         * if not otherwise known.</p>
         *
         * @type Date
         */
        timestamp: null,
        /**
         * <p>The payload of the item as a DOM element, or null if none.</p>
         *
         * @type Element
         */
        data: null
    }, "jabberwerx.PubSubItem");
    
    jabberwerx.PubSubNode = jabberwerx.Entity.extend(/** @lends jabberwerx.PubSubNode.prototype */ {
        /**
         * @class
         * <p>Represents a publish-subscribe node.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.PubSubNode">jabberwerx.PubSubNode</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new PubSubNode for the given JID, node, and
         * PubSubController (or delegate).</p>
         *
         * <p>If this PubSubNode is created with a delegate, the owning
         * controller is the delegate's controller. A delegated PubSubNode
         * (one that has a delegate) is mostly just a container of
         * published items, relying on the delegate to perform most
         * operations.</p>
         *
         * <p><b>NOTE:</b> This type should not be constructed directly.
         * Instead, use {@link jabberwerx.PubSubController#node} to obtain an
         * instance.</p>
         *
         * @param   {jabberwerx.JID} jid The JID
         * @param   {String} node The node
         * @param   {jabberwerx.PubSubController|jabberwerx.PubSubNode} ctrl The
                    owning controller or delegate
         * @throws  {TypeError} If {ctrl} is not valid; or if {node} is not
         *          valid
         * @extends jabberwerx.Entity
         * @constructs  jabberwerx.PubSubNode
         */
        init: function(jid, node, ctrl) {
            if (!node || typeof(node) != "string") {
                throw new TypeError("node must be a non-empty string");
            }
            
            if (ctrl instanceof jabberwerx.PubSubNode) {
                // store delegate and change to delegate's controller
                this.delegate = ctrl;
                ctrl = ctrl.controller;
            }
            if (!(ctrl instanceof jabberwerx.PubSubController)) {
                throw new TypeError("ctrl must be a jabberwerx.PubSubController or jabberwerx.PubSubNode");
            }
            
            this._super({jid: jid, node: node}, ctrl);
            
            // array of PubSubItems
            this.properties.items = [];
            // map of delegated JIDs (jid + node ==> PubSubNode)
            this.properties.delegated = {};
            
            this.applyEvent("pubsubNodeCreated");
            this.applyEvent("pubsubNodeDeleted");
            this.applyEvent("pubsubItemsChanged");
            this.applyEvent("errorEncountered");
            
            if (!this.delegate) {
                this.controller.client.event("beforeMessageReceived").bindWhen(
                        "event[xmlns=http://jabber.org/protocol/pubsub#event]>*[node=" + this.node + "]",
                        this.invocation("_handleNotify"));
            }
        },
        /**
         * <p>Destroys this PubSubNode. This method unbinds registered
         * callbacks, destroys any delegated PubSubNodes, and then calls
         * the superclass' implementation.</p>
         */
        destroy: function() {
            if (!this.delegate) {
                this.controller.client.event("beforeMessageReceived").unbind(
                        this.invocation("_handleNotify"));
            }
            if (this.properties.delegated) {
                // cleanup delegates
                var client = this.controller.client;
                
                for (var key in this.properties.delegated) {
                    var d = client.entitySet.entity(key, this.node);
                    if (d) {
                        d.destroy();
                    }
                }
            }
        
            this._super();
        },
        
        /**
         * <p>Retrieves the current items for this PubSubNode. The returned
         * array's items are indexed by both the natural array index (e.g.
         * items[0], items[1]) and by the "item:{item.id}" (e.g.
         * items["item:current"], items["item:some-random-id"]).</p>
         *
         * @returns  {jabberwerx.PubSubItem[]} The current items
         */
        getItems: function() {
            return jabberwerx.$.extend([], this.properties.items);
        },
        
        /**
         * <p>Retrieves the delegated PubSubNode for the given JID. If
         * such a delegated node does not exist, one is created.</p>
         *
         * @param   {String|jabberwerx.JID} jid The JID of the delegated node
         * @returns {jabberwerx.PubSubNode} The delegated node
         * @throws  {TypeError} if {jid} is not a valid JID
         * @throws  {jabberwerx.PubSubNode.DelegatedNodeError} If this
         *          PubSubNode is already delegated.
         */
        getDelegatedFor: function(jid) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedNodeError();
            }
            
            jid = jabberwerx.JID.asJID(jid).getBareJID();
            if (jid.equals(this.jid)) {
                return this;
            }
            
            var delegated = this.controller.node(this.node, jid);
            delegated.delegate = this;
            this.properties.delegated[jid.toString()] = true;
            
            return delegated;
        },
        
        /**
         * <p>Subscribes to this PubSubNode. This method sends the
         * explicit subscription request to the node, using the
         * connected user's bare JID.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if subscribe failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is defined and not a function
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is not
         *          connected
         * @see     #unsubscribe
         */
        subscribe: function(cb) {
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            if (this.properties.subscription || this.delegate) {
                if (cb) {
                    cb.call(this);
                }
                
                return;
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            query.element("subscribe").
                  attribute("node", this.node).
                  attribute("jid", client.connectedUser.jid);
                  
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "subscribe",
                        error: err
                    });
                } else {
                    that.properties.subscription = "explicit";
                
                    if (that.autoRetrieve) {
                        that.retrieve();
                    }
                }
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        /**
         * <p>Unsubscribes from this PubSubNode. This method sends the
         * explicit unsubscription request to the node.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if unsubscribe failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is defined and not a function
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is not
         *          connected
         * @see     #subscribe
         */
        unsubscribe: function(cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            if (this.delegate) {
                if (cb) {
                    cb.call(this);
                }
                
                return;
            }
            
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            query.element("unsubscribe").
                  attribute("node", this.node).
                  attribute("jid", client.connectedUser.jid);
                  
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    // ignore error if not subscribed
                    var notSub = jabberwerx.$(err).children(
                            "not-subscribed[xmlns=http://jabber.org/protocol/pubsub#errors]");
                    if (!notSub.length) {
                        that.event("errorEncountered").trigger({
                            operation: "unsubscribe",
                            error: err
                        });
                    }
                    
                    // clear out the error
                    err = undefined;
                }
                
                // assume we are no longer subscribed
                delete that.properties.subscription;
                
                // retract all existing items
                that._cleanItems();
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        
        /**
         * <p>Retrieves the items for this PubSubNode. This method updates
         * the list of items returned by {@link #getItems} with the current
         * remote state of the node.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if retrieve failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is defined and not a function
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is not
         *          connected
         * @see     #getItems
         */
        retrieve: function(cb) {
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            query.element("items").
                  attribute("node", this.node);
                  
            var that = this;
            client.sendIq("get", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "retrieve",
                        error: err
                    });
                } else {
                    var items = jabberwerx.$(stanza).find("pubsub[xmlns=http://jabber.org/protocol/pubsub]>items");
                    that._cleanItems();
                    that.properties.items = [];
                    that._updateItems(items);
                }
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        /**
         * <p>Publishes an item to this PubSubNode.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if publish failed
         *  }
         * </pre>
         *
         * @param   {String} [id] The item id
         * @param   {Element} [payload] The item payload
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is not valid; If {paylaod} is defined
         *          and not an Element
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is
         *          not connected
         */
        publish: function(id, payload, cb) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedError();
            }
            
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            if (payload && !jabberwerx.isElement(payload)) {
                throw new TypeError("payload must be undefined or an element");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            var item = query.element("publish").
                             attribute("node", this.node).
                             element("item");
            if (id) {
                item.attribute("id", id);
            }
            if (payload) {
                item.node(payload);
            }
            
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "publish",
                        error: err
                    });
                }
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        /**
         * <p>Retracts an item from this PubSubNode.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if retract failed
         *  }
         * </pre>
         *
         * @param   {String} id The item id
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is not valid; if {id} is empty or
         *          not a string
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is
         *          not connected
         */
        retract: function(id, cb) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedError();
            }
            
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (!(id && typeof(id) == "string")) {
                throw new TypeError("id must be a non-empty string");
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            var item = query.element("retract").
                             attribute("node", this.node).
                             element("item");
            item.attribute("id", id);
            
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "retract",
                        error: err
                    });
                }
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        
        /**
         * @private
         */
        _purge: function(cb) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedError();
            }

            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            query.element("purge").
                  attribute("node", this.node);
                  
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "purge",
                        error: err
                    });
                }
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        /**
         * <p>Creates the node on the pubsub service.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if create failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is not valid.
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is
         *          not connected
         * @throws  {jabberwerx.PubSubNode.DelegatedNodeError} If this
         *          is a delegated node
         */
        createNode: function(cb) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedNodeError();
            }
            
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}pubsub");
            query.element("create").
                  attribute("node", this.node);
                  
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "createNode",
                        error: err
                    });
                } else {
                    that.event("pubsubNodeCreated").trigger();
                }
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        /**
         * <p>Deletes the node on the pubsub service.</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if create failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is not valid.
         * @throws  {jabberwerx.Client.NotConnectedError} If the client is
         *          not connected
         * @throws  {jabberwerx.PubSubNode.DelegatedNodeError} If this
         *          is a delegated node
         */
        deleteNode: function(cb) {
            if (this.delegate) {
                throw new jabberwerx.PubSubNode.DelegatedNodeError();
            }
            
            var client = this.controller.client;
            if (!client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            var query = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub#owner}pubsub");
            query.element("delete").
                  attribute("node", this.node);

            // note "we" are trying to do the delete
            this._deletePending = true;
            var that = this;
            client.sendIq("set", this.jid, query.data, function(stanza) {
                var err = that._checkError(stanza);
                if (err) {
                    that.event("errorEncountered").trigger({
                        operation: "deleteNode",
                        error: err
                    });
                } else {
                    that._cleanItems();
                    that.event("pubsubNodeDeleted").trigger();
                }
                delete that._deletePending;
                
                if (cb) {
                    cb.call(that, err);
                }
            });
        },
        
        /**
         * @private
         */
        _cleanItems: function() {
            if (!this.properties.items.length) {
                // nothing to do
                return;
            }
            
            var items = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/pubsub}items");
            jabberwerx.$.each(this.properties.items, function() {
                items.element("retract").
                      attribute("id", this.id);
            });
            this._updateItems(jabberwerx.$(items.data));
        },
        /**
         * @private
         */
        _checkError: function(stanza) {
            return jabberwerx.$(stanza).children("error").get(0);
        },
        
        /**
         * @private
         */
        _handleNotify: function(evt) {
            var publisher = evt.data.getFromJID().getBareJID();
            if (this.jid && !this.jid.equals(publisher)) {
                // not really ours
                return false;
            }
            
            var delegated = (this.delegate) ?
                            this :                              // already delegated
                            this.getDelegatedFor(publisher);    // find the delegated
            
            if (delegated !== this) {
                // pass through to delegated
                return delegated._handleNotify(evt);
            }

            // notification intended for this instance!
            
            var selected = evt.selected;
            var items;
            var gone = false;
            
            // look for a publisher in ESA
            var addrs = jabberwerx.$(evt.data.getNode()).children("addresses[xmlns=http://jabber.org/protocol/address]");
            var publisher = addrs.find("address[type=replyto]").attr("jid");
            
            switch (selected.nodeName) {
                case "items":
                    // normal items push...
                    this._updateItems(jabberwerx.$(selected), publisher);
                    break;
                case "delete":
                    // node deleted!
                    gone = true;
                    // fall through
                case "purge":
                    // all items deleted...
                    this._cleanItems();
                    break;
            }
            
            if (gone && !this._deletePending) {
                this.event("pubsubNodeDeleted").trigger();
            }
            return true;
        },
        
        /**
         * @private
         */
        _updateItems: function(items, publisher) {
            var that = this;
            var added = [], upped = [], remed = [];
            jabberwerx.$.each(items.children(), function() {
                var id = jabberwerx.$(this).attr("id");
                var key = "item:" + id;
                var it;
                
                if (this.nodeName == "retract") {
                    it = that.properties.items[key];
                    if (it) {
                        remed.push(it);
                        
                        // remove it and de-index it
                        var idx = jabberwerx.$.inArray(it, that.properties.items);
                        delete that.properties.items[key];
                        that.properties.items.splice(idx, 1);
                    }
                } else if (this.nodeName == "item") {
                    if (publisher && !jabberwerx.$(this).attr("publisher")) {
                        jabberwerx.$(this).attr("publisher", publisher);
                    }
                    
                    it = that.properties.items[key];
                    if (it) {
                        // update the existing PubSubItem
                        it._update(this);
                        upped.push(it);
                    } else {
                        // push it and index it
                        it = new jabberwerx.PubSubItem(this);
                        that.properties.items.push(it);
                        that.properties.items[key] = it;
                        added.push(it);
                    }
                }
            });
            
            // fire pubsub item changes
            var delegate = (this.delegate) ?
                           this.delegate.event("pubsubItemsChanged") :
                           undefined;
            if (added.length) {
                this.event("pubsubItemsChanged").trigger({
                    operation: "added",
                    items: added
                }, delegate);
            }
            if (upped.length) {
                this.event("pubsubItemsChanged").trigger({
                    operation: "updated",
                    items: upped
                }, delegate);
            }
            if (remed.length) {
                this.event("pubsubItemsChanged").trigger({
                    operation: "removed",
                    items: remed
                }, delegate);
            }
            
            // indicate this entity is updated
            this.update();
        },
        
        /**
         * <p>Flag to determine if items are automatically
         * retrieved from the pubsub node upon subscription.</p>
         *
         * @type    Boolean
         */
        autoRetrieve: false
    }, "jabberwerx.PubSubNode");
    
    /**
     * @class
     * <p>Error thrown by PubSubNode to indicate a method is
     * not supported by delegated nodes.</p>
     * 
     * @extends jabberwerx.util.NotSupportedError
     */
    jabberwerx.PubSubNode.DelegatedNodeError = jabberwerx.util.Error.extend.call(
            jabberwerx.util.NotSupportedError,
            "this operation is not supported by delegated nodes");
}
