/**
 * filename:        XDataFormField.js
 * created at:      2009/11/10T11:20:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.XDataFormField = jabberwerx.JWModel.extend(/** @lends jabberwerx.XDataFormField.prototype */ {
        /**
         * @class
         * <p>Holds collection of properties for data form field.</p>
         *
         * @description
         * <p>Creates a new XDataFormField with the given DOM element or given var name and values.</p> 
         * 
         * @param       {String} [varName] Represents field id. 
         * @param       {String} [values] Field value       
         * @param       {Element} [fieldNode] The DOM element representing the field node.
         * @throws      {TypeError} if any of the parameters are not valid
         * @constructs jabberwerx.XDataFormField 
         * @extends jabberwerx.JWModel
         */
        init: function(varName, values, fieldNode) {
            this._super();
            if (fieldNode) {           
                if (jabberwerx.isElement(fieldNode)) {
                    var that = this;
                    fieldNode = jabberwerx.$(fieldNode);
                    this._var = fieldNode.attr("var");
                    this._type = fieldNode.attr("type");
                    this._label = fieldNode.attr("label");
                    this._values = [];
                    var values = fieldNode.children("value");
                    jabberwerx.$.each(values, function() {
                        that._values.push(jabberwerx.$(this).text());
                    });                        
                    this._desc = fieldNode.children("desc").text();
                    this._required = fieldNode.children("required")[0] ? true : false;
                    this._options = [];
                    var options = fieldNode.children("option");                    
                    jabberwerx.$.each(options, function() {
                       that._options.push({label: jabberwerx.$(this).attr("label"),
                                           value: jabberwerx.$(this).children("value").text()});
                    });            
                 }
                 else {
                     throw new TypeError("fieldNode must be an Element");
                 }                 
             }
             else { 
                 if (!varName && !values) {
                     throw new TypeError("one of the constructor parameters for XDataFormField should be not null");
                 }
                 if (varName) {
                     this._var = varName;
                 }
                 this.setValues(values);
             }   
        },
        
        /**
         * <p>Destroys this data form.</p>
         */
        destroy: function() {
            this._super();
        },

        /**
         * <p>Gets the var name for the field element.</p>
         * @returns {String} Field var name
         */
        getVar: function() {
            return this._var;   
        },
        
        /**
         * <p>Sets the var name for the field element.</p>
         * @param {String} [var_name] Field var name
         */
        setVar: function(var_name) {
            this._var = var_name;   
        },        
        
        
        /**
         * <p>Gets the type for the field element.</p>
         * @returns {String} Field type
         */
        getType: function() {
            return this._type;   
        },
        
        /**
         * <p>Sets the type for the field element.</p>
         * @param {String} [type] Field type
         */
        setType: function(type) {
            this._type = type;
        },
        
        /**
         * <p>Gets the label for the field element.</p>
         * @returns {String} Field label
         */
        getLabel: function() {
            return this._label;   
        },
        
        /**
         * <p>Sets the label for the field element.</p>
         * @param {String} [label] Field label
         */
        setLabel: function(label) {
            this._label = label;   
        },                  
        
        /**
         * <p>Gets value(s) for the field element.</p>
         * @returns {Array} Array of field values 
         */
        getValues: function() {
            var values = [].concat(this._values);
            return values;   
        },
        
        /**
         * <p>Sets values for the field element.</p>
         * @param {Array|Object} [values] Array of field values or a single value
         * @throws {TypeError} if {values} is not String, number or Array
         */
        setValues: function(values) {
            this._values = [];
            if (values) {
                if (typeof values == 'string' || typeof values == 'boolean') {
                    this._values.push(values);
                } else if (values instanceof Array) {
                      for(var i=0; i<values.length; i++) {
                          this._values.push(values[i]);
                      }            
                } else {
                    throw new TypeError("values must be string, number or array");         
                }
            }           
        },        
        /**
         * <p>Gets the desciption for the field element.</p>
         * @returns {String} Field description
         */
        getDesc: function() {
            return this._desc;   
        },
        
        /**
         * <p>Sets the desciption for the field element.</p>
         * @param {String} [desc] Field description
         */
        setDesc: function(desc) {
           this._desc = desc;
        },
               
        /**
         * <p>Gets options for the field element.</p>
         * @returns {Array} Label/value list, e.g. {'label':'My label','value':'My Value'}
         */
        getOptions: function() {
            var options = [].concat(this._options);
            return options;              
        },
        
        /**
         * <p>Sets options for the field element.</p>
         * @param {Array} [options] Label/value list, e.g. {'label':'My label','value':'My Value'}
         */
        setOptions: function(options) {
            this._options = [];
            var that = this;
            jabberwerx.$.each(options, function() {
                if (this.label && this.value) {
                    that._options.push({label: this.label, value: this.value}); 
                }                               
            });
               
        }, 
        
        /**
         * <p>Gets the "required" flag for the field element.</p>
         * @returns {Boolean} Indicator for the required field
         */
        getRequired: function() {
            return this._required;   
        },
        
        /**
         * <p>Sets the "required" flag for the field element.</p>
         * @param {Boolean} [required] Indicator for the required field
         */
        setRequired: function(required) {
            this._required = required;

        },  
        
        /**
         * <p>Gets the DOM of the field element.</p> DOM:
         *   <pre class="code">&lt;field type='list-multi'
         *       label='What features will the bot support?'
         *       var='features'&gt;
         *       &lt;option label='Contests'&gt;&lt;value&gt;contests&lt;/value&gt;&lt;/option&gt;
         *       &lt;option label='News'&gt;&lt;value&gt;news&lt;/value&gt;&lt;/option&gt;
         *       &lt;option label='Polls'&gt;&lt;value&gt;polls&lt;/value&gt;&lt;/option&gt;
         *       &lt;option label='Reminders'&gt;&lt;value&gt;reminders&lt;/value&gt;&lt;/option&gt;
         *       &lt;option label='Search'&gt;&lt;value&gt;search&lt;/value&gt;&lt;/option&gt;
         *       &lt;value&gt;news&lt;/value&gt;
         *       &lt;value&gt;search&lt;/value&gt;
         *  &lt;/field&gt;
         *  </pre>
         *
         *
         * @param {jabberwerx.NodeBuilder} form DOM element of the field
         */
        getDOM: function(form) {
            var field = form.element("field");
            
            if (this._var) {
                field.attribute("var",this._var);
            }
            if (this._type) {
                 field.attribute("type",this._type);
            }
            if (this._label) {
                 field.attribute("label",this.label);
            }
            for(var i=0; i<this._values.length; i++) {
                field.element("value").text(this._values[i]);
            }              
            if (this._required) {
                 field.element("required");
            }
            if (this._desc) {
                 field.element("desc").text(this._desc);
            }
            var that = this;
            jabberwerx.$.each(this._options, function() {
                field.element("option").attribute("label", this.label).text(this.value);       
            });
                                                    
              
        },                   

        /**
         * <p>Determines if the given field matches this field. This
         * method returns <tt>true</tt> if all properties of this field
         * are equal to {fields}'s.</p>
         *
         * @param {jabberwerx.XDataFormField} field The field to match against
         * @returns {Boolean} <tt>true</tt> if {fields}'s properties matches
         *          this XDataFormField.
         */
        equals: function(field) {
            if (field === this) {
                return true;
            }
            if (!field) {
                return false;
            }
            if (this.getVar() != field.getVar()) {
                return false;
            }
            if (this.getType() != field.getType()) {
                return false;
            }
            if (this.getLabel() != field.getLabel()) {
                return false;
            }
            if (this.getDesc() != field.getDesc()) {
                return false;
            }
            
            var values = field.getValues();
            for(var idx=0; idx<this._values.length; idx++)  { 
                if (typeof this._values[idx] == 'function') {
                    continue; 
                }    
                if (this._values[idx] != values[idx]) {
                    return false;
                }        
            }
                
            var options = field.getOptions(); 
            for(var idx=0; idx<this._options.length; idx++) {
                if (typeof this._options[idx] == 'function') {
                    continue; 
                }         
                if (this._options[idx].label != options[idx].label ||
                    this._options[idx].value != options[idx].value) {
                    return false;
                }        
            }              
                                              
            return true;                                 
        },

       /**
         * <p>Performs validation for XDataField.</p>
         * @throws  {jabberwerx.XDataFormField.InvalidXDataFieldError} If any of the rules for field's values have been violated       
         */
        validate: function() {
            //Validate type first
            if (this._type) { 
                if (this._type != "boolean" &&
                    this._type != "fixed" &&
                    this._type != "hidden" && 
                    this._type != "jid-multi" &&
                    this._type != "jid-single" && 
                    this._type != "list-multi" &&
                    this._type != "list-single" &&
                    this._type != "text-multi" &&
                    this._type != "text-private" &&
                    this._type != "text-single") {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                        "field type should comply with XEP-0004",
                        {field:this._var});
                }
            }
            
            //Only "multi" fields should be allowed to have multiple values
            if (this._type != "list-multi" &&
                this._type != "text-multi" &&
                this._type != "jid-multi") {
                if (this._values.length > 1) {
                    throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                        "field is not allowed to have multiple values",
                        {field:this._var});
                    
                } 
             }  
             if (this._required && this._values.length == 0) {
                 throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                     "field is required to have a value",
                     {field:this._var});
                 
             }
             
             if (this._type == "boolean" && this._values.length > 0) {
                 if (this._values[0] == "true" || this._values[0] == true) {
                     this._values[0] = "1";
                 }
                 if (this._values[0] == "false" || this._values[0] == false) {
                     this._values[0] = "0";
                 }
                 if (this._values[0] != "0" && this._values[0] != "1") {
                     throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                         "field of type boolean contains invalid value",
                         {field:this._var});
                     
                 }               
             }
             
             if (this._type == "jid-multi" || this._type == "jid-single") {
                  var jid;
                  for(var i=0; i<this._values.length; i++) {
                      try {
                          jid = jabberwerx.JID.asJID(this._values[i]);
	                  }
	                  catch(e) {
                          throw new jabberwerx.XDataFormField.InvalidXDataFieldError(
                              "field of type jid contains invalid jid type",
                              {field:this._var});

	                  }                      
                  }
                  if (this._type == 'jid-multi') {
                      jabberwerx.unique(this._values)
                  }                                    
                  
             } 
                                                 
        },
                    
        /**
         * @private
         */
        _type: null, 
        /**
         * @private
         */
        _label: null,                                      
        /**
         * @private
         */
        _var: null,
        /**
         * @private
         */
        _values: [], 
        /**
         * @private
         */
        _desc: null,          
        /**
         * @private
         */
        _required: false,
        /**
         * @private
         */
        _options: []                       
    }, "jabberwerx.XDataFormField");
    
   /**
     * @class
     *
     * <p>The object representation of an error thrown when invalid XDataField is encountered.</p>
     *
     * @property {String} field The field var name for which error has been encountered
     *
     * @description   
     * Constructs a new jabberwerx.XDataField.InvalidXDataFieldError from the given error message
     * and field var name.       
     * 
     *
     * @param       {String} [error] Error message 
     * @param       {String} [field] Field var name for which error has been encountered     
     * @extends jabberwerx.util.Error
     * @constructs jabberwerx.XDataField.InvalidXDataFieldError
     */
    jabberwerx.XDataFormField.InvalidXDataFieldError = jabberwerx.util.Error.extend.call(TypeError);
}
