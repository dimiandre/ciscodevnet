/**
 * filename:        SASL.js
 * created at:      2009/09/25T00:00:00-07:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.SASLMechanism = jabberwerx.JWBase.extend(/** @lends jabberwerx.SASLMechanism.prototype */{
        /**
         * @class
         * @minimal
         * <p>Base class for SASL support.</p>
         *
         * <p><b>NOTE:</b> When defining new SASL mechanisms, the call to
         * {@link #.extend} MUST include the mechanism name:</p>
         *
         * <pre class='code'>var namespace.SASLSomeClient = jabberwerx.SASLMechanism.extend(
         *      { ... },
         *      "namespace.SASLSomeClient",
         *      "SOME-MECH");
         * </pre>
         *
         * <p>The mechanism name then becomes a property of the class and its
         * instances.</p>
         *
         * @description
         * <p>Creates a new SASLMechanism with the given client. The name of
         * this SASLMechanism is established by its type definition via
         * {@link #.extend}.</p>
         *
         * <p>If {encoded} is <tt>true</tt>, then {@link #evaluateStart} and
         * {@link #evaluateChallenge} are expected to perform the base64
         * encoding and decoding; {@link #evaluate} will not automatically
         * perform this step.</p>
         *
         * @param       {jabberwerx.Client} client The client
         * @param       {Boolean} [encoded] <tt>true</tt> if the mechanism's
         *              {@link #evaluateStart} and {@link #evaluateChallenge}
         *              return data that is already base-64 encoded (default is
         *              <tt>false</tt>)
         * @throws      TypeError If {client} is not a jabberwerx.Client
         * @constructs  jabberwerx.SASLMechanism
         * @abstract
         * @extends     jabberwerx.JWBase
         */
        init: function(client, encoded) {
            this._super();
            
            this.mechanismName = this.constructor.mechanismName;
            this._encoded = Boolean(encoded);
            
            if (!(client && client instanceof jabberwerx.Client)) {
                throw new TypeError("client must be a jabberwerx.Client");
            }
            
            this.client = client;
        },
        
        /**
         * <p>Evaluates a step in the SASL negotation. This method
         * processes the SASL "challenge", "failure" and "success" stanzas, and
         * returns "auth" and "response" stanzas.</p>
         * 
         * <p>If called with no arguments, this method performs the initial
         * step:
         * <ol>
         * <li>{@link #evaluateStart} is called</li>
         * <li>The &lt;auth mechanism="{@link #mechanismName}"/&gt; is generated
         * and returned, including any initial data as base64-encoded text
         * content.</li>
         * </ol>
         * </p>
         *
         * <p>If called with a &lt;challenge/&gt; element, it performs the
         * subsequent steps:
         * <ol>
         * <li>The challenge data (if any) is decoded from base64</li>
         * <li>{@link #evaluateChallenge} is called, and the response
         * data noted</li>
         * <li>The &lt;response/&gt; stanza is generated and returned,
         * including any response data as base64-encoded text content</li>
         * </ol>
         * </p>
         *
         * <p>If called with a &lt;success/&gt; element, it performs the
         * finalization step:
         * <ol>
         * <li>The success data (if any) is decoded from base64</li>
         * <li>If there is success data, {@link #evaluateChallenge} is
         * called</li>
         * <li>{@link #complete} is checked to see that it is now equal
         * to <tt>true</tt></li>
         * </ol>
         * </p>
         *
         * <p>If called with a &lt;failure/&gt; element, it performs
         * error handling:
         * <ol>
         * <li>The condition from the failure is analyzed</li>
         * <li>A {@link jabberwrex.SASLMechanism.SASLAuthFailure} error is
         * thrown.</li>
         * </ol>
         * </p>
         *
         * @param   {Element} [input] The challenge to evaluate, or
         *          <tt>undefined</tt> for the initial step.
         * @returns {Element} The response, or <tt>null</tt> if no further
         *          responses are necessary.
         * @throws  {TypeError} If {input} is not an Element
         * @throws  {jabberwerx.SASLMechanism.SASLAuthFailure} If a problem
         *          is encountered
         */
        evaluate: function(input) {
            if (input && !jabberwerx.isElement(input)) {
                throw new TypeError("input must be undefined or an element");
            }
            
            var output = null;
            var failure = null;
            var data;
            if (!input) {
                if (this.started) {
                    jabberwerx.util.debug.log("SASL mechanism already started!");
                    throw this._handleFailure();
                }
                
                this.started = true;
                try {
                    data = this.evaluateStart();
                    data = this._encodeData(data);
                    
                    output = new jabberwerx.NodeBuilder("{urn:ietf:params:xml:ns:xmpp-sasl}auth").
                            attribute("mechanism", this.mechanismName).
                            text(data).
                            data;
                } catch (ex) {
                    jabberwerx.util.debug.log("SASL failed to initialize: " + ex);
                    throw this._handleFailure(ex);
                }
            } else {
                if (!this.started) {
                    jabberwerx.util.debug.log("SASL mechanism not yet started!");
                    throw this._handleFailure();
                }
                
                switch (input.nodeName) {
                    case "success":
                        try {
                            if (!this.complete) {
                                data = jabberwerx.$(input).text();
                                data = this._decodeData(data);
                                data = this.evaluateChallenge(data);
                            }
                        } catch (ex) {
                            jabberwerx.util.debug.log("SASL failed to evaluate success data: " + ex);
                            throw this._handleFailure(ex);
                        }
                        
                        if (data || !this.complete) {
                            jabberwerx.util.debug.log("SASL failed to complete upon <success/>");
                            throw this._handleFailure();
                        }
                        
                        break;
                    case "failure":
                        // some specific problem
                        {
                            var failure = this._handleFailure(jabberwerx.$(input).children().get(0));
                            jabberwerx.util.debug.log("SASL failure from server: " + failure.message);
                            throw failure;
                        }
                        break;
                    case "challenge":
                        if (this.complete) {
                            jabberwerx.util.debug.log("SASL received challenge after completion!");
                            throw this._handleFailure();
                        }
                        try {
                            data = jabberwerx.$(input).text();
                            data = this._decodeData(data);
                            data = this.evaluateChallenge(data);
                            data = this._encodeData(data);
    
                            output = new jabberwerx.NodeBuilder(
                                            "{urn:ietf:params:xml:ns:xmpp-sasl}response").
                                    text(data).
                                    data;
                        } catch (ex) {
                            jabberwerx.util.debug.log("SASL failed to evaluate challenge data: " + ex);
                            throw this._handleFailure(ex);
                        }
                        break;
                    default:
                        // some random problem!
                        jabberwerx.util.debug.log("unexpected stanza received!");
                        throw this._handleFailure();
                        break;
                }
            }
            
            return output;
        },
        /**
         * @private
         */
        _decodeData: function(data) {
            if (!data) {
                return "";
            }
            if (!this._encoded) {
                return jabberwerx.util.crypto.utf8Decode(jabberwerx.util.crypto.b64Decode(data));
            }
            return data;
        },
        /**
         * @private
         */
        _encodeData: function(data) {
            if (!data) {
                return "";
            }
            if (!this._encoded) {
                return jabberwerx.util.crypto.b64Encode(jabberwerx.util.crypto.utf8Encode(data));
            }
            
            return data;
        },
        
        /**
         * <p>Called by {@link #evaluate} to start use of this SASLMechanism.
         * Subclasses MUST override this method to perform the initial
         * step.</p>
         *
         * <p>This implementation always throws an Error.</p>
         *
         * @returns The initial data to send, or <tt>null</tt> to send
         *          an empty &lt;auth/&gt;
         */
        evaluateStart: function() {
            throw new Error("not implemented!");
        },
        /**
         * <p>Called by {@link #evaluate} to process challenge data into a
         * response. Subclasses MUST override this method to perform
         * the steps of SASL negotation appropriate to the mechanism. If
         * this step completes the negotation (even if part of a
         * &lt;challenge/&gt; instead of a &lt;success/&gt;), this method MUST
         * set the {@link #complete} flag to <tt>true</tt>.</p>
         *
         * <p>The input is the text content of the &lt;challenge/&gt; or
         * &lt;success/&gt;, decoded from base64.</p>
         *
         * <p>This implementation always throws an Error.</p>
         *
         * @param   {String} inb The input data ("" if there is no challenge
         *          data)
         * @returns {String} The output data, or <tt>null</tt> if no response
         *          data is available
         * @throws  {jabberwerx.SASLMechanism.SASLAuthFailure} If there is a
         *          problem evaluating the challenge.
         */
        evaluateChallenge: function(inb) {
            throw new Error("not implemented!");
        },
        /**
         * @private
         */
        _handleFailure: function(cond) {
            this.complete = true;
            if (cond instanceof jabberwerx.SASLMechanism.SASLAuthFailure) {
                return cond;
            } else if (jabberwerx.isElement(cond)) {
                var msg = "{urn:ietf:params:xml:ns:xmpp-sasl}" + cond.nodeName;
                
                return new jabberwerx.SASLMechanism.SASLAuthFailure(msg);
            } else {
                return new jabberwerx.SASLMechanism.SASLAuthFailure();
            }
        },
        
        /**
         * <p>Retrieves the connection/authentication properties from the
         * client.</p>
         *
         * @returns  {Object} The properties object from {@link #client}
         */
        getProperties: function() {
            return (this.client && this.client._connectParams) || {};
        },
        
        /**
         * The client to operate against
         * @type    jabberwerx.Client
         */
        client: null,
        /**
         * The mechanism name. This is automatically set by the init method
         * for jabberwerx.SASLMechanism, based on the mechanism name given when
         * the type is defined.
         * @type    String
         */
        mechanismName: "",
        /**
         * Flag to check to check if this SASLMechanism has completed the
         * authentication negotation (successful or not).
         * @type    Boolean
         */
        complete: false,
        /**
         * Flag to indicate the mechanism has been started.
         * @type    Boolean
         */
        started: false
    }, "jabberwerx.SASLMechanism");
    jabberwerx.SASLMechanism.SASLAuthFailure = jabberwerx.util.Error.extend("{urn:ietf:params:xml:ns:xmpp-sasl}temporary-auth-failure");
    
    /**
     * @private
     */
    jabberwerx.SASLMechanism._baseExtend = jabberwerx.SASLMechanism.extend;
    /**
     * <p>Defines a new subclass of jabberwerx.SASLMechanism. This method
     * overrides {@link JWBase.extend} to also include the name of the
     * SASL mechanism. This method will also register the new mechanism
     * with the global SASLMechanismFactory {@link jabberwerx.sasl}.</p>
     *
     * @param   {Object} props The properties and methods for the subclass
     * @param   {String} type The fully-qualified name of the subclass
     * @param   {String} mechname The SASL mechanism name this subclass
     *          supports
     * @returns {Class} The subclass of jabberwerx.SASLMechanism
     * @throws  {TypeError} If {mechname} is not a non-empty string
     */
    jabberwerx.SASLMechanism.extend = function(props, type, mechname) {
        if (!(mechname && typeof(mechname) == "string")) {
            throw new TypeError("name must be a non-empty string");
        }
        
        var subtype = jabberwerx.SASLMechanism._baseExtend(props, type);
        subtype.mechanismName = mechname.toUpperCase();
        if (    jabberwerx.sasl &&
                jabberwerx.sasl instanceof jabberwerx.SASLMechanismFactory) {
            jabberwerx.sasl.addMechanism(subtype);
        }
        
        return subtype;
    };
    
    jabberwerx.SASLMechanismFactory = jabberwerx.JWBase.extend(/** @lends jabberwerx.SASLMechanismFactory.prototype */{
        /**
         * @class
         * @minimal
         * <p>Factory class for managing and creating SASLMechanisms.</p>
         *
         * @description
         * <p>Creates a new SASLMechanismFactory.</p>
         *
         * @param       {String[]} [mechs] The list of enabled mechanisms
         * @constructs  jabberwerx.SASLMechanismFactory
         * @extends     jabberwerx.JWBase
         */
        init: function(mechs) {
            this._super();
            
            if   (!mechs) {
                if (jabberwerx._config.enabledMechanisms) {
                    mechs = jabberwerx._config.enabledMechanisms;
                } else {
                    mechs = [];
                }
            }
            
            this.mechanisms = mechs.concat();
        },
        
        /**
         * <p>Creates a SASLMechanism appropriate for the given list of
         * possible mechanisms.</p>
         *
         * @param   {jabberwerx.Client} client The client to work against
         * @param   {String[]} mechs The list of possible mechanisms to use
         * @returns {jabberwerx.SASLMechanism} The SASLMechanism object to use, or
         *          <tt>null</tt> if an appropriate mechanism could not be
         *          found
         * @throws  {TypeError} If {client} is not valid; or if {mechs} is not
         *          an array
         */
        createMechanismFor: function(client, mechs) {
            if (!jabberwerx.$.isArray(mechs)) {
                throw new TypeError("mechs must be an array of mechanism names");
            }
            if (!(client && client instanceof jabberwerx.Client)) {
                throw new TypeError("client must be an isntance of jabberwerx.Client");
            }
            
            // normalize mechanism names (all upper case)
            mechs = mechs.concat();
            for (var idx = 0; idx < mechs.length; idx++) {
                mechs[idx] = String(mechs[idx]).toUpperCase();
            }
            
            // find the mech!
            var selected = null;
            for (var idx = 0; !selected && idx < this.mechanisms.length; idx++) {
                var candidate = this._mechsAvail[this.mechanisms[idx]];
                if (!candidate) {
                    // enabled mech doesn't have an available mech
                    continue;
                }
                
                for (var jdx = 0; !selected && jdx < mechs.length; jdx++) {
                    if (mechs[jdx] != candidate.mechanismName) {
                        continue;
                    }
                    
                    // we have an available, enabled, AND supported mechanism!
                    try {
                        selected = new candidate(client);
                    } catch (ex) {
                        jabberwerx.util.debug.log("could not create SASLMechanism for " +
                                candidate.mechanismName + ": "+ ex);
                        // make sure we don't have a SASLMechanism
                        selected = null;
                    }
                }
            }
            return selected;
        },
        
        /**
         * <p>Adds the given mechanism to the map of available mechanisms.</p>
         *
         * @param   {Class} type The SASLMechanism to add
         * @throws  {TypeError} if {type} is not the class for a SASLMechanism
         */
        addMechanism: function(type) {
            if (!(jabberwerx.$.isFunction(type) && type.mechanismName)) {
                throw new TypeError("type must be the constructor for a SASLMechanism type");
            }
            
            this._mechsAvail[type.mechanismName] = type;
        },
        /**
         * <p>Removes the given mechanism from the map of available
         * mechanisms.</p>
         *
         * @param   {Class} type The SASLMechanism to remove
         * @throws  {TypeError} if {type} is not the class for a SASLMechanism
         */
        removeMechanism: function(type) {
            if (!(jabberwerx.$.isFunction(type) && type.mechanismName)) {
                throw new TypeError("type must be the constructor for a SASLMechanism type");
            }
            
            this._mechsAvail[type.mechanismName] = undefined;
            delete this._mechsAvail[type.mechanismName];
        },
        
        /**
         * @private
         * The map of mechanism to SASLMechanism types.
         */
        _mechsAvail: {},
        /**
         * <p>The list of SASL client mechanisms supported. Each element in
         * the array is expected to be the string name of the mechanism.</p>
         *
         * <p>The order of mechanisms in this array is significant. This factory
         * will create an instance of first SASLMechanism that matches, based on the
         * intersection of server-supplied and client-enabled mechanisms.</p>
         *
         * @type    String[]
         */
        mechanisms: []
    }, "jabberwerx.SASLMechanismFactory");
    
    if (!(jabberwerx.sasl && jabberwerx.sasl instanceof jabberwerx.SASLMechanismFactory)) {
        /**
         * The global SASL factory.
         *
         * @memberOf    jabberwerx
         * @see         jabberwerx.SASLMechanismFactory#mechanisms
         * @see         jabberwerx.SASLMechanismFactory#createMechanismFor
         * @see         jabberwerx.SASLMechanismFactory#addMechanism
         * @see         jabberwerx.SASLMechanismFactory#removeMechanism
         * @type        jabberwerx.SASLMechanismFactory
         */
        jabberwerx.sasl = new jabberwerx.SASLMechanismFactory([]);
        jabberwerx.$(document).ready(function() {
            //setup mechanisms from global config
            if (jabberwerx.sasl.mechanisms.length == 0) {
                jabberwerx.sasl.mechanisms = jabberwerx._config.enabledMechanisms.concat();
            }
        });
    }
}
