/**
 * filename:        Server.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {

    jabberwerx.Server = jabberwerx.Entity.extend(/** @lends jabberwerx.Server.prototype */{
        /**
         * @class
         * @minimal
         * <p>Represents the server to connect a {@link jabberwerx.Client} to.</p>
         *
         * @description
         * Creates a new Server with the given domain and owning
         * cache.
         * 
         * @param   {String} serverDomain The domain to connect to
         * @param   {jabberwerx.ClientEntityCache} [cache] The owning cache
         * @constructs jabberwerx.Server
         * @extends jabberwerx.Entity
         */
        init: function(serverDomain, cache) {
            this._super({jid: serverDomain}, cache);
        }
    }, 'jabberwerx.Server');
}
