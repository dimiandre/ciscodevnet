/**
 * filename:        MUCInvite.js
 * created at:      2009/11/10T11:20:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.MUCInvite = jabberwerx.JWModel.extend(/** @lends jabberwerx.MUCInvite.prototype */ {
        /**
         * @class
         * <p>Holds MUC Invite properties.</p>
         *
         * @description
         * Holds MUC Invite properties. MUC invites are described in XEP-0045 and XEP-0249. The room
         * property must be present; all other properties are optional.
         *
         * @param   {jabberwerx.Stanza} stanza The stanza containing the invite
         * @param   {jabberwerx.JID | String} room The room for which the invite is extended
         * @param   {jabberwerx.JID | String} [invitor] The user who extended the invitation
         * @param   {String} [reason] The reason for the invite
         * @param   {String} [password] The password for the room
         * @throws  {TypeError} if any of the parameters are not valid
         * @constructs jabberwerx.MUCInvite
         * @extends jabberwerx.JWModel
         */
        init: function(stanza, room, invitor, reason, password) {            
            if (!stanza || !room) {
                throw new TypeError("stanza and room parameters must be present");
            }
            
            this.setStanza(stanza);
            this.setRoom(room);
            this.setInvitor(invitor);
            this.setReason(reason);
            this.setPassword(password);
        },
      
        /**
         * Retries the stanza property.
         * @returns {jabberwerx.Stanza}
         */
        getStanza: function() {
            return this._stanza || null;
        },

        /**
         * Retrieves the room property.
         * @returns {jabberwerx.JID}
         */
        getRoom: function() {
            return this._room || null;
        },
        
		/**
         * Retrieves the invitor property.
         * @returns {jabberwerx.JID}
         */
        getInvitor: function() {
        	return this._invitor || null;
        },
        
		/**
         * Retrieves the reason property.
         * @returns {String}
         */
        getReason: function() {
            return this._reason || null;
        },
        
		/**
         * Retrieves the reason property.
         * @returns {String}
         */
        getPassword: function() {
            return this._password || null;
        },
        
        /**
         * Sets the stanza property.
         * @param   {jabberwerx.Stanza} stanza The stanza property is set to this
         *          value if not null or empty. 
         * @throws  {TypeError} If {stanza} is not valid
         */
        setStanza: function(stanza) {
            if (stanza) {
                if (stanza instanceof jabberwerx.Stanza) {    
                    this._stanza = stanza;
                } else {
                    throw new TypeError("stanza must be type jabberwerx.Stanza");
                }
            }
        },
        
        /**
         * Sets the room property.
         * @param   {jabberwerx.JID | String} room The room property is set to this
         *          value if not null or empty. 
         * @throws  {TypeError} If {room} is not valid
         */
        setRoom: function(room) {
            if (room) {
                try {
                    this._room = jabberwerx.JID.asJID(room);
                } catch(e) {
                    throw new TypeError(
                            "room must be type jabberwerx.JID or convertible to a jabberwerx.JID");
                }
            }
        },
        
        /**
         * Sets the invitor property.
         * @param   {jabberwerx.JID | String} invitor The invitor property is set to this
         *          value if not null or empty.
         * @throws  {TypeError} If {invitor} is not valid
         */
        setInvitor: function(invitor) {
            if (invitor) {
                try {
                    this._invitor = jabberwerx.JID.asJID(invitor);
                } catch(e) {
                    throw new TypeError(
                        "invitor must be type jabberwerx.JID or convertible to a jabberwerx.JID");
                }
            }
        },
        
        /**
         * Sets the reason property.
         * @param   {jabberwerx.JID | String} reason The reason property is set to this
         *          value if not null or empty.
         * @throws  {TypeError} If {reason} is not valid
         */
        setReason: function(reason) {
            if (reason) {
                if (typeof reason == "string" || reason instanceof String) {
                    this._reason = reason;
                } else {
                    throw new TypeError("reason must be a string");
                }
            }
        },

        /**
         * Sets the password property.
         * @param   {jabberwerx.JID | String} password The password property is set to this
         *          value if not null or empty.
         * @throws  {TypeError} If {password} is not valid
         */
        setPassword: function(password) {
            if (password) {
                if (typeof password == "string" || password instanceof String) {
                    this._password = password;
                } else {
                    throw new TypeError("password must be a string");
                }
            }
        }
    }, "jabberwerx.MUCInvite");
}
