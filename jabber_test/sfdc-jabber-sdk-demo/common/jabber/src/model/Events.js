/**
 * filename:        Events.js
 * created at:      2009/05/11T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    //Adding function() to keep private methods out of global scope.
    (function(){
        /** @private */
        var _jwaNotifierBinding = function(dispatch, name, mode) {
            var key = 'on:' + name.toLowerCase();
            var notifier = dispatch[key];
            
            if (!notifier && (mode == 'create')) {
                notifier = new jabberwerx.EventNotifier(dispatch, name);
                dispatch[key] = notifier;
            } else if (notifier && (mode == 'destroy')) {
                delete dispatch[key];
            }
            
            return notifier;
        };
        /** @private */
        var _jwaDispatchBinding = function(src, name, mode) {
            var dispatch = (src instanceof jabberwerx.EventDispatcher) ?
                    src :
                    src._eventDispatch;
            if (!(dispatch && dispatch instanceof jabberwerx.EventDispatcher)) {
                if (mode != 'create') {
                    return;
                }
                dispatch = new jabberwerx.EventDispatcher(src);
                src._eventDispatch = dispatch;
            }
            
            return _jwaNotifierBinding(dispatch, name, mode);
        };
        
        /**
         * @class
         * @minimal
         * The object representation for an event.
         *
         * @property {String} name The event name
         * @property {jabberwerx.EventNotifier} notifier The triggering notifier
         * @property {Object} source The source of the event
         * @property data Event-specific data
         * @property selected The results of a selection. This value is set if
         *           the callback was registered with a selector, and the selector
         *           matched.
         *
         * @description
         * Constructs a new jabberwerx.EventObject from the given notifier and
         * event data.
         *
         * @param {jabberwerx.EventNotifier} notifier The triggered notifier
         * @param [data] The event data
         * @constructs jabberwerx.EventObject
         * @see jabberwerx.EventNotifier#bindWhen
         */
        jabberwerx.EventObject = function(notifier, data) {
            this.notifier = notifier;
            this.name = notifier.name;
            this.source = notifier.dispatcher.source;
            this.data = data;
            this.selected = undefined;
        };
        
        jabberwerx.EventNotifier = jabberwerx.JWBase.extend(/** @lends jabberwerx.EventNotifier.prototype */{
            /**
             * @class
             * @minimal
             * Manages notifying listeners for a given event name.
             * 
             * @property {jabberwerx.EventDispatcher}  dispatcher The owning dispatcher
             * @property {String} name The event name
             *
             * @description
             * Constructs a new EventNotifier with the given dispatcher and
             * event name.
             *
             * This constructor should not be called directly.  Instead, it is
             * constructed by the {@link jabberwerx.EventDispatcher} as part of its
             * constructor, or internally as needed.
             *
             * @param {jabberwerx.EventDispatcher} dispatcher The owning dispatcher
             * @param {String} name The event name
             * @constructs jabberwerx.EventNotifier
             * @extends JWBase
             */
            init: function(dispatcher, name) {
                this._super();
            
                this.dispatcher = dispatcher;
                this.name = name.toLowerCase();
                
                this._callbacks = [];
            },
            /**
             * Registers the given callback with this EventNotifier.
             *
             * <p>The callback is expected to have the following signature:</p>
             * <pre>callback(evt)</pre>
             *
             * <p>Where {evt} is the {@link jabberwerx.EventObject} for a given
             * triggering. The callback may return any value (other than
             * {undefined}), which will be provided to the source. Any specific
             * requirements around the value returned is specific to the event
             * name and event source.</p>
             *
             * <p>Callbacks are remember by their object reference, and are
             * considered to be unique. Registering the same function multiple
             * times removes any previous registration, and applies {cb} to the
             * current position and with the supplied additional arguments.</p>
             *
             * @param {Function} cb The callback to register or update
             * @throws {TypeError} If {cb} is not a function
             */
            bind: function(cb) {
                this.bindWhen(undefined, cb);
            },
            /**
             * Registers the given callback, filtering via the given selector. The
             * registered callback is only executed if the given selector indicates
             * the data passed to {@link #trigger} matches.
             *
             * <p>A selector may be either undefined, a jQuery selector string or a
             * function. If {selector} is undefined, then this method performs the
             * same registration as {@link #bind}.</p>
             *
             * <p>If {selector} is a string, it is used as the selector in
             * jabberwerx.$(), with data (coerced into a DOM Node) as the
             * context. If there are any results from the jQuery selection,
             * they are added to the event object's 'selected' property, and
             * {cb} is executed. Note that 'selected' is 'unwrapped' if the
             * selection is a single node; otherwise it is an array of the
             * selected nodes.</p>
             *
             * <p>If {selector} is a function, it is passed the event data, and is
             * expected to return a value (that does not evaluate as false) if the
             * data matches. For example, the following selector function would
             * match any events where the data is a Room:</p>
             *
             * <pre class="code">
             * var selector = function(data) {
             *     if (data instanceof jabberwerx.Room) {
             *         return data; //non-null object is "true"
             *     }
             *     return false; //prevents matching
             * }
             * </pre>
             *
             * <p>The result of {selector} is stored in the {@link
             * jabberwerx.EventObject#selected} property.</p>
             *
             * <p>The callback is expected to have the following signature:</p>
             * <pre>callback(evt)</pre>
             *
             * <p>Where {evt} is the {@link jabberwerx.EventObject} for a given
             * triggering. The callback may return any value (other than
             * {undefined}), which will be provided to the source. Any specific
             * requirements around the value returned is specific to the event
             * name and event source.</p>
             *
             * <p>Callbacks are remember by their object reference, and are
             * considered to be unique. Registering the same function multiple
             * times removes any previous registration, and applies {cb} to the
             * current position and with the supplied additional arguments.</p>
             *
             * @param {String|Function|undefined} selector The selector, as either a
             *        jQuery selector string or a function
             * @param {Function} cb The callback to register or update
             * @throws {TypeError} If {cb} is not a function, or if {selector} is
             *         not of the expected types
             */
            bindWhen: function(selector, cb) {
                if (!jabberwerx.$.isFunction(cb)) {
                    new TypeError("callback must be a function");
                }
                
                this.unbind(cb);
                switch (typeof selector) {
                    case 'undefined':
                        //nothing to do
                        break;
                    case 'function':
                        //nothing to do
                        break;
                    case 'string':
                        var filter = selector;
                        selector = function(data, evt) {
                            var node;
                            if (data instanceof jabberwerx.Stanza) {
                                node = data.getDoc();
                            } else {
                                //Hope for the best; although jQuery won't blow up
                                //if data isn't an acceptable context type
                                node = data;
                            }
                            
                            var selected = jabberwerx.$(filter, node);
                            
                            switch (selected.length) {
                                case 0:
                                    //No results: fail
                                    return false;
                                case 1:
                                    //single result: return unwrapped
                                    return selected[0];
                                default:
                                    //any results: return as-is
                                    return selected;
                            }
                            
                            //If we've made it this far, we failed
                            return false;
                        };
                        break;
                    default:
                        throw new TypeError("selector must be undefined or function or string");
                }
                
                this._callbacks.push({
                    'filter': filter,
                    'selector': selector,
                    'cb': cb
                });
            },
            /**
             * Unregisters the given callback from this EventNotifier.
             *
             * @param {Function} cb The callbck to unregister
             */
            unbind: function(cb) {
                this._callbacks = jabberwerx.$.grep(this._callbacks, function(value) {
                    return value['cb'] !== cb;
                });
            },
            /**
             * Fires an event on all registered callbacks, with the given data.
             * This method creates a {@link jabberwerx.EventObject}, then
             * calls all of the registered callbacks. Once all of this notifier's
             * callbacks have been notified, all callbacks registered on {@link
             * jabberwerx#.globalEvents} for this event are notified.
             *
             * @param   [data] data specific to this event triggering
             * @param   {jabberwerx.EventNotifier} [delegated] the notifier to
             *          delegate event triggering to after calling this
             *          notifier's registered callbacks.
             * @throws  {TypeError} If {delegated} is defined and is not an
             *          instance 
             */
            trigger: function(data, delegated) {
                var evt;
                
                if (data instanceof jabberwerx.EventObject) {
                    //this is a delegation
                    evt = data;
                    //substitute notifier to the current one
                    evt.notifier = this;
                } else {
                    //this is an origination
                    evt = new jabberwerx.EventObject(this, data);
                }
        
                var results = [];
                jabberwerx.reduce(this._callbacks, function(item) {
                    var     cb = item['cb'];
                    var     filter = item['selector'];
                    var     retval;
                    
                    if (!cb || !jabberwerx.$.isFunction(cb)) {
                        return;
                    }
                    
                    var selected = undefined;
                    if (filter) {
                        selected = filter(evt.data);
                        if (!selected) { return; }
                    }
        
                    try {
                        //updated on each call, regardless of filtering
                        evt.selected = selected;
                        retval = cb.call(cb, evt);
                    } catch (ex) {
                        /*DEBUG-BEGIN*/
                        jabberwerx.util.debug.error('callback on ' + evt.name + ' failed: ' + ex + " :: "  + (ex.toSource&& ex.toSource()) + " " + (ex.stack || ""));
                        /*DEBUG-END*/
                    }
        
                    if (retval !== undefined) {
                        results.push(retval);
                    }
                });
                
                if (!delegated) {
                    delegated = _jwaNotifierBinding(jabberwerx.globalEvents, this.name);
                } else if (!(delegated instanceof jabberwerx.EventNotifier)) {
                    throw new TypeError("delegated must be a EventNotifier");
                }
                
                if (delegated && delegated !== this) {
                    results = results.concat(delegated.trigger(evt));
                }
                
                return results;
            },
            
            /**
             * Marks this type for inline serialization.
             *
             * @returns {Boolean} always true
             */
            shouldBeSavedWithGraph: function() { return true; },
          
            /**
             * serialize the original string selectors passed to bindWhen.
             * 
             * While callbacks are invocations, the selector functions created in bindWhen are anonymous and
             * are not serialized. Serialize original string as needed, selecgor functions to be recreated after 
             * graph is restored.
             */
            wasUnserialized: function() {
                //rebuild anon selector functions as needed
                var callbacks = this._callbacks;
                callbacks = jabberwerx.$.map(callbacks, function(oneCB, oneKey) {
                    if (jabberwerx.util.isJWInvocation(oneCB.cb)) {
                        var method = oneCB.cb.methodName, target = oneCB.cb.object;
                        if (!(target && method && target[method])) {
                            jabberwerx.util.debug.log("throwing out bad callback: " + target + "[" + method + "]");
                            return null;
                        }
                    }
                    
                    if (oneCB.filter && !oneCB.selector && (typeof oneCB.filter == 'string')) {
                        var oneFilter = oneCB.filter;
                        oneCB.selector = function(data, evt) {
                            var node;
                            if (data instanceof jabberwerx.Stanza) {
                                node = data.getDoc();
                            } else {
                                //Hope for the best; although jQuery won't blow up
                                //if data isn't an acceptable context type
                                node = data;
                            }
                            
                            var selected = jabberwerx.$(oneFilter, node);
                            
                            switch (selected.length) {
                                case 0:
                                    //No results: fail
                                    return false;
                                case 1:
                                    //single result: return unwrapped
                                    return selected[0];
                                default:
                                    //any results: return as-is
                                    return selected;
                            }
                            
                            //If we've made it this far, we failed
                            return false;
                        };
                    }
                
                    return oneCB;
                });
                
                this._callbacks = callbacks;
            }
        }, "jabberwerx.EventNotifier");
        
        jabberwerx.EventDispatcher = jabberwerx.JWBase.extend(/** @lends jabberwerx.EventDispatcher.prototype */{
            /**
             * @class
             * @minimal
             * Manages a collection of events for a given source.
             *
             * <p>Each event for this dispatcher is represented by a
             * {@link jabberwerx.EventNotifier}, as a property of this dispatcher.
             * To access a specific notifier, use the following notation:</p>
             *
             * <pre class="code">dispatcher['on:&lt;name&gt;']</pre>
             *
             * <p>Where &lt;name&gt; is the name of the event (lower case).</p>
             *
             * @property source The source of events
             *
             * @description
             * Constructs new EventDispatcher with the given source.
             *
             * @param {Object} src The source for events
             * @constructs jabberwerx.EventDispatcher
             * @see jabberwerx.JWModel#event
             * @see jabberwerx.JWModel#applyEvent
             */
            init: function(src) {
                this._super();
            
                this.source = src;
                if (src !== jabberwerx && jabberwerx.globalEvents) {
                    this.globalEvents = jabberwerx.globalEvents;
                }
            },
            /**
             * Marks this type for inline serialization.
             *
             * @returns {Boolean} always true
             */
            shouldBeSavedWithGraph: function() { return true; },
            /**
             * Called just after to unserializing. This method removes the global
             * dispatcher {@link jabberwerx.globalEvents} from being a property.
             * 
             */
            wasUnserialized: function() {
                jabberwerx.globalEvents = this.globalEvents;
            }
            
        }, "jabberwerx.EventDispatcher");
        
        jabberwerx.GlobalEventDispatcher = jabberwerx.EventDispatcher.extend(/** @lends jabberwerx.GlobalEventDispatcher.prototype */{
            /**
             * @class
             * @minimal
             * The type for the global event dispatcher, {@link jabberwerx.globalEvents}.
             *
             * @extends jabberwerx.EventDispatcher
             * @description
             *
             * Creates a new GlobalEventsDispatcher
             *
             * @throws {Error} if called after {jabberwerx.globalEvents} is already
             * defined.
             * @constructs jabberwerx.GlobalEventDispatcher
             */
            init: function() {
                this._super(jabberwerx);
                
                if (jabberwerx.globalEvents && jabberwerx.globalEvents !== this) {
                    throw new Error("only one global events dispatcher can exist!");
                }
            },
            
            /**
             * Registers a callback for the given event name. This method behaves
             * just as {@link jabberwerx.EventNotifier#bind}. This function also
             * ensures that a notifier exists for {name}.
             *
             * @param {String} name The event name to register on
             * @param {Function} cb The callback to register or update
             */
            bind: function(name, cb) {
                var notifier = _jwaNotifierBinding(this, name, 'create');
                notifier.bind(cb);
            },
            /**
             * Registers a callback for the given event name. This method behaves
             * just as {@link jabberwerx.EventNotifier#bindWhen}. This function
             * also ensures that a notifier exists for {name}.
             *
             * @param {String} name The event name to register on
             * @param {String|Function|undefined} selector The selector, as either
             *        a jQuery selector string or a function
             * @param {Function} cb The callback to register or update
             */
            bindWhen: function(name, selector, cb) {
                var notifier = _jwaNotifierBinding(this, name, 'create');
                notifier.bindWhen(selector, cb);
            },
            /**
             * Unregisters a callback for the given event name. This method behaves
             * just as {@link jabberwerx.EventNotifier#unbind}.
             *
             * @param {String} name The event name to unregister on
             * @param {Function} cb The callback to unregister
             */
            unbind: function(name, cb) {
                var notifier = _jwaNotifierBinding(this, name);
                
                if (notifier) {
                    notifier.unbind(cb);
                }
            },
            
            /**
             * Prevents this type from inline serialization.
             *
             * @returns {Boolean} always false
             */
            shouldBeSerializedInline: function() { return false; },
            /**
             * Marks this type for general graph saving.
             *
             * @returns {Boolean} always true
             */
            shouldBeSavedWithGraph: function() { return true; },
            /**
             * Called just prior to the object being serialized. This method
             * "forgets" the source, to prevent the global "jabberwerx" namespace
             * from being serialized.
             */
            willBeSerialized: function() {
                this.source = undefined;
            },
            /**
             * Called after the object is deserialized and rehydrated. This method
             * "remembers" the source as the global "jabberwerx" namespace.
             */
            wasUnserialized: function() {
                this.source = jabberwerx;
            }
        }, "jabberwerx.GlobalEventDispatcher");
        
        if (!(  jabberwerx.globalEvents &&
                jabberwerx.globalEvents instanceof jabberwerx.GlobalEventDispatcher)) {
            /**
             * The global event dispatcher. Callbacks registered on this
             * dispatcher are executed when any event of a given name is
             * triggered.
             *
             * <p>The list of all known events are found in
             * <a href="../jabberwerxEvents.html">JabberWerx AJAX Events</a>.</p>
             * 
             * @memberOf jabberwerx
             * @see jabberwerx.GlobalEventDispatcher#bind
             * @see jabberwerx.GlobalEventDispatcher#unbind
             * @type jabberwerx.GlobalEventDispatcher
             */
            jabberwerx.globalEvents = new jabberwerx.GlobalEventDispatcher();
        }
        
        /**
         * Locates the jabberwerx.EventNotifier for the given name.
         *
         * @returns {jabberwerx.EventNotifier} The notifier for {name}, or
         * {null} if not found
         */
        jabberwerx.JWModel.prototype.event = function(name) {
            return _jwaDispatchBinding(this, name);
        };
        /**
         * Establishes the event handling for a given event name. This
         * function ensures that a {@link jabberwerx.EventDispatcher} exists,
         * and that the dispatcher contains a {@link jabberwerx.EventNotifier}
         * for {name}.
         *
         * @param {String} name The event name
         * @returns {jabberwerx.EventNotifier} the notifier for {name}
         */
        jabberwerx.JWModel.prototype.applyEvent = function(name) {
            return _jwaDispatchBinding(this, name, 'create');
        };

    })();
}
