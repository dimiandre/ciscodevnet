/**
 * filename:        QuickContact.js
 * created at:      2009/10/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.cisco) {    
    jabberwerx.cisco.QuickContact = jabberwerx.Contact.extend(/** @lends jabberwerx.cisco.QuickContact.prototype */ {
        /**
         * @class
         * <p>QuickContact object which is temporary subscription contact. This object SHOULD NOT
         * be created directly. Instead, a subscription to a contact should be initiated using
         * {@link jabberwerx.cisco.QuickContactController#subscribe}. Upon receiving a
         * presence update for this temporarily subscribed to contact, a new QuickContact object
         * for this contact will be created.</p>
         *
         * @description
         * <p>Creates a new QuickContact with the given jid and QuickContacController.</p>
         *
         * @param   {jabberwerx.JID|String} jid A jid corresponding to the QuickContact been created.
         * @param   {jabberwerx.cisco.QuickContactController} quickContactCtrl The creating
         *          QuickContactController
         * @throws  {TypeError} If {quickContactCtrl} is not a valid QuickContactController
         * @constructs jabberwerx.cisco.QuickContact
         * @extends jabberwerx.Contact
         */
        init: function(jid, quickContactCtrl) {
            if (!(quickContactCtrl &&
                  quickContactCtrl instanceof jabberwerx.cisco.QuickContactController)) {
                throw new TypeError("quickContactCtrl must be provided and must be of type" +
                                    "jabberwerx.cisco.QuickContactController");
            }
            this._super(jid, quickContactCtrl);
            
            this.properties.temp_sub = true;
        },

        /**
         * @private
         * Implementation of the empty jabberwerx.Entity._handleUnavailable method.
         */
        _handleUnavailable: function(presence) {
            var pres = this.getPrimaryPresence();
            if (!pres) {
                this._insertPresence(presence);
            } else if (pres.getType() == "unavailable") {
                this._clearPresenceList();
                this._insertPresence(presence);
            }
        }
    }, 'jabberwerx.cisco.QuickContact');
}
