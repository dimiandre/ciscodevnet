/**
 * filename:    Stream.js
 * created at:  2009/12/01T00:00:00-07:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {
    jabberwerx.Stream = jabberwerx.JWModel.extend(/** @lends jabberwerx.Stream.prototype */{
        /**
         * @private
         * @class
         * <p>Manages a stream of elements into and out of a server. This
         * class provides the BOSH layer implementation.</p>
         *
         * @description
         * <p>Creates a new jabberwerx.Stream.</p>
         *
         * @extends JWModel
         * @constructs jabberwerx.Stream
         */
        init: function() {
            this.applyEvent("streamOpened");
            this.applyEvent("streamClosed");
            this.applyEvent("streamElementsReceived");
            this.applyEvent("streamElementsSent");

            // initialize the queues
            this._recvQ = new jabberwerx.Stream.Queue();
            this._sendQ = new jabberwerx.Stream.Queue();
            this._xhrs = new jabberwerx.Stream.Queue();

            // setup values for all XMLHttpRequests
            this._xhrSetup = {
                cache: false,
                xhr: this.invocation("_createXHR", jabberwerx.$.ajaxSettings.xhr),
                complete: this.invocation("_handleXHR"),
                contentType: "text/xml",
                dataType: "text",
                global: false,
                processData: false,
                type: "POST"
            };
        },

        /**
         * @private
         * <p>Retrieves the properties of this Stream. This method
         * returns a snapshot of the properties at the time it is called;
         * updates to the returned properties are not reflected into this
         * Stream, nor vice-versa.</p>
         *
         * <p>The specifics of the returned properties object are implemention
         * dependent.</p>
         *
         * @return  {Object} A hashtable of the current stream properties
         */
        getProperties: function() {
            return jabberwerx.$.extend(true, {}, this._boshProps);
        },

        /**
         * <p>Determines if this stream is already open.</p>
         *
         * @return  {Boolean} <tt>true</tt> if open.
         */
        isOpen: function() {
            return this._opened;
        },

        /**
         * <p>Determines if this stream is connected in a secure or trusted
         * manner.</p>
         *
         * @return  {Boolean} <tt>true</tt> If the stream is considered secure
         */
        isSecure: function() {
            return this._boshProps.secure || false;
        },

        /**
         * <p>Retrieves the domain for this stream.</p>
         *
         * @return  {String} The domain, or <tt>null</tt> if not known.
         */
        getDomain: function() {
            return this._boshProps.domain || null;
        },
        /**
         * <p>Retrieves the session ID for this stream. For BOSH,
         * this value is the "sid".</p>
         *
         * @return  {String} The current session ID, or <tt>null</tt> if not
         *          known
         */
        getSessionID: function() {
            return this._boshProps.sid || null;
        },
        /**
         * <p>Retrieves the timeout for this stream.</p>
         *
         * @return  {Number} The timeout in seconds
         */
        getTimeout: function() {
            return this._boshProps.timeout || 60;
        },

        /**
         * <p>Connects this stream to the remote endpoint. This method
         * prepares the BOSH request queue, and sends the initial
         * &lt;body/&gt;.</p>
         *
         * <p>The value of {params} is expected to be an object containing
         * the following properties:</p
         * <pre class="code">{
         *      // domain of remote service (REQUIRED)
         *      domain: "example.com",
         *      // URL to connect to remote service (OPTIONAL)
         *      httpBindingURL: "/httpbinding",
         *      // preferred connection/request timeout in seconds (OPTIONAL)
         *      timeout: 30
         * }</pre>
         *
         * @param   {Object} params The connection parameters
         * @throws  {TypeError} if {params} does not contain a valid domain
         * @throws  {jabberwerx.Stream.AlreadyOpenError} If this stream is already
         *          open
         */
        open: function(params) {
            if (this.isOpen()) {
                throw new jabberwerx.Stream.AlreadyOpenError();
            }

            // make sure we're cleared out...
            this._reset();

            // copy and validate/default
            this._boshProps = jabberwerx.$.extend({}, params || {});
            if (!this._boshProps.domain) {
                throw new TypeError("domain must be specified");
            }
            if (!this._boshProps.timeout) {
                this._boshProps.timeout = jabberwerx.Stream.DEFAULT_TIMEOUT;
            }
            if (!this._boshProps.wait) {
                this._boshProps.wait = jabberwerx.Stream.DEFAULT_WAIT;
            }

            // setup binding URL
            var url = /^(?:(?:([0-9a-zA-Z]+\:)\/\/)?(?:([0-9a-zA-Z\.\-]+)(?:\:([0-9]+))?))?(?:\/(.*))?$/.
                      exec(this._boshProps.httpBindingURL || "");
            if (!url) {
                throw new TypeError("httpBindingURL not specified correctly");
            }

            // setup protocol
            if (!url[1]) {
                url[1] = document.location.protocol;
            }

            var domain = "";
            // setup host
            if (!url[2]) {
                url[2] = document.location.host;
            }else {
            	if (!jabberwerx.util.validate.isIPAddress(url[2])) {
            	    var index = url[2].indexOf('.');
            	    domain = (index != -1) ? url[2].substring(index):""; //e.g ".sample.com"
            	}
            	if (this._connectToNode) {
            		url[2] = (jabberwerx.util.validate.isIPAddress(this._connectToNode)||this._connectToNode.indexOf(domain)>0) ? this._connectToNode:(this._connectToNode + domain);
            	} else if (this._boshProps.homeNode) {
            		url[2] = (jabberwerx.util.validate.isIPAddress(this._boshProps.homeNode)||this._boshProps.homeNode.indexOf(domain)>0) ? this._boshProps.homeNode:(this._boshProps.homeNode + domain);
            	}
            }

            // setup port
            if (!url[3]) {
                url[3] = "";
            }

            // setup path
            if (!url[4]) {
                url[4] = "";
            }
            /*If the client is served from https://myproxyserver:1234/sampleclient.html
               and the bindingURL is "/httpbinding"
               the documnet.location.host will be "myproxyserver:1234"
               and the url[3] will be "".
               so it is true for (document.location.port != url[3])
               this is wrong. To fix this, we now do comparision between document.location.port 
               vs  url[2].split(":")[1]||url[3]
             */
            var port = url[2].split(":")[1]||url[3];
            this._boshProps.secure = this._boshProps.secure || (url[1] == "https:");
            this._boshProps.crossSite = (document.location.protocol != url[1]) ||
                                        (document.location.host != url[2]) ||
                                        (document.location.port != port); 
            //update _connectToNode
            if (this._boshProps.crossSite) {
            	this._connectToNode = url[2];
            	jabberwerx.util.debug.log("Cross site is true.");
             } else {
            	//when using proxy, the format of httpbindingURL is "/server"
            	url[4] = this._connectToNode ||this._boshProps.homeNode||url[4];
            	this._connectToNode = url[4];
            }
            
            jabberwerx.util.debug.log("Update _connectToNode = " + this._connectToNode);
            this._boshProps.httpBindingURL = url[1] + "//" +
                                             url[2] +
                                             (url[3] ? ":" + url[3] : "") + "/" +
                                             url[4];
            jabberwerx.util.debug.log("Bosh URL is: " + this._boshProps.httpBindingURL);
            
            this._boshProps.networkAttempts = 0;

            this._boshProps.operation = "open";
            this._sendRequest();

            this._boshProps.heartbeat = setInterval(
                    this.invocation("_heartbeat"),
                    jabberwerx.Stream.HEARTBEAT_INTERVAL);
        },
        /**
         * <p>Reopens this Stream. this method sends the stream restart request
         * to the server, and awaits a proper response.</p>
         *
         * <p>If the server does not immediately respond to the restart with a
         * new stream:features, his method generates a &lt;stream:features/&gt;
         * containing &lt;bind/&gt; and &lt;session/&gt;.</p>
         *
         * @throws  {jabberwerx.Stream.NotOpenError} If this Stream is not
         *          currently open.
         */
        reopen: function() {
            if (!this.isOpen()) {
                throw new jabberwerx.Stream.NotOpenError();
            }

            // do a timeout in case the server doesn't actually support
            // xmpp:restart=true
            this._boshProps.opening = setTimeout(
                    this.invocation("_handleOpen"),
                    2000);
            // hard-coded with a reasonably acceptable delay for now
            // This timeout will be removed when all supported server
            // platforms implement XEP-206 version 1.2
            this._boshProps.operation = "reopen";
            this._sendRequest({restart: true});
        },
        /**
         * <p>Disconnects this stream from the remote endpoint. This method
         * signals to the server to terminate the HTTP session.</p>
         */
        close: function() {
            if (this.isOpen()) {
                this._sendRequest({type: "terminate"});
            } else {
                this._reset();
            }
        },

        /**
         * <p>Sends the given element to the remote endpoint. This method
         * enques the element to send, which gets processed during the
         * heartbeat.</p>
         *
         * @param   {Element} elem The element to send
         * @throws  {TypeError} is {elem} is not a DOM element
         * @throws  {jabberwerx.Stream.NotOpenError} If this Stream is not open
         */
        send: function(elem) {
            if (!jabberwerx.isElement(elem)) {
                throw new TypeError("elem must be a DOM element");
            }

            if (!this.isOpen()) {
                throw new jabberwerx.Stream.NotOpenError();
            }

            this._sendQ.enque(elem);
        },

        /**
         * @private
         */
        _sendRequest: function(props, data) {
            props = jabberwerx.$.extend({}, this._boshProps, props);
            data = data || [];

            var rid = 0, body;
            var resend = false;
            if (props.resend) {
                rid = props.resend.id;
                body = jabberwerx.util.unserializeXML(props.resend.body);
                resend = true;

                data = jabberwerx.$(body).children();

                props.rid = rid + 1;
                delete props.resend;
            } else {
                if (this._xhrs.size() > 1 && data.length) {
                    this._sendQ.enque(data);
                    return;
                }

                if (!props.rid) {
                    // make sure initial RID is "large"
                    var initial;
                    initial = Math.floor(Math.random() * 4294967296);
                    initial = (initial <= 32768) ?
                              initial + 32768 :
                              initial;
                    props.rid = initial;
                } else if (this._boshProps.rid >= 9007199254740991) {
                    // make sure RID does not exceed limit!
                    var err = new jabberwerx.Stream.ErrorInfo(
                            "{urn:ietf:params:xml:ns:xmpp-streams}policy-violation",
                            "BOSH maximum rid exceeded");
                    this._handleClose(er.getNode());
                    return;
                }
                rid = props.rid++;

                body = new jabberwerx.NodeBuilder("{http://jabber.org/protocol/httpbind}body");
                if (props.type) {
                    body.attribute("type", props.type);
                }

                if (!props.sid) {
                    if (data.length) {
                        // enque until session is established...
                        this._sendQ.enque(data);
                        data = [];
                    }

                    body.attribute("xmlns:xmpp", "urn:xmpp:xbosh").
                         attribute("hold", "1").
                         attribute("ver", "1.9").
                         attribute("from", props.jid).
                         attribute("to", props.domain).
                         attribute("secure", props.secure).
                         attribute("wait", props.wait || 30).
                         attribute("{urn:xmpp:xbosh}xmpp:version", "1.0");
                } else {
                    body.attribute("sid", props.sid);
                    if (props.restart) {
                        body.attribute("{urn:xmpp:xbosh}xmpp:restart", "true").
                             attribute("to", props.domain);
                        this._boshProps.restart = true;
                    }
                }
                body.attribute("rid", rid);

                if (data.length) {
                    for (var idx = 0; idx < data.length; idx++) {
                        body.node(data[idx]);
                    }
                }
                body = body.data;
            }

            if (!props.requests) {
                props.requests = new jabberwerx.Stream.Queue();
            }
            props.requests.enque({
                id: rid,
                body: body.xml
            });
            var setup = {
                async: true,
                data: props.requests.tail().body,
                timeout: props.wait * 1000 + 5000,
                url: props.httpBindingURL
            };

            setup = jabberwerx.$.extend(setup, this._xhrSetup);
            if (this._boshProps) {
                this._boshProps = props;
            }
            if (!resend && data.length) {
                this.event("streamElementsSent").trigger(jabberwerx.$(data));
            }
            var xhr = jabberwerx.$.ajax(setup);
            this._xhrs.enque(xhr);
        },
        /**
         * @private
         */
        _createXHR: function(xhrFn) {
            xhrFn = jabberwerx.$.ajaxSettings.xhr;
            var xhr = null;

            if (this._boshProps.crossSite && typeof(XDomainRequest) != "undefined") {
                var that = this;
                var done = this._boshProps.type == "terminate";
                var xdr = new XDomainRequest();

                // Enough of a XMLHttpRequest-like object for $.ajax
                // to function; proxy to XDomainRequest where we can,
                // no-op where we can't
                var xhr = {
                    readyState: 0,
                    abort: function() {
                        xdr.abort();
                        this.readyState = 0;
                    },
                    open: function() {
                        xdr.open.apply(xdr, arguments);
                        this.readyState = 1;
                        this.onreadystatechange && this.onreadystatechange.call(this);
                        this.async = arguments[2] || true;
                    },
                    send: function() {
                        this.readyState = 2;
                        this.onreadystatechange && this.onreadystatechange.call(this);
                        xdr.send.apply(xdr, arguments);
                    },
                    setRequestHeader: function() {
                        // NOOP
                    },
                    getResponseHeader: function() {
                        // NOOP
                    }
                };

                /**
                 * @private
                 */
                var onreadyCB = function(status) {
                    xhr.onreadystatechange && xhr.onreadystatechange.call(this, status);
                };

                // XDomainRequest callbacks - map back to onreadystatechange
                xdr.onload = function() {
                    xhr.responseText = xdr.responseText;
                    xhr.status = 200;
                    xhr.readyState = 4;
                    onreadyCB();
                };
                xdr.onprogress = function() {
                    xhr.readyState = 3;
                    onreadyCB();
                };
                xdr.onerror = function() {
                    xhr.readyState = 0;
                    xhr.status = 500;   // some sort of server error
                    onreadyCB("error");
                };
                xdr.ontimeout = function() {
                    xhr.readyState = 0;
                    xhr.status = 408;   // timeout
                    onreadyCB("timeout");
                };
            } else {
                xhr = xhrFn();
            }

            return xhr;
        },

        /**
         * @private
         */
        _heartbeat: function() {
            var elems = this._recvQ.empty();
            if (elems.length) {
                elems = jabberwerx.$(elems);
                this.event("streamElementsReceived").trigger(elems);
            }

            if (!this.isOpen() && !this._boshProps.operation) {
                return;
            }
            if (!this._sendQ.size() && this._xhrs.size()) {
                return;
            }

            this._sendRequest({}, this._sendQ.empty());
        },
        /**
         * @private
         */
        _handleXHR: function(xhr, status) {
            // check for dehydration
            if (this._dehydrated) {
                return;
            }
            this._xhrs.deque(xhr);

            var failFn = function(err, resend) {
                var boshProps = this._boshProps;
                if (!boshProps) {
                    // no BOSH props, nothing to do
                    return;
                }
                if (boshProps.type == "terminate") {
                    // should be finished, nothing to do
                    this._handleClose();
                    return;
                }
                if (boshProps.networkAttempts++ < 3) {
                    jabberwerx.util.debug.log("network timeout retry " +
                            boshProps.networkAttempts);
                    if (resend) {
                        resend = boshProps.requests.deque();
                    }
                    if (resend) {
                        boshProps.resend = resend;
                    }

                    // wait for normal heartbeat
                    return;
                }

                // getting this far means we really did fail
                this._handleClose(err && err.getNode());
            };

            if (status != "success") {
                // network level error!
                var err;

                switch (status) {
                    case "timeout":
                        // server unresponsive...
                        err = jabberwerx.Stream.ERR_REMOTE_SERVER_TIMEOUT;
                        break;
                    case "error":
                        // http error...
                        err = jabberwerx.Stream.ERR_SERVICE_UNAVAILABLE;
                        break;
                    case "parseerror":
                        // not XML...
                        err = jabberwerx.Stream.ERR_XML_NOT_WELL_FORMED;
                        break;
                    default:
                        // really bad...
                        err = jabberwerx.Stream.ERR_UNDEFINED_CONDITION;
                        break;
                }

                failFn.call(this, err, true);
                return;
            }

            // parse the response
            var dom = xhr.responseText;
            if (!dom) {
                // no data == malformed XML
                failFn.call(this, jabberwerx.Stream.ERR_XML_NOT_WELL_FORMED);
                return;
            }
            try {
                dom = jabberwerx.util.unserializeXML(dom);
            } catch (ex) {
                //parse error
                failFn.call(this, jabberwerx.Stream.ERR_XML_NOT_WELL_FORMED);
                return;
            }
            if (!dom) {
                // no data == malformed XML
                failFn.call(this, jabberwerx.Stream.ERR_XML_NOT_WELL_FORMED);
                return;
            }
            dom = jabberwerx.$(dom);
            if (!dom.is("body[xmlns='http://jabber.org/protocol/httpbind']")) {
                failFn.call(this, jabberwerx.Stream.ERR_SERVICE_UNAVAILABLE);
                return;
            }
            this._boshProps.networkAttempts = 0;

            if (this._boshProps && this._boshProps.requests) {
                this._boshProps.requests.deque();
            }

            var content = dom.children();
            if (!this._boshProps.sid) {
                // expecting an initial BOSH body
                var attr;

                attr = dom.attr("sid");
                if (attr) {
                    this._boshProps.sid = attr;
                }

                attr = dom.attr("wait");
                if (attr) {
                    this._boshProps.wait = parseInt(attr);
                }

                attr = dom.attr("inactivity");
                if (attr) {
                    this._boshProps.timeout = parseInt(attr);
                }
            } else if (!this._boshProps.restart && dom.attr("ack")) {
                var rid = this._boshProps.rid - 1;      // adjust for auto increment
                var ack = parseInt(dom.attr("ack"));
                var resend = this._boshProps.requests.find(function(item) {
                    return (item.id == ack) ? item : null;
                });

                var err;
                // ignore ack for current or next rid
                if (rid != ack && (rid + 1) != ack) {
                    if (resend) {
                        jabberwerx.util.debug.log("resending unacknowledged rid " + ack);
                        this._boshProps.requests.deque(resend);
                        this._sendRequest({resend: resend});
                    } else {
                        jabberwerx.util.debug.log("acknowledged rid " + ack +
                                                  " outside accepted window (" +
                                                  rid + ".." + (rid + 1) + ")!");
                        this._handleClose(jabberwerx.Stream.ERR_REMOTE_CONNECTION_FAILED.getNode());
                        return;
                    }
                }
            }

            if (content.length) {
                var feats = null, err = null;

                // filter features and error
                content = content.map(function() {
                    switch (this.nodeName) {
                        case "stream:features":
                            feats = this;
                            break;
                        case "stream:error":
                            err = this;
                            break;
                        default:
                            // retain "sent"
                            return this;
                    }

                    return null;
                });

                if (feats) {
                    // report open, but continue...
                    this._handleOpen(feats);
                }

                if (content.length) {
                    this._recvQ.enque(content.get());
                }

                if (err) {
                    // close and report
                    this._handleClose(err);
                    return;
                }
            }

            var err;
            switch (dom.attr("type") || this._boshProps.type) {
                case "terminate":
                    // should be closed now...
                    if (!this._boshProps.type) {
                        // server-side terminate, probably has an error...
                        switch (dom.attr("condition") || "") {
                            case "":
                                //no error...
                                err = null;
                                break;
                            case "bad-request":
                                err = jabberwerx.Stream.ERR_BAD_REQUEST;
                                break;
                            case "host-gone":
                                err = jabberwerx.Stream.ERR_SERVICE_UNAVAILABLE;
                                break;
                            case "other-request":
                                err = jabberwerx.Stream.ERR_CONFLICT;
                                break;
                            case "policy-violation":
                                err = jabberwerx.Stream.ERR_POLICY_VIOLATION;
                                break;
                            case "system-shutdown":
                                err = jabberwerx.Stream.ERR_SYSTEM_SHUTDOWN;
                                break;
                            default:
                                // blanket error...
                                err = jabberwerx.Stream.ERR_UNDEFINED_CONDITION;
                                break;
                        }
                    }
                    this._handleClose(err && err.getNode());
                    return;
                case "error":
                    break;
            }
        },
        /**
         * @private
         */
        _handleOpen: function(feats) {
            if (this._boshProps.opening) {
                // cancel any pending timeout
                clearTimeout(this._boshProps.opening);
                delete this._boshProps.opening;
            }

            // assume we need to clear the restart flag
            delete this._boshProps.restart;
            // assume the operation completed
            delete this._boshProps.operation;

            if (!jabberwerx.isElement(feats)) {
                // called from timeout...
                // fake stream:features with bind & session-start
                feats = new jabberwerx.NodeBuilder("{http://etherx.jabber.org/streams}stream:features");
                feats.element("{urn:ietf:params:xml:ns:xmpp-bind}bind");
                feats.element("{urn:ietf:params:xml:ns:xmpp-session}session");
                feats = feats.data;
            }

            // flag and report open
            var that = this;
            setTimeout(function() {
                that._opened = true;
                that.event("streamOpened").trigger(feats);
            }, 1);
        },
        /**
         * @private
         * Called to handle the stream closing. This method may be called
         * multiple times, as each pending request completes.
         */
        _handleClose: function(err) {
            // Check to see if we were open before...
            var open = this.isOpen();
            var oper = this._boshProps.operation;

            // clean up (marking stream as !open)
            this._reset();
            if (open || oper) {

                // flag and report open
                var that = this;
                setTimeout(function() {
                    // trigger event if initially open
                    that.event("streamClosed").trigger(err);
                }, 10);
            }
        },

        /**
         * @private
         */
        _reset: function() {
            // cancel heartbeat
            clearInterval(this._boshProps.heartbeat);

            // reset state
            this._opened = false;
            this._boshProps = {};
            this._sendQ.empty();
            this._xhrs.empty();
        },

        /**
         * <p>Called just prior to this stream being serialized. This method
         * converts any pending elements into their XML equivalents, and aborts
         * all pending BOSH requests.</p>
         */
        willBeSerialized: function() {
            this._dehydrated = true;

            if (this._boshProps && this._boshProps.networkAttempts) {
                // browsers occassionally kill a connection just before dehydration
                this._boshProps.networkAttempts--;
            }
            if (this.isOpen()) {
                //pause!
                clearInterval(this._boshProps.heartbeat);
            }

            var elems;

            // convert all queued sent elements to XML
            elems = this._sendQ.empty();
            elems = jabberwerx.$.map(elems, function() {
                return this.xml;
            });
            this._sendQ.enque(elems);

            // convert all queued received elements to XML
            elems = this._recvQ.empty();
            elems = jabberwerx.$.map(elems, function() {
                return this.xml;
            });
            this._recvQ.empty();

            // forget all pending requests
            delete this._xhrs;
        },
        /**
         * <p>Called just after this stream is unserialized. This method
         * converts any pending XML into their DOM equivalents, and
         * resumes the BOSH request loop if the stream was previously
         * opened.</p>
         */
        wasUnserialized: function() {
            // re-initialize request queue
            this._xhrs = new jabberwerx.Stream.Queue();

            // convert all queued XML to elements
            var elems;
            elems = this._sendQ.empty();
            elems = jabberwerx.$.map(elems, function() {
                return jabberwerx.util.unserializeXML(String(this));
            });
            this._sendQ.enque(elems);

            delete this._dehydrated;

            // TODO: should be moved to "graphUnserialized"??
            if (this.isOpen()) {
                // resume!
                this._boshProps.resend = this._boshProps.requests.deque();
                this._boshProps.heartbeat = setInterval(
                        this.invocation("_heartbeat"),
                        jabberwerx.Stream.HEARTBEAT_INTERVAL);
            }
        },

        /**
         * @private
         */
        _opened: false,
        /**
         * @private
         */
        _boshProps: {},
        /**
         * @private
         */
        _xhrSetup: {},
        /**
         * @private
         */
        _xhrs: null,
        /**
         * @private
         */
        _sendQ: null,
        /**
         * @private
         */
        _recvQ: null,
        /**
         * @ private
         * The node we are connecting to
         * @type String
         */ 
       _connectToNode: null
    }, "jabberwerx.Stream");

    jabberwerx.Stream.Queue = jabberwerx.JWModel.extend(/** @lends jabberwerx.Stream.Queue.prototype */{
        /**
         * @private
         * @class
         * <p>Implementation of a data queue. This class provides convenience
         * methods around a JavaScript array object to better support enquing
         * and dequing items.</p>
         *
         * @description
         * <p>Creates a new Queue.</p>
         *
         * @constructs  jabberwerx.Stream.Queue
         * @extends     JWModel
         */
        init: function() {
            this._super();
        },

        /**
         * Retrieves the first item in this queue, if any.
         *
         * @returns  The first item, or <tt>null</tt> if empty
         */
        head: function() {
            return this._q || null;
        },
        /**
         * Retrieves the last item in this queue, if any.
         *
         * @returns  The last item, or <tt>null</tt> if empty
         */
        tail: function() {
            return this._q[this._q.length - 1] || null;
        },

        /**
         * <p>Adds one or more items to the end of this queue. This method
         * supports a variable number of arguments; each argument provided
         * is appended to the queue.</p>
         *
         * <p>If {item} (or subsequent arguments) is an Array, the elements
         * in that array are appended directly. If an actually array needs
         * to be appended, it must be wrapped with another array:</p>
         *
         * <pre class="code">queue.enque([ anotherArray ]);</pre>
         *
         * @parm    item The item to add to this Queue
         * @returns  {Number} The size of this Queue
         */
        enque: function(item) {
            // NOTE: item is treated as a placeholder
            for (var idx = 0; idx < arguments.length; idx++) {
                item = arguments[idx];
                var tmp = [this._q.length, 0].concat(item);
                this._q.splice.apply(this._q, tmp);
            }

            return this.size();
        },
        /**
         * <p>Removes an item from this queue. If {item} is provided,
         * this method attempts to find and remove it from this Queue.</p>
         *
         * @param   [item] The item to remove from this Queue
         * returns   The removed item, or <tt>null</tt> if this Queue is
         *          unchanged.
         */
        deque: function(item) {
            if (item !== undefined) {
                var idx = jabberwerx.$.inArray(item, this._q);
                if (idx != -1) {
                    this._q.splice(idx, 1);
                } else {
                    item = null;
                }
            } else {
                item = this._q.shift() || null;
            }

            return item;
        },
        /**
         * <p>Locates the first matching item within this Queue.</p>
         *
         * <p>The signature of {cmp} must be the following:</p>
         * <pre class='code'>
         * var cmp = function(item) {
         *      // the current item to determine matches
         *      item;
         *      // return the item that matches
         * }
         * </pre>
         *
         * @param   {Function} cmp The comparison function
         * @returns The matching item, or <tt>null</tt> if none
         */
        find: function(cmp) {
            if (!jabberwerx.$.isFunction(cmp)) {
                throw new TypeError("comparator must be a function");
            }
            var result = null;
            jabberwerx.$.each(this._q, function() {
                result = cmp(this);
                return (result !== undefined);
            });

            return result;
        },
        /**
         * <p>Removes all items from this Queue.</p>
         *
         * @return  {Array} The previous contents of this Queue
         */
        empty: function() {
            var q = this._q;
            this._q = [];

            return q;
        },

        /**
         * <p>Retrieves the current size of this Queue.</p>
         *
         * @return  {Number} The size
         */
        size: function() {
            return this._q.length;
        },

        /**
         * @private
         */
        _q: []
    }, "jabberwerx.Stream.Queue");

    /**
     * @private
     * The default session timeout in seconds (5 minutes).
     *
     * @type    Number
     */
    jabberwerx.Stream.DEFAULT_TIMEOUT = 300;
    /**
     * @private
     * The default polling wait in seconds (30 seconds).
     *
     * @type    Number
     */
    jabberwerx.Stream.DEFAULT_WAIT = 30;

    /**
     * @private
     * The heartbeat interval in milliseconds (10 milliseconds)
     */
    jabberwerx.Stream.HEARTBEAT_INTERVAL = 10;

    /**
     * @class jabberwerx.Stream.NotOpenError
     * @minimal
     * <p>Error thrown if the Stream must be open for the method to
     * complete.</p>
     * @description
     * <p>Creates a new NotOpenError with the given message.</p>
     * @constructs jabberwerx.Stream.NotOpenError
     * @extends jabberwerx.util.Error
     */
    jabberwerx.Stream.NotOpenError = jabberwerx.util.Error.extend("Stream not open");
    /**
     * @class jabberwerx.Stream.AlreadyOpenError
     * @minimal
     * <p>Error thrown if the Stream must NOT be open for the method to
     * complete.</p>
     * @description
     * <p>Creates a new AlreadyOpenError with the given message.</p>
     * @constructs jabberwerx.Stream.AlreadyOpenError
     * @extends jabberwerx.util.Error
     */
    jabberwerx.Stream.AlreadyOpenError = jabberwerx.util.Error.extend("Stream is already open");

    jabberwerx.Stream.ErrorInfo = jabberwerx.JWModel.extend(/** @lends jabberwerx.Stream.ErrorInfo.prototype */{
        /**
         * @class
         * @minimal
         * <p>Representation of stream error information.</p>
         *
         * @description
         * <p>Creates a new ErrorInfo with the given information.</p>
         * @extends JWModel
         * @constructs jabberwerx.Stream.ErrorInfo
         * @param   {String} [cond] The error condition
         * @param   {String} [text] The error text description
         */
        init: function(cond, text) {
           this._super();

           this.condition = cond || "{urn:ietf:params:xml:ns:xmpp-streams}undefined-condition";
           this.text = text || "";

           this.toString = this._toErrString;
        },

        /**
         * <p>Retrieves the element for this ErrorInfo. The returned element
         * is as follows:</p>
         *
         * <pre class="code">
         *  &lt;stream:error xmlns:stream="http://etherx.jabber.org/streams"&gt;
         *      &lt;{condition-local-name} xmlns="urn:ietf:params:xml:ns:xmpp-streams"/&gt;
         *      &lt;text xmlns="urn:ietf:params:xml:ns:xmpp-streams"&gt;{text}&lt;/text&gt;
         *  &lt;/error&gt;
         * </pre>
         *
         * @returns  {Element} The DOM representation
         */
        getNode: function() {
            var builder = new jabberwerx.NodeBuilder("{http://etherx.jabber.org/streams}stream:error");
            builder.element(this.condition);
            if (this.text) {
                builder.element("{urn:ietf:params:xml:ns:xmpp-streams}text").
                        text(this.text);
            }

            return builder.data;
        },

        /**
         * <p>Called after this object is rehydrated. This method sets the toString
         * method as expected.</p>
         */
        wasUnserialized: function() {
            // IE work-around
            this.toString = this._toErrString;
        },

        /**
         * @private
         */
        _toErrString: function() {
            return this.condition;
        },

        /**
         * <p>The condition of this ErrorInfo.</p>
         * @type   String
         */
        condition: "",
        /**
         * <p>The descriptive text for this ErrorInfo.</p>
         * @type   String
         */
        text: ""
    }, "jabberwerx.Stream.ErrorInfo");

    /**
     * <p>Creates a ErrorInfo based on the given node.</p>
     *
     * @param   {Element} node The XML &lt;error/&gt;
     * @returns  {jabberwerx.Stream.ErrorInfo} The ErrorInfo
     * @throws  {TypeError} If {node} is not an element
     */
    jabberwerx.Stream.ErrorInfo.createWithNode = function(node) {
        if (!jabberwerx.isElement(node)) {
            throw new TypeError("node must be an Element");
        }
        node = jabberwerx.$(node);
        var cond = node.
                children("[xmlns=urn:ietf:params:xml:ns:xmpp-streams]:not(text)").
                map(function() {
                    return "{urn:ietf:params:xml:ns:xmpp-streams}" + this.nodeName;
                }).get(0);
        var text = node.
                children("text[xmlns=urn:ietf:params:xml:ns:xmpp-streams]").
                text();

        // TODO: search for known errors first?
        return new jabberwerx.Stream.ErrorInfo(cond, text);
    };

    /**
     * <p>ErrorInfo for a bad request error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_BAD_REQUEST = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}bad-request");
    /**
     * <p>ErrorInfo for a conflict error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_CONFLICT = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}conflict");
    /**
     * <p>ErrorInfo for a policy violation error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_POLICY_VIOLATION = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}policy-violation");
    /**
     * <p>ErrorInfo for a remote connection error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_REMOTE_CONNECTION_FAILED = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}remote-connection-failed");
    /**
     * <p>ErrorInfo for a remote server timeout error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_REMOTE_SERVER_TIMEOUT = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}remote-server-timeout");

    /**
     * <p>ErrorInfo for a service unavailable error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_SERVICE_UNAVAILABLE = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}service-unavailable");
    /**
     * <p>ErrorInfo for a system shutdown error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_SYSTEM_SHUTDOWN = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}system-shutdown");
    /**
     * <p>ErrorInfo for a service unavailable error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_UNDEFINED_CONDITION = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}undefined-condition");
    /**
     * <p>ErrorInfo for a malformed xml error.</p>
     *
     * @type    jabberwerx.Stream.ErrorInfo
     */
    jabberwerx.Stream.ERR_XML_NOT_WELL_FORMED = new jabberwerx.Stream.ErrorInfo(
            "{urn:ietf:params:xml:ns:xmpp-streams}xml-not-well-formed");
}
