/**
 * filename:        PEPNode.js
 * created at:      2010/04/08T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.PEPNode = jabberwerx.PubSubNode.extend(/** @lends jabberwerx.PEPNode.prototype */{
        /**
         * @class
         * <p>Represents a PEP node.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.PubSubNode">jabberwerx.PubSubNode</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new PEPNode for the given JID, node, and
         * PEPController.</p>
         *
         * <p><b>NOTE:</b> This type should not be constructed directly.
         * Instead, use {@link jabberwerx.PEPController#node} to obtain an
         * instance.</p>
         *
         * @param   {jabberwerx.JID} jid The JID
         * @param   {String} node The node
         * @param   {jabberwerx.PEPController|jabberwerx.PEPNode} ctrl The owning
         *          controller or delegate
         * @throws  {TypeError} If {ctrl} is not valid; or if {node} is not
         *          valid
         * @extends jabberwerx.PubSubNode
         * @constructs  jabberwerx.PEPNode
         */
        init: function(jid, node, ctrl) {
            this._super(jid, node, ctrl);
        },
        /**
         * <p>Destroys this PEPNode. This method first unsubscribes then
         * calls the superclass' implementation.</p>
         */
        destroy: function() {
            this.unsubscribe();
        
            this._super();
        },
        
        /**
         * <p>Subscribes to this PEPNode. This method performs an implicit
         * subscription by updating the capabilities to add the feature
         * "<PEPNode.node>+notify".</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if subscribe failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is defined and not a function
         * @see     #unsubscribe
         */
        subscribe: function(cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }

            if (!this.delegate) {
                var caps = this.controller.client.controllers.capabilities;
                if (this.jid) {
                    caps.addFeatureToJid(this.jid, this.node + "+notify");
                } else {
                    caps.addFeature(this.node + "+notify");
                }
                this.properties.subscription="implicit";
            }
            
            if (this.autoRetrieve) {
                this.retrieve();
            }
            
            if (cb) {
                cb.call(this);
            }
        },
        /**
         * <p>Unsubscribes from this PEPNode. This method performs an implicit
         * unsubscription by updating the capabilities to remove the feature
         * "<PEPNode.node>+notify".</p>
         *
         * <p>The callback, if defined, is expected to match the following:
         * <pre class="code">
         *  function callback(err) {
         *      this;   // this PubSubNode
         *      err;    // the stanza error if unsubscribe failed
         *  }
         * </pre>
         *
         * @param   {Function} [cb] The callback
         * @throws  {TypeError} If {cb} is defined and not a function
         * @see     #subscribe
         */
        unsubscribe: function(cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be undefined or a function");
            }
            
            if (this.delegate) {
                if (cb) {
                    cb.call(this);
                }
                
                return;
            }
            
            var caps = this.controller.client.controllers.capabilities;
            if (this.jid) {
                caps.removeFeatureFromJid(this.jid, this.node + "+notify");
            } else {
                caps.removeFeature(this.node + "+notify");
            }
            delete this.properties.subscription;
        },
        
        /**
         * Creates the node in the pub-sub service. This implementation always
         * throws a {@link jabberwerx.util.NotSupportedError}.
         *
         * @param   {Function} [cb] The callback
         * @throws  {jabberwerx.util.NotSupportedError} This method is not
         *          supported
         */
        createNode: function(cb) {
            throw new jabberwerx.util.NotSupportedError();
        },
        /**
         * Deletes the node in the pub-sub service. This implementation always
         * throws a {@link jabberwerx.util.NotSupportedError}.
         *
         * @param   {Function} [cb] The callback
         * @throws  {jabberwerx.util.NotSupportedError} This method is not
         *          supported
         */
        deleteNode: function(cb) {
            throw new jabberwerx.util.NotSupportedError();
        }
    }, "jabberwerx.PEPNode");
    /**
     * @constant
     * @description
     * An item ID used for simple single-item publishing.
     */
    jabberwerx.PEPNode.CURRENT_ITEM = "current";
}
