/**
 * filename:        XDataFormItem.js
 * created at:      2010/02/23T11:20:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.XDataFormItem = jabberwerx.JWModel.extend(/** @lends jabberwerx.XDataFormItem.prototype */ {
        /**
         * @class
         * <p>Holds collection of jabberwerx.XDataFormField objects. This class is used in with XDataForm type "result".</p>
         *
         * @description
         * <p>Creates a new XDataFormItem based on the passed DOM element.</p> 
         * 
         * @param       {Element} [itemNode] The DOM element representing the item node.
         * @throws      {TypeError} If parameter is not valid
         * @constructs jabberwerx.XDataFormItem 
         * @extends JWModel
         */
        init: function(itemNode) {
            this._super();
            this._DOM = itemNode;
            if (itemNode) {           
                if (jabberwerx.isElement(itemNode)) {
                    var that = this;
                    var fieldNodes = jabberwerx.$(itemNode).children("field");
                    var field;
                    jabberwerx.$.each(fieldNodes, function() {
                        field = new jabberwerx.XDataFormField(null, null, this); 
                        that.fields.push(field);
                    });                        
                 }
                 else {
                     throw new TypeError("itemNode must be an Element");
                 }                 
             }
        },
        
        /**
         * <p>Destroys this item.</p>
         */
        destroy: function() {
            this._super();
        },
                     

        /**
         * <p>Gets the DOM of the item element.</p>
         *
         *
         * @returns {Element} DOM element of the item
         */
        getDOM: function() {
            return _DOM;            
        },                   

        /**
         * <p>Determines if the given item matches this item. This
         * method returns <tt>true</tt> if all fields are equal.</p>
         *
         * @param {jabberwerx.XDataFormItem} item The item to match against
         * @returns {Boolean} <tt>true</tt> if {item}'s fields matches
         *          this XDataFormItem.
         */
        equals: function(item) {
            if (item === this) {
                return true;
            }
            if (!item) {
                return false;
            }
            
            if (item.fields.length != this.fields.length) {
                return false;
            }
            
            for(var idx=0; idx<this.fields.length; idx++)  { 
                if (!this.fields[idx].equals(item.fields[idx])) {
                    return false;
                }        
            }             
                                              
            return true;                                 
        },

        /**
         * <p>Returns field based on var name or null if not found or name is null.</p>
         * @param {String} [name] Var name for the field  
         * @returns {jabberwerx.XDataFormField}
         */
        getFieldByVar: function(name) {
            if (!name) {
                return null;
            }
            var idx = this._indexOf(this.fields, name);
            if (idx != -1) {
                return this.fields[idx];
            }          
            return null;          
        },
        
        /**
         * <p>Returns field values based on var name or null if not found or name is null.</p>
         * @param {String} [name] Var name for the field 
         * @returns {String} 'FORM_TYPE' value for the form
         */
        getFieldValues: function(name) {
            var field = this.getFieldByVar(name);
            if (field) {
                return field.getValues();
            } 
            return null;        
        },
        
       /**
         * <p>Returns index of the field for the given var name or -1 if not found or the var is null.</p>
         * @private
         * @param {[jabberwerx.XDataFormField]} [fields] Array of fields to search.
         * @param {String} [name] Array of fields to search.
         * @returns {Number} Index of the field
         */        
        _indexOf: function(fields, name) {
            if (!name) {
                return -1;
            }
            for(var i=0; i<fields.length; i++) {
                if (fields[i].getVar() == name) {
                    return i;
                }
            }
            return -1;
        },
        
        /**
         * Array of jabbewerx.XDataFormField objects
         * @type  jabbewerx.XDataFormField[]
         */
        fields: []                       
    }, "jabberwerx.XDataFormItem");
     
}
