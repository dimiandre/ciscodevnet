/**
 * filename:        JID.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.JID = jabberwerx.JWModel.extend(/** @lends jabberwerx.JID.prototype */{
        /**
         * @class
         * @minimal
         * <p>Represents a JID identifier. A JID has the general string
         * form:</p>
         *
         * <pre class="code">node@domain/resource</pre>
         *
         * <p>Where {node} (and its trailing '@') and {resource} (and its
         * leading '/') are optional.</p>
         * 
         * @description
         * <p>Creates a new JID.</p>
         *
         * <p>The value of {arg} must be one of the following:</p>
         * <ul>
         * <li>a string representation of a JID (e.g.
         * "username@hostname/resource")</li>
         * <li>An object with the following properties:
         * <pre class='code'>{
            // REQUIRED: domain portion of JID
            domain: "hostname",
            // OPTIONAL: node portion of JID
            node: "username",
            // OPTIONAL: resource portion of JID
            resource: "resource",
            // OPTIONAL: "true" if the node is unescaped, and should
            // be translated via {@link #.escapeNode}
            unescaped: true|false
         * }</pre></li>
         * </ul>
         *
         * @param   {String|Object} arg The string or parts for a JID
         * @throws  {jabberwerx.JID.InvalidJIDError} If a JID cannot be created from
         *          {arg}.
         * @constructs jabberwerx.JID
         * @extends jabberwerx.JWModel
         */
        init: function(arg) {
            if (arg instanceof jabberwerx.JID) {
                arg = {
                    "node": arg.getNode() || null,
                    "domain": arg.getDomain() || null,
                    "resource": arg.getResource() || null
                };
            } else if (typeof(arg) == "string") {
                //We'll parse this ourselves...
                var result = /^(?:([^\/]+)@)?([^@\/]+)(?:\/(.+))?$/.exec(arg);
                if (!result) {
                    throw new jabberwerx.JID.InvalidJIDError("JID did not match the form 'node@domain/resource'");
                }
                full = arg.toString();
                arg = {
                    "node": (result[1] || undefined),
                    "domain": result[2],
                    "resource": (result[3] || undefined)
                };
            } else if (!arg) {
                throw new jabberwerx.JID.InvalidJIDError("argument must be defined and not null");
            } else {
                // Clone argument
                arg = jabberwerx.$.extend({}, arg);
            }
        
            var prepFN = function(test) {
                if (/[ \t\n\r@\:\<\>\&'"\/]/.test(test)) {
                    throw new jabberwerx.JID.InvalidJIDError("invalid characters found");
                }
                
                return test.toLowerCase();
            };
            
            // prep domain
            if (!arg.domain) {
                throw new jabberwerx.JID.InvalidJIDError("'' or null or undefined domain not allowed");
            } else {
                arg.domain = prepFN(arg.domain, true);
            }

            // prep node
            if (arg.node == "") {
                throw new jabberwerx.JID.InvalidJIDError("'' node with @ not allowed");
            } else if (arg.node) {
                if (arg.unescaped) {
                    arg.node = jabberwerx.JID.escapeNode(arg.node);
                }
                arg.node = prepFN(arg.node, true);
            }
            
            // prep resource
            if (arg.resouce == "") {
                throw new jabberwerx.JID.InvalidJIDError("'' resource with / not allowed");
            }
            
            this._domain = arg.domain;
            this._node = arg.node || "";
            this._resource = arg.resource || "";
            this._full = "" +
                    (arg.node ? arg.node + "@" : "") +
                    arg.domain +
                    (arg.resource ? "/" + arg.resource : "");
            
            // IE6 doesn't like how we override toString.
            if (jabberwerx.$.browser.msie) {
                this.toString = function () {
                    return this._full;
                }
            }
        },
        
        /**
         * <p>Returns the bare JID form of this JID. A bare JID consists of
         * the node and domain, but not the resource. If this JID is already
         * a bare JID, this method returns the current JID. Otherwise, a new
         * JID object is created that contains only the node and domains.</p>
         *
         * @returns {jabberwerx.JID} The "bare" JID from this JID
         */
        getBareJID: function() {
            if (!this.getResource()) {
                return this;
            } else {
                return new jabberwerx.JID({
                    "node": this.getNode(),
                    "domain": this.getDomain()
                });
            }
        },
        /**
         * <p>Returns the bare JID form of this JID as a string. This method
         * is a convenience over calling getBareJID().toString().</p>
         *
         * @returns {String} The "bare" JID, as a string
         * @see #getBareJID
         * @see #toString
         */
        getBareJIDString: function() {
            return this.getBareJID().toString();
        },
        
        /**
         * <p>Retrieves the domain value for this JID.</p>
         *
         * @returns  {String} The JID's domain
         */
        getDomain: function() {
            return this._domain;
        },
        /**
         * <p>Retrieves the node value for this JID. If this
         * JID does not have a node, this method returns "".</p>
         *
         * @returns  {String} The JID's node
         */
        getNode: function() {
            return this._node;
        },
        /**
         * <p>Retrieves the resource value for this JID. If this
         * JID does not have a resource, this method returns "".</p>
         *
         * @returns  {String} The JID's resource
         */
        getResource: function() {
            return this._resource;
        },
        
        /**
         * <p>Retrieves the string respesenting this JID.</p>
         *
         * @returns  {String} The string representation
         */
        toString: function() {
            return this._full;
        },
        /**
         * <p>Retrieves the display string representing this JID.
         * This method returns a JID with the node portion unescaped
         * via {@link jabberwerx.JID#.unescapeNode}. As such, the returned string may
         * not be a valid JID.</p>
         *
         * @returns  {String} The display string representation
         */
        toDisplayString: function() {
            var result = this.getDomain();
            var part;
            
            part = this.getNode();
            if (part) {
                result = jabberwerx.JID.unescapeNode(part) + "@" + result;
            }
            
            part = this.getResource();
            if (part) {
                result = result + "/" + part;
            }
            
            return result;
        },
        
        /**
         * <p>Determines if this JID is equal to the given JID.</p>
         *
         * @param   {String|jabberwerx.JID} jid The JID (or string representing
         *          a JID) to compare to
         * @returns {Boolean} <tt>true</tt> if this JID and {jid} represent
         *          the same JID value
         */
        equals: function(jid) {
            try {
                jid = jabberwerx.JID.asJID(jid);
                return this.toString() == jid.toString();
            } catch (ex) {
                return false;
            }
        },
        /**
         * <p>Compares this JID to the given JID for ordering. The order of
         * two JIDs is determined by their parts as follows:</p>
         *
         * <ol>
         *  <li>domain (returns -1 or 1 if not equal)</li>
         *  <li>node (returns -1 or 1 if not equal)</li>
         *  <li>resource (returns -1 or 1 if not equal)</li>
         * </ol>
         *
         * @param   {String|jabberwerx.JID} jid The JID (or string represting
         *          a JID) to compare to
         * @returns {Number} -1, 0, or 1 if this JID is before, equal to, or
         *          after {jid}
         * @throws  {jabberwerx.JID.InvalidJIDError} If {jid} is not a valid JID
         */
        compareTo: function(jid) {
            jid = jabberwerx.JID.asJID(jid);
            var cmp = function(v1, v2) {
                if          (v1 < v2) {
                    return -1;
                } else if   (v1 > v2) {
                    return 1;
                }
                
                return 0;
            };
            //var     val1 = this.toString(), val2 = test.toString();
            var result;
            if ((result = cmp(this.getDomain(), jid.getDomain())) != 0) {
                return result;
            }
            if ((result = cmp(this.getNode(), jid.getNode())) != 0) {
                return result;
            }
            if ((result = cmp(this.getResource(), jid.getResource())) != 0) {
                return result;
            }
            
            return 0;
        },
        
        /**
         * <p>Determines if this type should be saved with the rest of a
         * persistence graph, as a stand-alone object.</p>
         *
         * @returns  {Boolean}   Always <tt>false</tt>
         */
        shouldBeSavedWithGraph: function() {
            return false;
        },
        /**
         * <p>Determines if this type should be serialized inline with other
         * primitive (boolean, number, string) or inlinable types.</p>
         *
         * @returns  {Boolean}   Always <tt>true</tt>
         */
        shouldBeSerializedInline: function() {
            return true;
        },
        /**
         * <p>Called just after this JID is unserialized. This method recreates
         * the internal structures from the string representation.</p>
         */
        wasUnserialized: function() {
            // IE6 doesn't like how we override toString.
            if (jabberwerx.$.browser.msie) {
                this.toString = function () {
                    return this._full;
                }
            }
        },
        
        /**
         * @private
         */
        _node: "",
        /**
         * @private
         */
        _domain: "",
        /**
         * @private
         */
        _resource: "",
        /**
         * @private
         */
        _full: ""
    }, 'jabberwerx.JID');
    
    /**
     * @class jabberwerx.JID.InvalidJIDError
     * @minimal
     * <p>Error thrown when invalid JID strings are encountered.</p>
     *
     * @extends jabberwerx.util.Error
     */
    jabberwerx.JID.InvalidJIDError = jabberwerx.util.Error.extend.call(
            TypeError,
            "The JID is invalid");
    
    /**
     * <p>Converts an object into a jabberwerx.JID. This method
     * uses the following algorithm:</p>
     *
     * <ol>
     * <li>If {val} is an instance of jabberwerx.JID, return it</li>
     * <li>Create a new jabberwerx.JID from val</li>
     * </ol>
     *
     * @param val The value to convert to a JID
     * @returns {jabberwerx.JID} The JID object from {val}
     * @throws {jabberwerx.JID.InvalidJIDError} If {val} could not be converted into a JID
     */
    jabberwerx.JID.asJID = function(val) {
        if (val instanceof jabberwerx.JID) {
            return val;
        } else {
            return new jabberwerx.JID(val);
        }
    };
    
    /**
     * <p>Translates the given input into a valid node for a JID. This method
     * performs the translation according to the escaping rules in XEP-0106:
     * JID Escaping.</p>
     * 
     * @param   {String} input The string to translate
     * @returns {String} The translated string
     * @throws  {TypeError} if {input} is not a string; or if {input}
     *          starts or ends with ' '
     * @see     <a href='http://xmpp.org/extensions/xep-0106.html'>XEP-0106: JID Escaping</a>
     */
    jabberwerx.JID.escapeNode = function(input) {
        if (typeof(input) != "string") {
            throw new TypeError("input must be a string");
        }
        
        if (input.charAt(0) == ' ' || input.charAt(input.length - 1) == ' ') {
            throw new TypeError("input cannot start or end with ' '");
        }
        
        var ptn = /([ "&'\/:<>@])|(\\)(20|22|26|27|2f|3a|3c|3e|40|5c)/gi;
        var repFN = function(found, m1, m2, m3) {
            switch (m1 || m2) {
                case ' ':   return "\\20";
                case '"':   return "\\22";
                case '&':   return "\\26";
                case '\'':  return "\\27";
                case '/':   return "\\2f";
                case ':':   return "\\3a";
                case '<':   return "\\3c";
                case '>':   return "\\3e";
                case '@':   return "\\40";
                case '\\':  return "\\5c" + m3;
            }
            
            return found;
        };
        
        return input.replace(ptn, repFN);
    };
    /**
     * <p>Translates the given input from a valid node for a JID. This method
     * performs the translation according to the unescaping rules in XEP-0106:
     * JID Escaping.</p>
     * 
     * @param   {String} input The string to translate
     * @returns {String} The translated string
     * @throws  {TypeError} if {input} is not a string
     * @see     <a href='http://xmpp.org/extensions/xep-0106.html'>XEP-0106: JID Escaping</a>
     */
    jabberwerx.JID.unescapeNode = function(input) {
        if (typeof(input) != "string") {
            throw new TypeError("input must be a string");
        }
        
        var ptn = /(\\20|\\22|\\26|\\27|\\2f|\\3a|\\3c|\\3e|\\40)|(\\5c)(20|22|26|27|2f|3a|3c|3e|40|5c)/gi;
        var repFN = function(found, m1, m2, m3) {
            switch (m1 || m2) {
                case "\\20":    return ' ';
                case "\\22":    return '"';
                case "\\26":    return '&';
                case "\\27":    return '\'';
                case "\\2f":    return '/';
                case "\\3a":    return ':';
                case "\\3c":    return '<';
                case "\\3e":    return '>';
                case "\\40":    return '@';
                case "\\5c":    return '\\' + m3;
            }
            
            return found;
        };
        
        return input.replace(ptn, repFN);
    };
}
