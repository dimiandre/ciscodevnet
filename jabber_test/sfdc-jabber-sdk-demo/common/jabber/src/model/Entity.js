/**
 * filename:        Entity.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    //Adding function() to keep private methods out of global scope.
    (function(){
        /**
         * @private
         */
        var __jwesAsKey = function(jid, node) {
            return "[" + (jid || "") + "]:[" + (node || "") + "]";
        };
        
        jabberwerx.Entity = jabberwerx.JWModel.extend(/** @lends jabberwerx.Entity.prototype */{
            /**
             * @class
             * @minimal
             * <p>
             * Something addressable by JID and/or node: user, server, room, etc. For this release, clients
             * are not considered entities; there's a single global client.
             * </p>
             * 
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.Entity">jabberwerx.Entity</a></li>
             * </ul>
             *
             * To subscribe for a single entities primary presence updates use:<p><br>
             * <i>entity.event('primaryPresenceUpdated').bind......</i><br>
             * To subscribe for all entities primary presence updates use:<br>
             * <i>jabberwerx.globalEvents.bind('primaryPresenceChanged',......</p>
             *
             * @description
             * <p>Creates a new Entity with the given key and controller/cache.</p>
             * 
             * <p>The value of key is expected to be an object with at least one of
             * the following properties:</p>
             *
             * <ul>
             * <li><b>jid:</b> The JID for this Entity (must either be undefined or
             * represent a valid JID)</li>
             * <li><b>node:</b> The sub node for this Entity (must either be
             * undefined or a non-empty string)</li>
             * </ul>
             *
             * <p>The value of {ctrl} may be undefined, a
             * {@link jabberwerx.Controller} or a
             * {@link jabberwerx.ClientEntityCache}. If it is a Controller,
             * its {@link jabberwerx.Controller#updateEntity} and
             * {@link jabberwerx.Controller#removeEntity} method will be called
             * as appropriate. If it is a ClientEntityCache, the event notifiers
             * for "entityCreated", "entityUpdated", and "entityDestroyed" are
             * retained and used as appropriate.</p>
             *
             * @param   {Object} key The JID and/or node identifying this entity
             * @param   {jabberwerx.Controller|jabberwerx.EntitySet} [ctrl] The
             *          controller or cache for this entity
             * @constructs jabberwerx.Entity
             * @extends jabberwerx.JWModel
             */
            init: function(key, ctrl) {
                this._super();
    
                if (!key && !(key.jid || key.node)) {
                    throw new TypeError("key must contain a jid and/or a node");
                }
    
                // setup key
                if (key.jid) {
                    this.jid = jabberwerx.JID.asJID(key.jid);
                }
                if (key.node) {
                    this.node = key.node;
                }
                this._mapKey = __jwesAsKey(this.jid, this.node);
    
                var cache;
                if (ctrl instanceof jabberwerx.Controller) {
                    this.controller = ctrl;
                    cache = ctrl.client && ctrl.client.entitySet;
                } else if (ctrl && ctrl.register && ctrl.unregister && ctrl.entity) {
                    cache = ctrl;
                }
                
                if (cache) {
                    this.cache = cache;
                    this._eventing = {
                        "added"   : cache.event("entityCreated"),
                        "updated" : cache.event("entityUpdated"),
                        "deleted" : cache.event("entityDestroyed")
                    };
                } else {
                    this._eventing = {
                        "added"   : null,
                        "updated" : null,
                        "deleted" : null
                    };
                }
                
                // Set up event
                this.applyEvent('primaryPresenceChanged');
                this.applyEvent("resourcePresenceChanged");
                
                //IE6 workaround
                if (jabberwerx.$.browser.msie) {
                    this.toString = function () {
                        return this.__toStringValue();
                    }
                }
            },
            
            /**
             * <p>Destroys this entity. In most cases, this method should not
             * be called directly. Instead, call {@link #remove}.</p>
             */
            destroy: function() {
                if (this.cache) {
                    this.cache.unregister(this);
                }
                
                this._super();
            },
            
            /**
             * <p>Applies the values from the given entity to this one.
             * This method copies the groups, displayName, properties, features,
             * and identities from {entity} into this one. It then calls
             * {@link #update}, which may trigger an "entityUpdated" event.</p>
             *
             * @param   {jabberwerx.Entity} entity The entity to apply
             * @param   {Boolean} [noupdate] <tt>true</tt> to indicate that
             *          an update should NOT be triggered
             * @returns {jabberwerx.Entity} This entity
             * @throws  {TypeError} if {entity} is not an instance of Entity.
             */
            apply: function(entity, noupdate) {
                if (!(entity && entity instanceof jabberwerx.Entity)) {
                    throw new TypeError("entity is not valid");
                }
    
                jabberwerx.$.extend(this, {
                    _displayName: entity._displayName,
                    _groups: jabberwerx.$.extend([], entity._groups),
                    _presenceList: jabberwerx.$.extend([], entity._presenceList),
                    properties: jabberwerx.$.extend(true, {}, entity.properties),
                    features: jabberwerx.$.extend([], entity.features),
                    identities: jabberwerx.$.extend([], entity.identities)
                });
                
                if (!noupdate) {
                    this.update();
                }
                
                return this;
            },
            
            /**
             * @private
             */
            __toStringValue: function() {
                return "entity<" + this.getClassName() + ">[" +
                        this._mapKey + "]: " +
                        this.getDisplayName();
            },
            /**
             * <p>Determines if the given entity matches this Entity. This
             * method returns <tt>true</tt> if the jids and nodes of this Entity
             * are equal to {entity}'s.</p>
             *
             * @param {jabberwerx.Entity} entity The entity to match against
             * @returns {Boolean} <tt>true</tt> if {entity}'s identity matches
             *          this Entity.
             */
            matches: function(entity) {
                if (entity === this) {
                    return true;
                }
                
                if (!entity) {
                    return false;
                }
                
                return this._mapKey == entity._mapKey;
            },
            
            /**
             * <p>Determines if this entity is active. This method returns
             * <tt>true</tt> if the entity has at least one available presence
             * in its list.</p>
             *
             * <p>Subclasses may override this method to provide an alternative
             * means of determining its active state.</p>
             *
             * @returns {Boolean} <tt>true</tt> if the entity is active
             */
            isActive: function() {
                return (this._presenceList.length > 0 &&
                        this._presenceList[0].getType() != "unavailable");
            },
            
            /**
             * Gets the primary presence object of this entity. If the primary presence for this object
             * does not exist then null is returned.
             * @returns {jabberwerx.Presence} primary presence
             */
            getPrimaryPresence: function() {
                return this._presenceList[0] || null;
            },
            
            /**
             * Returns a sorted array of all presence objects for this entity
             * @returns {jabberwerx.Presence[]} an array of presence objects
             */
            getAllPresence: function() {
                return this._presenceList;
            },
            
            /**
             * Gets the presence object for a particular resource under this entity.
             * @param {String} resource The resource to get the presence object for
             * @returns {jabberwerx.Presence} The presence object for the resource. If the resource does not exist or does not have
             * a presence object associated with it then null is returned.
             */
            getResourcePresence: function(resource) {
                var fullJid = this.jid.getBareJIDString() + '/' + resource;
                var presence = null;
                jabberwerx.$.each(this._presenceList, function() {
                    if (this.getFrom() == fullJid) {
                        // Assign value to return value and exit jabberwerx.$.each loop
                        presence = this;
                        return false;
                    }
                    return true;
                });
                return presence;
            },
            
            /**
             * <p>Update presence for this Entity.</p>
             *
             * <p>If {presence} results in a change of the primary resource for
             * this entity, a "primaryPresenceChanged" event is triggered. A
             * "resourcePresenceChanged" event is always triggered by this method,
             * before "primaryPresenceChanged" (if applicable).</p>
             *
             * @param   {jabberwerx.Presence} [presence] The presence used to update. If this parameter 
             *          is null or undefined the presence list for this entity is cleared. 
             * @returns  {Boolean} <tt>true</tt> if primary presence changed. 
             * @throws  {TypeError} If {presence} exists but is not a valid availability or 
             *          unavailability presence for this entity
             */
            updatePresence: function(presence) {
            var retVal = false;

                if (!presence) {
                    if (this.getPrimaryPresence()) {
                        // Trigger a resourcePresenceChanged event and remove every presence object in
                        // the presence list
                        var len = this._presenceList.length;
                        for (var i=0; i<len; i++) {
                            var presence = this._presenceList.pop();
                            this.event("resourcePresenceChanged").trigger({
                                fullJid: presence.getFromJID(),
                                presence: null,
                                nowAvailable: false
                            });
                        }

                        this.event("primaryPresenceChanged").trigger({
                            presence: null,
                            fullJid: this.jid.getBareJID()
                        });

                        retVal = true;
                    }
                } else {
                    if (!(presence instanceof jabberwerx.Presence)) {
                        throw new TypeError("presence is not a valid type");
                    }

                    var jid = presence.getFromJID();
                    if (jid.getBareJIDString() != this.jid.getBareJIDString()) {
                        throw new jabberwerx.Entity.InvalidPresenceError("presence is not for this entity: " + this);
                    }

                    if (presence.getType() && presence.getType() != "unavailable") {
                        throw new jabberwerx.Entity.InvalidPresenceError("presence is not the correct type");
                    }
                    //If the user resource presence went from unavailable to available, set nowAvailable to true
                    var resPresence = this.getResourcePresence(jid.getResource());
                    var nowAvailable;
                    if (!resPresence || resPresence.getType() == "unavailable") {
                        nowAvailable = Boolean(!presence.getType());
                    } else {
                        nowAvailable = false;
                    }
                   
                   
                    // Keep reference to primary presence before insert is made
                    var primaryPresence = this._presenceList[0] || null;

                    // Remove old presence object and add new presence object
                    this._removePresenceFromList(jid.toString());
                    if (presence.getType() != "unavailable") {
                        if (!this.isActive()) {
                            this._clearPresenceList();
                        }
                        this._insertPresence(presence);
                    } else {
                        this._handleUnavailable(presence);
                    }

                    this.event("resourcePresenceChanged").trigger({
                        fullJid: jid,
                        presence: presence,
                        nowAvailable: nowAvailable
                    });
                    if (primaryPresence !== this._presenceList[0]) {
                        var primaryJid;
                        primaryPresence = this._presenceList[0] || null;
                        primaryJid = primaryPresence ?
                            primaryPresence.getFromJID() :
                            jid.getBareJID();
                        this.event("primaryPresenceChanged").trigger({
                            presence: primaryPresence,
                            fullJid: primaryJid
                        });
                        retVal = true;
                    }
                }

                return retVal;
            },

            /**
             * @private
             * This function gets called when an unavailable presence get sent to
             * {@link jabberwerx.Entity#updatePresence}.
             */
            _handleUnavailable: function(presence) {
            },

            /**
             * @private
             */
            _insertPresence: function(presence) {
                this._presenceList.push(presence);
                this._presenceList.sort(function (a, b) {
                    return b.compareTo(a);
                });
            },

            /**
             * @private
             */
            _clearPresenceList: function() {
                this._presenceList = [];
            },

            /**
             * @private
             * Removes the presence object specified by the jid parameter from the _presenceList
             * @param {String} jid
             * @returns {Boolean} true if presence object was found and removed, false if could not be found
             */
            _removePresenceFromList: function(jid) {
                for(var i=0; i<this._presenceList.length; i++) {
                    if (this._presenceList[i].getFrom() == jid) {
                        this._presenceList.splice(i,1);
                        return true;
                    }
                }
                return false;
            },
            
            /**
             * <p>Retrieves the display name for this Entity. This method
             * returns the explicitly set value (if one is present), or a
             * string using the following format:</p>
             *
             * <div class="code">{&lt;node&gt;}&lt;jid&gt;</div>
             *
             * <p>Where &lt;jid&gt; is {@link #jid} (or "" if not defined), and
             * &lt;node&gt; is {@link #node} (or "" if not defined).</p>
             *
             * <p>Subclasses overriding this method SHOULD also override
             * {@link #setDisplayName}.</p>
             *
             * @returns {String} The display name
             */
            getDisplayName: function() {
                var name = this._displayName;
                if (!name) {
                    var jid = (this.jid && this.jid.toDisplayString());
                    name = (this.node && "{" + this.node + "}") + (jid || "");
                }
                
                return name;
            },
            /**
             * <p>Changes or removes the expclit display name for this Entity.
             * If the value of {name} is non-empty String, it is set as the
             * explicit display name. Otherwise any previous value is cleared.</p>
             *
             * <p>If this entity has a controller associated with it, its
             * {@link jabberwerx.Controller#updateEntity} is called, passing
             * in this Entity. Otherwise this method attempts to trigger a
             * "entityUpdated" event on the associated event cache.</p>
             *
             * <p>Subclasses overriding this method SHOULD also override
             * {@link #getDisplayName}.</p>
             *
             * @param {String} name The new display name
             */
            setDisplayName: function(name) {
                var nval = (String(name) || "");
                var oval = this._displayName;
                
                if (oval != nval) {
                    this._displayName = nval;
                    this.update();
                }
            },
            /**
             * @private
             */
            _displayName: "",
            
            /**
             * <p>Retrieves the groups for this Entity. The returned
             * array is never {null} or {undefined}.</p>
             *
             * @returns {String[]} The array of groups (as strings)
             */
            getGroups: function() {
                return this._groups;
            },
            /**
             * <p>Changes or removes the groups for this Entity. This method
             * uses the following algorithm:</p>
             *
             * <ol>
             * <li>If {groups} is an array, it is cloned (with duplicate values
             * removed) and replaces any previous groups.</li>
             * <li>If {groups} is a single string, all previous groups are replaced
             * with an array containing this value.</li>
             * <li>Otherwise, the previous groups are replaced with an empty
             * array.</li>
             * </ol>
             * @param {String[]|String} [groups] The name of groups
             */
            setGroups: function(groups) {
                if (jabberwerx.$.isArray(groups)) {
                    groups = jabberwerx.unique(groups.concat([]));
                } else if (groups) {
                    groups = [ groups.toString() ];
                } else {
                    groups = [];
                }
            
                this._groups = groups;
                this.update();
            },
            /**
             *
             */
            _groups : [],
            
            /**
             * <p>Triggers an update of this entity. If the entity has a
             * controller, then {@link jabberwerx.Controller#updateEntity} is
             * called. Otherwise if this entity has an owning cache, an
             * "entityUpdated" event is fired on that cache for this
             * entity.</p>
             *
             */
            update: function() {
                if (this.controller && this.controller.updateEntity) {
                    this.controller.updateEntity(this);
                } else if (this._eventing["updated"]) {
                    this._eventing["updated"].trigger(this);
                }
            },
            /**
             * <p>Removes this entity. If the entity has a controller, then
             * {@link jabberwerx.Controller#removeEntity} is called. Otherwise
             * {@link #destroy} is called.</p>
             */
            remove: function() {
                if (this.controller && this.controller.removeEntity) {
                    this.controller.removeEntity(this);
                } else {
                    this.destroy();
                }
            },
            
            /**
             * <p>Determines if this Entity supports the given feature.</p>
             *
             * @param   {String} feat The feature to check for
             * @returns  {Boolean} <tt>true</tt> if {feat} is supported
             */
            hasFeature: function(feat) {
                if (!feat) {
                    return false;
                }
                
                var result = false;
                jabberwerx.$.each(this.features, function() {
                    result = String(this) == feat;
                    return !result; //return false to break the loop if we found it
                });
                
                return result;
            },
            /**
             * <p>Determines if this Entity supports the given identity (as a
             * single "category/type" string).</p>
             *
             * @param   {String} id The identity to check for as a "category/type"
             *          string
             * @return  {Boolean} <tt>true</tt> if {id} is supported
             */
            hasIdentity: function(id) {
                if (!id) {
                    return false;
                }
                
                var result = false;
                jabberwerx.$.each(this.identities, function() {
                    result = String(this) == id;
                    return !result; //return false to break the loop if we found it
                });
                
                return result;
            },
            
            /**
             * <p>Retrieves the string value of this Entity.</p>
             *
             * @return  {String} The string value
             */
            toString: function() {
                return this.__toStringValue();
            },
            
            /**
             * <p>The JID for this Entity. This property may be null if the entity
             * is not JID-addressable.</p>
             *
             * @type jabberwerx.JID
             */
            jid: null,
            /**
             * <p>The node ID for this Entity. This property may be "" if the entity
             * is not node-addressable.</p>
             *
             * @type String
             */
            node: "",
    
            /**
             * <p>The properties for this Entity. This is an open-ended hashtable,
             * with the specifics defined by subclasses.</p>
             *
             */
            properties: {},
            
            /**
             * <p>The set of features for this Entity. This is a unique array of
             * Service Discovery feature strings.</p>
             *
             * @type    {String[]}
             */
            features: [],
            /**
             * <p>The set of identities for this Entity. This is a unique array of
             * Service Discovery category/type strings.</p>
             *
             * @type    {String[]}
             */
            identities: [],
            
            /**
             * @private
             * Stores a list of jabberwerx.Presence objects for each resource of this entity. Do not directory modify this
             * but instead use the getPrimaryPresence, getAllPresence, getResourcePresence, addPresence and removePresence methods
             * @type [jabberwerx.Presence]
             */
            _presenceList: []
        }, 'jabberwerx.Entity');
        
        jabberwerx.Entity.InvalidPresenceError = jabberwerx.util.Error.extend("The provided presence is not valid for this entity");
        
        jabberwerx.EntitySet = jabberwerx.JWModel.extend(/** @lends jabberwerx.EntitySet */{
            /**
             * @class
             * @minimal
             * <p>A repository for Entity objects, based on JID and/or node.</p>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.EntitySet">jabberwerx.EntitySet</a></li>
             * </ul>
             *
             * @description Create a new EntitySet.
             * 
             * 
             * @constructs jabberwerx.EntitySet
             * @extends jabberwerx.JWModel
             */
            init: function() {
                this._super();
    
                this.applyEvent("entityCreated");
                this.applyEvent("entityDestroyed");
                this.applyEvent("entityUpdated");
                this.applyEvent("entityRenamed");
                this.applyEvent("entityAdded");
                this.applyEvent("entityRemoved");
                this.applyEvent("batchUpdateStarted");
                this.applyEvent("batchUpdateEnded");
            },
    
            /**
             * Fetch method for any known-existing {@link jabberwerx.Entity} object.
             * If the object does not already exist, `undefined` is returned.
             * 
             * @param {jabberwerx.JID|String} [jid] The JID of an entity to fetch.
             * @param {String} [node] The node of an entity to fetch.
             * @type jabberwerx.Entity
             */
            entity: function(jid, node) {
                return this._map[__jwesAsKey(jid, node)];
            },
            
            /**
             * Registers the given entity to this EntitySet.
             * 
             * @param {jabberwerx.Entity} entity The entity to register.
             * @returns {Boolean} <tt>true</tt> if this EntitySet's data was
             *          changed by this call.
             * @throws  {TypeError} if {entity} is not a valid Entity.
             */
            register: function(entity) {
                if (!(entity && entity instanceof jabberwerx.Entity)) {
                    throw new TypeError("entity is not an Entity");
                }
                
                var prev = this.entity(entity.jid, entity.node);
                if (prev === entity) {
                    return false;
                }
                
                if (prev) {
                    this.unregister(prev);
                }
                this._updateToMap(entity);
                this.event("entityAdded").trigger(entity);
                
                return true;
            },
            /**
             * Unregister the given entity from this EntitySet.
             * 
             * @param {jabberwerx.Entity} entity The entity to unregister.
             * @returns {Boolean} <tt>true</tt> if the EntitySet's data was
             *          changed by this call.
             */             
            unregister: function(entity) {
                var present = (this.entity(entity.jid, entity.node) !== undefined);
                this._removeFromMap(entity);
                if (present) {
                    this.event("entityRemoved").trigger(entity);
                }
                
                return present;
            },
            
            /**
             * @private
             */
             _renameEntity: function(entity, njid, nnode) {
                var ojid = entity.jid;
                var onode = entity.node;
                this._removeFromMap(entity);
                
                if (njid) {
                    njid = jabberwerx.JID.asJID(njid);
                } else {
                    njid = undefined;
                }
                entity.jid = njid;
                if (nnode) {
                    nnode = String(nnode);
                } else {
                    nnode = undefined;
                }
                entity.node = nnode;
                this._updateToMap(entity);
                
                var data = {
                    entity: entity,
                    jid: ojid,
                    node: nnode
                };
                this.event("entityRenamed").trigger(data);
             },
            
            /**
             * Visits each entity in this EntitySet. The given function is executed
             * as op(entity). For each execution of &lt;op&gt;, the sole argument
             * is the current entity.
             *
             * @param {function} op The function called for each entity
             */
            each: function(op) {
                if (!jabberwerx.$.isFunction(op)) {
                    throw new TypeError('operator must be a function');
                }
                
                var     that = this;
            
                jabberwerx.$.each(this._map, function() {
                    var     retcode;
                
                    if (this instanceof jabberwerx.Entity) {
                        retcode = op(this);
                    }
                    
                    return (retcode !== false);
                });
            },
    
            /**
             * Returns an array of the entities registered on this EntitySet.
             * @returns {jabberwerx.Entity[]} An array of the entities registered on this EntitySet
             */
            toArray: function() {
                var     arr = [];
                
                this.each(function(entity) {
                    arr.push(entity);
                });
    
                return arr;
            },
            
            /**
             * <p>Gets a set of all the groups to which the entities in this entity set belong. The
             * contents of the group are unique (i.e. no copies of group names within the array).</p>
             * @returns {String[]} A string array of the group names
             */
            getAllGroups: function() {
                var groups = [];
                this.each(function(entity) {
                    jabberwerx.$.merge(groups, entity.getGroups());
                });
                return jabberwerx.unique(groups);
            },
            
            /**
             * The map of keys (jid/node) to entities
             *
             * @private
             */
            _map: {},
    
            /**
             * Updates the map of entities to include the given entity.
             */
            _updateToMap: function (ent) {
                this._map[__jwesAsKey(ent.jid, ent.node)] = ent;
            },
            _removeFromMap: function(ent) {
                delete this._map[__jwesAsKey(ent.jid, ent.node)];
            }
        }, 'jabberwerx.EntitySet'); 
        
        /**
         * <p>Error when attempting to add an entity already contained by an EntitySet.</p>
         *
         * @constructs  jabberwerx.EntitySet.EntityAlreadyExistsError
         * @extends     jabberwerx.util.Error
         */
        jabberwerx.EntitySet.EntityAlreadyExistsError = jabberwerx.util.Error.extend('That JID is already taken!.');
    })();
}
