/**
 * filename:        SASLMechs.js
 * created at:      2009/09/29T00:00:00-07:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx && jabberwerx.SASLMechanism) {
    jabberwerx.SASLPlainMechanism = jabberwerx.SASLMechanism.extend(
    /** @lends jabberwerx.SASLPlainMechanism.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Implements the PLAIN SASL mechanism.</p>
         *
         * @description
         * Creates a new SASLPlainMechanism with the given client.
         *
         * @param   {jabberwerx.Client} client The client
         * @throws  {TypeError} If {client} is not valid
         * @constructs  jabberwerx.SASLPlainMechanism
         * @extends     jabberwerx.SASLMechanism
         */
        init: function(client) {
            this._super(client);
        },
        
        /**
         * Called by {@link jabberwerx.SASLMechanism#evaluate} to generate initial data. This method
         * creates the string "\\u0000username\\u0000password", using
         * the connection parameters specified in {@link jabberwerx.SASLMechanism#client}.</p>
         *
         * <p><b>NOTE:</b> If the BOSH connection is not made over a
         * secure connection, or if the jabberwerx_config.unsecureAllowed
         * flag is <tt>false</tt>, this method throws a SASLAuthFailure
         * ("{urn:ietf:params:xml:ns:xmpp-sasl}mechanism-too-weak").</p>
         *
         * @returns {String} The credential data
         * @throws  {jabberwerx.SASLMechanism.SASLAuthFailure} If PLAIN is not
         *          supported by current configuration
         */
        evaluateStart: function() {
            // TODO: Look for something better!
            var params = this.getProperties();
            if (!this.client.isSecure()) {
                throw new jabberwerx.SASLMechanism.SASLAuthFailure("{urn:ietf:params:xml:ns:xmpp-sasl}mechanism-too-weak");
            }
            var nilChar = String.fromCharCode(0);
            var usr = (params && params.jid && params.jid.getNode()) || "";
            var pwd = (params && params.password) || "";
            return nilChar + usr + nilChar + pwd;
        },
        /**
         * <p>Called by {@link jabberwerx.SASLMechanism#evaluate} to process a challenge. This
         * implementation throws a SASLAuthFailure if {inb} is a non-empty
         * string, or if this mechanism is already complete.
         *
         * @throws  {jabberwerx.SASLMechanism.SASLAuthFailure} If the
         *          challenge is invalid (non-empty)
         */
        evaluateChallenge: function(inb) {
            if (inb || this.complete) {
                throw new jabberwerx.SASLMechanism.SASLAuthFailure();
            }
            this.complete = true;
        }
    }, "jabberwerx.SASLPlainMechanism", "PLAIN");
    
    jabberwerx.SASLDigestMd5Mechanism = jabberwerx.SASLMechanism.extend(
    /** @lends jabberwerx.SASLDigestMd5Mechanism.prototype */ {
        /**
         * @class
         * @minimal
         * <p>Implements the DIGEST-MD5 SASL mechanism.</p>
         *
         * @description
         * Creates a new SASLDigestMd5Mechanism with the given client.
         *
         * @param   {jabberwerx.Client} client The client
         * @throws  {TypeError} If {client} is not valid
         * @constructs  jabberwerx.SASLDigestMd5Mechanism
         * @extends     jabberwerx.SASLMechanism
         */
        init: function(client) {
            this._super(client);
        },
        
        /**
         * Called by {@link jabberwerx.SASLMechanism#evaluate} to generate initial data. This method
         * sets up the staging methods for subsequenct steps.</p>
         *
         * @returns {String} <tt>null</tt>
         */
        evaluateStart: function() {
            this._stage = this._generateResponse;
            
            return null;
        },
        /**
         * <p>Called by {@link jabberwerx.SASLMechanism#evaluate} to process a challenge. This
         * implementation follows RFC 2831's flow, which is two steps:</p>
         *
         * <ol>
         *  <li>DIGEST: Performs the actual digest calculations</li>
         *  <li>VERIFY: Verifies the auth response from the server</li>
         * </ol>
         *
         * @param   {String} inb The challenge data, decoded from Base64
         * @returns {String} The response data
         */
        evaluateChallenge: function(inb) {
            var inprops, outprops;
            
            if (this.complete && !this._stage) {
                return;
            }
            
            if (!this._stage) {
                jabberwerx.util.debug.log("DIGEST-MD5 in bad stage");
                throw new jabberwerx.SASLMechanism.SASLAuthFailure();
            }
            inprops = this._decodeProperties(inb);
            outprops = this._stage(inprops);
            
            return this._encodeProperties(outprops);
        },
        /**
         * @private
         */
        _generateResponse: function(inprops) {
            var params = this.getProperties();
            var user, pass, domain;
            user = (params.jid && params.jid.getNode()) || "";
            domain = (params.jid && params.jid.getDomain()) || "";
            pass = params.password || "";
            
            var realm = inprops.realm || domain;
            var nonce = inprops.nonce;
            var nc = inprops.nc || "00000001";
            var cnonce = this._cnonce((user + "@" + realm).length);
            var uri = "xmpp/" + domain;
            var qop = "auth";   //no integrity or confidentiality
            
            // calculate A1
            var A1;
            A1 = jabberwerx.util.crypto.rstr_md5(jabberwerx.util.crypto.utf8Encode(user + ":" + realm + ":" + pass));
            A1 = A1 + jabberwerx.util.crypto.utf8Encode(":" + nonce + ":" + cnonce);
            
            // calcuate A2
            var A2;
            A2 = jabberwerx.util.crypto.utf8Encode("AUTHENTICATE:" + uri);
            
            // calculate response
            var rsp = [ jabberwerx.util.crypto.str2hex(jabberwerx.util.crypto.rstr_md5(A1)),
                        nonce,
                        nc,
                        cnonce,
                        qop,
                        jabberwerx.util.crypto.str2hex(jabberwerx.util.crypto.rstr_md5(A2))
                ].join(":");
            rsp = jabberwerx.util.crypto.hex_md5(jabberwerx.util.crypto.utf8Encode(rsp));
            
            var outprops = {
                "charset" : "utf-8",
                "digest-uri" : uri,
                "cnonce": cnonce,
                "nonce": nonce,
                "nc" : nc,
                "qop" : qop,
                "username": user,
                "realm": realm,
                "response": rsp
            };
            
            this._authProps = outprops;
            this._stage = this._verifyRspAuth;
            
            return outprops;
        },
        /**
         * @private
         */
        _verifyRspAuth: function(inprops) {
            if (inprops) {
                inprops = jabberwerx.$.extend({}, inprops, this._authProps || {});
                var params = this.getProperties();
                var user, pass, domain;
                user = (params.jid && params.jid.getNode()) || "";
                domain = (params.jid && params.jid.getDomain()) || "";
                pass = params.password || "";
                
                var realm = inprops.realm || domain;
                var nonce = inprops.nonce;
                var nc = inprops.nc || "00000001";
                var cnonce = inprops.cnonce;
                var uri = "xmpp/" + domain;
                var qop = "auth";   //no integrity or confidentiality
                
                // calculate A1
                var A1;
                A1 = jabberwerx.util.crypto.rstr_md5(jabberwerx.util.crypto.utf8Encode(user + ":" + realm + ":" + pass));
                A1 = A1 + jabberwerx.util.crypto.utf8Encode(":" + nonce + ":" + cnonce);
                
                // calcuate A2
                var A2;
                A2 = jabberwerx.util.crypto.utf8Encode(":" + uri);
                
                // calculate response
                var rsp = [ jabberwerx.util.crypto.str2hex(jabberwerx.util.crypto.rstr_md5(A1)),
                            nonce,
                            nc,
                            cnonce,
                            qop,
                            jabberwerx.util.crypto.str2hex(jabberwerx.util.crypto.rstr_md5(A2))
                    ].join(":");
                rsp = jabberwerx.util.crypto.hex_md5(jabberwerx.util.crypto.utf8Encode(rsp));
                
                if (rsp != inprops.rspauth) {
                    jabberwerx.util.debug.log("response auth values do not match");
                    throw new jabberwerx.SASLMechanism.SASLAuthFailure();
                }
            }
            
            this.complete = true;
            this._stage = null;
        },
        
        /**
         * @private
         */
        _decodeProperties: function(str) {
            var ptn = /([^"()<>\{\}\[\]@,;:\\\/?= \t]+)=(?:([^"()<>\{\}\[\]@,;:\\\/?= \t]+)|(?:"([^"]+)"))/g;
            var props = {};
            var field;
            
            if (!str) {
                str = "";
            }
            while (field = ptn.exec(str)) {
                props[field[1]] = field[2] || field[3] || "";
            }
            
            return props;
        },
        /**
         * @private
         */
        _encodeProperties: function(props) {
            var quoted = {
                "username": true,
                "realm": true,
                "nonce": true,
                "cnonce": true,
                "digest-uri": true,
                "response": true
            };
            var tmp = [];
            
            for (var name in props) {
                var val = quoted[name] ?
                        '"' + props[name] + '"' :
                        props[name];
                tmp.push(name + "=" + val);
            }
            
            return tmp.join(",");
        },
        
        /**
         * @private
         */
        _stage: null,
        /**
         * @private
         */
        _cnonce: function(size) {
            var data = "";
            for (var idx = 0; idx < size; idx++) {
                data += String.fromCharCode(Math.random() * 256);
            }
            
            return jabberwerx.util.crypto.b64Encode(data);
        }
    }, "jabberwerx.SASLDigestMd5Mechanism", "DIGEST-MD5");    
}
