/**
 * filename:        MUCRoom.js
 * created at:      2009/06/05T10:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.MUCOccupant = jabberwerx.Entity.extend(/** @lends jabberwerx.MUCOccupant.prototype */ {
        /**
         * @class
         * <p>Represents an occupant in a Multi-User Chat (MUC) room.</p>
         *
         * @description
         * <p>Creates a new instance of MUCOccupant with the given room and
         * nickname. The created occupant is registered with the owning
         * room's {@link jabberwerx.MUCRoom#occupants} as part of this
         * call.</p>
         *
         * <p><b>NOTE:</b> This type should not be directly created. MUCRooms
         * create occupants as necessary.</p>
         *
         * @param {jabberwerx.MUCRoom} room The owning room
         * @param {String} nick The nickname
         * @throws {TypeError} If {room} is not a valid MUCRoom; or if {nick}
         *         is not a non-empty string
         * @constructs jabberwerx.MUCRoom
         * @extends jabberwerx.Entity
         */
        init: function(room, nick) {
            if (!(room && room instanceof jabberwerx.MUCRoom)) {
                throw new TypeError("room must be a non-null instance of jabberwerx.MUCRoom");
            }
            if (!nick) {
                throw new TypeError("nick must be a non-empty string");
            }

            var jid = new jabberwerx.JID({
                node: room.jid.getNode(),
                domain: room.jid.getDomain(),
                resource: nick
            });

            this._super({jid: jid}, room.occupants);

            this.room = room;
            room.occupants.register(this);
        },

        /**
         * <p>Retrieves the nickname for this MUCOccupant. This is a
         * convenience that always returns the resource portion of the
         * occupant's JID.</p>
         *
         * @returns {String} The nickname
         */
        getNickname: function() {
            return this.jid.getResource();
        },
        /**
         * <p>Retrieves the display name for this MUCOccupant. This method
         * overrides the base to only return {@link #getNickname}.</p>
         *
         * @returns {String} The display name (occupant nickname)
         */
        getDisplayName: function() {
            return this.getNickname();
        },
        /**
         * <p>Override of the base to prevent display name changes.</p>
         *
         * @param {String} [name] The new display name (ignored)
         */
        setDisplayName: function(name) {
            //do nothing
        },

        /**
         * <p>Update presence for this MUCOccupant.</p>
         *
         * <p>This method overrides the base to only retain at most one
         * presence instance. This method always results in both
         * "resourcePresenceChanged" and "primaryPresenceChanged" events
         * being triggered.</p>
         *
         * @param {jabberwerx.Presence} presence The presence to update from
         * @returns {Boolean} <tt>true</tt> if primary presence changed.
         * @throws {TypeError} If {presence} is not a valid availability or
         *         unavailability presence for this entity
         */
        updatePresence: function(presence) {
            if (!(presence && presence instanceof jabberwerx.Presence)) {
                throw new TypeError("must provide a valid non-subscription Presence");
            }

            if (presence.getFrom() != this.jid) {
                throw new TypeError("presence not appropriate to this occupant");
            }

            var type = presence.getType() || "";
            if (type && type != "unavailable") {
                throw new TypeError("must provide a valid non-subscription Presence");
            }

            if (type == "unavailable") {
                this._presenceList.splice(0,1);
            } else if (this._presenceList.length) {
                this._presenceList[0] = presence;
            } else {
                this._presenceList.push(presence);
            }

            this.event("resourcePresenceChanged").trigger({
                fullJid: this.jid,
                presence: presence,
                nowAvailable: false
            });
            this.event("primaryPresenceChanged").trigger({
                fullJid: this.jid,
                presence: (presence.getType() == "unavailable" ? null : presence)
            });

            return true;
        },

        /**
         * <p>Determines if this MUCOccupant represents the current user.</p>
         *
         * @returns {Boolean} <tt>true</tt> if this occupant is for the current
         *          logged-in user.
         */
        isMe: function() {
            return (this.room && this.room.me == this);
        },

        /**
         * <p>The owning MUC Room.</p>
         *
         * @type jabberwerx.MUCRoom
         */
        room: null
    }, "jabberwerx.MUCOccupant");

    jabberwerx.MUCOccupantCache = jabberwerx.EntitySet.extend(/** @lends jabberwerx.MUCOccupantCache.prototype */{
        /**
         * @class
         * <p>The entity cache for MUC room occupants.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.EntitySet">jabberwerx.EntitySet</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.EntitySet_Cache">jabberwerx.EntitySet Cache</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new MUCOccupantCache with the given room.</p>
         *
         * @param {jabberwerx.MUCRoom} room The owning room
         * @throws {TypeError} if {room} is not a valid MUCRoom
         * @constructs jabberwerx.MUCOccupantCache
         * @extends jabberwerx.EntitySet
         */
        init: function(room) {
            if (!(room && room instanceof jabberwerx.MUCRoom)) {
                throw new TypeError("must provide a valid MUCRoom");
            }

            this._super();
            this.room = room;
        },

        /**
         * <p>Registers the given occupant to this cache. This method
         * overrides the base to ensure {entity} is a MUCOccupant, then
         * to trigger the "entityCreated" event once added.</p>
         *
         * @param {jabberwerx.MUCOccupant} entity The occupant to register
         * @throws {TypeError} If {entity} is not a valid MUCOccupant
         */
        register: function(entity) {
            if (!(entity && entity instanceof jabberwerx.MUCOccupant)) {
                throw new TypeError("only MUCOccupants can be registered");
            }

            if (this._super(entity)) {
                this.event("entityCreated").trigger(entity);
            }
        },
        /**
         * <p>Unregisters the given occupant from this cache. This method
         * overrides the base to ensure {entity} is a MUCOccupant, then
         * to trigger the "entityDestroyed" event once removed.</p>
         *
         * @param {jabberwerx.MUCOccupant} entity The occupant to unregister
         * @throws {TypeError} If {entity} is not a valid MUCOccupant
         */
        unregister: function(entity) {
            if (!(entity && entity instanceof jabberwerx.MUCOccupant)) {
                throw new TypeError("only MUCOccupants can be registered");
            }

            if (this._super(entity)) {
                if (this.room.me === entity) {
                    this.room.me = null;
                }
                this.event("entityDestroyed").trigger(entity);
            }
        },

        /**
         * @private
         */
        _clear: function() {
            var that = this;
            jabberwerx.$.each(this.toArray(), function() {
                that.unregister(this);
            });
        },

        /**
         * <p>Renames an occupant to the new JID. This method reindexes the
         * given entity to the new JID, then triggers the "entityRenamed"
         * event.</p>
         *
         * <p><b>NOTE:</b> This method should not be called directly. Instead,
         * the owning room will call this method when a nickname change is
         * detected.</p>
         *
         * @param {jabberwerx.MUCOccupant} entity The occupant to rename
         * @param {jabberwerx.JID|String} njid The new JID for the occupant
         */
        rename: function(entity, njid) {
            this._renameEntity(entity, njid, "");
        },

        /**
         * <p>Retrieves the occupant with the given nickname.</p>
         *
         * @param {String} nick The nickname of the occupant to retrieve
         * @returns {jabberwerx.MUCOccupant} The occupant for {nick}, or
         *          undefined if not found.
         * @throws {TypeError} If {nick} is not a non-empty string.
         */
        occupant: function(nick) {
            if (!nick) {
                throw new TypeError("nickname must be a non-empty string");
            }

            var jid = this.room.jid;
            jid = new jabberwerx.JID({
                node: jid.getNode(),
                domain: jid.getDomain(),
                resource: nick
            });
            return this.entity(jid);
        },

        /**
         * <p>The owning MUCRoom.</p>
         *
         * @type jabberwerx.MUCRoom
         */
        room: null
    }, "jabberwerx.MUCOccupantCache");

    jabberwerx.MUCRoom = jabberwerx.Entity.extend(/** @lends jabberwerx.MUCRoom.prototype */{
        /**
         * @class
         * <p>Represents a Multi-User Chat (MUC) room.</p>
         *
         * <p>This entity has the following in its {@link #properties} map:</p>
         * <table>
         * <tr><th>Name</th><th>Type</th><th>Description</th></tr>
         * <tr><td>subject</td><td>String</td><td>The room's subject</td></tr>
         * </table>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.MUCRoom">jabberwerx.MUCRoom</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new instance of MUCRoom with the given JID and
         * MUCController.</p>
         *
         * <p><b>NOTE:</b> This type should not be directly created. Instead,
         * users should call {@link jabberwerx.MUCController#room} to
         * obtain an instance.</p>
         *
         * @param {jabberwerx.JID|String} jid The JID of the room.
         * @param {jabberwerx.MUCController} ctrl The owning MUCController.
         * @throws {TypeError} If {ctrl} is not a valid MUCController
         * @throws {jabberwerx.JID.InvalidJIDError} if {jid} is not an instance of
         *         jabberwerx.JID, and cannot be converted to one
         *
         * @constructs jabberwerx.MUCRoom
         * @extends jabberwerx.Entity
         */
        init: function(jid, ctrl) {
            if (!(ctrl && ctrl instanceof jabberwerx.MUCController)) {
                throw new TypeError("MUCController must be provided");
            }

            this._super({jid: jabberwerx.JID.asJID(jid).getBareJID()}, ctrl);
            this._state = "offline";
            this.occupants = new jabberwerx.MUCOccupantCache(this);

            // setup events
            this.applyEvent("roomCreated");
            this.applyEvent("roomEntered");
            this.applyEvent("roomExited");
            this.applyEvent("errorEncountered");

            this.applyEvent("beforeRoomBroadcastSent");
            this.applyEvent("roomBroadcastSent");
            this.applyEvent("roomBroadcastReceived");

            this.applyEvent("beforeRoomSubjectChanging");
            this.applyEvent("roomSubjectChanging");
            this.applyEvent("roomSubjectChanged");

            // setup callbacks
            if (this.controller.client) {
                var client = this.controller.client;

                client.event("presenceReceived").bindWhen(
                        "presence[type=error][from^=" + this.jid + "]",
                         this.invocation("_handleRoomErrored"));
                client.event("messageReceived").bindWhen(
                        "message[from^=" + this.jid + "]",
                        this.invocation("_handleMessageReceived"));
                client.event("presenceSent").bindWhen(
                        "presence:not([to]):not([type])",
                        this.invocation("_handlePresenceSent"));
            }
        },
        /**
         * <p>Destroys this MUCRoom. This method overrides the base
         * to unbind callbacks.</p>
         */
        destroy: function() {
            this.exit();
            if (this.controller.client) {
                var client = this.controller.client;

                client.event("presenceReceived").unbind(
                        this.invocation("_handleRoomErrored"));
                client.event("messageReceived").unbind(
                        this.invocation("_handleMessageReceived"));
                client.event("presenceSent").unbind(
                        this.invocation("_handlePresenceSent"));
            }

            this._super();
        },

        /**
         * <p>Retrieves the display name of this MUCRoom. This method
         * overrides the base to always return the node portion of the
         * room's JID.</p>
         *
         * @returns {String} The display name of this Room
         */
        getDisplayName: function() {
            return jabberwerx.JID.unescapeNode(this.jid.getNode());
        },
        /**
         * <p>Override of the base to prevent display name changes.</p>
         *
         * @param {String} [name] The new display name (ignored)
         */
        setDisplayName: function(name) {
            //do nothing
        },

        /**
         * <p>Enters this MUC room. This method enters existing rooms or creates
         * new ones as needed. New rooms are configurable through this method's callbacks</p>
         * <p>{enterArgs} may be either a callback function or an object that may contain mapped functions
         *  and other room specific entry data like a password .
         *  If {enterArgs} is a function it is expected to match the following signature:</p>
         * <p><pre class="code">
         * function callback(err) {
         *     this;    //refers to this room
         *     err;     //if entering failed, this is the jabberwerx.Stanza.ErrorInfo
         *              //explaining the failure, undefined if
         *              //entering succeeded
         * }
         * </pre></p>
         * <p>If {enterArgs} is an object it may define any of the following:</p>
         * <p><pre class="code">
         *      successCallback: function() {
         *          this; //refers to this room
         *      }
         *      errorCallback: function(err, aborted) {
         *          this; //refers to this room
         *          err; //jabberwerx.Stanza.ErrorInfo
         *          aborted //Boolean Room creation was user aborted before configuration was complete
         *      }
         *      configureCallback: function() {
         *          this; //refers to this room
         *      }
         * </pre></p>
         * <p>{enterArgs} may also contain a String password property that contains a plaintext password.  for example:
         * <pre class="code">
         *  room.enter("foo@bar", {password: "foopass"})
         * </pre>
         * The password is sent in the clear. A not-authorized error wil be fired (through errorCallback) 
         * if the password is invalid or not provided) and required.</p>
         * <p> The <code>successCallback</code> callback is called and a "roomEntered" event fired once the room
         *  has been successfully entered or, if a new room, created and configured. If the <code>configureCallback</code> callback
         * is defined and invoked the callee <b>must</b> call {@link #applyConfig} to continue creating the room. A "roomCreated" 
         * event is fired when the room has been created but before configuration. Canceling a configuration during
         * room creation cancels the creation of the room. NOTE <code>successCallback</code> and <code>errorCallback</code> are
         * not called if the create is canceled.
         * If {enterArgs} is a function or <code>configureCallback</code> is not defined the default configuration is used to create the room.
         * </p>
         * <p>If a second enter (with the same nick) is attempted the the method calls successCallback immediately and returns.</p>
         * @param {String} nick The nickname to use in this room
         * @param {Function|Object} [enterArgs] The function (or map of functions)to callback on if
         *        entering succeeded, failed or an intermediate state like configuring.
         * @throws {TypeError} If {enterArgs} is defined and is not a valid function, or an object, or
         *         if {nick} is not a non-empty string.
         * @throws {jabberwerx.MUCRoom.RoomActiveError} If the room
         *         for {jid} is already active, and nickname different
         *         from {nick} was used to enter it or if room is currently trying to enter.
         * @throws {jabberwerx.Client.NotConnectedError} If the client
         *         is not connected
         */
        enter: function(nick, enterArgs) {
            var cbmap = enterArgs ? (jabberwerx.$.isFunction(enterArgs) ? {successCallback: enterArgs, errorCallback: enterArgs} : enterArgs) : {};

            //defined callbacks must be functions
            if (enterArgs &&
                ((cbmap.successCallback && !jabberwerx.$.isFunction(cbmap.successCallback)) ||
                 (cbmap.errorCallback && !jabberwerx.$.isFunction(cbmap.errorCallback)) ||
                 (cbmap.configureCallback && !jabberwerx.$.isFunction(cbmap.configureCallback)))) {
                    throw new TypeError("Defined callback must be a function");
            }
            if (!nick || !jabberwerx.util.isString(nick)) {
                throw new TypeError("Nick must be a non-empty string");
            }
            if (!this.controller.client.isConnected()) {
                throw new jabberwerx.Client.NotConnectedError();
            }
            //error if attempting an enter while attempting an enter
            if (this._state == "initializing") {
                throw new jabberwerx.MUCRoom.RoomActiveError();            
            } else if (this.isActive()) {
                //error if a second attempt at entry while already in room, success immediately if entering w/ same nick
                if (this.me && (this.me.getNickname() != nick)) {
                    throw new jabberwerx.MUCRoom.RoomActiveError();
                }
                if (cbmap.successCallback) {
                    cbmap.successCallback.call(this);
                }
                return;
            }

            var that = this;
            this._state = "initializing";

            var fn = function(evt) {
                if (evt.name == "roomcreated") {
                    //swap applyConfig to create-time wrapper
                    that._origApply = that.applyConfig;
                    that.applyConfig = that._creationApply;

                    if (cbmap.configureCallback) {
                        cbmap.configureCallback.call(that);
                    } else {
                        //submit empty form to accept defaults, fires roomEntered
                        that.applyConfig(new jabberwerx.XDataForm("submit"));
                    }
                } else {                    
                    if (evt.name == "errorencountered") {
                        that._state = "offline"; //prevent roomExited                     
                        that.exit();
                        if (cbmap.errorCallback) {
                            cbmap.errorCallback.call(that, 
                                jabberwerx.Stanza.ErrorInfo.createWithNode(evt.data.error), 
                                evt.data.aborted);
                        }
                    } else if (evt.name == "roomentered" && cbmap.successCallback) {
                        cbmap.successCallback.call(that);
                    }
                    //cleanup eventhandlers on error, entered or create cancel, all stop entering.
                    that.event("errorEncountered").unbind(arguments.callee);
                    that.event("roomEntered").unbind(arguments.callee);
                    that.event("roomCreated").unbind(arguments.callee);
                }
            };
            this.event("errorEncountered").bind(fn);
            this.event("roomEntered").bind(fn);
            this.event("roomCreated").bind(fn);

            //finally send initial presence
            this.me = new jabberwerx.MUCOccupant(this, nick);
            var stanza = this.controller.client.getCurrentPresence();
            if (stanza) {
                stanza = stanza.clone();
            } else {
                //no current presence, gen up an available for the room
                stanza = new jabberwerx.Presence();
                stanza.setPresence('', '');
            }
            stanza.setTo(this.me.jid);
            var builder = new jabberwerx.NodeBuilder(stanza.getNode());
            var xtag = builder.element("{http://jabber.org/protocol/muc}x");
            if (enterArgs && enterArgs.password) {
                xtag.element("password").text(enterArgs.password);
            }
            this.controller.client.sendStanza(stanza);
        },
        /**
         * <p>Exits this room. This method sends the unavailable presence
         * to the room, and eventually triggers a "roomExited" event.</p>
         */
        exit: function() {
            if (this.isActive() && this.controller.client.isConnected()) {
                var stanza = new jabberwerx.Presence();
                stanza.setTo(this.me.jid);
                stanza.setType("unavailable");
                this.controller.client.sendStanza(stanza);
            } else if (this.me) {
                if (this.isActive()) {
                    this._state = "offline";
                    this.event("roomExited").trigger();
                }
                this.me.remove();
            }
        },

        /**
         * <p>Changes the current user's nickname with the room. This
         * method sends the necessary protocol, that ultimately results
         * an "entityRenamed" event being triggered for the user's
         * occupant.</p>
         *
         * <p>The optional function {cb} is expected to match the
         * following signature:</p>
         * <p><pre class="code">
         * function callback(err) {
         *     this;    //refers to this room
         *     err;     //if entering failed, this is the error
         *              //explaining the failure, undefined if
         *              //entering succeeded
         * }
         * </pre></p>
         *
         * <p>If the user's nickname in the room is already {nick}, this
         * method does nothing.</p>
         *
         * @param {String} nick The new nickname
         * @param {Function} [cb] The function to callback on when
         *        the change nickname attempt succeeds or fails.
         * @throws {TypeError} if {cb} is not a valid function or undefined;
         *         or if {nick} is not a non-empty string
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} if the room is
         *         not currently active.
         */
        changeNickname: function(nick, cb) {
            if (this.me && this.me.getNickname() == nick) {
                //short circuit
                return;
            }

            if (!nick) {
                throw new TypeError("nickname must be a non-empty string");
            }
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be a function or undefined");
            }
            if (!this.isActive()) {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }

            var jid = new jabberwerx.JID({
                node: this.jid.getNode(),
                domain: this.jid.getDomain(),
                resource: nick
            });
            var stanza = this.controller.client.getCurrentPresence().clone();
            stanza.setTo(jid);

            if (cb) {
                var that = this;
                var fn = function(evt) {
                    var err = (evt.name == "errorencountered") ?
                        evt.data :
                        undefined;

                    if (err || (evt.name == "entityrenamed" && evt.data === that.me)) {
                        cb.call(that, err);
                        that.event("errorEncountered").unbind(arguments.callee);
                        that.occupants.event("entityRenamed").unbind(arguments.callee);
                    }
                };
                this.event("errorEncountered").bind(fn);
                this.occupants.event("entityRenamed").bind(fn);
            }
            this.controller.client.sendStanza(stanza);
        },
        /**
         * <p>Changes or clears the subject in the room. This method
         * sends the necessary protocol, that results in the following
         * events:</p>
         *
         * <ol>
         * <li>triggers a "beforeRoomSubjectChanging" event with the candidate
         * jabberwerx.Message, just prior to sending; This allows for the
         * subject changing protocol to be modified.</li>
         * <li>triggers a "roomSubjectChanging" event just after the subject
         * changing protocol is sent.</li>
         * <li>triggers a "roomSubjectChanged" event when the room broadcasts
         * the change to all occupants (including the current user).</li>
         * <li>triggers an "entityUpdated" event after the local subject
         * property is changed.</li>
         * </ol>
         *
         * <p>The optional function {cb} is expected to match the
         * following signature:</p>
         * <p><pre class="code">
         * function callback(err) {
         *     this;    //refers to this room
         *     err;     //if entering failed, this is the error
         *              //explaining the failure, undefined if
         *              //entering succeeded
         * }
         * </pre></p>
         *
         * @param {String} subject The new subject
         * @param {Function} [cb] The function to callback on when
         *        the change subject attempt succeeds or fails.
         * @throws {TypeError} if {cb} is not a valid function or undefined
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} if the room is
         *         not currently active.
         */
        changeSubject: function(subject, cb) {
            if (cb && !jabberwerx.$.isFunction(cb)) {
                throw new TypeError("callback must be a function or undefined");
            }
            if (!this.isActive()) {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }

            var stanza = new jabberwerx.Message();
            // TODO: attach ID for tracking?
            stanza.setTo(this.jid);
            stanza.setType("groupchat");
            stanza.setSubject(subject || "");

            // Changing a room's subject to nothing doesn't actually change it in XCP (oh well)

            if (cb) {
                var that = this;
                var fn = function(evt) {
                    var err = (evt.name == "errorencountered") ?
                            evt.data :
                            undefined;

                    cb.call(that, err);
                    that.event("errorEncountered").unbind(arguments.callee);
                    that.event("roomSubjectChanged").unbind(arguments.callee);
                };
                this.event("errorEncountered").bind(fn);
                this.event("roomSubjectChanged").bind(fn);
            }

            this.event("beforeRoomSubjectChanging").trigger(stanza);
            this.controller.client.sendStanza(stanza);
            this.event("roomSubjectChanging").trigger(stanza);
        },
        /**
         * <p>Sends a broadcast message to the room. This method sends the
         * necessary protocol, and results in the following events:</p>
         *
         * <ol>
         * <li>triggers a "beforeRoomBroadcastSent" event with the candidate
         * jabberwerx.Message, just prior to sending; This allows for the
         * broadcast protocol to be modified.</li>
         * <li>triggers a "roomBroadcastSent" event just after the broadcast
         * protocol is sent.</li>
         * <li>triggers a "roomBroadcastReceived" event when the room
         * broadcasts the message to all occupants (including the current
         * user).</li>
         * </ol>
         *
         * <tt>msg</tt> may be plaintext, a root HTML element, a body element 
         *  in the XHTML namespace (see xep-71) or an array of HTML tags.         
         *  see {@link jabberwerx.Message#setHTML} for additional information.
         *
         * @param {String|DOM|Array} msg The message to send
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} if the room is
         *         not currently active.
         */
        sendBroadcast: function(msg) {
            if (!msg) {
                //let's be silent on this one...
                return;
            }
            if (!this.isActive()) {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }

            var stanza = new jabberwerx.Message();
            stanza.setTo(this.jid);
            stanza.setType("groupchat");
            if (jabberwerx.isElement(msg) || jabberwerx.$.isArray(msg)) {
                stanza.setHTML(msg);
            } else {
                stanza.setBody(msg);
            }

            this.event("beforeRoomBroadcastSent").trigger(stanza);
            this.controller.client.sendStanza(stanza);
            this.event("roomBroadcastSent").trigger(stanza);
        },

        /**
         * <p>Retrieves the primary presence for this entity. This
         * implementation always returns the presence for the current
         * user in the room, or <tt>null</tt> if the room is not
         * active.</p>
         *
         * @returns {jabberwerx.Presence} The primary presence for the room
         */
        getPrimaryPresence: function() {
            if (this.me) {
                return this.getResourcePresence(this.me.getNickname());
            }

            return null;
        },
        /**
         * <p>Determines if this room is currently active. This implementation
         * returns <tt>true</tt> an occupant for the current user exists, and
         * has an available presence for that occupant.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the room is active
         */
        isActive: function() {
            return Boolean(this._state == "active");
            return Boolean(this.getPrimaryPresence());
        },

        /**
         * <p>Updates for this Entity. This method overrides the base to update
         * presence for the room's occupants via
         * {@link jabberwerx.MUCOccupant#updatePresence}, with the following
         * additional actions:</p>
         *
         * <ul>
         * <li>If the presence is an available presence for a previously
         * non-existent occupant, a new MUCOccupant is added (resulting in
         * an "entityCreated" event).</li>
         * <li>If the presence is an unavailable presence for a previously
         * existent occupant (and is not part of a nickname change), that
         * MUCOccupant is removed (resulting in an "entityDestroyed"
         * event).</li>
         * <li>If the presence is for a nickname change, the associated
         * entity's JID is changed, and an "entityRenamed" event
         * triggered.</li>
         * <li>If the presence is an available presence for the current user
         * and indicates the room is pending configuration, teh "instant room"
         * (or "accept defaults") protocol from XEP-0045 is sent to unlock
         * the room.</li>
         * <li>If the presence is an unavailable presence for the current user,
         * a "roomExited" event is triggered.</li>
         * </ul>
         *
         * @param {jabberwerx.Presence} presence The presence to update from
         * @returns {Boolean} <tt>true</tt> if the presence updated the current
         *          user's occupant
         * @throws {TypeError} If {presence} is not appropriate for this
         *         MUCRoom
         */
        updatePresence: function(presence) {
            if (!(presence && presence instanceof jabberwerx.Presence)) {
                throw new TypeError("must provide a valid non-subscription Presence");
            }

            var jid = presence.getFromJID();
            if (jid.getBareJIDString() != this.jid) {
                throw new TypeError("presence not appropriate to this room");
            }

            var type = presence.getType() || "";
            if (type && type != "unavailable") {
                throw new TypeError("must provide a valid non-subscription Presence");
            }

            var node = jabberwerx.$(presence.getNode()).children("x[xmlns^=http://jabber.org/protocol/muc]");
            var item = node.children("item");
            var status = node.children("status");

            var occupant = this.occupants.entity(jid);
            var result = (occupant && occupant.isMe());
            this._removePresenceFromList(jid);
            switch (type) {
                case "":
                    this._presenceList.push(presence);
                    if (!occupant) {
                        occupant = new jabberwerx.MUCOccupant(this, jid.getResource());
                    }

                    var prsCount = occupant.getAllPresence().length;
                    occupant.updatePresence(presence);
                    if (occupant.isMe() && prsCount == 0) {
                        //This is "me" entering the room
                        //check each status for created code
                        if (node.children("status[code=201]").length == 1) {
                            //trigger created event to handle configuration
                            this.event("roomCreated").trigger();
                            return; //no presence eventing till creation is done
                        } else {
                            // Room already exists on server, finish enter
                            this._state = "active";
                            this.event("roomEntered").trigger();
                        }
                    }

                    break;
                case "unavailable":
                    if (occupant && occupant.getPrimaryPresence() !== null) {
                        // this is actually an occupant leaving the room...
                        if (status.attr("code") == "303") {
                            // Nickname change in progress
                            var nnick = item.attr("nick");
                            var onick = occupant.nick;
                            var njid = new jabberwerx.JID({
                                node: this.jid.getNode(),
                                domain: this.jid.getDomain(),
                                resource: nnick
                            });

                            // remember presence so room still appears active
                            var tmpprs = presence.clone();
                            //prs.setFrom(njid.toString());
                            //prs.setType();
                            //this._presenceList.push(presence);
                            this.occupants.rename(occupant, njid);
                            // not really unavailable, so suppress primary update
                            result = false;
                        } else {
                            var myself = occupant.isMe();
                            occupant.updatePresence(presence);
                            if (myself) {
                                this._state = "offline";
                                this.event("roomExited").trigger();
                                this.occupants._clear();
                                this._presenceList = [];
                            }
                            occupant.destroy();
                        }

                    }
                    break;
            }

            this.event("resourcePresenceChanged").trigger({
                fullJid: jid,
                presence: presence,
                nowAvailable: false
            });
            if (result) {
                this.event("primaryPresenceChanged").trigger({
                    fullJid: (type != "unavailable") ? jid : this.jid,
                    presence: (type != "unavailable") ? presence : null
                });
            }

            return result;
        },

        /**
         * <p>Send an invite from this room to specified jids.</p>
         *
         * <p>Send an invite to each specified jid using the given reason. Invite may
         *  be sent through the room (via XEP 0045 7.5.2) or directly to the given jids
         *  (via xep 0249).  Invalid jids are ignored. This method will return a list of all
         *  jids sent invites.
         *  {reason} defaults to {@link jabberwerx.MUCRoom.DefaultInviteReason} </p>
         *
         *  @todo once client caps are fully implemented caps will be checked for direct
         *              invite support before attempting direct invite.
         *  @todo once we support room configurations room will be checked before attempting
         *              mediated invite.
         *  @todo future implementation will use client caps, room configuration and
         *              mediated flag to pick the best send invite method.
         *
         * @param {JID | String | JID[] | String[]} toJids List or single (full or bare) jid(s)
         * @param {String} [reason] Message to send along with invite
         * @param {Boolean} [mediated] Send invite through conference server if possible
         * @returns {JID[]} List of bare jids we succesfully sent invites
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} if the room is
         *         not currently active.
         */
        invite: function(toJids, reason, mediated) {
            if (!this.isActive()) {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }
            var result = [];
            if (!reason) {
                reason = jabberwerx.MUCRoom.DefaultInviteReason;
            }
            if (!toJids) {
                toJids = [];
            } else if (!jabberwerx.$.isArray(toJids)) {
                toJids = [toJids];
            }

            //template message
            var iMsg = new jabberwerx.Message();
            var updateMessage = function(jid) {
                return iMsg;
            }
            //todo if mediated, check if room allows invites && we have enough privs, throw exception
            if (mediated) {
                //only "message/x/invite to"  changes with each jid, everything else static
                /*
                                     * mediated protocol
                                     * <message
                                     *       from='crone1@shakespeare.lit/desktop'
                                     *       to='darkcave@chat.shakespeare.lit'>
                                     *   <x xmlns='http://jabber.org/protocol/muc#user'>
                                     *       <invite to='hecate@shakespeare.lit'>
                                     *           <reason>
                                     *               Hey Hecate, this is the place for all good witches!
                                     *           </reason>
                                     *       </invite>
                                     *   </x>
                                     * </message>
                                    */

                iMsg.setTo(this.jid);
                var xe = new jabberwerx.NodeBuilder(iMsg.getNode()).
                        element('{http://jabber.org/protocol/muc#user}x').
                        element('invite');
                xe.element('reason').text(reason);
                updateMessage = function(jid) {
                    xe.attribute('to', jid.toString());
                    return iMsg;
                };
            } else {
                //only "/message to" changes with each jid, everything else static
                /*
                                     *
                                     * direct protocol
                                     * <message
                                     *       from='crone1@shakespeare.lit/desktop'
                                     *       to='hecate@shakespeare.lit'>
                                     *   <x xmlns='jabber:x:conference'
                                     *           jid='darkcave@macbeth.shakespeare.lit'
                                     *           reason='Hey Hecate, this is the place for all good witches!'/>
                                     * </message>
                                     */
                new jabberwerx.NodeBuilder(iMsg.getNode()).
                        element('{jabber:x:conference}x').
                        attribute("reason", reason).
                        attribute("jid", this.jid.getBareJID().toString());
                updateMessage = function(jid) {
                    iMsg.setTo(jid);
                    return iMsg;
                };
            }

            for (var i = 0; i < toJids.length; i++) {
                //todo determine best invite method on a per jid basis?
                try {
                    var tjid = jabberwerx.JID.asJID(toJids[i]).getBareJID();
                    this.controller.client.sendStanza(updateMessage(tjid));
                    result.push(tjid);
                }
                catch (ex)  {
                    if (!(ex instanceof jabberwerx.JID.InvalidJIDError)) {
                        throw ex;
                    }
                    //just eat jabberwerx.JID.InvalidJIDError
                }
            }

            return result;
        },


        /**
         * <p>Fetch the room's configuration.
         * Fetch the room's configuration {@link jabberwerx.XDataForm}  and fire callback on result.
         * The callback is passed either the fetched form or an error. </p>
         * <p>The optional function {configCallback} is expected to match the
         * following signature:</p>
         * <p><pre class="code">
         * function callback(form, err) {
         *     form;    //the fetched {@link jabberwerx.XDataForm}
         *                  //null if fetch failed
         *     err;       //Error explaining the failure,
         *                  //undefined if fetch succeeded
         * }
         * </pre>
         * Errors may be
         * {@link jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT}  if the fetch times out,
         * or any error node found in the fetch result.</p>
         * @param {Function} configCallback Callback fired when fetch is completed.
         * @throws {TypeError} If callback is undefined or not a function.
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} If room has not entered (or attempted entry)
         */
         fetchConfig: function(configCallback) {
            if (!jabberwerx.$.isFunction(configCallback)) {
                throw new TypeError("fetchConfig requires a callback function");
            }
            //fetch may be called if initializing or active
            if (this._state == "offline") {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }
            var qb = new jabberwerx.NodeBuilder('{http://jabber.org/protocol/muc#owner}query').data;
            this.controller.client.sendIQ(
                "get",
                this.jid,
                qb,
                function (stanza) {
                    if (!stanza) {
                        configCallback(null, jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT);
                    } else {
                        var iq = new jabberwerx.IQ(stanza);
                        if (iq.isError()) {
                            configCallback(null, iq.getErrorInfo());
                        } else {
                            var frm = jabberwerx.$("x",iq.getNode()).get(0);
                            if (!frm) {
                                configCallback(null, jabberwerx.Stanza.ERR_SERVICE_UNAVAILABLE);
                            } else {
                                configCallback(new jabberwerx.XDataForm(null, frm));
                            }
                        }
                    }
                });
         },

        /**
         * <p>Set the room's configuration.
         * Use the given {@link jabberwerx.XDataForm} (or a generated cancel form if null) to
         * update the room's configuration. Fires the optional callback on result. </p>
         * <p>The optional function {configCallback} is expected to match the
         * following signature:</p>
         * <p><pre class="code">
         * function callback(err) {
         *     err;       //Error explaining the failure,
         *                  //undefined if set succeeded
         * }
         * </pre>
         * Errors may be
         * {@link jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT}  if the attempt times out,
         * or any error node found in the result.</p>
         * @param  {jabberwerx.XDataForm} configForm The xdata configuration to set, may be null.
         * @param {Function} [configCallback] Callback fired on attempt completion.
         * @throws {TypeError} If callback is defined but not a function or form is defined but not an XDataForm.
         * @throws {jabberwerx.MUCRoom.RoomNotActiveError} If room is not entered (or attempting entry)
         */
        applyConfig: function(configForm, configCallback) {
            if (configForm && !(configForm instanceof jabberwerx.XDataForm)) {
                throw new TypeError("configForm must be null or an XDataForm");
            }
            if (configCallback && !jabberwerx.$.isFunction(configCallback)) {
                throw new TypeError("A defined configCallback must be a function");
            }
            if (this._state == "offline") {
                throw new jabberwerx.MUCRoom.RoomNotActiveError();
            }
            
            var cancel = (!configForm || configForm.getType() == "cancel");
            if (cancel && configCallback) {
                    configCallback();
                }
            
            var that = this;
            var iqcb = cancel ? 
                function (stanza) {} : //ignore results if canceling
                function (stanza) {
                var err;
                if (!stanza) {
                    err = jabberwerx.Stanza.ERR_REMOTE_SERVER_TIMEOUT;
                } else {
                        err = new jabberwerx.IQ(stanza).getErrorInfo(); 
                }
                if (configCallback) {
                    configCallback(err)
                }
            };

            configForm = configForm ? configForm : new jabberwerx.XDataForm("cancel");
            var nb = new jabberwerx.NodeBuilder('{http://jabber.org/protocol/muc#owner}query');
            nb.node(configForm.getDOM().data);

            this.controller.client.sendIQ("set", this.jid, nb.data, iqcb);
        },

        /**
         * @private
         */
        _handlePresenceSent: function(evt) {
            if (this.isActive()) {
                var stanza = evt.data.clone();
                stanza.setTo(this.me.jid);

                this.controller.client.sendStanza(stanza);
            }
        },
        /**
         * @private
         */
        _handleRoomErrored: function(evt) {
            var data = evt.data;
            var err = jabberwerx.$(data.getNode()).children("error").get(0);
            var op = "";

            // try to guess the operation...
            switch(data.pType()) {
                case "message":
                    if (data.getSubject()) {
                        op = "changeSubject";
                    } else if (data.getBody()) {
                        op = "sendBroadcast";
                    }
                    break;
                case "presence":
                    if (this.isActive()) {
                        op = "changeNickname";
                    } else {
                        op = "enter";
                    }
                    break;
            }

            if (err) {
                this.event("errorEncountered").trigger({
                    operation: op,
                    error: err
                });
            }
            return true;
        },
        /**
         * @private
         */
        _handleMessageReceived: function(evt) {
            var stanza = evt.data;

            switch (stanza.getType()) {
                case "error":
                    this._handleRoomErrored(evt);
                    break;
                case "groupchat":
                    {
                        var subject = stanza.getSubject();
                        if (subject !== null) {
                            this.properties.subject = subject;
                            this.update();
                            this.event("roomSubjectChanged").trigger(stanza);
                        } else {
                            var body = jabberwerx.$.trim(stanza.getBody());
                            if (body) {
                                this.event("roomBroadcastReceived").trigger(stanza);
                            }
                        }
                    }
                    break;
                case "chat":
                    if (this.controller.client) {
                        var entity = this.occupants.entity(stanza.getFrom());
                        var chat = this.controller.client.controllers.chat;

                        if (chat) {
                            if (!chat.getSession(entity.jid)) {
                                var session = chat.openSession(entity.jid);
                                session.event('chatReceived').trigger(stanza);
                            }
                        }
                    }
                    break;
                default:
                    break;
            }

            return true;
        },

        /**
         * @private
         * applyConfig used when creating a room. method maps applyConfig back to the original on
         * sucessfull configuration or cancel. This method remains mapped through entry errors
         */
        _creationApply: function (configForm, configCallback) {
            if (!configForm || (configForm.getType() == "cancel")) {
                //aborting create/enter
                this.applyConfig = this._origApply;
                delete this._origApply;
                
                //immediatly callback and then finish off exit
                if (configCallback) {
                    configCallback();
                }
                //submit canceled form, ignore results
                this.applyConfig(configForm);
                //send unavail to finish off room destruction
                var stanza = new jabberwerx.Presence();
                stanza.setTo(this.me.jid);
                stanza.setType("unavailable");
                this.controller.client.sendStanza(stanza);
                //cleanup occupants to prevent normal updatePresence(unavailable) behavior
                this.occupants._clear();
                this._presenceList = [];
                //fire errorEncountered aborted event, eventually fires enter error callback
                this.event("errorEncountered").trigger({
                    operation: "enter",
                    error: jabberwerx.Stanza.ERR_BAD_REQUEST.getNode(),
                    aborted: true                            
                });                    
            } else { //submit the form by calling chained apply
            var that = this;
            this._origApply.call(this, configForm, function (cerr) {
                    if (configCallback) {
                        configCallback(cerr);
                    }
                    //switch to entered applyConfig on successfull apply 
                    if (!cerr) {
                        that.applyConfig = that._origApply;
                        delete that._origApply;  
                        
                        that._state = "active";
                        that.event("roomEntered").trigger();
                        //fire delayed "me" presence events (see updatePresence)
                        that._presenceList[0]
                        that.event("resourcePresenceChanged").trigger({
                            fullJid: that.me.jid,
                            presence: that._presenceList[0],
                            nowAvailable: false
                        });
                        that.event("primaryPresenceChanged").trigger({
                            fullJid: that.me.jid,
                            presence: that._presenceList[0]
                        });
                    } //else state remains the same
                });
                    }
        },

        /**
         * @private
         * State of room
         *      offline - only enter and exit (nop) may be invoked
         *      initializing - Attempting to create/join. fetchConfig,applyConfig, exit may be invoked
         *      active - room is fully entered. all methods may be invoked
         * @type String
         */
        _state: "offline",

        /**
         * <p>The cache of MUCOccupants for this room. This is the source for
         * the "entityCreated", "entityUpdated", "entityRenamed", and
         * "entityDestroyed" events on MUCOccupants.</p>
         *
         * @type jabberwerx.MUCOccupantCache
         */
        occupants: null,
        /**
         * <p>The MUCOccupant representing the current user.</p>
         *
         * @type jabberwerx.MUCOccupant
         */
        me: null
    }, "jabberwerx.MUCRoom");

    /**
     * <p>Error thrown when attempting to enter a room that is already
     * active.</p>
     */
    jabberwerx.MUCRoom.RoomActiveError = jabberwerx.util.Error.extend("room already active");
    /**
     * <p>Error thrown when attempting operations on an inactive room.</p>
     */
    jabberwerx.MUCRoom.RoomNotActiveError = jabberwerx.util.Error.extend("room is not active");

    /**
     * @constant
     * Default reason to use with invites when one is not specified
     */
    jabberwerx.MUCRoom.DefaultInviteReason = "You have been invited to a conference room.";

    /**
     * @constant
     * Default phrase to send when changing the subject of a room
     */
    jabberwerx.MUCRoom.DefaultSubjectChange = "has changed the subject to: {0}";
}
