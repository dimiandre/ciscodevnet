/**
 * filename:        Translator.js
 * created at:      2010/04/28T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx) {
    jabberwerx.Translator = jabberwerx.JWBase.extend( /** @lends jabberwerx.Translator.prototype */{
        /**
         * @class
         * @minimal
         * <p>Performs localization of strings.</p>
         *
         * @description
         * <p>Creates a new Translator.</p>
         *
         * <p><b>NOTE:</b> This class should not be created directly. Instead,
         * use the global localizing method @{link jabberwerx._}.</p>
         *
         * @constructs jabberwerx.Translator
         * @extends jabberwerx.JWBase
         */
        init: function() {
            this._super();
        },
        
        /**
         * <p>Localizes and formats the given input string.</p>
         *
         * <p>This method attempts to find a localized version of {istr} in
         * a table. If one is found, that localized form is used for the rest
         * of this; otherwise {istr} is used as-is. Then the string is searched
         * for substitutions, and the results are returned.</p>
         *
         * <p>Substitutions take the form "{&lt;number&gt;}", where &lt;number&gt;
         * is the index of the arugment passed into this method. For example:</p>
         *
         * <pre class='code'>
         *  var l10n = new jabberwerx.Translator()'
         *  var result = l10n.format("Connected to {0} as {1}",
         *                           client.connectedServer.jid,
         *                           client.connectedUser.jid);
         * </pre>
         *
         * <p>Returns a string where "{0}" is replaced with the string value of
         * "client.connectedServer.jid" and "{1}" is replaced with the string
         * of "client.connectedUser.jid". If a matching argument is not found,
         * the original pattern is returned (e.g. "{0}").</p>
         *
         * @param   {String} istr The input string
         * @returns {String} The formatted/localized string
         */
        format: function(istr /** params **/) {
            var ostr = this._updates[istr] || this._mappings[istr];
            if (!ostr) {
                ostr = istr;
            }
            
            var ptn = /\{([0-9]+)\}/g;
            var args = jabberwerx.$.makeArray(arguments).slice(1);
            var substFn = function(match, idx) {
                idx = parseInt(idx);
                if (isNaN(idx)) {
                    return match;
                }
                
                var found = args[idx];
                if (found === null || found === undefined) {
                    return match;
                }
                
                return found;
            };
            var ostr = ostr.replace(ptn, substFn);
            
            return ostr;
        },
        /**
         * <p>Loads a translation table for the given locale.</p>
         *
         * <p>The translation table is expected to be a JSON-formatted
         * map of keys to values</p>:
         *
         * <pre class='code'>
         *  {
         *      "first key" : "The First Key",
         *      "service {0} unavailable" : "The service {0} is not available at this time.  Please try again later"
         *  }
         * </pre>
         *
         * <p>Translation are declared via &lt;link/&gt; elements within the
         * HTML page, with the type 'text/javascript' and the rel 'translation',
         * and optionally an xml:lang to declare the locale it represents. For
         * example, a tranlsation table for American English (en-US) would be
         * declared in the HTML as follows:</p>
         *
         * <pre class='code'>
         *  &lt; link rel='translation' type='text/javascript' xml:lang='en-US' href='path/to/tranlsation.js'/&gt;
         * </pre>
         *
         * <p>The lookup attempts to find the best match for {locale} using
         * the following algorithm:</p>
         *
         * <ol>
         * <li>The specific value for {locale}, if specified (e.g. en-US)</li>
         * <li>The language-only value for {locale}, if specified (e.g. en)</li>
         * <li>A default (no xml:lang declared on &lt;link/&gt;)</li>
         * </ol>
         *
         * @param   {String} [locale] The locale to load, or "" to
         *          use the platform default.
         * @throws  {Error} If a translation table for {locale}
         *          cannot be found or loaded
         */
        load: function(locale) {
            if (!locale) {
                // make best attempt at determining the user's locale
                locale = (jabberwerx.$.browser.msie) ?
                         navigator.userLanguage :
                         navigator.language;
            }
            if (this.locale == locale) {
                // already loaded this translation, return
                return;
            }
            
            // Function to create a function that filters
            var filterFN = function(l) {
                return function() {
                    var lang = (jabberwerx.$(this).attr("xml:lang") || "").toLowerCase();
                    return (lang == l) ? this : null;
                };
            };
            
            // Find matches, grouped by specificity
            var localeFull = locale.toLowerCase();
            var localePart = locale.split("-")[0].toLowerCase();
            var tmpLinks = jabberwerx.$("link[rel='translation'][type='text/javascript']");
            var links = jabberwerx.$();
            links = jabberwerx.$.merge(links, tmpLinks.map(filterFN("")));
            links = jabberwerx.$.merge(links, tmpLinks.map(filterFN(localePart)));
            links = jabberwerx.$.merge(links, tmpLinks.map(filterFN(localeFull)));
            
            if (!links.length) {
                throw new TypeError("no translation links found");
            }
            
            var mappings = {};
            var processed = 0;
            links.each(function() {
                var url = jabberwerx.$(this).attr("href");
                if (!url) {
                    // TODO: loggit
                    return true;
                }
                var data = null;
                var completeFn = function(xhr, status) {
                    if (status != "success") { return; }
                    data = xhr.responseText;
                };
                var setup = {
                    async: false,
                    cache: true,
                    complete: completeFn,
                    dataType: "text",
                    processData: false,
                    timeout: 1000,
                    url: url
                };
                jabberwerx.$.ajax(setup);
                if (!data) {
                    jabberwerx.util.debug.log("no translation data returned from " + url);
                }
                try {
                    data = eval("(" + data + ")");
                } catch (ex) {
                    jabberwerx.util.debug.log("could not parse translation data from " + url);
                }
                mappings = jabberwerx.$.extend(mappings, data);
                processed++;
            });
            
            if (!processed) {
                throw new TypeError("no valid translations found");
            }
            this._mappings = mappings;
            this.locale = locale;
        },
        
        /**
         * <p>Adds or updates the translation for the given key.</p>
         *
         * @param   {String} key The key to translate on
         * @param   {String} value The new replacement translation
         * @throws  {TypeError} If {key} or {value} are not valid Strings
         */
        addTranslation: function(key, value) {
            if (!(key && typeof(key) == "string")) {
                throw new TypeError();
            }
            if (!(value && typeof(value) == "string")) {
                throw new TypeError();
            }
            this._updates[key] = value;
        },
        /**
         * <p>Removes the translation for the given key.</p>
         *
         * @param   {String} key The key to translate on
         * @throws  {TypeError} If {key} or is not a valid String
         */
        removeTranslation: function(key) {
            if (!(key && typeof(key) == "string")) {
                throw new TypeError();
            }
            delete this._updates[key];
        },
        
        /**
         * The current locale for this jabberwerx.Translator
         *
         * @type String
         * @see jabberwerx.Translator#format
         */
        locale: undefined,
        
        /**
         * @private
         */
        _mappings: {},
        /**
         * @private
         */
        _updates: {}
    }, "jabberwerx.Translator");
    
    /**
     * <p>The global translator instance. Use this instead of
     * creating new instances of Translator.</p>
     *
     * @type jabberwerx.Translator
     */
    jabberwerx.l10n = new jabberwerx.Translator();
    
    /**
     * @function
     * @description
     * <p>Localizes and formats the given input string. This method performs
     * the same operations as {@link jabberwerx.Translator#format}, but as a
     * singleton method.</p>
     *
     * @param   {String} istr The input string
     * @returns {String} The formatted/localized string
     */
    jabberwerx._ = (function(l10n) {
        var fn;
        fn = function() {
            return l10n.format.apply(l10n, arguments);
        };
        fn.instance = l10n;
        
        return fn;
    })(jabberwerx.l10n);
    
    // auto-load translation for platform locale
    jabberwerx.$(document).ready(function() {
        var locale = navigator.language;
        try {
            jabberwerx.l10n.load(locale);
            // DEBUG-BEGIN
            jabberwerx.util.debug.log("Loaded translation for " + locale);
            // DEBUG-END
        } catch (e) {
            // log the failure to load a default
            jabberwerx.util.debug.log("Could not find a translation for " + locale);
        }
    });
}
