/**
 * filename:        Client.js
 * created at:      2009/00/00T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

    if (jabberwerx) {
        jabberwerx.ClientEntityCache = jabberwerx.EntitySet.extend(/** @lends jabberwerx.ClientEntityCache.prototype */ {
            /**
             * @class
             * @minimal
             * <p>The client's collection of entities. This is the central
             * repositories for entities within a client.</p>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.EntitySet">jabberwerx.EntitySet</a></li>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.EntitySet_Cache">jabberwerx.EntitySet Cache</a></li>
             * </ul>
             *
             * @description
             *
             * @extends jabberwerx.EntitySet
             * @constructs jabberwerx.GlobalEntityCache
             */
            init: function() {
                this._super();
            },

            /**
             * @private
             */
            _batchCount: 0,
            /**
             * Indicates that a batch process is starting. This method increments
             * an internal counter of batch processing requests. If there are no
             * other batch processing requests pending, this method triggers a
             * "batchUpdateStarted" event.
             *
             * @returns {Boolean} <tt>true</tt> if a batch is already in
             *          progress prior to this call
             */
            startBatch: function() {
                var count = this._batchCount;

                this._batchCount++;
                if (count == 0) {
                    this.event("batchUpdateStarted").trigger();
                }

                return (count != 0);
            },

            /**
             * Indicates that a batch process is ending. This method decrements
             * an internal counter of batch processing requests. If this call
             * ends all other batch processing requests, this method triggers a
             * "batchUpdatedEnded" event.
             *
             * @returns {Boolean} <tt>true</tt> if a batch is still in
             *          progress after this call
             */
            endBatch: function() {
                var running = true;

                switch (this._batchCount) {
                    case 0:
                        running = false;
                        break;
                     case 1:
                        running = false;
                        this.event("batchUpdateEnded").trigger();
                        //fall through
                    default:
                        this._batchCount--;
                        break;
                }

                return running;
            },

            /**
             * Registers the given entity to this cache. This method
             * overrides the base class to:</p>
             *
             * <ol>
             * <li>Validate that another entity matching {entity} is not
             * already registered (throwing a
             * {@link jabberwerx.EntitySet.EntityAlreadyExistsError}).</li>
             * <li>trigger the "entityCreated" event if this cache was changed
             * as a result of this call</li>
             * </ol>
             * @param {jabberwerx.Entity} entity The entity to register
             *
             */
            register: function(entity) {
                if (!(entity && entity instanceof jabberwerx.Entity)) {
                    throw new TypeError("entity is not an Entity");
                }

                var prev = this.entity(entity.jid, entity.node);
                if (prev && prev !== entity) {
                    throw new jabberwerx.EntitySet.EntityAlreadyExistsError();
                } else if (!prev) {
                    this.event("entityCreated").trigger(entity);
                }

                return this._super(entity);
            },
            /**
             * Unregisters the given entity from this cache. This method
             * overrides the base class to trigger the "entityDestroyed" event.
             *
             * @param {jabberwerx.Entity} entity The entity to unregister
             */
            unregister: function(entity) {
                var result = this._super(entity);
                if (result) {
                    this.event("entityDestroyed").trigger(entity);
                }

                return result;
            },

            /**
             * Factory method for {@link jabberwerx.LocalUser} objects. If a local object for
             * the passed JID already exists, that object is returned instead of a new object.
             *
             * @param {jabberwerx.JID|String} jid The JID for this user.
             * @returns {jabberwerx.LocalUser} A LocalUser for the given arguments
             * @throws  {TypeError} if {jid} is not a valid JID
             * @throws  {jabberwerx.EntitySet.EntityAlreadyExistsError} if an
             *          entity for {jid} already exists, but is not a LocalUser
             */
            localUser: function(jid) {
                var ent = this.entity(jid);
                if (!(ent && ent instanceof jabberwerx.LocalUser)) {
                    ent = new jabberwerx.LocalUser(jid, this);
                    this.register(ent);
                }

                return ent;
            },
            /**
             * Factory method for {@link jabberwerx.Server} objects. If a local object for
             * the passed JID already exists, that object is returned instead of a new object.
             *
             * @param {String} serverDomain The domain for this server, eg, "jabber.com".
             * @returns {jabberwerx.Server} A Server for the given arguments
             * @throws  {TypeError} if serverDomain is not a valid JID
             * @throws  {jabberwerx.EntitySet.EntityAlreadyExistsError} if an
             *          entity for {serverDomain} already exists, but is not a
             *          Server.
             */
            server: function(serverDomain) {
                var ent = this.entity(serverDomain);
                if (!ent || !(ent instanceof jabberwerx.Server)) {
                    ent = new jabberwerx.Server(serverDomain, this);
                    this.register(ent);
                }

                return ent;
            },
            /**
             * Factory method for {@link jabberwerx.TemporaryEntity} objects. If a local object for
             * the passed JID already exists, that object is returned instead of a new object.
             *
             * @param {jabberwerx.JID|String} jid The JID of the temporary entity object to get or create.
             * @returns {jabberwerx.TemporaryEntity} A TemporaryEntity for the
             *          given JID
             * @throws  {TypeError} if {jid} is not a valid JID
             * @throws  {jabberwerx.EntitySet.EntityAlreadyExistsError} if an
             *          entity for {jid} already exists, but is not a
             *          TemporaryEntity
             */
            temporaryEntity: function(jid) {
                var ent = this.entity(jid);
                if (!(ent && ent instanceof jabberwerx.TemporaryEntity)) {
                    ent = new jabberwerx.TemporaryEntity(jid, this);
                    this.register(ent);
                }

                return ent;
            }
        }, "jabberwerx.ClientEntityCache");

        jabberwerx.Client = jabberwerx.JWModel.extend(/** @lends jabberwerx.Client.prototype */{
            /**
             * @class
             * @minimal
             * <p>The role of the Client is to provide an interface to the XMPP protocol.
             * It sends and recieves communication over its connection on behalf of any
             * other model objects.</p>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.Client">jabberwerx.Client</a></li>
             * </ul>
             *
             * @description
             *
             * @param {String} [resourceName] the client resource name to use.
             *
             * @constructs jabberwerx.Client
             * @extends jabberwerx.JWModel
            */
            init: function(resourceName) {
                this._super();

                //Events
                this.applyEvent('clientStatusChanged');

                this.applyEvent('beforeIqSent');
                this.applyEvent('iqSent');
                this.applyEvent('beforeIqReceived');
                this.applyEvent('iqReceived');
                this.applyEvent('afterIqReceived');

                this.applyEvent('beforeMessageSent');
                this.applyEvent('messageSent');
                this.applyEvent('beforeMessageReceived');
                this.applyEvent('messageReceived');
                this.applyEvent('afterMessageReceived');

                this.applyEvent('beforePresenceSent');
                this.applyEvent('presenceSent');
                this.applyEvent('beforePresenceReceived');
                this.applyEvent('presenceReceived');
                this.applyEvent('afterPresenceReceived');

                this.applyEvent('reconnectCountdownStarted');
                this.applyEvent('reconnectCancelled');

                this.entitySet = new jabberwerx.ClientEntityCache();
                
                if (resourceName && typeof resourceName == 'string') {
                    this.resourceName = resourceName;
                }
                else{
                    this._autoResourceName = true;
                }
                
                this._stream = new jabberwerx.Stream();                

                //handle iq:version, time
                with (this.event('afterIqReceived')) {
                    bindWhen('iq[type="get"] *[xmlns="urn:xmpp:time"]',
                             this.invocation('_handleEntityTime'));
                    bindWhen('iq[type="get"] *[xmlns="jabber:iq:time"]',
                             this.invocation('_handleIqTime'));
                }

                // _handlePresenceIn will be called for all presence stanzas with no type or type unavailable
                this.event('presenceReceived').bind(this.invocation('_handlePresenceIn'));
                //this.event("presenceSent").bind(this.invocation("_handlePresenceOut"));
            },
            /**
             * Destroys this client. This method walks through all of the controllers,
             * destroying them.
             */
             destroy: function() {
                jabberwerx.$.each(this.controllers, function() {
                    this.destroy();
                });
                this._super();
             },
            
            /**
             * @private
             */
            _setStreamHandler: function(evt, funcName) {
                this._clearStreamHandler(evt);
                this._streamHandlers[evt] = funcName;
                this._stream.event(evt).bind(this.invocation(this._streamHandlers[evt]));
            },
            
            /**
             * @private
             */
            _clearStreamHandler: function(evt) {
                if (this._streamHandlers[evt] !== undefined) {
                    this._stream.event(evt).unbind(this.invocation(this._streamHandlers[evt]));
                    delete this._streamHandlers[evt];
                }
            },
            
            /**
             * @private
             */
            _clearStreamHandlers: function() {
                this._clearStreamHandler('streamOpened');
                this._clearStreamHandler('streamClosed');
                this._clearStreamHandler('streamElementsReceived');
                this._clearStreamHandler('streamElementsSent');  
                this._streamHandlers = {};
            },

            /**
             * <p>Connect to the XMPP server. the value of {arg} is expected to
             * be an object with any of the following properties:</p>
             *
             * <table>
             * <tr><th>Name</th><th>Type</th><th>Description</th></tr>
             * <tr><td>httpBindingURL</td><td>String</td><td>URL for BOSH
             * connection</td></tr>
             *<tr><td>httpBindingURL_secondary</td><td>String</td><td>The secondary/backup URL for BOSH
             * connection</td></tr>
             *<tr><td>serviceDiscoveryEnabled</td><td>Boolean</td><td>The flag to indicate whether the 
             * service discovery is on or off on the client.</td></tr>
             * <tr><td>successCallback</td><td>Function</td><td>Called when
             * the client successfully connects. It is expected to have the
             * following signature:
             * <div><pre class="code">
             * function success() {
             *    this; //The client
             * }
             * </pre></div></td></tr>
             * <tr><td>errorCallback</td><td>Function</td><td>Called when
             * the client fails to connect. It is expected to have the
             * following signature:
             * <div><pre class="code">
             * function failed(err) {
             *    this; //The client
             *    err;  //An object describing the error
             * }
             * </pre></div></td></tr>
             * </table>
             *
             * @param {jabberwerx.JID|String} jid The JID to connect to the
             *        XMPP server. If anonymous login is required, only specify 
             *        the domain as the jid i.e. "example.com"
             * @param {String} [password] The password for the given JID. If using anonymous login
             *        (i.e. the domain is passed as the jid value) the password argument is optional.
             * @param {Object} arg Object containing the arguments to connect with.
             */
            connect: function(jid, password, arg) {
                // notes the method is running
                // would not be necessary if we had real multi-threading
                var inProgress = arguments.callee.inProgress || {};
                if (inProgress[this]) {
                    // DEBUG-BEGIN
                    jabberwerx.util.debug.log("client.connect already in progress!");
                    // DEBUG-END
                    return;
                }
                inProgress[this] = true;
                arguments.callee.inProgress = inProgress;
                
                //helper function to create user/server entites as needed, maintains entitycache 
                //integrety by deleting invalid refs
                var __createEntities = function(client) {
                    var user;
                    jabberwerx.$.each(client.entitySet.toArray(), function() {
                        if (this instanceof jabberwerx.LocalUser) {
                            if (client._connectParams.jid.equals(this.jid)) {
                                user = this;
                            } else {
                                this.remove();
                            }
                        }
                    });
                    client.connectedUser = user ||
                                         client.entitySet.localUser(client._connectParams.jid);

                    // Locate server for {domain}, clearing all others
                    var server;
                    jabberwerx.$.each(client.entitySet.toArray(), function() {
                        if (this instanceof jabberwerx.Server) {
                            if (client._connectParams.jid.getDomain() == this.jid.getDomain()) {
                                server = this;
                            } else {
                                this.remove();
                            }
                        }
                    });
                    client.connectedServer = server || 
                        client.entitySet.server(client._connectParams.jid.getDomain());
                }
                
                //bind connect success and error callbacks to approriate events
                var __bindCallbacks = function(client) {
                    var successCB = client._connectParams.arg.successCallback;
                    var errorCB = client._connectParams.arg.errorCallback;
                    if (successCB || errorCB) {
                        var fn = function(evt) {
                            var evtCon = (evt.data.next == jabberwerx.Client.status_connected);
                            var evtDiscon = (evt.data.next == jabberwerx.Client.status_disconnected);
                            if (evtCon || evtDiscon) {
                                client.event('clientStatusChanged').unbind(fn);
                                if (successCB && evtCon) {
                                    successCB(client);
                                } else if (errorCB && evt.data.error && evtDiscon) {
                                    errorCB(evt.data.error);
                                }
                            }
                        };
                        client.event('clientStatusChanged').bind(fn);
                    }
                }
                
                //bail if not disconnected.
                if (this.clientStatus != jabberwerx.Client.status_disconnected) {
                    // DEBUG-BEGIN
                    jabberwerx.util.debug.log("client not disconnected!");
                    // DEBUG-END
                    delete inProgress[this];
                    return;
                }
                
                if (!arg) { arg = {}; }
                
                // Cancel a reconnect countdown if one was in progress
                this.cancelReconnect();
                this._clearStreamHandlers(); //there can be only one... set of handlers
                this._stream.close();        //make sure it's really really closed!

                // Store parameters for reconnect if required
                try {
                    var cjid = jabberwerx.JID.asJID(jid);
                    
                    // No jid nodename provided ... anonymous login required
                    if ( !cjid.getNode() ) {
                        
                        // set flag for in-band registration
                        arg.register = true;
                        
                        // create a new jid using a random node name
                        cjid = new jabberwerx.JID({
                                                    domain: cjid.getDomain(),
                                                    node: "CAXL_" + jabberwerx.util.crypto.generateUUID()
                        });
                        
                        // create a random password if required
                        if ( !password ) {
                            password = jabberwerx.util.crypto.b64_sha1(jabberwerx.util.crypto.generateUUID());
                        }
                    }
                    this._connectParams = {
                        jid: cjid,
                        password: password,
                        username: cjid.getNode(),
                        domain: cjid.getDomain(),
                        resource: this.resourceName,
                        // be sure to remember the correct httpBindingURL
                        httpBindingURL: arg.httpBindingURL || jabberwerx._config.httpBindingURL,
                        httpBindingURL_secondary: arg.httpBindingURL_secondary || jabberwerx._config.httpBindingURL_secondary,
                        sdEnabled: arg.serviceDiscoveryEnabled || jabberwerx._config.serviceDiscoveryEnabled,
                        secure: arg.unsecureAllowed || jabberwerx._config.unsecureAllowed || false,
                        timeout: arg.timeout || null,
                        wait: arg.wait || null,                             
                        arg: arg
                    };
                    
                    var stringJID = cjid.getBareJIDString();
                    var cachedUserHomeNode = null;
                    var cachedUserBackupNode = null;
                    if (this._connectParams.sdEnabled) {
                        cachedUserHomeNode = this._getNodeInfo('_jw_store_.cup.sd.home.'+ stringJID);
                        cachedUserBackupNode = this._getNodeInfo('_jw_store_.cup.sd.backup.'+ stringJID);
                        //Adding logging
                        jabberwerx.util.debug.log("User home node is : " + this._userSDInfo.home);
                        jabberwerx.util.debug.log("_connectSecondaryBindingURL= " + this._connectSecondaryBindingURL);
                        jabberwerx.util.debug.log("Stored home node is : " + cachedUserHomeNode);
                        jabberwerx.util.debug.log("Stored backup node is : " + cachedUserBackupNode);
                        jabberwerx.util.debug.log("_connectToNode node is : " + this._stream._connectToNode);
                        if(!this._connectParams.arg.reconnecting ){
                           //first connect, initialize _candidateNodes
                           jabberwerx.util.debug.log("Initializing candidate nodes array.");
                           this._candidateNodes = [];
                           if(cachedUserHomeNode){
                       	      this._candidateNodes[0] = cachedUserHomeNode;
                       	      this._stream._connectToNode = this._candidateNodes[0];//always try the home node first
                       	      if(cachedUserBackupNode){
                       	         this._candidateNodes[1] = cachedUserBackupNode;
                       	      }
                           }
                       }
                   }else {
                	 jabberwerx.util.debug.log("Note: Service discovery is disabled on the client.");
                   }
                    
                    this._openStream();
                    //create entities, callback bindings after stream open request
                    //auth/unknown stream exceptions will be handled through status_disconnected from here
                    //ensure exactly one localuser and server are in entity cache
                    //create entities, callback bindings
                    __createEntities(this);
                    //bind any callbacks to appropriate client events 
                    __bindCallbacks(this);
                    //finally change staus to reconnecting/connecting
                    if (this._connectParams.arg.reconnecting) {
                        this.setClientStatus(jabberwerx.Client.status_reconnecting);
                        this._connectParams.arg.reconnecting = false;
                    } else {
                        this.setClientStatus(jabberwerx.Client.status_connecting);
                    }
                    
                    this._connectionAttempts++;
                } catch (ex) {
                    // configuration problem
                    this._connectParams = {}; //clear apparntly bad connect info
                    throw new jabberwerx.Client.ConnectionError(ex.message || 'invalid connection information');                    
                } finally {
                    delete inProgress[this];
                }
            },
            
            /**
             * @private
             */
            _openStream: function() {
                //if in-band registration is required, do that before authorization
                if (this._connectParams.arg.register) {
                    this._setStreamHandler('streamOpened','_handleRegistrationOpened');
                } else if(this._connectParams.sdEnabled) {
                    this._setStreamHandler('streamOpened','_serviceDiscoveryOpened');
                } else {
                	this._setStreamHandler('streamOpened','_handleAuthOpened');
                }
                this._setStreamHandler('streamClosed','_handleClosed');
                
                //pass only what stream needs to open, ie not password or anything else we want to obfuscate
                var streamOpts = {
                    jid: this._connectParams.jid.getBareJIDString(),
                    domain: this._connectParams.domain,
                    timeout: this._connectParams.timeout,
                    wait: this._connectParams.wait,
                    secure: this._connectParams.secure,
                    httpBindingURL: (this._connectSecondaryBindingURL) ? this._connectParams.httpBindingURL_secondary : this._connectParams.httpBindingURL,
                    homeNode:  this._userSDInfo.home || this._candidateNodes[0]
                }
                try {
                    this._stream.open(streamOpts);                
                } catch (ex) {
                    //need to cleanup here, nothing else will
                    this._clearStreamHandlers();
                    throw ex;
                }
            },

            /**
             * @private
             */
            _serviceDiscoveryOpened: function(feats)  {

               try {
                   //connected, so reset _connectSecondaryBindingURL
                   this._connectSecondaryBindingURL = false;
                   this._candidateNodes =[];//cleanup the candidate nodes array
                   this._clearStreamHandler('streamOpened');
                   this._connectParams.feats = jabberwerx.$(feats.data);
                        
                    //check for the home and backup node
                    var nodes = [];
                    var jidString =this._connectParams.jid.getBareJIDString();
                    jabberwerx.$(feats.data).find("mechanisms[xmlns='urn:ietf:params:xml:ns:xmpp-sasl']>hostname").each(
                        function() {
                            nodes.push(jabberwerx.$(this).text());
                        });
                    if(nodes.length==0){
                       //Server HA is disabled or User is unassigned or Server is running a pre CUP8.5 version & does not support HA
                       jabberwerx.util.debug.log("No user's home and backup node returned from server, this might be caused by \
                                                 1) CUP Server is running a pre CUP 8.5 version & does not support High Availability or \
                                                 2) High Availability may be disabled on the cluster or \
                                                 3) User is not assigned to a node within the subcluster.");
                       //no home/backup node returned, reset the home/backup node value to null.
                       this._userSDInfo = {home:null, backup:null};
                       this._removeNodeInfo('_jw_store_.cup.sd.home.'+jidString);
                       this._removeNodeInfo('_jw_store_.cup.sd.backup.'+jidString);
                       //Just carry on
                       this._handleAuthOpened(feats);
                    }
                    else {
                       this._userSDInfo.home = nodes[0];
                       this._storeNodeInfo('_jw_store_.cup.sd.home.'+jidString, this._userSDInfo.home);
                       if (nodes[1]){
                          this._userSDInfo.backup = nodes[1];
                          this._storeNodeInfo('_jw_store_.cup.sd.backup.'+jidString, this._userSDInfo.backup);
                       } else {
                       	  this._userSDInfo.backup = null;
                       	  this._removeNodeInfo('_jw_store_.cup.sd.backup.'+jidString);
                       }
                      
                       jabberwerx.util.debug.log("User [" + jidString + "]'s home node is " + this._userSDInfo.home +" and backup node is " + this._userSDInfo.backup);

                       if (this._connectedRightNode()){
                           jabberwerx.util.debug.log("Connected with the right node [" + this._stream._connectToNode + "].");
                           this._handleAuthOpened(feats);
                       }
                       else {
                           this._clearStreamHandlers();
                           //reset the stream
                           this._stream.close();
                           this._stream._connectToNode = this._userSDInfo.home;
                           //connect to user's home node
                           jabberwerx.util.debug.log("Now connecting to home node [" + this._stream._connectToNode + "].");
                           //just in case home node is down after service discovery
                           if (this.clientStatus == jabberwerx.Client.status_connecting && this._userSDInfo.backup){
                           	   //add home and backup node to the _canidateNodes array
                           	   this._candidateNodes[0] = this._userSDInfo.home;
                           	   this._candidateNodes[1] = this._userSDInfo.backup;
                           }
                           setTimeout(this.invocation('_openStream'),500);
                       }
                    }
               }catch (ex) {
                   this._handleConnectionException(ex);
               }
            },
           
           /**
             * @private
             * True if the connectToNode is one of user's home node and backup node.
             */
            _connectedRightNode: function(){
              var home = this._switchHost(this._connectParams.httpBindingURL,this._userSDInfo.home);
              var backup = this._switchHost(this._connectParams.httpBindingURL,this._userSDInfo.backup);
              return this._stream._connectToNode && (this._stream._connectToNode == home || this._stream._connectToNode == backup);
            },
           /**
             * @private
             * Store user's home/backup node
             */
            _storeNodeInfo: function(key, value){
				if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
					throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
				}
				// Hack: When the data saved is of format IP Address, we get exceptions when reading its saved value. So adding a prefix here.
				jabberwerx.util.debug.log('Store: (key, value) = (' + key + ','+"caxl::"+value+')');
				jabberwerx.$.jStore.store(key,"caxl::"+value);
           },
           	              /**
             * @private
             * Remove user's home/backup node
             */
            _removeNodeInfo: function(key){
				if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
					throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
				}
				jabberwerx.util.debug.log('Remove: (key) = (' + key + ')');
				jabberwerx.$.jStore.remove(key);
           },
           /**
             * @private
             * Get stored user's home/backup node
             * @return {String|null} node info or null if no value was found in storage for the provided key. 
             */
            _getNodeInfo: function(key){
				if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
					throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
				}
				
				var nodeInfo = jabberwerx.$.jStore.store(key);
				
				if(typeof(nodeInfo) == "string")
					return nodeInfo.substring(6,nodeInfo.length);
				return null;
     	   },
           /**
             * @private
             * Get next connect to node 
             */
            _getNextConnectToNode: function(){
           	  var node = this._stream._connectToNode;
           	  if(node == this._switchHost(this._connectParams.httpBindingURL,this._userSDInfo.home)){
           	  	  if(this._userSDInfo.backup){
           	  	  	  node = this._userSDInfo.backup;
           	  	  }
           	  }else if (node == this._switchHost(this._connectParams.httpBindingURL,this._userSDInfo.backup)){
           	  	  node = this._userSDInfo.home;
           	  }
             return node;
     	   },            
            /**
             * @private
             */
            _handleRegistrationOpened: function()  {
                try {
                    //remove this handler one time stream open event handler
                    this._clearStreamHandler('streamOpened');

                    // build iq stanza to register
                    var registerIQ = new jabberwerx.IQ();
                    registerIQ.setType("set");
                    registerIQ.setID("register");
                        
                    var builder = new jabberwerx.NodeBuilder('{jabber:iq:register}query');
                    builder = builder.element("username").
                        text(this._connectParams.username).
                        parent;
                    builder = builder.element("password").
                        text(this._connectParams.password).
                        parent;
                    registerIQ.setQuery(builder .data);

                    //bind inband registration handler to stream element received event
                    this._setStreamHandler('streamElementsReceived', '_handleRegisterElements');
                    this._stream.send(registerIQ.getNode());                     
                } catch (ex) {
                    this._handleConnectionException(ex);                    
                }            
            },
            
            /**
             * @private
             */
            _handleRegisterElements: function(elem) {
                try{
                    //remove this handler
                    this._clearStreamHandler('streamElementsReceived');
                    
                    //check for errors
                    var errNode = jabberwerx.$(elem.data).find("error");
                    if (errNode && errNode.length != 0) {
                        //clear the in-band registration flag
                        this._connectParams.arg.register = false;
                        //handle any errors found
                        this._handleConnectionException(errNode);
                    }
                    else{
                        //in-band registartion successful ... reset the stream
                        this._stream.close();
                    }
                
                } catch (ex) {
                    this._handleConnectionException(ex);
                }
            },
            
            /**
             * @private
             */
            _handleAuthOpened: function(feats) {
                try {
                    //remove this handler one time stream open event handler
                    this._clearStreamHandler('streamOpened');

                    //setup our sasl client
                    this._connectParams.feats = jabberwerx.$(feats.data);
                        
                    //check for sasl auth support
                    var supportedMechs = []
                    //""
                    jabberwerx.$(feats.data).find("mechanisms[xmlns='urn:ietf:params:xml:ns:xmpp-sasl']>mechanism").each(
                        function() {
                            supportedMechs.push(jabberwerx.$(this).text().toUpperCase());
                        });
                        
                    //match mech
                    this._authMech = jabberwerx.sasl.createMechanismFor(this, supportedMechs);
                    if (!this._authMech) {
                        throw new jabberwerx.Client.ConnectionError("{urn:ietf:params:xml:ns:xmpp-sasl}mechanism-too-weak");
                    }
                    //bind sasl auth handler stream element received event
                    this._setStreamHandler('streamElementsReceived', '_handleAuthElements');
                    this._handleAuthElements(); //start sasl
                } catch (ex) {
                    this._handleConnectionException(ex);                    
                }
            },
            
            /**
             * @private
             */
            _handleAuthElements: function(elem) {
                try {
                    elem = elem && jabberwerx.$(elem.data).get(0); //node as saslmech and stream expect a node
                    elem = this._authMech.evaluate(elem);
                    if (elem) {
                        this._stream.send(elem);
                    } else {
                        var authComplete = this._authMech.complete;
                        this._authMech = undefined;
                        delete this._authMech;
                        
                        if (!authComplete) {
                            throw new jabberwerx.SASLMechanism.SASLAuthFailure();
                        } else {
                            //reopen for binding
                            this._setStreamHandler('streamOpened', '_handleBindOpened');
                            this._stream.reopen();
                        } 
                    }
                } catch (ex) {
                    this._handleConnectionException(ex);
                }
                return true;
            },
            
            /**
             * @private
             */
            _handleBindOpened: function(feats) {
                try {
                    //remove this one time stream open event handler
                    this._clearStreamHandler('streamOpened');                
                    //setup for session  start
                    this._connectParams.bindJID = null;
                    feats = jabberwerx.$(feats.data);//cheesy copy
                    this._connectParams.feats = feats; //keep these around to check for session later
                    
                    if (feats.find("bind[xmlns='urn:ietf:params:xml:ns:xmpp-bind']").length > 0) {
                        var bindIQ = new jabberwerx.IQ();
                        bindIQ.setType("set");
                        bindIQ.setID("resource-bind");
                        
                        var builder = new jabberwerx.NodeBuilder('{urn:ietf:params:xml:ns:xmpp-bind}bind');
                        // resourceName exists ... use it for binding
                        if (this.resourceName) {
                            builder = builder.element("resource").
                                    text(this._connectParams.resource).
                                    parent;
                        }
                        bindIQ.setQuery(builder.data);
                        
                        this._setStreamHandler("streamElementsReceived", "_handleBindElements");
                        this._stream.send(bindIQ.getNode());
                        this._bindResourceTimer = setTimeout(this.invocation("_handleBindTimeout"), ((jabberwerx._config.bindRetryCountdown <= 0)? 30 : jabberwerx._config.bindRetryCountdown)* 1000);                    
                    } else {
                        this._connected();
                    }
                } catch (ex) {
                    this._handleConnectionException(ex);
                }
            },
            
            /**
             * @private
             */
            _handleBindElements: function(elements) {
                try {
                    //clear bind timer
                    clearTimeout(this._bindResourceTimer);
                    var ele = jabberwerx.$(elements.data);
                    //setup for session set
                    var newjid = ele.find("bind>jid");
                    if (newjid.length > 0) {
                        this._connectParams.bindJID = jabberwerx.$(newjid).text();
                        // resourceName MUST always be whatever the server has specified
                        // extract the resource name of the new jid ... store resource name
                        var jid = jabberwerx.JID.asJID(this._connectParams.bindJID);
                        this.resourceName = jid.getResource();                             

                        if (this._connectParams.feats.find("session[xmlns='urn:ietf:params:xml:ns:xmpp-session']").length > 0) {
                            var sessIQ = new jabberwerx.IQ();
                            sessIQ.setType("set");
                            sessIQ.setID("session");
                            sessIQ.setQuery(
                                new jabberwerx.NodeBuilder('{urn:ietf:params:xml:ns:xmpp-session}session').data
                            );
                        
                            this._setStreamHandler('streamElementsReceived','_handleSessionElements');
                            this._stream.send(sessIQ.getNode());
                        } else {
                            this._connected();
                        }
                    } else {
                        this._handleConnectionException(ele.children("error").get(0));
                    }
                } catch (ex) {
                    this._handleConnectionException(ex);
                }
            },
            
            /**
             * @private
             */
            _handleSessionElements: function(elements) {
                try {
                    var ele = jabberwerx.$(elements.data);
                    if (ele.attr("type") == "result") {
                        this._connected();
                    } else {
                        this._handleConnectionException(ele.children("error").get(0));
                    }
                } catch (ex) {
                    this._handleConnectionException(ex);
                }
            },
            
            /**
             * @private
             * Reopen the stream if the bind resource timeout
             */
           _handleBindTimeout: function() {
           	   //time out reopen the stream
           	   jabberwerx.util.debug.log('No response from server for binding resourse request, reopen the stream....');
           	   try {
           	      this._clearStreamHandlers();
           	      this._stream.close();
           	   // re-open the stream, giving the server some cool down time
               setTimeout(this.invocation("_openStream"), 1000);
           	   } catch (ex) {
           	   	  jabberwerx.util.debug.log(ex); 
           	   }
           },
            /**
             * if fqdn is specified in the bindurl, this function builds fqdn for the host value sent.
             * if bindurl has just the hostname, this function returns the host value back without any changes.
             * if bindurl has IP address, this function returns the host value back without any changes.
             * if host value is IP address or null, this function returns the host value back without any changes.
             * if host value is FQDN, this function returns the host value back without any changes.
             * @private
             * @param {string} bindurl HttpBinding url 
             * @param {String|IPaddress} host Host name
             */
            _switchHost: function(bindurl,host){
            	//if host value is IP address, return the host back.
            	if(!host || jabberwerx.util.validate.isIPAddress(host))return host;
            	
            	var url = /^(?:(?:([0-9a-zA-Z]+\:)\/\/)?(?:([0-9a-zA-Z\.\-]+)(?:\:([0-9]+))?))?(?:\/(.*))?$/.
                      exec(bindurl); 
            	
            	if(!url)return host;
            	var txt = url[2];
            	
            	// if bindurl contains IP address, return the host back.
            	if(!txt || jabberwerx.util.validate.isIPAddress(txt))return host;
            	var alphanum1= txt.substring(0,txt.indexOf(".") == -1 ? txt.length : txt.indexOf("."));
            	
            	//if host already contain domain suffix (fqdn), return host back
            	if(host.indexOf(txt.substring(alphanum1.length,txt.length))>0)return host;
	        	//builds FQDN
		        txt = txt.replace(alphanum1,host);
		        return txt;
            },
            
            _hasDomain: function(bindurl, host){
            	if(this._switchHost(bindurl,host) === host)return true;
            	return false;
            },         

            
            /**
             * @private
             * close received while opening a stream
             */
            _handleClosed: function(err) {
                var msg = 'closed: ' + (err&&err.data?jabberwerx.util.serializeXML(err.data):'no error');
                jabberwerx.util.debug.log(msg);  
                
                // stream close is due to in-band registration ... do not do normal disconnect logic
                if (this._connectParams.arg.register) {
                    // clear the in-band registration flag
                    this._connectParams.arg.register = false;
                    
                    // re-open the stream, giving the server some cool down time
                    setTimeout(this.invocation("_openStream"), 500);
                } else if (this._connectParams.sdEnabled && !this._connectSecondaryBindingURL && this.clientStatus == jabberwerx.Client.status_connecting) {
                	switch (this._candidateNodes.length){
                		case 2: //connection failed to the home node
                			if(!this._hasDomain(this._connectParams.httpBindingURL,this._candidateNodes[0])) {
                				this._candidateNodes[0]=this._switchHost(this._connectParams.httpBindingURL,this._candidateNodes[0]);
                				jabberwerx.util.debug.log("Trying with domain [" + this._candidateNodes[0] + "]");
                				this._stream._connectToNode = this._candidateNodes[0];
                				break;
                			}						
                			jabberwerx.util.debug.log("Failed to connect to home node [" + this._candidateNodes[0] + "], and now try the backup node [" + this._candidateNodes[1] + "].");
                			this._candidateNodes.shift();//remove the home node from array
                			this._stream._connectToNode = this._candidateNodes[0];//try the backup node now
                			break;
                		case 1://connection failed to the backup node,or the home node if there is no backup node available
                			if(!this._hasDomain(this._connectParams.httpBindingURL,this._candidateNodes[0])) {
                				this._candidateNodes[0]=this._switchHost(this._connectParams.httpBindingURL,this._candidateNodes[0]);
                				jabberwerx.util.debug.log("Trying with domain [" + this._candidateNodes[0] + "]");
                				this._stream._connectToNode = this._candidateNodes[0];
                				break;
                			}
                			jabberwerx.util.debug.log("Both home and backup node are down, retrying the SD-Primary node, binding URL =" + this._connectParams.httpBindingURL);
                			this._candidateNodes.shift();//remove the backup node from array
                			//reset _connectToNode and home node, and use the httpBindingURL when build BOSH URL
                			this._stream._connectToNode = null;
                			this._userSDInfo.home = null;
                			break;
                		case 0://connection failed to the sd-primary node 
                			this._stream._connectToNode = null;// try the sd-secondary node now
                			this._connectSecondaryBindingURL = true;
                			jabberwerx.util.debug.log("Failed to connect to SD-Primary Node, binding URL =" + this._connectParams.httpBindingURL + ",  and now try the SD-Secondary Node binding URL=" + this._connectParams.httpBindingURL_secondary);
                			break;
                		default:
                			jabberwerx.util.debug.log("Error: the _candidateNodes.length is out of range!");
                	}
                    this._stream.close();
                    this._openStream();
                } else {
                    this._disconnected(err.data);  
                }
            },
            
            /**
             * @private
             */
            _handleConnectionException: function(ex) {
                this._clearStreamHandlers(); //ignore any streamClosed events, to us it's already closed
                try {
                    this._stream.close();
                } catch (e) {}; //ignore stream state exceptions, can't really handle them
                //make an error node and fire through disconnected
                var n = this._exceptionToErrorNode(ex);
                jabberwerx.util.debug.log("Exception during connection: " + jabberwerx.util.serializeXML(n));
                this._disconnected(n);                
            },
            
            /**
             * @private
             */
            _exceptionToErrorNode: function(ex) {
                if (jabberwerx.isElement(ex)) {
                    return ex;
                }
                
                var err = new jabberwerx.NodeBuilder("error");
                if (ex instanceof jabberwerx.SASLMechanism.SASLAuthFailure) {
                    err.element(ex.message);
                } else if (ex instanceof TypeError) {
                    err.element("{urn:ietf:params:xml:ns:xmpp-stanzas}bad-request");
                    if (ex.message) {
                        err.element("text").text(ex.message);
                    }
                } else {
                    //If this is a conflict exception, add the namespace to err data
                    //This allows ErrorReporter to provide a useful message for the conflict exception                   
                    var errNode = jabberwerx.$(ex).find("conflict");
                    if (errNode && errNode.length != 0) {
                        //Get the namespace
                        var ns = jabberwerx.$(errNode).attr("xmlns");
                        //Appended message key to error node
                        err.element("{" + ns + "}conflict");                       
                    } else {
                        var emsg =( ex && ex.message) ? ex.message : "{urn:ietf:params:xml:ns:xmpp-stanzas}internal-server-error";
                        try {
                            err.element(emsg);
                        } catch (e) {
                            err.element("{urn:ietf:params:xml:ns:xmpp-stanzas}internal-server-error");
                            //add element message as text
                            err.element("text").text(emsg);                        
                        }
                    }
                }
                return err.data;
            },
            
            /**
             * Disconnect from the server.
             */
            disconnect: function() {
                if (this.isConnected()) {
                    // if the resourceName was server generated ... clear it on disconnect
                    if (this._autoResourceName) {
                        this.resourceName = null;
                    }
                    this.setClientStatus(jabberwerx.Client.status_disconnecting);
                    this._stream.close();
                    this._connectionAttempts = 0;
                    this._connectSecondaryBindingURL = false;
                }
            },

            /**
             * @type Boolean
             * @return Whether the client is currently connected
             */
            isConnected: function() {
                //check client state rather than stream open state, stream could be open while in other client states
                return this.clientStatus == jabberwerx.Client.status_connected;
            },

           /**
            * @private
            * Set client status
            * @param {Number} status A Client.status_... constant
            * @param {Error} [error] An error object
            */
            setClientStatus: function(status, error) {
                var prev = this.clientStatus;
                this.clientStatus = status;

                if (prev && (prev != status)) {
                    var data = {
                        previous: prev,
                        next: status,
                        error: error
                    };
                    this.event('clientStatusChanged').trigger(data);
                }

                /* DEBUG-BEGIN */
                jabberwerx.util.debug.log('client status: ' + this.getClientStatusString(status),
                        'clientStatus');
                /* DEBUG-END */
            },

            /**
             * @param {Number} status A Client.status_... constant
             * @returns A string version of the status.
             * @type String
             */
            getClientStatusString: function(status) {
                switch (status) {
                    case jabberwerx.Client.status_connected:
                        return jabberwerx._("Connected to {0} as {1}.",
                                (this.connectedServer ? this.connectedServer.jid : jabberwerx._("(unknown)")),
                                (this.connectedUser ? this.connectedUser.jid : jabberwerx._("(unknown)")));
                    case jabberwerx.Client.status_connecting:
                        return jabberwerx._("Attempting to connect");
                    case jabberwerx.Client.status_error:
                        return jabberwerx._("Connection error");
                    case jabberwerx.Client.status_disconnected:
                        return jabberwerx._("Disconnected");
                    case jabberwerx.Client.status_disconnecting:
                        return jabberwerx._("Disconnecting");
                    case jabberwerx.Client.status_reconnecting:
                        return jabberwerx._("Reconnecting");
                }
            },

            /**
             * <p>Retrieves the current presence for this client. If not
             * connected, this method returns <tt>null</tt>.</p>
             *
             * @param   {Boolean} [primary] Optional flag which, if true, this method will return
             *          the primary presence for this client. If false or not specified, the
             *          resource presence is retrieved.
             * @return  {jabberwerx.Presence} The current presence, or
             *          <tt>null</tt> if none
             */
            getCurrentPresence: function(primary) {
                var me = this.connectedUser;
                return (me && 
                    (primary ? me.getPrimaryPresence() : me.getResourcePresence(this.resourceName)))
                    || null;
            },

            /**
             * Send an XMPP stanza.
             *
             * <p>If {rootName} is an instance of {@link jabberwerx.Stanza}, this method ignores
             * all other properties and sends the stanza directly. Otherwise, it is
             * expected to be the name of the root node (e.g. presence, iq, message).</p>
             *
             * @param {jabberwerx.Stanza|Element|String} rootName The name of the root node (presence, iq, etc), or the object
             *                 representation of the stanza.
             * @param {String} type The type attribute to set on the root, if any. (Set to '' or null to not set a type).
             * @param {String} to The to attribute to set on the root, if any. (Set to '' or null to not set a type).
             * @param {String} content XML content of the stanza.
             */
            sendStanza: function(rootName, type, to, content) {
                var s;

                if (rootName instanceof jabberwerx.Stanza) {
                    s = rootName.clone();
                } else if (jabberwerx.isElement(rootName)) {
                    s = jabberwerx.Stanza.createWithNode(rootName);
                } else {
                    s = new jabberwerx.Stanza(rootName);
                    if (to) {
                        s.setTo(to.toString());
                    }
                    if (type) {
                        s.setType(type.toString());
                    }

                    if (content) {
                        if (typeof content == 'string') {
                            content = jabberwerx.util.unserializeXML(content);
                        }

                        new jabberwerx.NodeBuilder(s.getNode()).node(content);
                    }

                    s = jabberwerx.Stanza.createWithNode(s.getNode());
                }

                type = s.pType();
                this.event('before' + type + 'Sent').trigger(s);
                this._stream.send(s.getNode());
                
                // deal with presence special
                if (s instanceof jabberwerx.Presence) {
                    var presence = s;
                    var type = presence.getType();
                    if ((!type || (type == "unavailable")) && !presence.getTo()) {
                        presence = presence.clone();
                        presence.setFrom(this.connectedUser.jid + "/" + this.resourceName);
    
                        this.connectedUser.updatePresence(presence);
                    }
                }
            },
            /**
             * Send an XMPP message. Assumes chat-type content with a body.
             *
             * @param {String} to The to attribute to set on the message.
             * @param {String} body The body of the message.
             * @param {String} [subject] The subject of the message
             * @param {String} [type] The type attribute to set on the message.
             * @param {String} [thread] A thread identifier, if any.
             */
            sendMessage: function(to, body, subject, type, thread) {
                this._assertConnected();
                var m = new jabberwerx.Message();
                if (to instanceof jabberwerx.Entity) {
                    to = to.jid.toString();
                }
                m.setTo(new jabberwerx.JID(to));
                m.setBody(body);
                if (subject) {
                    m.setSubject(subject);
                }
                if (thread) {
                    m.setThread(thread);
                }
                if (type) {
                    m.setType(type);
                }

                // a bit hackish: in chat messages, add an XHTML-IM body,
                // even if they don't have any special markup in them.
                if (type === 'undefined' || type == 'chat') {
                    with (new jabberwerx.NodeBuilder(m.getNode())) {
                        element('{http://jabber.org/protocol/xhtml-im}html').
                                element('{http://www.w3.org/1999/xhtml}body').text(body);
                    }
                }

                this.sendStanza(m);
            },
            _iqStamp: 0,
            /**
             * Alias for {@link jabberwerx.Client#sendIq}.
             */
            sendIQ: function(type, to, content, callback, timeout) {
                return this.sendIq.apply(this, arguments);
            },
            /**
             * Send an XMPP IQ stanza.
             *
             * @param {String} type The type attribute to set on the iq.
             * @param {String} to The to attribute to set on the iq.
             * @param {String} content XML content of the iq stanza.
             * @param {Function} [callback] A callback to be invoked when
             *                   either an IQ is recieved with a matching id,
             *                   or {null} if {timeout} is reached.
             * @param {Number} [timeout] A timeout (in seconds) wait on
                               a result or error IQ with a matching id
             * @type String
             * @returns The id set on the outgoing stanza.
             * @throws TypeError If {callback} is defined and is not a
             *         function; or if {timeout} is defined and not a
             *         number
             */
            sendIq: function(type, to, content, callback, timeout) {
                if (callback !== undefined && !jabberwerx.$.isFunction(callback)) {
                    throw new TypeError("callback must be a function");
                }
                if (    timeout !== undefined &&
                        typeof(timeout) != "number" &&
                        !(timeout instanceof Number)) {
                    throw new TypeError("timeout must be a number");
                }

                var i = new jabberwerx.IQ();
                var id = "iq" + new Date().valueOf() + this._iqStamp++;
                if (type) {
                    i.setType(type);
                }
                if (to) {
                    i.setTo(to);
                }
                i.setID(id);
                if (content) {
                    if (typeof(content) == 'string') {
                        content = jabberwerx.util.unserializeXML(content);
                    }

                    new jabberwerx.NodeBuilder(i.getNode()).node(content);
                }

                if (callback) {
                    var that = this;
                    var tid = undefined;
                    var fn = function(evt) {
                        var elem = evt.data;

                        if (jabberwerx.isDocument(elem)) {
                            elem = elem.documentElement;
                        } else if (elem instanceof jabberwerx.Stanza) {
                            elem = elem.getNode();
                        }

                        elem = jabberwerx.$(elem);
                        if (elem.attr("id") != id) {
                            return;
                        }
                        if (elem.attr("type") != "result" && elem.attr("type") != "error") {
                            return;
                        }

                        callback(elem.get()[0]);
                        evt.notifier.unbind(arguments.callee);

                        if (tid) {
                            window.clearTimeout(tid);
                            tid = undefined;
                        }
                    };
                    timeout = Number(timeout || 0);
                    if (timeout > 0) {
                        var tfn = function() {
                            if (tid) {
                                that.event('beforeIqReceived').unbind(fn);
                                callback();
                            }
                        }
                        tid = window.setTimeout(tfn, timeout * 1000);
                    }

                    var idSel = '[id="' + id + '"]';
                    this.event('beforeIqReceived').bind(fn);
                }
                this.sendStanza(i);

                return id;
            },
            /**
             * Send an XMPP presence stanza
             *
             * @param {String} [show] Optional show value: 'away', 'available', 'dnd', etc.
             * @param {String} [status] Optional status message.
             * @param {String} [to] Optional `to` attribute to set on the outgoing stanza.
             */
            sendPresence: function(show, status, to) {
                var p = new jabberwerx.Presence();
                if (typeof show == 'string') {
                    p.setShow(show);
                }
                if (typeof status == 'string') {
                    p.setStatus(status);
                }
                if (to !== 'undefined') {
                    p.setTo(to);
                }

                this.sendStanza(p);
            },

            /**
             * Runs the passed jQuery selector string against the passed stanza.
             * @param {String} stanzaDoc XML of stanza
             * @param {String} selector jQuery selector
             * @returns A bare array (not jQuery-enhanced) of matching nodes, or a single node if only one matches.
             * @type Array|Node
             */
            selectNodes: function(stanzaDoc, selector) {
                var filteredDoc = stanzaDoc;
                jabberwerx.util.debug.log('running jquery with selector: ' + selector + " on doc:\n\n" + filteredDoc.xml, 'stanzaSelectors');
                var result = jabberwerx.$(selector, filteredDoc);
                var nodes = [];
                result.each(function(){ nodes.push(this); });
                if (nodes.length == 1) {
                    return nodes[0];
                }
                if (nodes.length == 0) {
                    return null;
                }
                return nodes;
            },

            /**
             * Gets all presence objects for the jid specified. Usual usage would be for bare jids. If a full jid is passed through
             * then only one presence object will be returned in the array.
             * @param {String|jabberwerx.JID} jid The jid to get all presence for
             * @returns An array of {@link jabberwerx.Presence} objects. Note that this array may be empty if no presence objects
             * are attached to this jid's entity
             * @type jabberwerx.Presence[]
             */
            getAllPresenceForEntity: function(jid) {
                var retVal = [];

                if (! (jid instanceof jabberwerx.JID)) {
                    jid = new jabberwerx.JID(jid);
                }

                var entity = this.entitySet.entity(jid.getBareJIDString());
                if (entity) {
                    if (!jid.getResource()) {
                        // This is a bare jid
                        retVal = entity.getAllPresence();
                    } else {
                        retVal = [entity.getResourcePresence(jid.getResource())];
                    }
                }
                return retVal;
            },

            /**
             * Gets the primary presence for the jid specified. If the jid is in bare jid format (ex. 'user@host') then the
             * highest priority presence for that entity will be returned. If the jid is in full jid format (ex. 'user@host/resource')
             * then the presence object for that resource only will be returned.
             * @param {String|jabberwerx.JID} jid The jid to get the primary presence of
             * @returns The primary presence or null if the entity / resource for this jid does not exist
             * @type jabberwerx.Presence
             */
            getPrimaryPresenceForEntity: function(jid) {
                if (! (jid instanceof jabberwerx.JID)) {
                    jid = new jabberwerx.JID(jid);
                }

                var entity = this.entitySet.entity(jid.getBareJIDString());
                if (entity) {
                    if (jid.getResource()) {
                        return entity.getResourcePresence(jid.getResource());
                    } else {
                        return entity.getPrimaryPresence();
                    }
                }
                return null;
            },

            /**
             * Handles presence stanzas with either no type or a type value of unavailable. Is invoked when the presenceReceived
             * event is fired and the presence has no type or a type of unavailable.
             * @private
             * @param {jabberwerx.EventObject} eventObj The eventObj passed through on the event trigger
             */
            _handlePresenceIn: function(eventObj) {
                var entity;
                var presence = eventObj.data;
                var type = presence.getType();
                if (!type || type == 'unavailable') {
                    var bareJidStr = presence.getFromJID().getBareJIDString();
                    if (bareJidStr) {
                        if (presence.getType() == 'unavailable') {
                            // If the type of the presence stanza is unavailabe then we want
                            // to remove the corresponding entity presence property (if the
                            // entity for the bare jid exists)
                            entity = this.entitySet.entity(bareJidStr);
                            if (entity) {
                                entity.updatePresence(presence);
                            }
                        } else {
                            entity = this._findOrCreateEntity(bareJidStr);
                            if(!(entity instanceof(jabberwerx.TemporaryEntity))){
                                entity.updatePresence(presence);
                            }
                        }
                    }
                }
            },

            /**
             * Checks for an entity corresponding to the jid in the entitySet. If none found then create a new one.
             * @private
             * @param {String} jid The jid for which the entity should be found or created.
             * @returns The found or created entity object.
             * @type jabberwerx.Entity 
             */
            _findOrCreateEntity: function(jid) {
                var entity = this.entitySet.entity(jid);
                if (!entity) {
                    // Create new jabberwerx.TemporaryEntity
                    entity = this.entitySet.temporaryEntity(jid);
                }
                return entity;
            },
            /**
             * @private
             */
            _removeAllTemporaryEntities: function() {
                var ents = this.entitySet.toArray();

                jabberwerx.$.each(ents, function() {
                    if (this instanceof jabberwerx.TemporaryEntity) {
                        this.remove();
                    }

                    return true;
                });
            },

            /**
             * @private
             */
            willBeSerialized: function () {
                //obfuscate our password for store
                if (this._connectParams && this._connectParams.password) {
                    //this persist may have come mid authentication (and unit testing)
                    //clearing password here may cause persisted authentication attempt to fail (mid SASL for instance) but chances are this code will never execute
                    if (jabberwerx._config.baseReconnectCountdown == 0) {
                        this._connectParams.password = "";
                    } else {
                        this._connectParams.password = jabberwerx.util.encodeSerialization(this._connectParams.password);
                    }
                }
            },

            /**
             * @private
             */
            wasUnserialized: function () {
                //unobfuscate our password
                if (this._connectParams && this._connectParams.password) {
                    this._connectParams.password = jabberwerx.util.decodeSerialization(this._connectParams.password);
                }
            },

            /**
             * @private
             * @throws {jabberwerx.Client.NotConnectedError}
             */
            _assertConnected: function() {
                if (!this.isConnected()) {
                    throw new jabberwerx.Client.NotConnectedError();
                }
            },

            /**
             * @private
             */
            _connected: function() {
                //clear password from memory
                if (jabberwerx._config.baseReconnectCountdown == 0) {
                    this._connectParams.password = "";
                }
                //setup running element handlers
                this._clearStreamHandler("streamOpened");
                this._setStreamHandler("streamElementsReceived", "_handleElementsReceived");
                this._setStreamHandler("streamElementsSent", "_handleElementsSent");

                //set our new jid
                if (this._connectParams.bindJID) {
                    var jid = jabberwerx.JID.asJID(this._connectParams.bindJID);
                    if (!jid.getBareJID().equals(this.connectedUser.jid)) {
                        // username/domain changed...
                        this.entitySet._renameEntity(this.connectedUser, jid);
                    }
                    // assume resource changed...
                    this.resourceName = jid.getResource();
                }
                this._connectParams.bindJID = null;
                //reset _connectSecondaryBindingURL when connected
                this._connectSecondaryBindingURL = false;
                this._reconnectAttempts = 0;
                // register for feature query callback
                this.setClientStatus(jabberwerx.Client.status_connected);
            },

            /**
            * @private
            */
            _handleDisconnect: function() {
                if (this.connectedUser) {
                    var prs = new jabberwerx.Presence();
                    prs.setFrom(this.connectedUser.jid + "/" + this.resourceName);
                    prs.setType("unavailable");
                    this.connectedUser.updatePresence(prs);
                }
                this._disconnected();
            },

            /**
            * @private
            */
            _disconnected: function(err) {
                //clear password from memory, could be overkill
                if (jabberwerx._config.baseReconnectCountdown == 0) {
                    // _startREconnectCountdown will skip reconnection attempt (now invalid) if base == 0
                    this._connectParams.password = "";
                    delete this._connectParams.password;
                }

                // try reconnect if not an error while disconnecting or connecting and this not a system-shutdown or resource conflict
                if (err &&
                    (this.clientStatus != jabberwerx.Client.status_disconnecting) &&
                    (this.clientStatus != jabberwerx.Client.status_connecting) && 
                    (jabberwerx.$("conflict", err).length == 0) &&
                    (jabberwerx.$("system-shutdown", err).length == 0 ||
                      (this._userSDInfo.home && this._userSDInfo.backup))) {
                    	//best effort to determine whether the user has been moved
                    	var isUserMoved = jabberwerx.$("see-other-host", err).length != 0;
                    	if (this._reconnectAttempts++ >= 1 || isUserMoved ){
                    		this._stream.close();
                    		this._reconnectAttempts = 1;
                    		if(isUserMoved && this._userSDInfo.backup){
                    			//just try the backup node
                    			this._stream._connectToNode = this._userSDInfo.backup;
                    		}else{
                    		    //try another node in the _userSDInfo
                    		    this._stream._connectToNode = this._getNextConnectToNode();
                    		}
                    		jabberwerx.util.debug.log("Connection failed, and now try : " + this._stream._connectToNode);
                    	} else if (jabberwerx._config.baseReconnectCountdown > 0){
                    		 //this only happen once when the first connection drops
                    		 //reconnect to previous connected node
                    		  jabberwerx.util.debug.log("Reconnect to the last connected node: " + this._stream._connectToNode);
                    	}
                        this._startReconnectCountdown();
                }else {
                      //clean up the user's home/backup node and last connect to node in the browser
                      this._userSDInfo = {};
                      this._stream._connectToNode = null;
                }
                
                //cleanup
                this._clearStreamHandlers();
                if (this.connectedUser) {
                    // clear presence!
                    this.connectedUser.updatePresence();
                }
                this.connectedUser = null;
                this.connectedServer = null;
                this._authMech = undefined;
                delete this._authMech;                
                this._removeAllTemporaryEntities();
                
                // if the resourceName was server generated and not a reconnection ... clear it
                if (this._autoResourceName && !this._countDownOn) {
                    this.resourceName = null;
                }
                //reset _connectSecondaryBindingURL 
                this._connectSecondaryBindingURL = false;
                
                //event to every one else
                this.setClientStatus(jabberwerx.Client.status_disconnected, err);            
           },
            
            /**
             * @private
             */
            _handleElementsReceived: function(evt) {
                //for each element, pump through _handleStanzaIn
                var elements = jabberwerx.$(evt.data);
                for (var i = 0; i < elements.length; ++i) {
                    this._handleElementIn(elements.get(i));
                }
            },
            _handleElementsSent: function(evt) {
                var elements = jabberwerx.$(evt.data);
                for (var i = 0; i < elements.length; ++i) {
                    this._handleElementOut(elements.get(i));
                }
            },
            
            /**
             * Cancels a reconnect if one is currently in the pipeline. Triggers the
             * reconnectCancelled event.
             */
            cancelReconnect: function() {
                if (this._reconnectTimerID !== null) {
                    clearTimeout(this._reconnectTimerID);
                    this._reconnectTimerID = null;
                    this._countDownOn = false;
                    this.event('reconnectCancelled').trigger();
                }
            },

            /**
             * <p>Determines if this client is connected in a secure or trusted
             * manner.</p>
             *
             * @returns <tt>true</tt> If the stream is considered secure
             * @type Boolean 
             */
            isSecure: function() {
                return this._stream.isSecure();
            },
        
            /**
             * @private
             * Starts the countdown for the reconnect.
             */
            _startReconnectCountdown: function() {
                var base = jabberwerx._config.baseReconnectCountdown;
                
                // Only try to reconnect if base is greater than zero
                if (base > 0) {
                    var reconnectCountdown = base + Math.round( (Math.random() - 0.5) * base / 5);
                    this._reconnectTimerID = setTimeout(
                                               this.invocation('_reconnectTimeoutHandler'),
                                               reconnectCountdown * 1000);
                    this._countDownOn = true;
                    this.event('reconnectCountdownStarted').trigger(reconnectCountdown);
                }
            },

            /**
             * @private
             * Handles the reconnect timer timeout. Set the client state to reconnecting and attempt
             * a reconnect.
             */
            _reconnectTimeoutHandler: function() {
                this._countDownOn = false;
                this._reconnectTimerID = null;
                this._connectParams.arg.reconnecting = true;
                this.connect(this._connectParams.jid, this._connectParams.password,
                             this._connectParams.arg);
            },
        
            /**
             * @private
             */
            _handleElementOut: function(stanza) {
                stanza = jabberwerx.Stanza.createWithNode(stanza);
                
                var stanzaType = stanza.pType();
                //var msg = "triggering " +stanzaType + "Sent event with: " + stanza.xml();
                this.event(stanzaType + "Sent").trigger(stanza);
            },
            /**
             * @private
             */
            _handleElementIn: function(stanza) {
                stanza = jabberwerx.Stanza.createWithNode(stanza);
                
                stanza.setTo(stanza.getToJID());
                stanza.setFrom(stanza.getFromJID());

                var stanzaType = stanza.pType();
                var notifiers = [
                        this.event('before' + stanzaType + 'Received'),
                        this.event(stanzaType + 'Received'),
                        this.event('after' + stanzaType + 'Received')
                ];

                var results = jabberwerx.reduce(notifiers, function(notifier, handled) {
                    handled = Boolean(handled || false);
                    if (!handled) {
                        var results = notifier.trigger(stanza);
                        if (results) {
                            for (var idx = 0; idx < results.length; idx++) {
                                handled = handled || Boolean(results[idx]);
                            }
                        }
                    }

                    return handled;
                });

                if (    !results &&
                        stanzaType == 'iq' &&
                        (stanza.getType() == 'get' || stanza.getType() == 'set')) {
                    //automatically send feature-not-implemented
                    stanza = stanza.errorReply(jabberwerx.Stanza.ERR_FEATURE_NOT_IMPLEMENTED);
                    this.sendStanza(stanza);
                }
            },

            /**
             * @private
             */
            _handleIqTime: function(evt) {
                var now = new Date();
                var tz;
                tz = now.toString();
                tz = tz.substring(tz.lastIndexOf(' ') + 1);

                var iq = evt.data;
                iq = iq.reply();
                with (new jabberwerx.NodeBuilder(iq.getQuery())) {
                    element('display').text(now.toLocaleString());
                    element('utc').text(jabberwerx.generateTimestamp(now, true));
                    element('tz').text(tz);
                }
                this.sendStanza(iq);

                return true;
            },
            /**
             * @private
             */
            _handleEntityTime: function(evt) {
                var now = new Date();
                var tzo;
                with (now) {
                    var h, m;

                    tzo = getTimezoneOffset();
                    h = tzo / 60;
                    m = tzo % 60;

                    tzo =   (tzo > 0 ? '-' : '+') +
                            (h < 10 ? '0' + h : h) + ':' +
                            (m < 10 ? '0' + m : m);
                }

                var iq = evt.data;
                iq = iq.reply();
                with (new jabberwerx.NodeBuilder(iq.getChild())) {
                    element('tzo').text(tzo);
                    element('utc').text(jabberwerx.generateTimestamp(now, false));
                }
                this.sendStanza(iq);

                return true;
            },

            /**
             * @private
             */
            _generateUsername: function() {
                return /*DEBUG-BEGIN*/'_cf_' +/*DEBUG-END*/ hex_md5(this._guid + ((this._connectionAttempts) + (new Date().valueOf)));
            },
            /**
             * @private
             */
            _generatePassword: function(username) {
                return hex_md5(username + (window.innerHeight + window.innerWidth));
            },

            /**
             * The collection of registered Controllers for this Client.
             * Controllers are responsible for registering themselves with
             * their owning client.
             */
            controllers: {},
            /**
             * The resource name for this client.
             * @type String
             */
            resourceName: null,
            /**
             * The user with whose JID we are currently logged in.
             * @type jabberwerx.User
             */
            connectedUser: null,
            /**
             * @private
             * The stream 
             * @type jabberwerx.Stream
             */
            _stream: null,
            
            /**
             * @private
             *  currently assigned stream handlers
             */

            _streamHandlers: [],
            /**
             * The current client status.
             * @type Number
             */
            clientStatus: 3, //start off diconnected
            /**
             * The server to which we are currently connected.
             * @type jabberwerx.Server
             */
            connectedServer: null,
            /**
             * This entity set must be used to get references to any entity needed for the lifetime of this client/connection.
             * @type jabberwerx.EntitySet
             */
            entitySet: null,
            /**
             * Whether auto-registration is active.
             * @type Boolean
             */
            autoRegister: false,
            /**
             * @private
             */
            _connectionAttempts: 0,
            /**
             * @private
             */
            _reconnectTimerID: null,
            /**
             * @private
             */
            _connectParams: {},
            /**
             * @private
             */
            _autoResourceName: false,
            /**
             * @private
             */
            _countDownOn: false,
            /**
             * @private
             * True if need to connect to secondary httpbinding URL
             */
            _connectSecondaryBindingURL: false,
            /**
             * @private
             * Object contains users' home/backup node
             */
            _userSDInfo: {},
            /**
             *@private
             * Candidate nodeS for the first connection
             */
            _candidateNodes: [],
            /**
             *@private
             * bind resource timer
             */
            _bindResourceTimer: null,
            /**
             *@private
             * reconnection attempts
             */
            _reconnectAttempts: 0
        }, 'jabberwerx.Client');

        /**
         * @constant
         * Indicates the client is connecting
         */
        jabberwerx.Client.status_connecting = 1;
        /**
         * @constant
         * Indicates the client is connected
         */ 
         jabberwerx.Client.status_connected = 2;
        /**
         * @constant
         * Indicates the client is disconnected
         */
         jabberwerx.Client.status_disconnected = 3;
        /**
         * @constant
         * Indicates the client is disconnecting
         */ 
         jabberwerx.Client.status_disconnecting = 4;
        /**
         * @constant
         * Indicates the client is starting a reconnect attempt
         */
         jabberwerx.Client.status_reconnecting = 5;
         
        /**
         * @class jabberwerx.Client.NotConnectedError
         * @minimal
         * <p>Error to indicate the client is not connected, when the operation expects a
         * connection.</p>
         * @description
         * <p>Creates a new NotConnectedError with the given message.</p>
         * @extends jabberwerx.util.Error
         */
        jabberwerx.Client.NotConnectedError = jabberwerx.util.Error.extend('The client is not connected.');
        /**
         * @class
         * @minimal
         * <p>Error thrown when an error is encountered while trying to
         * establish the connection.</p>
         *
         * @description
         * <p>Creates a new ConnectionError with the given message.</p>
         *
         * @param {String} msg The error condition message
         * @extends jabberwerx.util.Error
         */
        jabberwerx.Client.ConnectionError = jabberwerx.util.Error.extend();
        /**
         * @class
         * @minimal
         * <p>Error thrown when an error is encountered after the connection
         * is established.</p>
         *
         * @description
         * <p>Creates a new DisconnectError with the given message.</p>
         *
         * @param {String} msg The error condition message
         * @extends jabberwerx.util.Error
         */
        jabberwerx.Client.DisconnectError = jabberwerx.util.Error.extend();
        
    }
