;(function(){
    if (jabberwerx) {
        jabberwerx.ChatSession = jabberwerx.JWModel.extend(/** @lends jabberwerx.ChatSession.prototype */ {
            /**
             * @class
             * <p>Represents a chat session with another user. There is only one ChatSession object
             * per bare jid. The locked resource (if the session is currently locked) is specified
             * by the jid property.</p>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ChatSession">jabberwerx.ChatSession</a></li>
             * </ul>
             *
             * @description
             * <p>Creates a new ChatSession with the given client, jid, and
             * optional thread</p>
             *
             * @param   {jabberwerx.Client} client The client object with which
             *          event observees are registered.
             * @param   {jabberwerx.JID|String} jid A JID object or JID string
             *          to use as a basis for a jid.
             * @param   {String} [thread] The thread value to use for this
             *          conversation.
             * @throws  {TypeError} if {client} is not valid
             * @throws  {jabberwerx.JID.InvalidJIDError} if {jid} does not represent
             *          a valid JID
             * @constructs jabberwerx.ChatSession
             * @extends jabberwerx.JWModel
             */
            init: function(client, jid, thread) {
                this._super();
                
                this._statesReady = false;
                if (client instanceof jabberwerx.Client) {
                    this.client = client;
                    this.controller = client.controllers.chat ||
                                      new jabberwerx.ChatController(client);
                } else {
                    throw new TypeError("client must be a jabberwerx.Client");
                }
                
                this.jid = jabberwerx.JID.asJID(jid);
                if (!thread) {
                    thread = this._generateRandomThreadValue();
                }
                this.thread = thread;
                
                // Set up events
                this.applyEvent('beforeChatSent');
                this.applyEvent('chatSent');
                this.applyEvent('chatReceived');
                this.applyEvent('threadUpdated');
                this.applyEvent('chatStateChanged');
                this.applyEvent('lockedResourceChanged');

                // Figure out if this is a private message.
                this.privateMessage = this.controller.isPrivateMessage(this.jid);
                if (this.privateMessage) {
                    this._MUCController = client.controllers.muc ||
                                          new jabberwerx.MUCController(client);

                } 

                this._bindHandlers();

                var handlerFunc = this.invocation("_entityChangedHandler");
                this._getEntitySet().event("entityCreated").bind(handlerFunc);
                this._getEntitySet().event("entityDestroyed").bind(handlerFunc);
                
                if (this.privateMessage) {
                    this._getEntitySet().event("entityRenamed").
                         bind(this.invocation("_entityRenamedHandler"));
                }
            },

            /**
             * @private
             */
            _bindHandlers: function() {
                var bindJid = (this.privateMessage) ?
                              this.jid.toString() :
                              this.jid.getBareJIDString();

                // Set up handlers
                var handlerFunc = this.invocation('_chatReceivedHandler');
                this.client.event('messageReceived').bindWhen("message[from^='"
                    + bindJid +"'][type!='groupchat']", handlerFunc);
                // Keep reference to handler for unbinding later
                this._handlerList.push({event: 'messageReceived', handler: handlerFunc});
                
                handlerFunc = this.invocation('_remoteStateChangedHandler');
                this.client.event('messageReceived').bindWhen("message[from^='"
                    + bindJid +"'][type='chat'] *[xmlns='http://jabber.org/protocol/chatstates']",
                    handlerFunc);
                this._handlerList.push({event: 'messageReceived', handler: handlerFunc});
                
                handlerFunc = this.invocation('_presenceChangeHandler');
                this.client.event('presenceReceived').bindWhen(this.invocation('_presenceReceivedFilter'), handlerFunc);
                this._handlerList.push({event: 'presenceReceived', handler: handlerFunc});
            },

            /**
             * @private
             */
            _getEntitySet: function() {
                if (this.privateMessage) {
                    return this._MUCController.room(this.jid).occupants;
                } else {
                    return this.client.entitySet;
                }
            },
            
            /**
             * <p>Retrieves the entity associated with this ChatSession.</p>
             *
             * @returns {jabberwerx.Entity} The entity for {@link jabberwerx.ChatSession#jid}
             */
            getEntity: function() {
                if (!this._entity) {
                    if (this.privateMessage) {
                        this._entity = this._getEntitySet().entity(this.jid);
                    } else {
                        var jid = this.jid.getBareJID();
                        this._entity = this._getEntitySet().entity(jid) ||
                                       new jabberwerx.TemporaryEntity(jid, this._getEntitySet());
                    }
                }
                
                return this._entity;
            },
            /**
             * @private
             */
            _entityChangedHandler: function(evt) {
                if (this.privateMessage && (this.jid.toString() != evt.data.jid.toString())) {
                    return;
                }
                if (this.jid.getBareJIDString() != evt.data.jid.getBareJIDString()) {
                    return;
                }
                
                switch (evt.name.substring("entity".length)) {
                    case "destroyed":
                        this._entity = undefined;
                        break;
                    case "created":
                        this._entity = evt.data;
                        break;
                }
            },

            /**
             * @private
             */
            _entityRenamedHandler: function(evt) {
                if (evt.data.jid && (evt.data.jid == this.jid)) {
                    this.jid = evt.data.entity.jid;

                    this._unbindHandlers();
                    this._bindHandlers();
                }
            },
            
            /**
             * Sends a message to the correspondent. Fires the
             * chatStateChanged, beforeChatSent and chatSent events.
             *
             * @param {String} body The body of the message to send
             */
            sendMessage: function(body) {
                if (!body) {
                    return;
                }
                this._statesReady = true;
                
                var msg = this._generateMessage('active', body);
                this.localState = 'active';
                this.event('chatStateChanged').trigger({jid: null,
                                                        state: this.localState});
                this.event('beforeChatSent').trigger(msg);
                this.client.sendStanza(msg);
                this.event('chatSent').trigger(msg);
            },
            
            /**
             * Set this clients chat state (XEP-0085). Can be one of 'active',
             * 'composing', 'paused', 'inactive' or 'gone'. Sends out message
             * specifying the new chat state provided the ChatControllers 
             * sendChatStates flag is set to true.
             *
             * @param   {String} state The state to set this client to.
             * @returns  {Boolean} true if successfully changed, otherwise false
             * @throws  {jabberwerx.ChatSession.StateNotSupportedError} thrown
             *          if an unsupported state is specified as the argument.
             */
            setChatState: function(state) {
                var retVal = false;
                if (this._setChatStateProperty(state)) {
                    retVal = true;
                    
                    var msg = this._generateMessage(state);
                    if (msg && this.client.isConnected()) {
                        this.client.sendStanza(msg);
                    }
                }
                return retVal;
            },
            
            /**
             * @private
             * Set the chat localState property. Only updates the localState
             * property if the argument differs from the current state.
             *
             * @param   {String} state The state to set this chat session to
             * @returns  {Boolean} true if successfully changed, otherwise false
             * @throws  {jabberwerx.ChatSession.StateNotSupportedError} thrown
             *          if an unsupported state is specified as the argument.
             */
            _setChatStateProperty: function(state) {
                var retVal = false;
                if (jabberwerx.$.inArray(state, ['active', 'composing', 'paused', 'inactive', 'gone']) >= 0) {
                    if (state != this.localState) {
                        this.localState = state;
                        this.event('chatStateChanged').trigger({jid: null,
                                                                state: this.localState});
                        retVal = true;
                    }
                } else {
                    var err = new jabberwerx.ChatSession.StateNotSupportedError("The chat state '"
                        + state + "' is not supported. Should be one of 'active', 'composing'," +
                        "'paused', 'inactive' or 'gone'");
                    throw err;
                }
                return retVal;
            },
            
            /**
             * Method called to clean up object. This method unbinds all
             * handlers for client events and sends 'gone' chat state.
             */
            destroy: function() {
                this._unbindHandlers();
                
                var handlerFunc = this.invocation("_entityChangedHandler");
                this._getEntitySet().event("entityCreated").unbind(handlerFunc);
                this._getEntitySet().event("entityDestroyed").unbind(handlerFunc);

                if (this.privateMessage) {
                    this._getEntitySet().event("entityRenamed").
                         unbind(this.invocation("_entityRenamedHandler"));
                }
                
                this.setChatState('gone');
                this.controller.closeSession(this);
                
                this._super();
            },

            /**
             * @private
             */
            _unbindHandlers: function() {
                var client = this.client;
                jabberwerx.$.each(this._handlerList, function() {
                    client.event(this.event).unbind(this.handler);
                });
            },
            
            /**
             * @private
             * Generates a Message object which can be sent to the correspondent.
             * Will only include the current chatstate when the ChatControllers
             * sendChatStates flag is set to true.
             *
             * <tt>body</tt> may be plaintext, a root HTML element, a <body/> element 
             *  in the XHTML namespace (see xep-71) or an array of HTML elements.         
             *  see {@link jabberwerx.Stanza.setHTML} for additional information.
             *
             * @param   {String} chatstate The chatstate to include in this
             *          message
             * @param   {String|DOM|Array} [body] The plaintext or HTML body to insert into the Message
             *          object.
             * @returns  {jabberwerx.Message} The resultant message object
             */
            _generateMessage: function(chatstate, body) {
                if (!this._statesReady) {
                    return null;
                }
                var msg = null;
                if (body) {
                    msg = new jabberwerx.Message();
                    msg.setThread(this.thread);
                    if (jabberwerx.isElement(body) || jabberwerx.$.isArray(body)) {
                        msg.setHTML(body);
                    } else {
                        msg.setBody(body);
                    }
                }
                if (this.controller.sendChatStates) {
                    msg = msg || new jabberwerx.Message();
                    var nodeBuilder = new jabberwerx.NodeBuilder(msg.getNode());
                    nodeBuilder.element('{http://jabber.org/protocol/chatstates}' + chatstate);
                }
                if (msg) {
                    msg.setType('chat');
                    msg.setTo(this.jid);
                }
                return msg;
            },
            
            /**
             * @private
             */
            _chatReceivedHandler: function(eventObj) {
                var msg = eventObj.data;
                this._updateLockedResource(msg.getFromJID());
                this._updateThread(msg.getThread());

                this._statesReady = !msg.isError();

                if (msg.getBody() || msg.isError()) {
                    this.event('chatReceived').trigger(msg);
                }
                return false;
            },
            
            /**
             * @private
             */
            _updateLockedResource: function(jid) {
                // private messages don't do this.
                if (this.privateMessage) {
                    return;
                }
                if (jid.getResource() != this.jid.getResource()) {
                    this.jid = jid;
                    this.event('lockedResourceChanged').trigger(this.jid);
                }
            },
            
            /**
             * @private
             */
            _updateThread: function(thread) {
                if (thread && thread != this.thread) {
                    this.thread = thread;
                    this.event('threadUpdated').trigger({jid: this.jid, thread: this.thread});
                }
            },
            
            /**
             * @private
             */
            _remoteStateChangedHandler: function(eventObj) {
                var stateNode = eventObj.selected;
                if (this.remoteState != stateNode.nodeName) {
                    // Set the remoteState property and fire the chatStateChanged event
                    this.remoteState = stateNode.nodeName;
                    this.event('chatStateChanged').trigger({jid: this.jid, state: this.remoteState});
                    // Unlock the resource and create a new thread id if the new state is "gone"
                    if (this.remoteState == "gone") {
                        this._updateLockedResource(eventObj.data.getFromJID().getBareJID());
                        this.thread = this._generateRandomThreadValue();
                    }
                }
                return false;
            },
            
            /**
             * @private
             */
            _presenceChangeHandler: function(eventObj) {
                this._updateLockedResource(eventObj.data.getFromJID().getBareJID());
                return false;
            },
            
            /**
             * @private
             */
            _generateRandomThreadValue :function() {
                var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
                var string_length = 10;
                var threadValue = '';
                for (var i=0; i<string_length; i++) {
                    var rnum = Math.floor(Math.random() * chars.length);
                    threadValue += chars.substring(rnum,rnum+1);
                }
                return 'JW_' + threadValue;
            },
            
            /**
             * @private
             */
            _presenceReceivedFilter: function(presence) {
                var type = presence.getType();
                var presJid = (this.privateMessage) ?
                              presence.getFromJID().toString() :
                              presence.getFromJID().getBareJIDString();
                var jid = (this.privateMessage) ?
                          this.jid.toString() :
                          this.jid.getBareJIDString();
                if (presJid == jid && (!type || type == 'unavailable') ) {
                    return presence;
                }
                return false;
            },
            
            /**
             * The jid with which this chat session is communicating.
             * If the chat session has locked onto a resource, the resource
             * value can be found by using {@link jabberwerx.JID#getResource}.
             * @type jabberwerx.JID
             */
            jid : null,
            
            /**
             * Thread element for this chat session. Included in all outgoing
             * messages. Mirrors the value of the thread element been received
             * in messages from the correspondent (if present).
             * @type String
             */
            thread : null,
            
            /**
             * Our current chat state (XEP-0085) in this chat session. It can
             * be one of 'active', 'composing', 'paused', 'inactive' or 'gone'.
             * State is set to active automatically when {@link #sendMessage}
             * is called and set to gone automatically when {@link #destroy} is
             * called.
             *
             * <p><b>Note: Do not set this property directly; Use
             * {@link #setChatState} instead.</b></p>
             * @type String
             */
            localState: null,
            
            /**
             * The state of the correspondent in this session.
             * @type String
             */
            remoteState: null,
            
            /**
             * Used to bind to events and send messages
             * @type jabberwerx.Client
             */
            client : null,
            
            /**
             * The Controller that created this ChatSession
             * @type jabberwerx.ChatController
             */
            controller : null,

            /**
             * Indicates if this session is for a private message.
             * @type Boolean
             */
            privateMessage : false,
            
            /**
             * @private
             * <p>Keeps a list of handlers registered for client events. This
             * list is then used to unbind from those events when the session
             * is to be closed. Objects place in this list should be of the
             * format:</p>
             * <p><pre class="code">{
             *      event: String,
             *      handler: function
             * }</pre></p>
             */
            _handlerList : []
        }, 'jabberwerx.ChatSession');
        
        /**
         * Thrown when a client tries to set this chat session's state to an
         * unsupported state.
         */
        jabberwerx.ChatSession.StateNotSupportedError = jabberwerx.util.Error.extend();
    }
})();
