/**
 * filename:        MUCInviteView.js
 * created at:      2009/11/09T15:15:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */ 
if ((jabberwerx) && (jabberwerx.ui)) {
    jabberwerx.ui.MUCInviteView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MUCInviteView.prototype */{
        /**
         * @class
         * <p>View for accepting or ignoring MUC invites.</p>
         *
         * <p>This displays the room, invitor, reason and nickname for entering the room with.
         * It also gives the user the choice to join the room or ignore the invite.</p>
         * 
         * <p>An event (actionComplete) will be triggered when an action has been undertaken.
         * The action will have been to either join the room or ignore the invite.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.MUCInviteView">jabberwerx.ui.MUCInviteView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a MUCInviteView object.</p>
         * @param   {jabberwerx.MUCController} muc The MUCController
         * @param   {jabberwerx.MUCInvite} mucInvite The muc invite for which this view is been
         *          created.
         * @param   {String} [nickname] Used to initialise the nickname element text element
         * @throws  {TypeError} If muc or mucInvite are not valid
         * @constructs jabberwerx.ui.MUCInviteView
         * @extends jabberwerx.ui.JWView
         */
        init: function(muc, mucInvite, nickname) {
            this._super();

            if (!(muc && muc instanceof jabberwerx.MUCController)) {
                throw new TypeError("muc must be a jabberwerx.MUCController");
            }
            if (!(mucInvite && mucInvite instanceof jabberwerx.MUCInvite)) {
                throw new TypeError("mucInvite must be a jabberwerx.MUCInvite");
            }

            this.muc = muc;
            this.mucInvite = mucInvite;
            // If the nickname argument is a string then set _nick to it's value, otherwise use
            // the connectedUser's display name.
            this._nick = 
              ( (typeof nickname == "string" || nickname instanceof String) && nickname) || 
              (this.muc.client.connectedUser && this.muc.client.connectedUser.getDisplayName());
            
            this.applyEvent("actionComplete");
        },

        /**
         * <p>Creates the DOM to display.</p>
         *
         * <p>Binds the input elements to their respective action methods.</p>
         * <p>Creates the DOM to display. This method creates the following
         *
         * DOM:</p>
         * <pre class="code">&lt;div class="jabberwerx muc_invite_view"&gt;
         *      &lt;div class="room_div"&gt;
         *          &lt;label for="room"&gt;Room:&lt;/label&gt;
         *          &lt;span class="room" id="room"&gt;<i>mucInvite.room</i>&lt;/span&gt;
         *      &lt;/div&gt;
         *      &lt;div class="invitor_div"&gt;
         *          &lt;label for="invitor"&gt;Invitor:&lt;/label&gt;
         *          &lt;span class="invitor" id="invitor"&gt;<i>mucInvite.invitor</i>&lt;/span&gt;
         *      &lt;/div&gt;
         *      &lt;div class="reason_div"&gt;
         *          &lt;label for="reason"&gt;Reason:&lt;/label&gt;
         *          &lt;span class="reason" id="reason"&gt;<i>mucInvite.reason</i>&lt;/span&gt;
         *      &lt;/div&gt;
         *      &lt;div class="nick_div"&gt;
         *          &lt;label for="nick"&gt;Nickname:&lt;/label&gt;
         *          &lt;input type="text" class="nick" id="nick"&gt;<i>nickname</i>&lt;/input&gt;
         *      &lt;/div&gt;
         *      &lt;div class="action_div"&gt;
         *          &lt;input class="action_button join" type="button" val="Join"/&gt;
         *          &lt;input class="action_button ignore" type="button" val="Ignore"/&gt;
         *      &lt;/div&gt;
         * &lt;/div&gt;</pre>
         **@param {Document} doc The document to use for creating content
         * @returns {Element} The generated element to display
         *
         */
        createDOM: function(doc) {
            // Create the overall container div
            var builder = jabberwerx.$("<div/>", doc)
                .addClass("jabberwerx muc_invite_view");
            
            // Create the room label and span
            var roomDiv = jabberwerx.$("<div/>").
                addClass("room_div").
                appendTo(builder);
            jabberwerx.$("<label/>").
                attr("for", "room").
                text(jabberwerx._("Room:")).
                appendTo(roomDiv);
            jabberwerx.$("<span/>").
                addClass("room").
                attr("id", "room").
                attr("title",jabberwerx.JID.unescapeNode(this.mucInvite.getRoom().getNode())).
                text(jabberwerx.JID.unescapeNode(this.mucInvite.getRoom().getNode())).
                appendTo(roomDiv);
            
            // Create the invitor label and span, if an invitor is present in mucInvite
            if (this.mucInvite.getInvitor()) {
                var invitorDiv = jabberwerx.$("<div/>").
                    addClass("invitor_div").
                    appendTo(builder);
                jabberwerx.$("<label/>").
                    attr("for", "invitor").
                    text(jabberwerx._("Invitor:")).
                    appendTo(invitorDiv);
                jabberwerx.$("<span/>").
                    addClass("invitor").
                    attr("id", "invitor").
                    attr("title",this.mucInvite.getInvitor().getBareJID().toDisplayString()).
                    text(this.mucInvite.getInvitor().getBareJID().toDisplayString()).
                    appendTo(invitorDiv);
            }
            
            // Create the reason label and span, if a reason is present in mucInvite
            if (this.mucInvite.getReason()) {
                var reasonDiv = jabberwerx.$("<div/>").
                    addClass("reason_div").
                    appendTo(builder);
                jabberwerx.$("<label/>").
                    attr("for", "reason").
                    text(jabberwerx._("Reason:")).
                    appendTo(reasonDiv);
                jabberwerx.$("<span/>").
                    addClass("reason").
                    attr("id", "reason").
                    text(this.mucInvite.getReason()).
                    appendTo(reasonDiv);
            }
            
            // Create the nickname label and text input
            var nickDiv = jabberwerx.$("<div/>").
                addClass("nick_div").
                appendTo(builder);
            jabberwerx.$("<label/>").
                attr("for", "nick").
                text(jabberwerx._("Nickname:")).
                appendTo(nickDiv);
            var nickInput = jabberwerx.$('<input type="text"/>').
                addClass("nick").
                attr("id", "nick").
                val(this._nick || "").
                appendTo(nickDiv);
            
            // Create the action buttons
            var actionDiv = jabberwerx.$("<div/>").
                addClass("action_div").
                appendTo(builder);
            jabberwerx.$('<input type="button"/>').
                addClass("action_button").
                addClass("join").                
                val(jabberwerx._("Join")).
                click(this.invocation("joinRoom")).
                appendTo(actionDiv);
            jabberwerx.$('<input type="button"/>').
                addClass("action_button").
                addClass("ignore").
                val(jabberwerx._("Ignore")).
                click(this.invocation("ignoreInvite")).
                appendTo(actionDiv);

            var errorMsgDiv = jabberwerx.$("<div/>").
                 addClass("error_msg_div").
                 appendTo(builder);
             
             jabberwerx.$("<span/>").
                    addClass("error_msg").
                    hide().
                    appendTo(errorMsgDiv);
            
                    
            // Return the root div.muc_invite_view
            return builder.get(0);
        },

        /**
         * <p>Shows the form if hidden in the document.</p>
         */
        show: function() {
            if (this.jq) {
                this.jq.show();
            }
        },

        /**
         * <p>Hides the form in the document.</p>
         */
        hide: function() {
            if (this.jq) {
                this.jq.hide();
            }
        },
        
        /**
         * <p>Join the room to which the invite has been received. Creates and joins the room
         * specified by the MUCInvite object and, upon the enter room action completing,
         * triggers the actionComplete event.</p>
         */
        joinRoom: function() {
            var nick = jabberwerx.$(".jabberwerx.muc_invite_view .nick_div #nick").val();
            var errorSpan = jabberwerx.$(".jabberwerx.muc_invite_view .error_msg_div .error_msg");
            if (!nick) {
                errorSpan.text(jabberwerx._("A nickname must be specified"));
                errorSpan.show();
            } else {
                errorSpan.hide();
                // Disable Join & Ignore buttons while the room is been entered
                jabberwerx.$(".jabberwerx.muc_invite_view .action_button.join").attr("disabled", "true");
                jabberwerx.$(".jabberwerx.muc_invite_view .action_button.ignore").attr("disabled", "true");
                
                // Get or create the room
                var room = this.muc.room(this.mucInvite.getRoom());
                
                // Enter the room and trigger actionComplete event upon enter completed
                var that = this;
                var cb = function(err) {
                    var actionObj = {action: jabberwerx.ui.MUCInviteView.ACTION_JOIN};
                    err && (actionObj.error = err);
                    // Enable Join & Ignore buttons again
                    jabberwerx.$(".jabberwerx.muc_invite_view .action_button.join").removeAttr("disabled");
                    jabberwerx.$(".jabberwerx.muc_invite_view .action_button.ignore").
                            removeAttr("disabled");
                    // Trigger event
                    that.event("actionComplete").trigger(actionObj);
                };
                
                room.enter(nick, {successCallback: cb, errorCallback: cb, password: this.mucInvite.getPassword()});
            }
        },
        
        /**
         * <p>Ignore the invite. Sends no response back to the server and triggers the
         * actionComplete event with the ACTION_IGNORE constant.</p>
         */
        ignoreInvite: function() {
            this.event("actionComplete").trigger(
                {action: jabberwerx.ui.MUCInviteView.ACTION_IGNORE});
        },
        
        /**
         * <p>The muc controller object used to join the room.</p>
         * @type    {jabberwerx.MUCController}
         */
        muc: null,

        /**
         * <p>The invite used to populate the information fields.</p>
         * @type    {jabberwerx.MUCInvite}
         */
        mucInvite: null,
        
        /**
         * @private
         * <p>The default nickname used to enter the room with. Used to initialy populate the
         * nickanme text input.</p>
         * @type    {String}
         */
        _nick: null
    }, 'jabberwerx.ui.MUCInviteView');
    
    /**
     * <p>Returned as part of the event object in the actionComplete event when join is the
     * action which has been taken.</p>
     * @type String
     */
    jabberwerx.ui.MUCInviteView.ACTION_JOIN = "join";
    
    /**
     * <p>Returned as part of the event object in the actionComplete event when ignore is the
     * action which has been taken.</p>
     * @type String
     */
    jabberwerx.ui.MUCInviteView.ACTION_IGNORE = "ignore";
}
