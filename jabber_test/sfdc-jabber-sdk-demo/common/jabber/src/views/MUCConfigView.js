/**
 * filename:        MUCConfigView.js
 * created at:      2010/02/04T15:15:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
 

if ((jabberwerx) && (jabberwerx.ui)) {
        jabberwerx.ui.MUCConfigView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MUCConfigView.prototype */{
            /**
             * @class
             * <p>View for displaying XDataForm for room configuration.</p>
             *
             * <p>This view displays the XDataForm received from the server. </p>
             * Possible states for the view:
             *   not initialized/initialized
             *   error/non error state
             * @description
             * <p>Creates a MUCConfigView object.</p>
             * @param   {jabberwerx.MUCRoom} mucRoom Muc room to configure.
             * @throws  {TypeError} If {mucRoom} is not valid
             * @constructs jabberwerx.ui.MUCConfigView
             * @extends jabberwerx.ui.JWView
             */
            init: function(mucRoom) {
                this._super();
                
                if (!(mucRoom && mucRoom instanceof jabberwerx.MUCRoom)) {
                    throw new TypeError("mucRoom must be a MucRoom");
                }                    
                this._initialized = false;
                this._error = false;         
                this.room = mucRoom;
                this.room.event("roomExited").bind(this.invocation("_handleRoomDestroyed"));
                this.room.fetchConfig(this.invocation('_handleConfigForm'));
            },
            /**
             * <p>Destroys this view. Unbinds invocations.</p>
             *
             */
            destroy: function() {
                this.room.event("roomExited").unbind(this.invocation("_handleRoomDestroyed"));
                this._super();
            },
           /**
            * <p>This implementation
            * extends the superclass to adjust the content and tablist to match
            * the new height.</p>
            *
            * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
            *          retrieve the current height)
            * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
            *          <b>NOT</b> be updated
            * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
            *          jabberwerx.ui.JWView (if changing)
            * @see     jabberwerx.ui.JWView#height
            */
            height: function(h, noupdate) {
                var result = this._super(h, true);
                if (h !== undefined && this.jq) {
                    var height = this.jq.get(0).clientHeight -
                                 (this.jq.find(".muc_config_buttons").outerHeight(true) +
                                 this.jq.find(".muc_config_error").outerHeight(true));
                    this.jq.find(".muc_config_form").height(height);
                    
                    if (!noupdate) {
                        this.update();
                    }
                }
            },
                    
            /**
             * <p>Creates the DOM for MUCConfigView.
             *  
             * DOM:</p>
             * <pre class="code">&lt;div class="jabberwerx muc_config_view" id="room1"&gt; 
             *      &lt;div class="muc_config_form"&gt;
             *      &lt;/div&gt;
             *      &lt;div class="muc_config_error"&gt;
             *      &lt;/div&gt;
             *      &lt;div class="muc_config_buttons"&gt;
             *          &lt;input class="muc_config_button_submit join" type="button" val="Submit"/&gt;

             *          &lt;input class="muc_config_button_cancel join" type="button" val="Cancel"/&gt;

             *      &lt;/div&gt;
             *      &lt;/div&gt;
           
             * &lt;/div&gt;</pre>
             * @param   {Element} doc Element for embedding the view           
             * @returns {Element} The generated element to display
             */
            createDOM: function(doc) {
                var div = jabberwerx.$("<div/>", doc).
                           addClass("jabberwerx muc_config_view");
                                           
                var config =  jabberwerx.$("<div/>"). 
                    addClass("muc_config_form").
                    appendTo(div);
                           
                var error =  jabberwerx.$("<div>&nbsp;</div>"). 
                    addClass("muc_config_error").
                    appendTo(div);
                
                var buttons =  jabberwerx.$("<div/>"). 
                    addClass("muc_config_buttons").
                    appendTo(div);  
      
                var submit = jabberwerx.$("<input type='button'/>").
                    addClass("muc_config_button_submit").
                    attr("value", jabberwerx._("Submit")).
                    click(this.invocation("submit")).
                    appendTo(buttons);                                 

                var cancel = jabberwerx.$("<input type='button'/>").
                    addClass("muc_config_button_cancel").
                    attr("value", jabberwerx._("Cancel")).
                    click(this.invocation("cancel")).
                    appendTo(buttons);  
                
                //Hide the view until response from the server is received.
                this.hide();
                // Return the root div.muc_config_view
                return div[0]; 
            }, 
                     
              /**

             * Get's invoked when config for XDataForm for the room is received (fetchConfig callback).

             * @private

             */   

            _handleConfigForm: function (form, error) {
                if (form) {
                    this._initialized = true;
                    if (!form.getTitle()) {
                        form.setTitle("Configure " + this.room.getDisplayName());
                    }
                    var formView = new jabberwerx.ui.XDataFormView(form);
                    formView.render().appendTo(jabberwerx.$(".muc_config_form", this.jq));
                    this.dimensions(this.dimensions());
                } else {
                    jabberwerx.$(".muc_config_button_submit", this.jq).attr("disabled","disabled");                    
                    this._showError(error);
                }
                this.show();
            },
 
            /**

             * Get's invoked when iq result for submitting the form is received (applyConfig callback).

             * @private

             */   

            _handleConfigSubmit: function (error) {
                if (error) {
                    this._showError(error);
                }
                else {
                    this.destroy();
                }
            },
            
            /**

             * Get's invoked in case of room exit event.

             * @private

             */   

            _handleRoomDestroyed: function (evt) {
                this.destroy();
            },            
            
         /**
           * <p>Updates view based on server response. Adjusts heights for the view elements 
           * based on server response. If error is received, the height for the XDataForm 
           * element is reduced to accomodate error element. Also sets the dimenstions for
           * XDataFormView . </p>
           *
           * @returns {Boolean} <tt>true</tt> if the view is updatable
           */                                
            update: function() {
                var retval = this._super();
                if (retval) {
                    this.jq.find(".muc_config_form .xdata_form_view").each(function() {
                        if (this.jw_view) {
                            this.jw_view.update();
                        }
                    });
                }
                return retval;
            },
            
            /**
             * <p>Shows the form if hidden in the document.</p>
             */
            show: function() {
                if (this.jq) {
                    this.jq.show();
                }
            },

            /**
             * <p>Hides the form in the document.</p>
             */
            hide: function() {
                if (this.jq) {
                    this.jq.hide();
                }
            },
            
            /**
             * <p>Submits entered values the form in the document, displays errors if any
             * occured or destroys the view.</p>
             */
            submit: function() {
                if (this._initialized) {
                    var form_view = jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view;
                    var form = null;
                    if (form_view) {
                        try {
		                    form = form_view.submit();
	                    }
	                    catch(e) {
	                        this._showError(e);
	                        return; 
	                    }
	                }
	                //If form is null, apply config will send the form type "cancel" request.
                    this.room.applyConfig(form, this.invocation('_handleConfigSubmit'));
	            } else {
	                this.destroy();
	            }        
            }, 
            
            /**
             * <p>Destroys the view if not initialized, otherwise
             * submits iq request for the form of type "cancel".</p>
             */
            cancel: function() {
                if (this._initialized) {
                    var form_view = jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view; 
                    if (form_view) {         
                        var form = form_view.cancel();
                        this.room.applyConfig(form, this.invocation('_handleConfigSubmit'));
                    }
                    else {
                        this.destroy();
                    }
                } else {
                    this.destroy();
                }
            },  
            
            /**
             * <p>Shows error text.</p>
             * @private

             */
            _showError: function(error) {
                var msg;
                if (error instanceof jabberwerx.Stanza.ErrorInfo) {
                    msg = jabberwerx.errorReporter.getMessage(error.getNode());
                    if (!msg) {
                        msg = error.getNode().xml;
                    }
                } 
                else if (error instanceof TypeError) {
                    msg = error.message;
                }
                else {
                    msg = error;
                }           
	            jabberwerx.$(".muc_config_error", this.jq).text(msg);
	            //This will allow only adjust height for the error handling once and
	            //will not shrink the height on any consequtive error display
	            if (!this._error) {
                    var height = jabberwerx.$(".muc_config_form", this.jq).height() -
                                 jabberwerx.$(".muc_config_error", this.jq).height(); 
                    jabberwerx.$(".muc_config_form", this.jq).height(height);
                    if ((jabberwerx.$(".xdata_form_view", this.jq).get(0) &&
                         jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view)) {
                        jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view.height(height);                   
                    } 
                }                  
                this._error = true;

            },
                                
            /**
             * <p>The muc room passed in the constructor.</p>
             * @type    {jabberwerx.MUCRoom}
             */
            room: null,
            
            /**
             * <p>Initialization flag.</p>
             * @type    {Boolean}
             * @private

             */
            _initialized: false,
            
            /**
             * <p>Error flag.</p>
             * @type    {Boolean}
             * @private

             */
            _error: false
                                 
        }, 'jabberwerx.ui.MUCConfigView');        
}
