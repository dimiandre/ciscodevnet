/**
 * filename:        XDataFormView.js
 * created at:      2009/11/09T15:15:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
 

if ((jabberwerx) && (jabberwerx.ui)) {
        jabberwerx.ui.XDataFormView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.XDataFormView.prototype */{
            /**
             * @class
             * <p>View for displaying XDataForm.</p>
             *
             * <p>This displays the XDataForm including title, instructions and all visible fields. </p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.XDataFormView">jabberwerx.ui.XDataFormView</a></li>
             * </ul>             
             * @description
             * <p>Creates a XDataFormView object.</p>
             * @param   {jabberwerx.XDataForm} dataForm Data form to initialize the view with.
             * @throws  {TypeError} If dataForm is not valid
             * @constructs jabberwerx.ui.XDataFormView
             * @extends jabberwerx.ui.JWView
             */
            init: function(dataForm) {
                this._super();
                
                this._dataForm = dataForm;
                if (!(dataForm && dataForm instanceof jabberwerx.XDataForm)) {
                    throw new TypeError("dataForm must be a XDataForm");
                }
                this.applyEvent("xdataItemSelected");
            },
            /**
             * <p>Updates this view. This method is called by {@link jabberwerx.ui.JWView#dimensions},
             * {@link jabberwerx.ui.JWView#height}, and {@link jabberwerx.ui.JWView#width} automatically, but may also be
             * called directly to force an update of the view.</p>
             *
             * <p>This method will size all child html controls on the form to fit within 
             * the width of client area. This is done here since width and height of the view 
             * should be esteblished before we can calculate the width of the client area, since 
             * we need to take scroll bars into account.</p>
             *
             * @returns {Boolean} <tt>true</tt> if the view is updatable (e.g. if
             *          {@link jabberwerx.ui.JWView#jq} is valid).
             */        
             update: function() {
                var retval = this._super();
                if (retval) {
                    var controlsWidth = this.jq.get(0).clientWidth;
                    var delta = (jabberwerx.$.browser.msie ? 8 : 0);
                    //IE6 does not seem to include padding and margins in client area width
                    this.jq.children(":not(.result_table)").width(controlsWidth-delta);                
                }
                
                return retval;
            },
            /**
             * <p>Creates the DOM for XDataForm to display including title, 
             * instructions and DOM's of each field.
             *  
             * DOM:</p>
             * <pre class="code">&lt;div class="jabberwerx xdata_form_view"&gt;
             *      &lt;table&gt;
             *          &lt;tr id="title_row"&gt;
             *          &lt;td class="title" colspan="2"&gt;Title text&lt;/td&gt;
             *          &lt;/tr&gt;
             *          &lt;tr id="instr"&gt;
             *          &lt;td class="title" colspan="2"&gt;Instructions&lt;/td&gt;
             *          &lt;/tr&gt;             
             *      &lt;/table&gt;
             *      &lt;/div&gt;
             * &lt;/div&gt;</pre>
             * @param   {Element} doc Element for embedding the view           
             * @returns {Element} The generated element to display
             */
            createDOM: function(doc) {
                var div = jabberwerx.$("<div/>", doc).
                           addClass("jabberwerx xdata_form_view");
                           
                                             
                if (this._dataForm.getTitle()) {
                    var titleRow = jabberwerx.$("<tr/>");
                       
                    var titleCell = jabberwerx.$("<div/>").
                        text(this._dataForm.getTitle()).
                        addClass("title").
                        appendTo(div);
                }
                if (this._dataForm.getInstructions()) {      
                    var instrRow = jabberwerx.$("<tr/>");
                        
                    var instrCell =  jabberwerx.$("<div/>"). 
                        text(this._dataForm.getInstructions()).
                        addClass("instr").
                        appendTo(div);
                }
                           

                //Iterate through elements and build a form
                var that = this;
                var table;
                var fields = this._dataForm.fields;
                var reported = this._dataForm.reported;
                var items = this._dataForm.items;
                var addFields = true;
                var result = false;
                if (fields.length) {
                    if (this._dataForm.getType() == "result") {
                        //Only display fields on the result form if reported and items are not present
                        if (reported.length || items.length) {                                       
                            addFields = false;       
                        } else {
                            //Fields will be displayed in the read-only manner
                            result = true;
                        }
                    } 
                    if (addFields) {
                        jabberwerx.$.each(fields, function() {
                            table = jabberwerx.$("<table/>").appendTo(div);
                            //If not field element, just display text from it                       
                            // Create divider for the new field element
                            var fieldRow = jabberwerx.$("<tr/>");                    
                            if (that._addFieldToForm(fieldRow, this, result)) {
                                fieldRow.appendTo(table);   
                            }
                        });
                    }
                }
                
                //Add table for result display with the header row, then 
                //iterate through reported elements to add column headers               
                if (reported.length) { 
                    table = jabberwerx.$("<table cellspacing='0' cellpadding='0'/>").
                                addClass("result_table").
                                appendTo(div);
                    var reportedRow = jabberwerx.$("<tr/>").
                                          addClass("result_row").
                                          appendTo(table);
                    jabberwerx.$.each(reported, function() {           
                        that._addReportedToForm(reportedRow, this);   
                    });  
                }
                              
                if (items.length) {
                    //Add table tag just in case if reported list has not been provided
                    if (!table) {
                        table = jabberwerx.$("<table cellspacing='0' cellpadding='0'/>").
                                    addClass("result_table").
                                    appendTo(div);
                    }
                    jabberwerx.$.each(items, function(n) {
                        var item = this;						                    
                        // Create row for the new item
                        var itemRow = jabberwerx.$("<tr/>").
                                      addClass((n + 1) % 2 == 0 ? "even" : "odd").
                                      attr("id", item._guid).
                                      appendTo(table);
                        itemRow.bind('click', function() {
                            that.event("xdataItemSelected").trigger({selected: item});
                        });

                        that._addItemToForm(itemRow, this);						
                    });  
                }
                
                // Return the passed
                return div[0]; 
            },

            /**
             * <p>Adds DOM element to the view for the passed fields.</p>
             * @private
             */ 
           _addFieldToForm: function(fieldRow, field, result) {      
                //field should contain type, label, var, desc
                
                if (field.getType() == "hidden") {
                    return false;
                }
                if (field.getVar()) {
                    fieldRow.attr("id", field.getVar());
                }                
                if (field.getLabel()) {
                     jabberwerx.$("<td />").
                        addClass("field_label").
                        text(field.getLabel()).
                        appendTo(fieldRow);
                }
                    
                var fieldType = field.getType() || "text-single"; 
                var fieldValue = jabberwerx.$("<td />").
                                  addClass("field_value").
                                  addClass(fieldType).
                                  appendTo(fieldRow);
                 
                 switch (fieldType) {
                     case 'boolean':                        
                         var check = jabberwerx.$("<input type='checkbox'/>").
                                      attr("title", field.getDesc()).                        
                                      appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             check.attr("title", field.getDesc());
                         }
                         if (field.getValues()[0] == "1") {                 
                             check.attr("checked","1");
                         } 
                         if (result) {
                             check.attr("disabled","disabled");
                         }
                         break;
                     case 'fixed':
                         var fixed = fieldValue.text(field.getValues()[0]);
                         if (field.getDesc()) {                 
                             fixed.attr("title", field.getDesc());
                         }                            
                         if (!field.getLabel()) {
                             // removing the class allows the cell to span the table
                             fieldValue.removeClass("field_value");
                         }
                         break;
                     case 'hidden':
                         break;
                     case 'jid-single':
                         var jid_single = jabberwerx.$("<input type='text'/>").
                                            addClass("line-single").                                                                                           
                                            attr("size", "1").               
                                            attr("title", field.getDesc()).                            
                                            appendTo(fieldValue);
                         if (field.getValues().length > 0) {             
                             jid_single.attr("value", field.getValues()[0]);   
                         }
                         if (field.getDesc()) {                 
                             jid_single.attr("title", field.getDesc());
                         }
                         if (result) {
                             jid_single.attr("disabled","disabled");
                         }                         
                         break;
                     case 'jid-multi':
                         var jid_multi = jabberwerx.$("<textarea/>").
                                          attr("row", "2").
                                          attr("wrap", "off").
                                          addClass("line-multi").
                                          appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             jid_multi.attr("title", field.getDesc());
                         }
                         //The values become individual lines in the textarea control                         
                         var jids = "";
                         for (var i = 0; i < field.getValues().length; i++) {
                             jids = jids + field.getValues()[i];
                             if (i < field.getValues().length - 1) {
                                 jids = jids + '\r';
                             }                 
                         }                         
                         if (field.getValues().length > 0) {             
                             jid_multi.text(jids);    
                         }
                         if (result) {
                             jid_multi.attr("disabled","disabled");
                         }                         
                         break;                                                                      
                     case 'list-single':
                         var list_single = jabberwerx.$("<select/>").
                                            attr("size", "1").
                                            addClass("list-single").                                                                  
                                            appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             list_single.attr("title", field.getDesc());
                         }                            
                                            
                         jabberwerx.$.each(field.getOptions(), function() {
                             var option = jabberwerx.$("<option/>").
                                           attr("value", this.value).
                                           text(this.label).
                                           appendTo(list_single);
                             var option_value = this.value;
                             jabberwerx.$.each(field.getValues(), function() {
                                 if (this == option_value) {
                                     option.attr("selected","selected");
                                 }
                             });                                                          
                         });
                         if (result) {
                             list_single.attr("disabled","disabled");
                         }                         
                         break;                                         
                     case 'list-multi':
                         var list_multi = jabberwerx.$("<select/>").
                                           addClass("list-multi").                                                                  
                                           attr("size", "3").
                                           attr("multiple", "multiple").
                                           appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             list_multi.attr("title", field.getDesc());
                         }                                                                       
                         jabberwerx.$.each(field.getOptions(), function() {
                             var option = jabberwerx.$("<option/>").
                                           attr("value", this.value).
                                           text(this.label).
                                           appendTo(list_multi);
                             var option_value = this.value;
                             jabberwerx.$.each(field.getValues(), function() {
                                 if (this == option_value) {
                                     option.attr("selected","selected");
                                 }
                             }); 
                         });
                         if (result) {
                             list_multi.attr("disabled","disabled");
                         }                       
                         break; 
                     case 'text-single':
                         var text_single = jabberwerx.$("<input type='text'/>").
                                            addClass("line-single").
                                            appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             text_single.attr("title", field.getDesc());
                         }                                                                                                                   
                         if (field.getValues().length > 0) {             
                             text_single.attr("value", field.getValues()[0]); 
                         }
                         if (result) {
                             text_single.attr("disabled","disabled");
                         }                         
                         break;                                                   
                     case 'text-multi':
                         var text_multi = jabberwerx.$("<textarea/>").
                                           attr("row", "2").
                                           attr("wrap", "off").
                                           addClass("line-multi"). 
                                           appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             text_multi.attr("title", field.getDesc());
                         }  
                         //The values become individual lines in the textarea control                                                                                                               
                         var lines = "";
                         for (var i = 0; i < field.getValues().length; i++) {
                             lines = lines + field.getValues()[i];
                             if (i < field.getValues().length - 1) {
                                 lines = lines + '\r';
                             }                 
                         }
                         if (field.getValues().length > 0) {             
                             text_multi.text(lines);    
                         }
                         if (result) {
                             text_multi.attr("disabled","disabled");
                         }                         
                         break;                    
                     case 'text-private':
                         var text_private = jabberwerx.$("<input type='password'/>").
                                             addClass("line-single").                      
                                             appendTo(fieldValue);
                         if (field.getDesc()) {                 
                             text_private.attr("title", field.getDesc());
                         }                                                                                                                                                            
                         if (field.getValues().length > 0) {                     
                             text_private.attr("value", field.getValues()[0]);
                         }
                         if (result) {
                             text_private.attr("disabled","disabled");
                         }                         
                         break;                                                                                                                                                                    
                  }
                  
                  return true;
            },
         
            /**
             * <p>Adds DOM element to the view for the passed result fields.</p>
             * @private
             */ 
            _addResultFieldToForm: function(fieldRow, field) {      
                //field should contain type, label, var, desc
                
                if (field.getType() == "hidden") {
                    return false;
                }
                 
                 var fieldType = field.getType() || "text-single";
                 var fieldValue = jabberwerx.$("<td />").
                                   addClass("field_value").
                                   addClass(fieldType).
                                   appendTo(fieldRow);
                 
                 if (field.getVar()) {
                     fieldValue.attr("id", field.getVar());
                 }
                 
                 switch (fieldType) {
                     case 'boolean': 
                         var check = jabberwerx.$("<img/>").appendTo(fieldValue);
                         if (field.getValues()[0] == "1") {                 
                             check.attr("src", jabberwerx.ui.getThemeImageURL("icon-boolean-true.png"));     
                         } else {
                             check.attr("src", jabberwerx.ui.getThemeImageURL("icon-boolean-false.png"));
                         }                                                                                                                        
                         break;
                     case 'fixed':
                     case 'jid-single':
                     case 'text-single':
                     case 'list-single':
                        if (field.getValues().length > 0) {
                            if (fieldType == "jid-single") {
                                fieldValue.append((jabberwerx.JID.unescapeNode(field.getValues()[0]) || "\u00A0") + "<br/>");
                            } else {
                                fieldValue.append((field.getValues()[0] || "\u00A0") + "<br/>");
                            }
                        } else {
                            fieldValue.text("\u00A0");
                        }
                        break;
                     case 'text-private':
                        if (field.getValues().length > 0) {             
                            fieldValue.append("******");   
                        } else {
                            fieldValue.text("\u00A0");
                        }
                        break;
                     case 'jid-multi':
                     case 'text-multi':
                     case 'list-multi':
                         //The values become individual lines                       
                         var lines = "";
                         for (var i = 0; i < field.getValues().length; i++) {
                             if (fieldType == "jid-multi") {
                                 lines = lines + jabberwerx.JID.unescapeNode(field.getValues()[i]);
                             } else {
                                 lines = lines + field.getValues()[i];
                             }
                             if (i < field.getValues().length - 1) {
                                 lines = lines + '<br/>';
                             }                 
                         }
                         if (field.getValues().length > 0) {             
                             fieldValue.append(lines);    
                         } else {
                             fieldValue.text("\u00A0");
                         }
                         break;                                                                      
                  }
                  
                  return true;
            }, 
        /**
         * <p>Adds DOM reported field cell (header cell) to the header row for the passed field.</p>
         * @private
         */ 
            _addReportedToForm: function(row, field) {
                //Hidden types should not be displayed
                if (field.getType() == 'hidden') {
                    return;
                }
                var cell = jabberwerx.$("<th/>").
                               addClass("field_reported").
                               text(field.getLabel() || field.getVar() || "\u00A0").
                               appendTo(row); 
                if (field.getVar()) {
                    cell.attr("id", field.getVar());
                }                                     
            },
      
       /**
        * <p>Adds DOM item fields (row cells) to the passed item row and item.</p>
        * @private
        */ 
            _addItemToForm: function(row, item) {
                var that = this;
                jabberwerx.$.each(item.fields, function() {
                    that._addResultFieldToForm(row, this);
                });
            },            
                        
            /**
             * <p>Creates XDataForm of type "submit".</p> 
             * @returns {XDataForm} Data form type submit.
             */    
            submit: function() {
                //Walk DOM and retrieve the form values
                var submitFields = {};
                //Iterate through all the fields that have vars defined
                var that = this;
                var fields = this._dataForm.fields;
                var values = [];          
                jabberwerx.$.each(fields, function() {
                    values = that.getFieldValues(this);
                    if (this.getVar()) {
                        if (values.length) {
                            submitFields[this.getVar()] = values;
                        }
                        else if (this.getValues().length > 0) {
                            submitFields[this.getVar()] = [];
                        }
                    }
                });  
                
                //Remove any existing validation error styling
                jabberwerx.$("*", this.jq).removeClass("form_validation_error");
                
                //Find corresponding html elements by var 
                //Get the value and append to the list                              
                try {
                    var submit = this._dataForm.submit(submitFields); 
                }
                catch(e) {
                    //Add styling to highlight validation error
                    jabberwerx.$("#" + e.field, this.jq).addClass("jabberwerx xdata_form_view form_validation_error");
                    throw e;
                }

                
                return submit;                         
            },

           /**
             * <p>Creates XDataForm of type "cancel".</p> 
             * @returns {XDataForm} Data form type cancel.
             */  
            cancel: function() {
                var cancel = this._dataForm.cancel(); 
                
                //Remove any existing validation error styling
                jabberwerx.$("*", this.jq).removeClass("form_validation_error");
                
                return cancel;              
            },  

              
        /**
         * <p>Returns array of field values for the passed field.</p>
         * @param {jabberwerx.XDataFormField} field Field for which to return the values
         * @returns {Array} Values for the field, can be empty.
         */                          
            getFieldValues: function(field) {
                var values = [];
                var html_elem;
                if (field && field instanceof jabberwerx.XDataFormField && field.getVar()) {    
                    switch (field.getType()) {
                        case 'fixed':
                            break;
                        case 'hidden':
                            break; 
                        case 'boolean':  
                            html_elem = this.jq.find("#" + field.getVar() + " td input");
                            if (html_elem.attr("type") == "checkbox") {
                                var checked = false;
                                if (html_elem.attr("checked")) {
                                    values.push(true);
                                } else {
                                    values.push(false);
                                }
                            } 
                            break;
                        case 'jid-single':
                        case 'text-single':
                             html_elem = this.jq.find("#" + field.getVar() + " td input");
                             if (html_elem.attr("type") == "text") {
                                 var html_value = html_elem.attr("value");
                                 if (html_value && html_value != "") {
                                     values.push(html_value);
                                 }
                             }
                             break;                                                          
                        case 'jid-multi':
                        case 'text-multi':
                             html_elem = this.jq.find("#" + field.getVar() + " td textarea");
                             var html_value = html_elem.attr("value");
                             var values = [];
                             if (html_value && html_value != "") {
                                 values = html_value.split("\n");
                             }
                             for (var i=0; i < values.length; i++) {
                                 values[i] = values[i].replace("\r","");
                             }
                             break;                            
                        case 'list-multi':
                        case 'list-single':
                             html_elem = this.jq.find("#" + field.getVar() + " td select option");
                             var selected = [];
                             jabberwerx.$.each(html_elem, function() {
                                 if (jabberwerx.$(this).attr("selected")) {
                                     selected.push(jabberwerx.$(this).attr("value"));
                                 }           
                             });                              
                             values = [].concat(selected);                       
                             break;
                         case 'text-private':
                             html_elem = this.jq.find("#" + field.getVar() + " td input");
                             if (html_elem.attr("type") == "password") {
                                 var html_value = html_elem.attr("value");
                                 if (html_value && html_value != "") {
                                     values.push(html_value);
                                 }
                             }
                             break;                             
                    }
               }
              
               return values;
            },            
            
            
            /**
             * @private 
             *<p>The data form object passed in the constructor.</p>
             * @type    jabberwerx.XDataForm
             */
            _dataForm: null
        }, 'jabberwerx.ui.XDataFormView');        
}
