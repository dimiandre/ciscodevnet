
;(function(){
	if (jabberwerx && jabberwerx.ui) {
		jabberwerx.ui.MessageView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MessageView.prototype */{
			/**
			 * @class
			 * <p>The view behind a single message in a list of messages.</p>
			 * 
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
             * </ul>
             *
			 * @description
			 * <p>Creates a new jabberwerx.ui.Message with the given data.</p>
			 *
			 * @param   {Object} data The data for the message
			 * @param   {Date} data.timestamp The message timestamp
			 * @param   {String} data.displayName The displayName of the sender
			 * @param   {String|Element} data.content The message content.
			 * @param   {String} [data.cssClassName] The CSS class for this message
			 * @constructs jabberwerx.ui.MessageView
			 * @extends jabberwerx.ui.JWView
			 */
			init: function(data/*{timestamp, displayName, content, cssClassName}*/) {
				this._super();
				
			    var replace = function(val) {
			        switch (val) {
			            case '<':   return "&lt;";
			            case '>':   return "&gt;";
			            case '&':   return "&amp;";
			        }
			        
			        return val;
			    };
			
                if (data.content) {
                    var msgData = jabberwerx.$("<span/>").
                        addClass("message");

                    var translation = jabberwerx.Message.translate(data.content, data.displayName);
                    if (translation) {
                        msgData.addClass("jabberwerx messageView xep245_presentation");
                        data.content = translation;
                    }

                    if (typeof data.content == "string") {
                        var content = String(data.content).replace(/[<>&]/g, replace);
                        content = content.replace(/\n/g, "<br>");
                        content = jabberwerx.ui.emoticons.translate(content);
                        msgData.append(content);
                    } else if (jabberwerx.isElement(data.content)) {
                        var xhtml = jabberwerx.$(data.content.xml);
						
                        xhtml = jabberwerx.$("<div/>").append(xhtml);
                        var replaceXhtml = xhtml.children("p:first");
                        replaceXhtml.replaceWith(replaceXhtml.contents());

                        xhtml = jabberwerx.ui.emoticons.translate(xhtml);

                        msgData.append(xhtml.contents());
                        msgData.find('a[href]').attr('target', '_blank');
                    }

                    this.content = msgData;
                }
				this.timestamp = jabberwerx.ui.getDateDisplayString(data.timestamp);
				this.displayName = data.displayName;
				if (typeof data.cssClassName != 'undefined') {
					this.cssClassName = data.cssClassName;
				}
			},
			
			/**
			 * <p>Creates the DOM representation of this MessageView. This
			 * implementation constructs the given fragment:</p>
			 *
			 * <pre class="code">
			 * &lt;div class='{cssClassName}'&gt;
			 *   &lt;span class='time'&gt;{timestamp}&lt;/span&gt;
			 *   &lt;span class='message'&gt;&lt;b&gt; {displayName}:&lt/b&gt; {content}
			 * &lt;/div&gt;
			 * </pre>
			 * @param {Document} doc The document to use for creating content
             * @returns {Element} The generated element to display
			 */
			createDOM: function(doc) {
				var dName = this.displayName;
				if (dName.length > 12) {
					dName = dName.substring(0, 12) + '...';
				}
				
                var builder = jabberwerx.$("<div/>", doc);
                builder.addClass(this.cssClassName);
                
                jabberwerx.$("<span/>").
                    addClass("time").
                    text(this.timestamp).
                    appendTo(builder);

                    
                jabberwerx.$("<b/>").
                    text(" " + dName + ": ").
                    appendTo(builder);

                this.content.appendTo(builder);
                
                
                return builder.get(0);
			},
			
			/**
			 * <p>The timestamp of this message, in a displayable string.</p>
			 *
			 * @type    String
			 */
			timestamp: '',
			/**
			 * <p>The display name of the sender.</p>
			 *
			 * @type    String
			 */
			displayName: '',
			/**
			 * <p>The message content.</p>
			 *
			 * @type    jQuery
			 */
			content: null,
			/**
			 * <p>The message CSS class, if any</p>
			 *
			 * @type    String
			 */
			cssClassName: ''
		}, 'jabberwerx.ui.MessageView');
	}
})();
