/**
 * filename:        ChatView.js
 * created at:      2009/01/01T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */


;(function(){
    if (jabberwerx && jabberwerx.ui) {

        jabberwerx.ui.ChatView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.ChatView.prototype */{
            /**
             * @class
             * <p>A view for a single 1-1 chat pane. This allows the user to send
             * and receive messages from the user of the given session.</p>
             *
             * @description
             * <p>Creates a new ChatView with the given session.</p>
             *
             * @param {jabberwerx.ChatSession} session The session associated
             * with this chat view.
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
             * </ul>
             *
             * @throws {TypeError} if {session} is not valid
             *
             * @constructs jabberwerx.ui.ChatView
             * @extends jabberwerx.ui.JWView
             */
            init: function(session) {
                this._super();

                if (!(session && session instanceof jabberwerx.ChatSession)) {
                    throw new TypeError("session must be a jabberwerx.ChatSession");
                }

                this.session = session;
                this.session.event("chatReceived").
                     bind(this.invocation("_handleChat"));
                this.session.event("chatSent").
                     bind(this.invocation("_handleChat"));
                this.session.event("chatStateChanged").
                     bind(this.invocation("_handleChatState"));
            },

            /**
             * <p>Destroys this ChatView. This unbinds all of the bound events
             * this object has.</p>
             */
            destroy: function() {
                this.session.event("chatReceived").
                     unbind(this.invocation("_handleChat"));
                this.session.event("chatSent").
                     unbind(this.invocation("_handleChat"));
                this.session.event("chatStateChanged").
                     unbind(this.invocation("_handleChatState"));

                if (this.session) {
                    this.session.destroy();
                }

                this._super();
            },

        /**
         * <p>Retrieves or changes the width of this view. This implementation
         * extends the superclass to adjust the content and tablist to match
         * the new width.</p>
         *
         * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
         *          retrieve the current width)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#width
         */
        width: function(w, noupdate) {
            var result = this._super(w, true);
            if (w !== undefined && this.jq) {
                //superclass may have altured witdh already, refresh
                var w = this.width();
                //subtract our margins
//                w = w - (this.jq.outerWidth(true) - w);
            if (w != 0) {
                if (jabberwerx.$.browser.webkit) {
                    w = w - (this.jq.outerWidth(true) - w);
                }
                this._presenceView.width(w, true);
                this._messageListView.width(w, true);
                this._messageInput.width(w, true);
                if (!noupdate) {
                    this.update();
                }
             }
            }
            return result;
        },

        /**
         * <p>Retrieves or changes the height of this view. This implementation
         * extends the superclass to adjust the content and tablist to match
         * the new height.</p>
         *
         * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
         *          retrieve the current height)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#height
         */
        height: function(h, noupdate) {

            var result = this._super(h, true);
            if (h !== undefined && this.jq) {
                //superclass adjusted height for css already
                var h = this.height();
                if (h) {
                    //subtract other controls in div
                    var delta = this._presenceView.jq.outerHeight(true) +
                                this._messageInput.jq.outerHeight(true);
                    //subtract presence and input heights, including margins
                    h -= delta;

                    this._messageListView.height(h, true);
                }
                if (!noupdate) {
                    this.update();
                }
            }
            return result;
        },

            /**
             * @private
             */
            _handleChat: function(evt) {
                var msg = evt.data;
                var client = this.session.client;
                var tab;
                var entity;
                if (evt.name == "chatsent") {
                    entity = client.connectedUser;
                    // forget tab (so attention is skipped)
                    tab = null;
                } else if (evt.name == "chatreceived") {
                    entity = this.session.getEntity();
                    // might need to request attention
                    tab = this.getTabControl();
                }

                if (this._messageListView) {
                    this._messageListView.addMessage(entity, msg);
                }

                if (tab) {
                    tab.requestAttention();
                }
            },

            /**
             * @private
             */
            _handleChatState: function(evt) {
                var info = evt.data;

                if (!info.jid) {
                    // our state changed; nothing to do
                    return;
                }

                var data = this.session.getEntity().getDisplayName();
                switch (info.state) {
                    case "composing":
                        data = jabberwerx._("{0} is replying", data);
                        break;
                    case "gone":
                        data = jabberwerx._("{0} has left the conversation", data);
                        break;
                    default:
                        // clear previous state
                        data = null;
                        break;
                }

                if (this._messageListView) {
                    /* DEBUG-BEGIN */
                    jabberwerx.util.debug.log("badge: " + data);
                    /* DEBUG-END */
                    
                    // Armor the result
                    data = jabberwerx.$("<span/>").text(data);
                    this._messageListView.setBadge(data);
                }
            },

            /**
             * <p>Triggers an update of this ChatView. This goes through
             * all of the widgets associated with this view and calls
             * their updates.</p>
             */
            update: function() {
                if (!this._super()) {
                    return false;
                }

                this._presenceView.update();
                this._messageListView.update();
                this._messageInput.update();

                return true;
            },

            /**
             * @private
             */
            _handleText: function(evt) {
                switch (evt.name) {
                    case "textsend":
                        if (evt.data == "/clear") {
                            this.clearHistory();
                            this.session.setChatState("active");
                        } else {
                            this.session.sendMessage(evt.data);
                        }
                        break;
                    case "texttypingstarted":
                        this.session.setChatState("composing");
                        break;
                    case "texttypingended":
                        this.session.setChatState("active");
                        break;
                    case "texttypingidled":
                        this.session.setChatState("paused");
                        break;
                }

                return true;
            },

            /**
             * <p>Clears the MessageHistory pane.</p>
             */
            clearHistory: function() {
                this._messageListView && this._messageListView.clearHistory();
            },

            /**
             * <p>Sets or clears the tab control. This method overrides
             * the mixin {@link jabberwerx.ui.Tabbable#setTabControl} to
             * set the label, and bind events for updating when this
             * ChatView's session's entity changes.</p>
             *
             * @param   {jabberwerx.ui.TabbedView.Tab} [tab] The new tab, or
             *          <tt>null</tt> if being removed from a tabbed view.
             */
            setTabControl: function(tab) {
                this._super(tab);

                if (tab) {
                    var entity = this.session.getEntity();
                    var cache = entity.cache;

                    tab.label(entity.getDisplayName());
                    cache.event("entityAdded").
                          bindWhen(this.invocation("_selectEntity"),
                                   this.invocation("_handleEntityChanged"));
                    cache.event("entityRemoved").
                          bindWhen(this.invocation("_selectEntity"),
                                   this.invocation("_handleEntityChanged"));
                    cache.event("entityUpdated").
                          bindWhen(this.invocation("_selectEntity"),
                                   this.invocation("_handleEntityChanged"));
                    cache.event("entityRenamed").
                          bindWhen(this.invocation("_selectEntity"),
                                   this.invocation("_handleEntityChanged"));
                } else {
                    var cache = this.session.getEntity().cache;
                    cache.event("entityAdded").
                          unbind(this.invocation("_handleEntityChanged"));
                    cache.event("entityRemoved").
                          unbind(this.invocation("_handleEntityChanged"));
                    cache.event("entityUpdated").
                          unbind(this.invocation("_handleEntityChanged"));
                    cache.event("entityRenamed").
                          unbind(this.invocation("_handleEntityChanged"));
                }
            },

            /**
             * @private
             */
            _selectEntity: function(data) {
                var entity = this.session.getEntity();

                var test = (data instanceof jabberwerx.Entity) ?
                        data :
                        data.entity;
                if (this.getTabControl() && entity.matches(test)) {
                    return entity;
                }

                return undefined;
            },
            /**
             * @private
             */
            _handleEntityChanged: function(evt) {
                var entity = evt.selected;
                var tab = this.getTabControl();

                if (tab) {
                    tab.label(entity.getDisplayName());
                }
            },

            /**
             * <p>Creates the DOM for a ChatView which contains a
             * ContactPresenceView, a MessageHistory and a TextInput
             * widget.</p>
             *
             * @param {Document} doc The document to use for creating content.
             * @returns {jQuery} The element containing the view.
             */
            createDOM: function(doc) {
                var chatView = jabberwerx.$("<div/>", doc).addClass("jabberwerx chatview");

                this._presenceView = new
                    jabberwerx.ui.ContactPresenceView(this.session.getEntity());
                this._presenceView.render().appendTo(chatView);

                this._messageListView = new jabberwerx.ui.MessageHistory(this.session);
                this._messageListView.render().appendTo(chatView);

                this._messageInput = new jabberwerx.ui.TextInput();
                this._messageInput.event("textSend").
                                   bind(this.invocation("_handleText"));
                this._messageInput.event("textTypingStarted").
                                   bind(this.invocation("_handleText"));
                this._messageInput.event("textTypingEnded").
                                   bind(this.invocation("_handleText"));
                this._messageInput.event("textTypingIdled").
                                   bind(this.invocation("_handleText"));
                this._messageInput.render().appendTo(chatView);

                return chatView;
            },
           /**
             * <p>Restore the DOM for a ChatView which contains a
             * ContactPresenceView, a MessageHistory and a TextInput
             * widget.</p>
             *
             * @param {Document} doc The document to use for creating content.
             * @returns {jQuery} The element containing the view.
             */ 
             restoreDOM: function(doc) {
                var chatView = jabberwerx.$("<div/>", doc).addClass("jabberwerx chatview");
                this._presenceView.render().appendTo(chatView);
                this._messageListView.render().appendTo(chatView);
                this._messageInput.render().appendTo(chatView);
                return chatView;
            },

            /**
             * <p>Clears out all of the objects creates by createDOM.</p>
             */
            destroyDOM: function() {
                this._presenceView.destroy();
                this._presenceView = null;

                this._messageListView.destroy();
                this._messageListView = null;

                this._messageInput.destroy();
                this._messageInput = null;
            },

            /**
             * @private
             */
            _presenceView: null,
            /**
             * @private
             */
            _messageListView: null,
            /**
             * @private
             */
            _messageInput: null
        }, 'jabberwerx.ui.ChatView');

        jabberwerx.ui.ChatView.mixin(jabberwerx.ui.Tabbable);
    }
})();
