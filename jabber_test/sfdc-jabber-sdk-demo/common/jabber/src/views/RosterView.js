/**
 * filename:        RosterView.js
 * created at:      2009/07/08T09:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.RosterView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.RosterView.prototype */{
        /**
         * @class
         * <p>View over an EntitySet (oftimes referred to as a "roster").</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.RosterView">jabberwerx.ui.RosterView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new RosterView with the given cache and mode. If
         * {mode} is {@link .groupmode_single}, then the display is effectively
         * a flat list of all entities. Otherewise, the display consists of
         * zero or more groupings that contain entities.</p>
         *
         * @param   {jabberwerx.EntitySet} cache The cache
         * @param   {Number} [mode] The default group mode (one of
         *          {@link jabberwerx.ui.RosterView.groupmode_collapsed},
         *          {@link jabberwerx.ui.RosterView.groupmode_expanded}, or
         *          {@link jabberwerx.ui.RosterView.groupmode_single})
         * @throws  {TypeError} If {cache} is not an instance of
         *          EntitySet; or if {mode} is not an accepted value
         * @constructs jabberwerx.ui.RosterView
         * @extends jabberwerx.ui.JWView
         */
        init: function(cache, mode) {
            this._super();
            
            if (!(cache && cache instanceof jabberwerx.EntitySet)) {
                throw new TypeError("cache must be a jabberwerx.EntitySet");
            }
            this.cache = cache;
            
            this._filteringCB = [];
            this._sortingCB = this.invocation("sortEntityByDisplayName");
            
            if (!mode) {
                mode = jabberwerx.ui.RosterView.groupmode_collapsed;
            } else if ( (mode != jabberwerx.ui.RosterView.groupmode_collapsed) &&
                        (mode != jabberwerx.ui.RosterView.groupmode_expanded) &&
                        (mode != jabberwerx.ui.RosterView.groupmode_single)) {
                throw new TypeError("mode is not a valid value");
            }
            this._default_state = mode;
            
            // setup defaults for grouping/sorting/filtering
            this._groupingCB = this.invocation("groupByGroups");
            
            // setup events
            this.applyEvent("rosterItemSelected");
            this.applyEvent("rosterGroupingRendered");
            this.applyEvent("rosterItemRendered");
            
            // setup event bindings
            var invoker = this.invocation("_handleEntityEvent");
            this.cache.event("entityAdded").bind(invoker);
            this.cache.event("entityRemoved").bind(invoker);
            this.cache.event("entityUpdated").bind(invoker);
            this.cache.event("entityRenamed").bind(invoker);
            
            this._groupings = [];
            // Prepares the groupings
            this._rebuildGroupings(true);
        },
        /**
         * <p>Destroys this <tt>RosterView</tt>. This method unbinds
         * all callbacks before calling the superclass' implementation.</p>
         */
        destroy: function() {
            // setup event bindings
            var invoker = this.invocation("_handleEntityEvent");
            this.cache.event("entityAdded").unbind(invoker);
            this.cache.event("entityRemoved").unbind(invoker);
            this.cache.event("entityUpdated").unbind(invoker);
            this.cache.event("entityRenamed").unbind(invoker);
            
            jabberwerx.globalEvents.unbind("primaryPresenceChanged",
                    this.invocation("_handlePrimaryPresence"));
            
            this._super();
        },

        /**
         * <p>Function that can be used to filter entities that
         * aren't online.</p>
         * @param {jabberwerx.Entity} entity The contact/roster
         * @returns {Boolean} <tt>true</tt> if {entity} is active
         */
        filterByActive: function(entity) {
            return entity.isActive();
        },
        
        /**
         * <p>Function that can be used to filter entities that
         * aren't contacts (i.e. local user and the server).
         * @param {jabberwerx.Entity} entity The contact/roster
         * @returns {Boolean} <tt>true</tt> if {entity} is an instance of {@link jabberwerx.Contact}
         */
        filterByContacts: function(entity) {
            return entity instanceof jabberwerx.Contact;
        },
        /**
         * <p>Retrieves the groups for this Entity
         * @param {jabberwerx.Entity} entity The Entity
         * @returns {String[]} The array of groups
         */
        groupByGroups: function(entity) {
            return (entity && entity.getGroups()) || [];
        },
        /**
         * <p>Compare two entities by display Name
         * @param {jabberwerx.Entity} a The contact/roster
         * @param {jabberwerx.Entity} b The contact/roster
         * @returns {Number} Returns <tt>0</tt> if {a}={b}; <tt>1</tt> if {a}>{b}; otherwise returns <tt>-1</tt>
         */
        sortByDisplayName: function(a, b) {
            var displayA = a.getDisplayName().toLowerCase();
            var displayB = b.getDisplayName().toLowerCase();
            
            if          (a < b) {
                return -1;
            } else if   (a > b) {
                return 1;
            }
            return 0;
        },

        /**
         * <p>Function that can be used for sorting to compare two
         * entites by display name.</p>
         * @param {jabberwerx.Entity} a The contact/roster
         * @param {jabberwerx.Entity} b The contact/roster
         * @returns {Number} Returns <tt>0</tt> if equals; <tt>1</tt> if greater; <tt>-1</tt> if less
         */
        sortEntityByDisplayName: function(a, b) {
            var displayA = a.entity.getDisplayName().toLowerCase();
            var displayB = b.entity.getDisplayName().toLowerCase();

            if (displayA < displayB) {
                return -1;
            } else if (displayA > displayB) {
                return 1;
            }
            return 0;
        },

        /**
         * <p>Creates the DOM for this <tt>RosterView</tt>. This method
         * creates the following basic structure:</p>
         *
         * <pre>
         * &lt;div class='jabberwerx roster'&gt;&lt;/div&gt;
         * </pre>
         * 
         * <p>If {@link #cache} contains any groupings, each is rendered
         * and appended to the container &lt;div/&gt;.</p>
         *
         * @param   {Document} doc The owning document
         * @returns {Element} The DOM for this <tt>RosterView</tt>
         */
        createDOM: function(doc) {
            var data = jabberwerx.$("<div></div>", doc).
                    addClass("jabberwerx roster");
                    
            jabberwerx.$.each(this._groupings, function(idx) {
                this.render().appendTo(data);
            });
            
            return data.get(0);
        },
        /**
         * <p>Destroys the DOM for this <tt>RosterView</tt>. This
         * implementation iterates through all the groupings,
         * calling {@link jabberwerx.ui.RosterView.Grouping#destroy}.</p>
         *
         */
        destroyDOM: function(doc) {
            jabberwerx.$.each(this._groupings, function() {
                this.destroy();
            });
            this._groupings = [];
        },
        
        /**
         * <p>Retrieves the default grouping display name. This is the value
         * displayed in the DOM for the "" grouping.</p>
         *
         * <p>This is not linked to
         * {@link jabberwerx.RosterController.defaultGroup}.</p>
         *
         * @returns {String} The default grouping name
         */
        getDefaultGroupingName: function() {
            return this._DefaultGroupingName;
        },
        /**
         * <p>Changes the default grouping display name. If {name} is
         * not equal to the current value, it is changed to {name} and
         * the default grouping (if any) is updated.</p>
         *
         * @param   {String} name The new default grouping name
         * @throws  {TypeError} If {name} is not a valid non-empty string.
         */
        setDefaultGroupingName: function(name) {
            if (!name) {
                throw new TypeError("name cannot be empty");
            }
            
            name = String(name);
            if (name != this._DefaultGroupingName) {
                this._DefaultGroupingName = name;
                if (this._groupings[""]) {
                    this._groupings[""].update();
                }
            }
        },
        
        /**
         * <p>Retrieves the grouping for the given name.</p>
         *
         * @param   {String} name The grouping name (or "" for the default
         *          grouping name).
         * @returns {jabberwerx.ui.RosterView.Grouping} The grouping for the
         *          given name, or {null} if not found
         */
        grouping: function(name) {
            return this._groupings[name || ""] || null;
        },
        
        /**
         * <p>Updates this <tt>RosterView</tt>'s display. This
         * implementation walks the current groupings, inserting any missing
         * into this <tt>RosterView</tt>'s DOM, then calling
         * {@link jabberwerx.ui.RosterView.Grouping#update}.</p>
         *
         * @returns {Boolean} <tt>true</tt> if this <tt>RosterView</tt>
         *          is updatable
         */
        update: function() {
            if (!this._super()) {
                return false;
            }
            
            var that = this;
            var prev = null;
            jabberwerx.$.each(this._groupings, function(idx) {
                if (!this.jq) {
                    this.render();
                    
                    if (!prev) {
                        this.jq.prepend(that.jq);
                    } else {
                        prev.after(this.jq);
                    }
                }
                prev = this.jq;
                
                this.update();
            });
            
            return true;
        },
        
        /**
         * @private
         * <p>Rebuilds the groupings for this <tt>RosterView</tt>. This
         * implementaiton removes each current grouping, then walks the
         * {@link #cache} to to-add/-update the groupings. If {initial}
         * is <tt>true</tt>, this method also binds a callback for
         * presence updates to each entity.</p>
         *
         * @param {Boolean} [initial] <tt>true</tt> if this should be
         * considered the initial call
         */
        _rebuildGroupings: function(initial) {
            // ...in with the new!
            var that = this;
            jabberwerx.$.each(this.cache.toArray(), function() {
                if (initial) {
                    that.event("primaryPresenceChanged").
                         bind(that.invocation("_handlePrimaryPresence"));
                }
                that._updateEntity(this);
            });
        },
        
        /**
         * @private
         */
        _updateEntity: function(entity) {
            var     groupings = [];
            
            var combineFilter = function(filter, value) {
                return filter(entity) && value;
            };
            var showEntity = jabberwerx.reduce(this._filteringCB, combineFilter, true);
            
            var     that = this;
            // walk the entity's groups
            var     grpWalk = function(grpName) {
                var grpView = groupings[grpName];
                if (!grpView) {
                    // check persisted
                    grpView = that._groupings[grpName];
                }
                if (!grpView) {
                    // create and persist
                    grpView = new jabberwerx.ui.RosterView.Grouping(
                            that,
                            grpName,
                            that._default_state);
                    that._groupings[grpName] = grpView;
                    
                    // TODO: sorted insert
                    var idx = that._sortGrouping(that._groupings, grpView);
                    that._groupings.splice(idx, 0, grpView);
                }
                
                if (!groupings[grpName]) {
                    // remember for now
                    groupings[grpName] = grpView;
                    groupings.push(grpView);
                }
                
                grpView._updateEntity(entity, showEntity);
            };
            
            if (this._default_state != jabberwerx.ui.RosterView.groupmode_single) {
                var grpList = this._groupingCB(entity) || [];
                jabberwerx.$.each(grpList, function() {
                    grpWalk(String(this));
                    return true;
                });
            }
            
            if (!groupings.length) {
                grpWalk("");
            }
            
            //walk the known groups
            jabberwerx.$.each(this._groupings, function() {
                if (this.name in groupings) {
                    // still exists
                    return true;
                }
                
                if (!this._removeEntity(entity)) {
                    // group is empty, remove it
                    var idx;
                    
                    idx = jabberwerx.$.inArray(this, that._groupings);
                    that._groupings.splice(idx, 1);
                    delete that._groupings[this.name];
                    
                    this.destroy();
                } else if (!groupings[this.name]) {
                    groupings[this.name] = this;
                    groupings.push(this);
                }
            });
            
            // render and update affected groupings
            var that = this;
            var renderGrouping = function(index, view) {
                if (index == 0) {
                    view.render().prependTo(that.jq);
                    view.update();
                    return;
                }

                if (!that._groupings[index].jq) {
                    that._groupings[index].render();
                    arguments.callee(index-1, that._groupings[index]);
                }

                that._groupings[index - 1].jq.after(view.jq);
                view.update();
            };
            jabberwerx.$.each(this._groupings, renderGrouping);
        },
        /**
         * @private
         */
        _removeEntity: function(entity) {
            //walk the known groups
            var that = this;
            jabberwerx.$.each(this._groupings.concat(), function() {
                if (this._removeEntity(entity) <= 0) {
                    // group is empty, remove it
                    var idx = jabberwerx.$.inArray(this, that._groupings);
                    that._groupings.splice(idx, 1);
                    delete that._groupings[this.name];
                    this.destroy();
                }
            });
            
            this.update();
        },
        
        /**
         * @private
         * 
         */
        _handleEntityEvent: function(evt) {
            var entity = evt.data;
            switch (evt.name) {
                case "entityrenamed":
                    // renamed is an anonymous object, not an entity
                    // grab the actual entity...
                    entity = evt.data.entity;
                    // fall through
                case "entityadded":
                    entity.event("primaryPresenceChanged").
                           bind(this.invocation("_handlePrimaryPresence"));
                    // fall through
                case "entityupdated":
                    this._updateEntity(entity);
                    break;
                case "entityremoved":
                    entity.event("primaryPresenceChanged").
                           unbind(this.invocation("_handlePrimaryPresence"));
                    this._removeEntity(entity);
                    break;
            }
        },
        /**
         * @private
         */
        _handlePrimaryPresence: function(evt) {
            var entity = evt.source;
            
            if (this.cache.entity(entity.jid)) {
                this._updateEntity(entity);
            }
        },
        
        /**
         * @private
         */
        _sortEntity: function(arr, val) {
            var high, low, idx;
            high = arr.length;
            low = -1;
            idx = 0;

            var compare = 0;
            while ((high - low) > 1) {
                idx = (high + low) >> 1;
        
                compare = this._sortingCB(arr[idx], val);
                if (compare < 0) {
                    low = idx;
                } else if (compare > 0) {
                    high = idx;
                } else {
                    break;
                }
            }

            if (idx < 0) {
                idx = 0;
            } else if (idx > arr.length) {
                idx = arr.length;
            } else {
                idx = (compare < 0) ? idx + 1 : idx;
            }

            return idx;
        },
        _sortGrouping: function(arr, val) {
            var high, low, idx;
            high = arr.length;
            low = -1;
            idx = 0;

            var compare = 0;
            while ((high - low) > 1) {
                idx = (high + low) >> 1;
        
                var nameA = arr[idx].name.toLowerCase();
                var nameB = val.name.toLowerCase();
                compare = (nameA < nameB) ?
                    -1 :
                    (nameA > nameB) ? 1 : 0;
                if (compare < 0) {
                    low = idx;
                } else if (compare > 0) {
                    high = idx;
                } else {
                    break;
                }
            }

            if (idx < 0) {
                idx = 0;
            } else if (idx > arr.length) {
                idx = arr.length;
            } else {
                idx = (compare < 0) ? idx + 1 : idx;
            }

            return idx;
        },
        /**
         * <p>Adds a filter for the contents of the RosterView.
         * Multiple filters can be applied. If one filter says an
         * entry should be hidden, the entry becomes hidden. Another
         * filter cannot override that.</p>
         * 
         * @param {function} filter A function that takes an Entity
         * and returns false if the entry should be hidden.
         *
         * @throws  {TypeError} If {filter} is not a function
         */
        addFilter: function(filter) {
            if (jabberwerx.$.isFunction(filter)) {
                var idx = jabberwerx.$.inArray(filter, this._filteringCB);
                if (idx == -1) {
                    this._filteringCB.push(filter);
                    this._rebuildGroupings();
                }
            } else {
                throw new TypeError("filter must be a function");
            }
        },
        /**
         * <p>Removes a filter from the RosterView.</p>
         *
         * @param {function} filter The function that was passed into
         * {@link jabberwerx.ui.RosterView#addFilter} that should
         * be removed.
         */
        removeFilter: function(filter) {
            var idx = jabberwerx.$.inArray(filter, this._filteringCB);
            if (idx != -1) {
                this._filteringCB.splice(idx, 1);
                this._rebuildGroupings();
            }
        },
        /**
         * <p>Removes all of the filters.</p>
         */
        clearFilters: function() {
            this._filteringCB = [];
            this._rebuildGroupings();
        },
        /**
         * <p>Sets the sort function for the entities.
         * Pass in nothing if you want to clear the sort function.</p>
         *
         * @param {function} sort The function called to compare two entities.
         * The function takes two parameters, both entities, and returns -1 if
         * the first one is less than the second one, 1 if the first one is
         * greater than the second one and 0 if they are equal.
         *
         * @throws  {TypeError} If {sort} is not a function or null
         */
        setSort: function(sort) {
            if (jabberwerx.$.isFunction(sort)) {
                this._sortingCB = sort;
                this._rebuildGroupings();
            } else if (sort == null) {
                // Fall back to default sort
                this._sortingCB = this.invocation("sortEntityByDisplayName");
                this._rebuildGroupings();
            } else {
                throw new TypeError("sort must be a function or null");
            }
        },

        graphUnserialized: function() {
            this._super();

            var idx = 0;
            for (idx = 0; idx < this._groupings.length; idx=idx+1) {
                var name = this._groupings[idx].name;
                this._groupings[name] = this._groupings[idx];
            }
        },
  
        _sortingCB: null,
        _filteringCB: null,
        _groupingCB: null,
        _DefaultGroupingName: "Unfiled",
        /**
         * <p>The <tt>EntitySet</tt> cache this <tt>RosterView</tt>
         * is a view for.</p>
         *
         * @type jabberwerx.EntitySet
         */
        cache: null
    }, "jabberwerx.ui.RosterView");

    /**
     * <p>Mode for a collapsed grouping.</p>
     */
    jabberwerx.ui.RosterView.groupmode_collapsed = 1;
    /**
     * <p>Mode for an expanded grouping.</p>
     */
    jabberwerx.ui.RosterView.groupmode_expanded = 2;
    /**
     * <p>Mode for a "single-mode" grouping.</p>
     */
    jabberwerx.ui.RosterView.groupmode_single = 4;
    jabberwerx.ui.RosterView.Grouping = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.RosterView.Grouping.prototype */{

        wasUnserialized: function() {
            this._super();
            this._restoreItemsMap = true;
        },   

        /**
         * @class
         * <p>View over a grouping of entities in a <tt>RosterView</tt>.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new <tt>Grouping</tt> with the given owning roster,
         * grouping name, and mode.</p>
         *
         * <p><b>NOTE:</b> This type should not be created directly. Instead
         * it is creating in <tt>RosterView</tt> to visually group
         * entities.</p>
         *
         * @param   {jabberwerx.ui.RosterView} roster The owning roster
         * @param   {String} name The grouping name ("" for the default)
         * @param   {Number} mode The grouping mode (one of
         *          {@link jabberwerx.ui.RosterView.groupmode_single},
         *          {@link jabberwerx.ui.RosterView.groupmode_collapsed},
         *          or {@link jabberwerx.ui.RosterView.groupmode_expanded})
         * @constructs jabberwerx.ui.RosterView.Grouping
         * @extends jabberwerx.ui.JWView
         */
        init: function(roster, name, mode) {
            this._super();
            
            this.mode = mode || jabberwerx.ui.RosterView.groupmode_collapsed;
            this._itemsMap = [];
            this.roster = roster;
            this.name = name;
            
            // placeholder of visible items, to keep proper track of
            // visual counts
            this._visibleItems = {
                size: 0
            };
            
            this.event("viewRendered").bind(this.invocation("_handleRendered"));
        },
        
        /**
         * <p>Expands this grouping. This method will make visible all of the
         * displayable entities it has, if previously collapsed.</p>
         *
         * <p><b>NOTE:</b> This method does nothing if the current mode
         * is {@link jabberwerx.ui.RosterView.groupmode_expanded} or
         * {@link jabberwerx.ui.RosterView.groupmode_single}.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the grouping was expanded
         */
        expand: function() {
            if (this.mode == jabberwerx.ui.RosterView.groupmode_collapsed) {
                this.mode = jabberwerx.ui.RosterView.groupmode_expanded;
                this.update();
                
                return true;
            }
            
            return false;
        },
        /**
         * <p>Collapses this grouping. This method will make hidden all of the
         * displayable entities it has, if previously expanded.</p>
         *
         * <p><b>NOTE:</b> This method does nothing if the current mode
         * is {@link jabberwerx.ui.RosterView.groupmode_collapsed} or
         * {@link jabberwerx.ui.RosterView.groupmode_single}.</p>
         *
         * @returns {Boolean}   <tt>true</tt> if the grouping was collapsed.
         */
        collapse: function() {
            if (this.mode == jabberwerx.ui.RosterView.groupmode_expanded) {
                this.mode = jabberwerx.ui.RosterView.groupmode_collapsed;
                this.update();
                
                return true;
            }
            
            return false;
        },
        /**
         * <p>This method toggles between expanded and collapsed.</p>
         *
         * <p><b>NOTE:</b> This method does nothing if the current mode
         * is {@link jabberwerx.ui.RosterView.groupmode_single}.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the grouping was collapsed or
         *          expanded.
         */
        toggle: function() {
            return (this.expand() || this.collapse());
        },
        
        /**
         * @private
         */
        _updateEntity: function(entity, showEntity) {
            if (this._restoreItemsMap) {
                var idx = 0;
                for (idx = 0; idx < this._itemsMap.length; idx=idx+1) {
                    var name = this._itemsMap[idx].entity._mapKey;
                    this._itemsMap[name] = this._itemsMap[idx];
                }
                this._restoreItemsMap = false;
            }

            var key = entity._mapKey;
            var view = this._itemsMap[key];
            if (!view) {
                view = new jabberwerx.ui.ContactPresenceView(entity, this.roster);
                view.event("viewActivated").bind(this.invocation("_handleItemActivated"));
                view.event("viewRendered").bind(this.invocation("_handleRendered"));
                this._itemsMap[key] = view;
            } else {
                var idx = jabberwerx.$.inArray(view, this._itemsMap);
                this._itemsMap.splice(idx, 1);
            }

            var sortIndex = this.roster._sortEntity(this._itemsMap, view);
            this._itemsMap.splice(sortIndex, 0, view);
            
            if (showEntity && !this._visibleItems[view._guid]) {
                this._visibleItems[view._guid] = view;
                this._visibleItems.size++;
            } else if (!showEntity && this._visibleItems[view._guid]) {
                delete this._visibleItems[view._guid];
                this._visibleItems.size--;
            }
            
            if (this.jq) {
                if (!view.jq) {
                    view.render();
                }

                if (sortIndex == 0) {
                    view.jq.prependTo(this.jq.children("div.items"));
                } else {
                    var that = this;

                    // If the sibling child we want to connect to doesn't
                    // have a 'jq' (because this.jq didn't exist at the
                    // time it was added), this section works backwards
                    // until it finds a sibling with a 'jq' or it gets to
                    // the first child. The whole time it is going backwards
                    // we are creating 'jq' objects for the siblings. On the
                    // way back, we are attaching the siblings to their previous
                    // sibling.
                    var mapItems = function(index, view) {
                        if (index == -1) {
                            view.jq.prependTo(that.jq.children("div.items"));
                            return;
                        }

                        if (!that._itemsMap[index].jq) {
                            that._itemsMap[index].render();
                            mapItems(index-1, that._itemsMap[index]);
                        }

                        that._itemsMap[index].jq.after(view.jq);
                    };
                    mapItems(sortIndex-1, view);
                }
                view.update();
                
                view.jq.toggle(showEntity);
            }
            
            return (this._itemsMap.length);
        },
        /**
         * @private
         */
        _removeEntity: function(entity) {
            var key = entity._mapKey;
            var view = this._itemsMap[key];
            if (view) {
                var idx = jabberwerx.$.inArray(view, this._itemsMap);
                this._itemsMap.splice(idx, 1);
                delete this._itemsMap[key];
                
                if (this._visibleItems[view._guid]) {
                    delete this._visibleItems[view._guid];
                    this._visibleItems.size--;
                }
                view.destroy();
            }
            
            return (this._itemsMap.length);
        },
        
        /**
         * <p>Creates DOM for this <tt>Grouping</tt>. This method generates
         * the following structure:</p>
         * <pre>
         *  &lt;div class='jabberwerx group section'&gt;
         *      &lt;div class='group title'&gt;
         *          &lt;span class='extrainfo'/&gt;
         *          &lt;a href='#'
         *              &lt;span class='displayname'/&gt;
         *          &lt;/a&gt;
         *      &lt;/div&gt;
         *      &lt;div class='group items'&gt;
         *          &lt;!-- ContactPresenceViews are inserted here --&gt;
         *      &lt;/div&gt;
         *  &lt;/div&gt;
         * </pre> 
         *
         * @param   {Document} doc The owning document
         * @returns {Element} The created DOM
         */
        createDOM: function(doc) {
            var data = jabberwerx.$("<div/>", doc).
                    addClass("jabberwerx group section");
                    
            if (this.mode == jabberwerx.ui.RosterView.groupmode_single) {
                data.addClass("single");
            }
            
            var heading, items;
            
            // setup heading (displayname, extrainfo)
            heading = jabberwerx.$("<div/>").
                    appendTo(data).
                    addClass("group title");
            jabberwerx.$("<span/>").
                    appendTo(heading).
                    addClass("extrainfo");
            heading = jabberwerx.$("<a/>").
                    appendTo(heading).
                    bind("click", this.invocation("_handleExpandCollapse")).
                    attr("href", "#");
            jabberwerx.$("<span/>").
                    appendTo(heading).
                    addClass("displayname").
                    text(this.name || this.roster.getDefaultGroupingName());
                    
            items = jabberwerx.$("<div/>").
                    appendTo(data).
                    addClass("group items").
                    hide();

            return data.get(0);
        },
        /**
         * <p>Updates this <tt>Grouping</tt>'s view. This method
         * does the following:</p>
         * <ul>
         * <li>Removes the "expanded" and "collapsed" class styles to
         * "div.group.title"</li>
         * <li>Sets the "span.displayname" text to {@link #name} (or
         * {@link jabberwerx.ui.RosterView#getDefaultGroupingName} if
         * "")</li>
         * <li>Sets the "span.extraninfo" text to
         * "&lt;visible&gt;/&lt;total&gt;" (where &lt;visible&gt; is the
         * number of visible entities, and &lt;total&gt; is the total number
         * of entities in this grouping)</li>
         * <li>If {@link #mode} is {@link jabberwerx.ui.RosterView.groupmode_single},
         * hides the "div.group.title" and shows the "div.group.items"</li>
         * <li>If {@link #mode} is {@link jabberwerx.ui.RosterView.groupmode_collapsed},
         * adds the "collapsed" class style to "div.group.title" and hides the
         * "div.group.items"</li>
         * <li>If {@link #mode} is {@link jabberwerx.ui.RosterView.groupmode_expanded},
         * adds the "expanded" class style to "div.group.title" and shows the
         * "div.group.items"</li>
         * <li>Walks the entities, calling
         * {@link jabberwerx.ui.ContactPresenceView#update} for each, and setting
         * to visible or hidden as dictated by the <tt>RosterView</tt>'s filters
         * (if any).</li>
         * </ul>
         *
         * @returns {Boolean} <tt>true</tt> if this display is updatable
         */
        update: function() {
            if (!this._super()) {
                return false;
            }
            
            var heading = this.jq.children("div.title");
            var items = this.jq.children("div.items");

            if (this.mode == jabberwerx.ui.RosterView.groupmode_single) {
                this.jq.addClass("single");
            } else {
                this.jq.removeClass("single");
            }
            
            heading.removeClass("expanded collapsed");
            heading.find("span.displayname").
                    text(this.name || this.roster.getDefaultGroupingName());
                    
            var totals = this._calculateInfo();
            heading.find("span.extrainfo").
                    text("" + totals.active + "/" + totals.total);
                    
            switch (this.mode) {
                case jabberwerx.ui.RosterView.groupmode_single:
                    heading.hide();
                    items.show();
                    break;
                case jabberwerx.ui.RosterView.groupmode_collapsed:
                    with (items) {
                        var css = (this.jq.children("div.items").length) ? "collapsed" : "";
                        hide();
                        css && heading.addClass(css);
                    }
                    break;
                case jabberwerx.ui.RosterView.groupmode_expanded:
                    with (items) {
                        var css = (this.jq.children("div.items").length) ? "expanded" : "";
                        show();
                        css && heading.addClass(css);
                    }
                    break;
            }
            
            var that = this;
            jabberwerx.$.each(this._itemsMap, function() {
                that._updateEntity(
                        this.entity,
                        Boolean(that._visibleItems[this._guid]));
            });
            
            return true;
        },
        /**
         * @private
         */
        _calculateInfo: function() {
            var activeCount = 0;
            for (var name in this._visibleItems) {
                if (name == "size") {
                    continue;
                }
                
                if (!this._visibleItems[name].entity.isActive()) {
                    continue;
                }
                
                activeCount++;
            }
            
            var totalCount = this._itemsMap.length;
            return {active: activeCount, total: totalCount};
        },
        
        /**
         * @private
         */
        _handleExpandCollapse: function(evt) {
            this.toggle();
            
            return false;
        },
        /**
         * @private
         */
        _handleItemActivated: function(evt) {
            var data = {
                type: evt.data.type,
                item: evt.source
            }
            this.roster.event("rosterItemSelected").trigger(data);
        },
        /**
         * @private
         */
        _handleRendered: function(evt) {
            if          (evt.source === this) {
                this.roster.event("rosterGroupingRendered").trigger({
                    "grouping": evt.source,
                    "dom": evt.data
                });
            } else if   (evt.source instanceof jabberwerx.ui.ContactPresenceView) {
                this.roster.event("rosterItemRendered").trigger({
                    "item": evt.source,
                    "dom": evt.data
                });
            }
        },
        
        /**
         * <p>The <tt>RosterView</tt> owning this <tt>Grouping</tt>.</p>
         *
         * @type jabberwerx.ui.RosterView
         */
        roster: null,
        /**
         * <p>The current mode for this <tt>Grouping</tt>. This value
         * is one of the following:</p>
         * <ul>
         * <li>{@link jabberwerx.ui.RosterView.groupmode_single}</li>
         * <li>{@link jabberwerx.ui.RosterView.groupmode_collapsed}</li>
         * <li>{@link jabberwerx.ui.RosterView.groupmode_expanded}</li>
         * </ul>
         *
         * <p><b>NOTE:</b> This propery should not be changed directly.
         * Instead it is changed via {@link #collapse}, {@link #expand},
         * or {@link #single}.</p>
         *
         * @type    {Number}
         */
        mode: 0
    }, "jabberwerx.ui.RosterView.Grouping");
    
}
