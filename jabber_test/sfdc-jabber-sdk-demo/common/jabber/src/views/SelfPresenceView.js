/**
 * filename:        SelfPresenceView.js
 * created at:      2009/06/30T12:50:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.SelfPresenceView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.SelfPresenceView.prototype */{
        /**
         * @class
         * <p>View for displaying and modifying the client's presence.</p>
         *
         * <p>The show states used by SelfPresenceView are a superset of what
         * is valid according to RFC 3921. Except where noted, the values are
         * content in the &lt;show/&gt; element of the presence. The value used
         * are:</p>
         * <ol>
         * <li><b>chat:</b> State to indicate a willingness to chat</li>
         * <li><b>available:</b> State to indicate general availability; this
         * state is represented in presence by <b>NOT</b> including a
         * &lt;show/&gt; element.</li>
         * <li><b>away:</b> State to indicate temporarily away.</li>
         * <li><b>xa:</b> State to indicate extended away.</li>
         * <li><b>dnd:</b> State to indicate busy or do not disturb.</li>
         * <li><b>unavaiable:</b> State to indicate offline; this state is
         * represented in presence via the type="unavailable" attribute.</li>
         * </ol>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.SelfContactView">jabberwerx.ui.SelfPresenceView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new SelfPresenceView with the given client.</p>
         *
         * @param   {jabberwerx.Client} client The client
         * @param   {Boolean} [primaryPresence] This optional flag dictates if the self presence
         *          shown in this view should be the resource presence for this client, or the
         *          primary presence of this client. It defaults to false (i.e. presence shown is
         *          resource presence).
         * @throws {TypeError} If {client} is not valid
         * @constructs jabberwerx.ui.SelfPresenceView
         * @extends jabberwerx.ui.JWView
         */
        init: function(client, primaryPresence) {
            this._super();
            
            if (!(client && client instanceof jabberwerx.Client)) {
                throw new TypeError("client must be a jabberwerx.Client");
            }
            
            this.applyEvent("presenceSelected");
            var choices = jabberwerx.$.extend(
                    {},
                    jabberwerx.ui.SelfPresenceView.default_status_choices);
            jabberwerx.$.each(choices, function(n, v) {
                choices[n] = jabberwerx.$.map(v, function(v) {
                    return jabberwerx._(v);
                });
                return true;
            });
            this._choices = choices;
            
            this.client = client;
            this.client.event("clientStatusChanged").
                    bind(this.invocation("_handleClientStatusChange"));
            if (this.client.isConnected()) {
                this.client.connectedUser.event("resourcePresenceChanged").
                        bind(this.invocation("_handlePresenceChange"));
            }
            this.persistOptions(jabberwerx.ui.JWView.persist_html);
            
            this._primaryPresence = (primaryPresence ? true : false);
        },
        
        /**
         * <p>Retrieves the list of choices for the given show type.</p>
         *
         * @param   {String} show The "show" state to retrieve (one of:
         *          "available", "chat", "away", "xa", "dnd", "unavailable")
         * @returns {String[]} The array of choices, or [] (empty array) if
         *          none.
         * @throws  {TypeError} if {show} is not valid
         * @see     #constructor
         */
        getStatusChoices: function(show) {
            show = this._validateShow(show);
            return [].concat(this._choices[show] || undefined);
        },
        /**
         * <p>Changes or removes the list of choices for the given show
         * type.</p>
         *
         * <p>If {choices} is <tt>null</tt>, <tt>undefined</tt>, or [],
         * then the choices for {show} are removed. Otherwise the list
         * is updated to the given array. {@link #update} is called
         * once the choices have been changed/removed.</p>
         *
         * @param   {String} show The "show" state to retrieve (one of:
         *          "available", "chat", "away", "xa", "dnd", "unavailable")
         * @param   {String[]} choices The array of choices, or
         *          {null|undefined|[]} to remove.
         * @throws  {TypeError} if {show} is not valid
         * @see     #constructor
         */
        setStatusChoices: function(show, choices) {
            show = this._validateShow(show);
            if (choices) {
                choices = jabberwerx.$.makeArray(choices);
            }
            if (!choices || !choices.length) {
                delete this._choices[show];
            } else {
                this._choices[show] = choices.concat();
            }
            this.update();
        },
        /**
         * @private
         */
        _validateShow: function(show) {
            if (!show) {
                show = "available";
            }
            
            show = String(show).toLowerCase();
            if (    show != "available" &&
                    show != "chat" &&
                    show != "away" &&
                    show != "xa" &&
                    show != "dnd" &&
                    show != "unavailable") {
                throw new TypeError("show value not correct");
            }
            
            return show;
        },
        
        /**
         * <p>Creates the DOM to display. This method creates the following
         * DOM:</p>
         * <pre class="code">&lt;div class="jabberwerx self presence status"&gt;
         *      &lt;img style="position:absolute"/&gt;
         *      &lt;select style="width:100%;"/&gt;
         * &lt;/div&gt;</pre>
         *
         * <p>It also binds a callback for "change" events on the
         * &lt;select/&gt; element.</p>
         * @param {Document} doc The document to use for creating content.
         * @returns {Element} The generated element to display
         */
        createDOM: function(doc) {
            var data = jabberwerx.$("<div/>").
                    addClass("jabberwerx self presence show").
                    text(" ");
            
            jabberwerx.$("<select/>").
                    bind("change", this.invocation("_handleSelectChange")).
                    appendTo(data);
                    
            return data.get(0);
        },
       /**
        * <p>Restore the DOM for a SelfPresenceView </p>
        *
        * @param {Document} doc The document to use for creating content.
        * @returns {jQuery} The element containing the view.
        */ 
        restoreDOM: function(doc) {
            var res = jabberwerx.$(this._super(doc));
            res.find("select").bind("change", this.invocation("_handleSelectChange"));
            return res.get(0);
        },
               
        /**
         * <p>Updates the display for this SelfPresenceView. This method
         * does the following:</p>
         *
         * <ol>
         * <li>Regenerates the status choices from {#link @getStatusChoices}
         * into the DOM, for each registered show state</li>
         * <li>Sets the active choice (adding it if not already
         * present) in the &lt;select/&gt;</li>
         * <li>Changes the &lt;img/&gt; src attribute to reflect the current
         * show state</li>
         * <li>Triggeres a "viewRendered" event</li>
         * </ol>
         *
         * <p>If {@link #allowUnavailable} is <tt>true</tt> (and an array of
         * status choices for "unavailable" is registered), it is added to the
         * &lt;select/&gt;.</p>
         *
         * @returns {Boolean} <tt>true</tt> if this SelfPresenceView is
         *          updatable
         */
        update:function() {
            // dump if not ready
            if (!this._super()) {
                return false;
            }
            
            // get current presentity
            var show, status;
            var prs = this.client.getCurrentPresence(this._primaryPresence);
            if (prs) {
                show = prs.getShow() || prs.getType() || "available";
                status = prs.getStatus() || "";
            } else {
                show = "unavailable";
                status = "";
            }

            // reset view            
            var selector = this.jq.find("select");
            selector.empty();
            
            // build the option groups
            var groupings = {};
            
            for (var idx in jabberwerx.ui.SelfPresenceView.sortedShows) {
                if (this._choices[jabberwerx.ui.SelfPresenceView.sortedShows[idx]]) {
                    var optgroup = this._createStatusGroup(
                            jabberwerx.ui.SelfPresenceView.sortedShows[idx],
                            this._choices[jabberwerx.ui.SelfPresenceView.sortedShows[idx]]);
                    if (optgroup.length) {
                        groupings[jabberwerx.ui.SelfPresenceView.sortedShows[idx]] = optgroup;
                        optgroup.appendTo(selector);
                    }
                }
            }
            
            this._displayCurrentStatus(selector, groupings, show, status);
            
            this.event("viewRendered").trigger(this.jq);
            
            return true;
        },
        /**
         * @private
         * <p>Creates a selection group for the given show state and status
         * choices. This method creates an &lt;optgroup/&gt;, with the CSS
         * classes "presence", "status" and {show}; and the first choice as
         * the label (or the show state if {choices} is empty). It then calls
         * {@link #_createStatusOption} for each item in {choices}.</p>
         *
         * @param   {String} show The show state
         * @param   {[String]} choices The status choices
         * @returns {jQuery} The jQuery-wrapped &lt;optgroup/&gt;
         */
        _createStatusGroup: function(show, choices) {
            var optgroup = jabberwerx.$("<optgroup/>").
                    attr("label", (choices && choices[0]) || jabberwerx._(show)).
                    addClass("presence show").
                    addClass(show);
            
            if (!choices) {
                choices = [];
            }
            for (var idx = 0; idx < choices.length; idx++) {
                var status = choices[idx];
                if (!status) { continue; }
                
                this._createStatusOption(show, status).
                        appendTo(optgroup);
            }
            
            return optgroup;
        },
        /**
         * @private
         * <p>Creates a selection option for the given show state and status
         * value. This method creates a &lt;option/&gt; with the value
         * "{show}:{status}", and {status} as the text content.</p>
         *
         * @param   {String} show The show state
         * @param   {String} status The status value
         * @returns {jQuery} The jQuery-wrapped &lt;option/&gt;
         */
        _createStatusOption: function(show, status) {
            return jabberwerx.$("<option/>").
                    text(status).
                    val(show + ":" + status);
        },
        /**
         * @private
         * <p>Updates the display with the client's current presence.</p>
         *
         * @param   {jQuery} selector the jQuery-wrapped &lt;selector/&gt;
         * @param   {Object} groupings the dictionary of show states to
         *          jQuery-wrapped &lt;optgroup/&gt;s
         * @param   {String} [show] The show state (defaults to "available")
         * @Param   {String} [status] The status value
         */
        _displayCurrentStatus: function(selector, groupings, show, status) {
            var choices = this.getStatusChoices(show);
        
            // set the display to match the current value
            var optgroup = groupings[show];
            if (!optgroup) {
                // show set not present; insert
                optgroup = this.
                        _createStatusGroup(show).
                        appendTo(selector);
            }
            
            // If status is empty or consists solely of whitespaces then replace
            if (jabberwerx.$.trim(status || "") == "") {
                // default to first in choices or show
                status = choices[0] || jabberwerx._(show);
            }
            
            
            // search for all option elements where the value string
            // starts with the text stored by show
            var opts = optgroup.find(
                    "option[value^=" + show + ":]");
            
            if (opts.length) {
                // of the options found look for the one with the desired text
                // if found return it otherwise return null
                opts = jabberwerx.$.map(opts, function( element, index ) {    
                    // get option text value
                    var opt = jabberwerx.$(element).val();
                    // compare option text value with desired text value
                    if (opt == (show + ":" + status)) {
                        return element;
                    }
                    return null;
                });
            }
            
            if (!opts.length) {
                // option not present; insert
                opts = this.
                        _createStatusOption(show, status).
                        prependTo(optgroup);
            }
            
            // remove previous show and apply current
            selector.val(show + ":" + status);
            selector.width(this.jq.width() - Math.abs(selector.outerWidth(true) - selector.outerWidth(false)));
            
            // disable if unavailable and unavailable is not allowed; enable otherwise
            if (show == "unavailable" && !this.allowUnavailable) {
                selector.attr("disabled", true);
            } else {
                selector.removeAttr("disabled");
                
                //remove unavailable group if not allowed
                if (!this.allowUnavailable && groupings["unavailable"]) {
                    groupings["unavailable"].remove();
                }
            }
            
            // setup title value
            var title = choices[0] || jabberwerx._(show);
            if (status) {
                title = title + " :: " + status;
            }
            this.jq.
                    attr("title", title).
                    removeClass("chat available away xa dnd unavailable").
                    addClass(show);
        },
        /**
         * @private
         * <p>Retrieves the URL to the presence image for the given show
         * state. This methods concatenates the result from
         * {@link jabberwerx.ui.getThemeImageURL} with "icon-presence-"
         * + {show} + ".png".</p>
         *
         * @param   {String} [show] the show state (defaults to "available")
         * @returns {String} The URL for the show state image
         */
        _getPresenceImage: function(show) {
            var url = "icon-presence-" + (show || "unavailable") + ".png";
            return jabberwerx.ui.getThemeImageURL(url);
        },
        
        /**
         * @private
         * <p>Callback triggered when a selection change is made. This method
         * does the following:</p>
         * <ol>
         * <li>Determines the selected show/status value</li>
         * <li>Triggers a "presenceSelected" event.</li>
         * <li>If <b>none</b> of the "presenceSelected" callbacks returned
         * <tt>true</tt>, {@link #_sendPresence} is called.</li>
         * <li>{@link #update} is called to reset the display.</li>
         * </ol>
         *
         * @param   {Event} evt The "onchange" event
         * @returns {Boolean} <tt>false</tt> to prevent this event from
         *          propagating
         */
        _handleSelectChange: function(evt) {
            var selector = jabberwerx.$(evt.target);
            var opt = selector.find("option:selected");
            var optgroup = opt.parent();
            var show, status = selector.val();
            
            try {
                var pos = status.indexOf(":");
                if (pos != -1) {
                    show = this._validateShow(status.substring(0, pos));
                    status = status.substring(pos + 1);
                } else {
                    show = undefined;
                }
                
                var data = {
                    show: show,
                    status: status,
                    priority: null
                };
                
                var result;
                result = this.event("presenceSelected").trigger(data) || [];
                result = jabberwerx.reduce(result,
                        function(item, value) {
                            return Boolean(item) || value;
                        }, false);
                
                if (!result) {
                    this._sendPresence(data.show, data.status, data.priority);
                }
            } catch (ex) {
                jabberwerx.util.debug.log("could not handle selection change: " + ex);
            }
            this.update();
            
            //prevent default processing of the event
            return false;
        },
        /**
         * @private
         * <p>Sends the {@link jabberwerx.Presence} for the given
         * information.</p>
         *
         * <p>The exact behavior depends on the value of {show}:</p>
         * <ul>
         * <li><b>unavailable:</b> A &lt;presence type="unavailable"/&gt;
         * is sent, then the client is disconnected.</li>
         * <li><b>available:</b> A &lt;presence/&gt; with no &lt;show/&gt;
         * is sent.</li>
         * <li>default: A &lt;presence/&gt; with the given &lt;show/&gt;
         * is sent.</li>
         * </ul>
         *
         * <p>In all cases, the {status} (if a non-empty string) and {priority}
         * (if a number and {show} is not "unavailable") are included in the
         * &lt;presence/&gt;.</p>
         *
         * @param   {String} [show] The show state (defaults to "available")
         * @param   {String} [status] The status value
         * @param   {String} [priority] The priority level
         */
        _sendPresence: function(show, status, priority) {
            var presence = new jabberwerx.Presence();
            var logoff = false;
            
            show = this._validateShow(show);
            switch (show || "available") {
                case "unavailable":
                    logoff = true;
                    presence.setType("unavailable");
                    break;
                case "available":
                    // do nothing
                    break;
                default:
                    presence.setShow(show);
            }
            
            if (status) {
                presence.setStatus(status);
            }
            
            priority = parseInt(priority);
            if (!isNaN(priority) && !logoff) {
                presence.setPriority(priority);
            }
            
            this.client.sendStanza(presence);
            if (logoff) {
                this.client.disconnect();
            }
        },

        /**
         * @private
         * <p>Callback for client status changes. This method is called when
         * {@link #client}'s status changes, and watches for the following:</p>
         * <ul>
         * <li><b>{@link jabberwerx.Client#status_connected}</b>: binds a
         * callback for the connected user's "resourcePresenceChanged"
         * event, then calls {@link #update}.</li>
         * <li><b>{@link jabberwerx.Client#status_disconnected}</b>: Calls
         * {@link #update}.</li>
         * </ul>
         *
         * @param   {jabberwerx.EventObject} evt The event
         */
        _handleClientStatusChange: function(evt) {
            switch (evt.data.next) {
                case jabberwerx.Client.status_connected:
                    this.client.connectedUser.event("resourcePresenceChanged").
                            bind(this.invocation("_handlePresenceChange"));
                    // fall through
                case jabberwerx.Client.status_disconnected:
                    this.update();
                    break;
            }
        },
        /**
         * @private
         * <p>Callback for presence changes. This method is called when the
         * connected user's "resourcePresenceChanged" event is triggered.
         * This method calls {@link #update} if the updated resource is
         * equal to {@link jabberwerx.Client#resourceName}, or unbinds this
         * callback if the event source is not the client's connected user.</p>
         *
         * @param   {jabberwerx.EventObject} evt The event
         */
        _handlePresenceChange: function(evt) {
            if (evt.source !== this.client.connectedUser) {
                evt.notifier.unbind(this.invocation("_handlePresenceChange"));
            } else if (this._primaryPresence ||
                       evt.data.fullJid.getResource() == this.client.resourceName) {
                this.update();
            }
        },
        
        /**
         * <p>The client to montior and affect for presence changes.</p>
         *
         * @type jabberwerx.Client
         */
        client: null,
        /**
         * <p>Flag to indicate if the "unavailable" selection group is an
         * allowed selection choice.</p>
         *
         * <p><b>NOTE:</b> If this flag is <tt>true</tt>, it is the
         * responsibility of the API user to handle the connection and
         * updating presence.</p>
         * @type Boolean
         */
        allowUnavailable: false
    }, "jabberwerx.ui.SelfPresenceView");
    
    /**
     * <p>The mapping of default status choices. The choices are:</p>
     * <pre class="code">{
     *  "available":[<b>"Available"</b>, "Ready to Chat!"],
     *  "away": [<b>"Away"</b>, "Stepped Out", "Out to Lunch"],
     *  "dnd": [<b>"Do Not Disturb"</b>, "Busy", "In a Meeting"],
     *  "unavailable": [<b>"Offline"</b>]
     * }</pre>
     */
    jabberwerx.ui.SelfPresenceView.default_status_choices = {
        "available": ["Available", "Ready to Chat!"],
        "away": ["Away", "Stepped Out", "Out to Lunch"],
        "dnd": ["Do Not Disturb", "Busy", "In a Meeting"],
        "unavailable": ["Offline"]
    };
    /**
     * <p> An array stores presence status.</p>
     * @type String[]
     */
    jabberwerx.ui.SelfPresenceView.sortedShows = ["chat","available", "away", "xa", "dnd", "unavailable"];    
}
