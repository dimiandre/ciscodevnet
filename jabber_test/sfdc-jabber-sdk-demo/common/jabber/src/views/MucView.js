/**
 * filename:        MucView.js
 * created at:      2009/01/01T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
 

;(function(){
    if (jabberwerx && jabberwerx.ui) {
                
        jabberwerx.ui.MucView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MucView.prototype */{
            /**
             * @class
             * <p>A view for a MUC room pane.</p>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
             * </ul>
             *
             * @description
             * <p>Creates a new MucView with the given MUCRoom.</p>
             * 
             * @param {jabberwerx.MUCRoom} mucRoom The MUC room associated
             * with this muc view.
             *
             * @throws {TypeError} if {mucRoom} is not valid
             *
             * @constructs jabberwerx.ui.MucView
             * @extends jabberwerx.ui.JWView
             */
            init: function(mucRoom) {
                this._super();

                if (!(mucRoom && mucRoom instanceof jabberwerx.MUCRoom)) {
                    throw new TypeError("mucRoom must be a jabberwerx.MUCRoom");
                }
                
                this._MucRoom = mucRoom;
                this._MucRoom.event("roomBroadcastReceived").
                     bind(this.invocation("_handleChat"));
                this._MucRoom.event("roomSubjectChanged").
                     bind(this.invocation("_handleSubjectChange"));

                this._MucRosterSize = jabberwerx.ui.MucView.defaultOccupantsListSize;
            },

            /**
             * <p>Destroys this MucView. This unbinds all of the bound events
             * this object has.</p>
             */
            destroy: function() {
                this._MucRoom.event("roomBroadcastReceived").
                     unbind(this.invocation("_handleChat"));
                this._MucRoom.event("roomSubjectChanged").
                     unbind(this.invocation("_handleSubjectChange"));
                     
                this._MucRoom.exit();

                this._super();
            },

            /**
             * @private
             */
            _handleSubjectChange: function(evt) {
                var subject = evt.data.getSubject();
                this._setTitle(this.jq, subject);
                
                var jid = evt.data.getFromJID();
                var occupant = this._MucRoom.occupants.entity(jid);
                if(!occupant) {
                    occupant = this._createMUCOccupant(jid);
                }
                
                if (evt.data.getBody() || evt.data.getHTML()) {
                    this._messageListView.addMessage(occupant, evt.data);
                } else {
                    var msg = evt.data.clone();
                    msg.setBody("/me " + jabberwerx._(jabberwerx.MUCRoom.DefaultSubjectChange, subject));
                    this._messageListView.addMessage(occupant, msg);
                }
            },

            /**
             * @private
             */
            _setTitle: function(jq, subject) {
                if (jq) {
                    var roomName = this._MucRoom.getDisplayName();
                    var subject = subject || "";
                    jq.find(".muc_title").text(roomName + ": " + subject);
                }
            },

            /**
             * @private
             */
            _handleChat: function(evt) {
                var msg = evt.data;
                var jid = msg.getFromJID();
                var occupant = this._MucRoom.occupants.entity(jid);
                if (!occupant) {
                    occupant = this._createMUCOccupant(jid);
                }
                
                if (this._messageListView) {
                    this._messageListView.addMessage(occupant, msg);
                }
            },
            
            /**
             * @private
             */
            _createMUCOccupant: function(jid) {
                // create a dummy entity to provide a display name
                var occupant = new jabberwerx.Entity({jid: jid});
                
                // determine displayname based on resource, then node, then domain
                var dispName = jid.getResource() ||
                               jabberwerx.JID.unescapeNode(jid.getNode() || "") ||
                               jid.getDomain();
                occupant.setDisplayName(dispName);
                
                return occupant;
            },

            /**
             * <p>Retrieves or changes the size (width) of the occupants list.</p>
             *
             * @returns {Number|jabberwerx.ui.MucView} The current tab size
             *          (if getting), or this MucView (if setting)
             * @throws  {jabberwerx.ui.MucView.occupantsListSizeInvalidError} If {size}
             *          is specified but is not a positive number
             */
            occupantsListSize: function(size) {
                if (size == undefined) {
                    return this._MucRosterSize;
                } else if (!(typeof(size) == "number" && size > 0)) {
                    throw new jabberwerx.ui.MucView.occupantsListSizeInvalidError();
                }

                this._MucRosterSize = size;
                this.width(this.width());

                return this;
            },

            /**
             * <p>Overloads jabberwerx.ui.JWView's height. If setting the height, this makes sure
             * the MessageHistory pane takes up the remaining space.</p>
             *
             * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
             *          retrieve the current height)
             * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
             *          <b>NOT</b> be updated
             * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
             *          jabberwerx.ui.JWView (if changing)
             * @see     jabberwerx.ui.JWView#dimensions
             */
            height: function(h, noupdate) {
                var result = this._super(h, true);

                if (h && this.jq) {
                    var titleView = this.jq.find(".muc_title");
                    var height = h - titleView.outerHeight(true) -
                                 this._messageInput.jq.outerHeight(true);
                    this._messageListView.height(height, true);

                    this._MucRoster.height(h, true);

                    this.jq.find(".jabberwerx.chatview").height(h, true);

                    if (!noupdate) {
                        this.update();
                    }
                }

                return result;
            },

            /**
             * <p>Overloads jabberwerx.ui.JWView's width. If setting the width, this makes sure
             * that new width gets propogated to the underlining widgets.</p>
             *
             * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
             *          retrieve the current width)
             * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
             *          <b>NOT</b> be updated
             * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
             *          jabberwerx.ui.JWView (if changing)
             * @see     jabberwerx.ui.JWView#dimensions
             */
            width: function(w, noupdate) {
                var result = this._super(w, true);

                if (w && this.jq) {
                    w = this.jq.width();

                    var occupantsListSize = this.occupantsListSize();
                    this._MucRoster.width(occupantsListSize);

                    var mucInterface = this.jq.find(".jabberwerx.chatview");
                    var delta = (mucInterface.innerWidth() - mucInterface.width());
                    var width = w - occupantsListSize - delta;

                    this.jq.find(".muc_title").width(width);
                    this._messageListView.width(width, true);
                    this._messageInput.width(width, true);

                    if (!noupdate) {
                        this.update();
                    }
                }

                return result;
            },
            
            /**
             * <p>Triggers an update of this MucView. This goes through
             * all of the widgets associated with this view and calls
             * their updates.</p>
             */
            update: function() {
                if (!this._super()) {
                    return false;
                }

                this._MucRoster.update();
                this._messageListView.update();
                this._messageInput.update();
                
                return true;
            },

            /**
             * @private
             */
            _send: function(evt) {
                var msg = evt.data;
                //msg could be either plaintext, an html dom or an array of html doms. if single element
                //use text when checking for commands
                if (jabberwerx.$.isArray(msg)) {
                    msg = 'foo'; //fallthrough to sendBraodcast               
                } else if (jabberwerx.isElement(msg)) {
                    msg = jabberwerx.$(msg).text();
                }
                var match = /^\/(subject|topic|nick)\s+(.+)/.exec(msg || "");
                switch ((match && match[1]) || msg) {
                    case "subject":
                        //fall through
                    case "topic":
                        this._MucRoom.changeSubject(match[2]);
                        break;
                    case "nick":
                        this._MucRoom.changeNickname(match[2]);
                        break;
                    case "/clear":
                        this.clearHistory();
                        break;
                    default:
                        this._MucRoom.sendBroadcast(evt.data); //not msg, use data
                        break;
                }
                        
                return true;
            },

            /**
             * <p>Clears the MessageHistory pane.</p>
             */
            clearHistory: function() {
                if (this._messageListView) {
                    this._messageListView.clearHistory();
                }
            },
            
            /**
             * <p>Sets or clears the tab control. This method overrides the mixin 
             * {@link jabberwerx.ui.Tabbable#setTabControl} to set the label, and bind events for
             * updating when the MUCRoom receives a broadcast or a a subject changed event.</p>
             *
             * @param   {jabberwerx.ui.TabbedView.Tab} [tab] The new tab, or
             *          <tt>null</tt> if being removed from a tabbed view.
             */
            setTabControl: function(tab) {
                this._super(tab);
                
                if (tab) {
                    tab.label(this._MucRoom.getDisplayName());
                    this._MucRoom.event("roomBroadcastReceived").
                        bind(this.invocation("_handleSubjectChangeOrBroadcast"));
                    this._MucRoom.event("roomSubjectChanged").
                        bind(this.invocation("_handleSubjectChangeOrBroadcast"));
                } else {
                    this._MucRoom.event("roomBroadcastReceived").
                        unbind(this.invocation("_handleSubjectChangeOrBroadcast"));
                    this._MucRoom.event("roomSubjectChanged").
                        unbind(this.invocation("_handleSubjectChangeOrBroadcast"));
                }
            },

            /** 
             * @private
             */
            _handleSubjectChangeOrBroadcast: function(evt) {
                var tab = this.getTabControl();
                if (tab) {
                    tab.requestAttention();
                }
            },

            /**
             * <p>Creates the DOM for MucView consisting of a RosterView,
             * a title area (room name and subject), a MessageHistory
             * and a TextInput widget.</p>
             *
             * @param {Document} doc The document to use for creating content.
             * @returns {jQuery} The element containing the widgets.
             */
            createDOM: function(doc) {
                var mucView = jabberwerx.$("<div/>", doc).addClass("jabberwerx mucview");

                this._MucRoster = new
                    jabberwerx.ui.RosterView(this._MucRoom.occupants,
                        jabberwerx.ui.RosterView.groupmode_single);
                this._MucRoster.render().appendTo(mucView);

                var mucInterface = jabberwerx.$("<div/>").appendTo(mucView).
                                               addClass("jabberwerx chatview");

                jabberwerx.$("<div/>").appendTo(mucInterface).
                            addClass("muc_title");
                this._setTitle(mucView);

                this._messageListView = new jabberwerx.ui.MessageHistory();
                this._messageListView.render().appendTo(mucInterface);

                this._messageInput = new jabberwerx.ui.TextInput();
                this._messageInput.event("textSend").
                                   bind(this.invocation("_send"));
                this._messageInput.render().appendTo(mucInterface);

                return mucView;
            },

            /**
             * @private
             */
            _messageListView: null,
            /**
             * @private
             */
            _messageInput: null,
            /**
             * @private
             */
            _MucRosterSize: 0
        }, 'jabberwerx.ui.MucView');
        
        /**
         * <p>Error thrown when the requested occupants list size is not
         * valid (not a positive integer).</p>
         */
        jabberwerx.ui.MucView.occupantsListSizeInvalidError =
            jabberwerx.util.Error.extend("the occupantsListSize is invalid");

        /**
         * <p>The default size for the occupants list in the MucView.</p>
         * @type Number
         */
        jabberwerx.ui.MucView.defaultOccupantsListSize = 180;

        jabberwerx.ui.MucView.mixin(jabberwerx.ui.Tabbable);
    }
})();
