/**
 * filename:        ContactPresenceView.js
 * created at:      2009/07/02
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
;(function(){
    if (jabberwerx && jabberwerx.ui) {
        jabberwerx.ui.ContactPresenceView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.ContactPresenceView.prototype */{

            /**
             * @class
             * <p>View for displaying a contact's presence.</p>
             *
             * <p>The view shows the following info for an entity:</p>
             * <ol>
             * <li><b>presence icon:</b> Icon showing presence state</li>
             * <li><b>display name:</b> The display name for the entity</li>
             * <li><b>presence status:</b> Optional presence status message</li>
             * </ol>
             *
             * <p>
             * This class provides the following events:
             * <ul>
             * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
             * </ul>
             *
             * @description
             * <p>Creates a new <tt>ContactPresenceView</tt> with the given
             * entity.</p>
             *
             * <p>The {delegate} is used to indicate updates to this view are
             * handled by the delegate object, and not by this view
             * directly.</p>
             *
             * @param {jabberwerx.Entity} entity The entity to view presence for
             * @param {Object} [delegate] The delegate for this view
             * @throws {TypeError} If {entity} is not a {@link jabberwerx.Entity}
             * @constructs jabberwerx.ui.ContactPresenceView
             * @extends jabberwerx.ui.JWView
             */
            init: function(entity, delegate) {
                this._super();
                
                if (!(entity && entity instanceof jabberwerx.Entity)) {
                    throw new TypeError("entity must be a jabberwerx.Entity");
                }
                this.entity = entity;
                
                // setup events
                this.applyEvent("viewActivated");
                
                // register for presence and entity events
                if (delegate) {
                    this.delegate = delegate;
                } else {
                    entity.event("primaryPresenceChanged").bind(this.invocation("_onPrimaryPresenceChanged"));
                    entity.cache.event("entityUpdated").bind(this.invocation("_onEntityUpdated"));
                    entity.cache.event("entityRenamed").bind(this.invocation("_onEntityUpdated"));
                    entity.cache.event("entityDestroyed").bind(this.invocation("_onEntityDestroyed"));
                    entity.cache.event("entityCreated").bind(this.invocation("_onEntityCreated"));
                }                
                this.persistOptions(jabberwerx.ui.JWView.persist_html);
            },

            /**
             * <p>Creates the DOM to display. This method creates the following
             * DOM:</p>
             * <pre class="code">&lt;div class="jabberwerx contact presence show"&gt;
             *      &lt;a class="contactlink"/&gt;
             *          &lt;span class="displayname"&gt;{entity.getDisplayName()}&lt;/span&gt;
             *      &lt;span class="status"/&gt;
             * &lt;/div&gt;</pre>
             * @param {Document} doc The document to use for creating content.
             * @returns {Element} The generated element to display
             */         
            createDOM: function(doc) { 
                var builder = jabberwerx.$("<div/>", doc).
                        addClass("jabberwerx contact presence show");
                
                var link = jabberwerx.$("<a/>").appendTo(builder).
                        addClass("contactlink").
                        attr("href", encodeURI("#" + this.entity.jid)).
                        bind("click", this.invocation("_onClick"));
                        
                jabberwerx.$("<span/>").appendTo(link).
                        addClass("displayname").
                        text(this.entity.getDisplayName());
                
                builder.append(" ");
                
                jabberwerx.$("<span/>").appendTo(builder).
                        addClass("status").
                        text("\u00a0");
                        
                return builder.get(0);
            },
            /**
             * <p>Restore the DOM to display.</p>
             *
             * @param {Document} doc The document to use for creating content
             * @returns {Element} The generated element to display
             */ 
            restoreDOM: function(doc) {
                var res = jabberwerx.$(this._super(doc));
                res.find("a.contactlink").bind("click", this.invocation("_onClick"));
                return res;
            },
            
            /**
             * <p>Updates the display for this ContactPresenceView</p>
             *
             * @returns {Boolean} <tt>true</tt> if this ContactPresenceView is
             *          updatable
             */            
            update: function() {
                // dump if not ready
                if (!this._super()) {
                    return false;
                }                
                var presence = this.entity.getPrimaryPresence();
                var show = (presence &&
                        (presence.getType() || presence.getShow() || "available")) ||
                        "unknown";
                var status = (presence && presence.getStatus()) || "";
                var title = jabberwerx._(jabberwerx.ui.ContactPresenceView.show_status[show]) + (status ? " :: " + status : "");
                this.jq.removeClass("chat available away xa dnd unavailable unknown").
                        addClass(show).
                        attr("title", title);
                this.jq.find("a").
                        attr("title", title);
                this.jq.find("span.status").text(status || "\u00a0");
                this.jq.find("span.displayname").text(this.entity.getDisplayName());
                
                return true;
            },
            
            /**
             * @private
             * Callback for "presenceReceived" event
             * 
             * @param {jabberwerx.EventNotifier} event The presence received event
             */
            _onPrimaryPresenceChanged: function(event) {
                this.update();
            },
            
            /**
             * @private
             * Callback for "entityUpdated" event. Update the display name of the entity
             * 
             * @param {jabberwerx.EventNotifier} event The entity updated event
             */            
            _onEntityUpdated: function(event) {
                this.update();
            },

            /**
             * @private
             * Callback for "entityDestroyed" event. Create a temporary entity for the destroyed entity
             * 
             * @param {jabberwerx.EventNotifier} event The entity destroyed event
             */  
            _onEntityDestroyed: function(event) {
                var newEntity = event.data;
                if (this.entity.jid.equals(newEntity.jid)) {
                    this.entity = this.entity.cache.entity(this.entity.jid) ||
                                  new jabberwerx.TemporaryEntity(
                                        this.entity.jid,
                                        this.entity.cache);
                    this.update();
                }
            },

            /**
             * @private
             * Callback for "entityCreated" event. Replace the existing entity with the newly created entity
             * 
             * @param {jabberwerx.EventNotifier} event The entity created event
             */ 
            _onEntityCreated: function(event) {
                var newEntity = event.data;
                if (this.entity.jid.equals(newEntity.jid)) {
                    this.entity = newEntity;
                    if (!this.delegate) {
                        this.entity.event("primaryPresenceChanged").
                                bind(this.invocation("_onPrimaryPresenceChanged"));
                    }
                    
                    this.update();
                }
            },
            /**
             * @private
             * <p>Callback for "click" events</p>
             *
             * @param {jQuery.Event} evt The jQuery/DOM event
             */
            _onClick: function(evt) {
                this.event("viewActivated").trigger({
                    type: "click"
                });
                return false;
            },
            
            /**
             * <p>The client to montior and affect for presence changes.</p>
             *
             * @type jabberwerx.Client
             */            
            entity : null
        }, 'jabberwerx.ui.ContactPresenceView');
    }
    
    /**
     * <p>The mapping of default show value to display text. The choices are:</p>
     * <pre class="code">{
     *  "chat":<b>"Ready to Chat!"</b>,
     *  "available":<b>"Available"</b>,
     *  "away": <b>"Away"</b>,
     *  "xa": <b>"Extended Away"</b>
     *  "dnd": <b>"Do Not Disturb"</b>,
     *  "unavailable": <b>"Offline"</b>,
     *  "unknown": <b>"Unknown"</b>
     * }</pre>
     */
    jabberwerx.ui.ContactPresenceView.show_status = {
        "chat": "Ready to Chat!",
        "available": "Available",
        "away": "Away",
        "xa": "Extended Away",
        "dnd": "Do Not Disturb",
        "unavailable": "Offline",
        "unknown":"Unknown"
    };
})();
