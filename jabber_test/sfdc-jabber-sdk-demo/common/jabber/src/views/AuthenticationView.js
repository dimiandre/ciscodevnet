/**
 * filename:        AuthenticationView.js
 * created at:      2009/01/01T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

;(function() {
    if ((jabberwerx) && (jabberwerx.ui)) {
        jabberwerx.ui.AuthenticationView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.AuthenticationView.prototype */{
        /**
         * @class
         * <p>View for logging into an XMPP server.</p>
         * <p>This allows the user to enter in his/her username and password
         * and log into the XMPP server.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * </ul>
         *
         * @description
         * Creates a new AuthenticationView with the given client and optional domain.
         *
         * @param {jabberwerx.Client} client The client
         * @param {String} [domain] The domain portion of the JID that's used to
         * login. If no domain is specified, the user has to specify the domain
         * part of their jid in the username field before logging in.
         * @throws {TypeError} If {client} is not valid
         * @throws {jabberwerx.JID.InvalidJIDError} If {domain} isn't a valid domain.
         * @constructs jabberwerx.ui.AuthenticationView
         * @extends jabberwerx.ui.JWView
         */
        init: function(client, domain) {
            this._super();

            if (!(client && client instanceof jabberwerx.Client)) {
                throw new TypeError("client must be a jabberwerx.Client");
            }

            this.client = client;
            if (domain) {
                this.domain = jabberwerx.JID.asJID(domain).getDomain();
            }
        },

        /**
         * <p>Creates the DOM to display.</p>
         *
         * <p>It also binds to the submit action of the form to
         * start the login process.</p>
         * @param {Document} doc The document to use for creating content
         * @returns {Element} The generated element to display
         */
        createDOM: function(doc) {
            var data = jabberwerx.$("<form/>", doc).
                    addClass("jabberwerx auth_form").
                    bind("submit", this.invocation("_login"));

            var userDiv = jabberwerx.$("<div/>").
                    appendTo(data).
                    addClass("username_div");
            jabberwerx.$("<label/>").appendTo(userDiv).
                          attr("for", "username").
                          text(jabberwerx._("Username:"));
            jabberwerx.$("<input type='text'/>").appendTo(userDiv).
                          addClass("username");

            var passDiv = jabberwerx.$("<div/>").
                    appendTo(data).
                    addClass("password_div");
            jabberwerx.$("<label/>").appendTo(passDiv).
                          attr("for", "password").
                          text(jabberwerx._("Password:"));
            jabberwerx.$("<input type='password'/>").appendTo(passDiv).
                          addClass("password");

            var submitDiv = jabberwerx.$("<div/>").
                    appendTo(data).
                    addClass("submit_div");
            jabberwerx.$("<label/>").appendTo(submitDiv).
                          attr("for", "auth_submit");
            jabberwerx.$("<input type='submit'/>").appendTo(submitDiv).
                          addClass("auth_submit").
                          attr("value", jabberwerx._("Login"));

            var statusDiv = jabberwerx.$("<div/>").appendTo(data).
                    attr("class", "auth_status_div").
                    text("\u00A0");
            jabberwerx.$("<span/>").appendTo(statusDiv).addClass("auth_status");

            return data.get(0);
        },

        /**
         * <p>Restore the DOM to display.</p>
         *
         * @param {Document} doc The document to use for creating content
         * @returns {Element} The generated element to display
         */
        restoreDOM: function(doc) {
            var res = jabberwerx.$(this.createDOM(doc));
            if (this._persistedUsername) {
                res.find('input.username').val(this._persistedUsername);
                this._persistedUsername = null;
            }
            if (this._persistedPassword) {
                res.find('input.password').val(jabberwerx.util.decodeSerialization(this._persistedPassword));
                this._persistedPassword = null;
            }
            if (this._persistedError) {
                res.find('span.auth_status').text(this._persistedError);
                this._persistedError = null;
            }
            return res.get(0);
        },

        /**
         * @private
         * <p>Starts the login process. Updates the status section with errors
         * if the username is invalid or if the login process failed on the
         * servers end.</p>
         *
         * @param {JQuery.Event} evt The event object describing the event.
         */
        _login: function(evt) {
            try {
                this._updateStatus();

                var userQuery = this.jq.find("input.username");
                var username = userQuery.val();

                var passQuery = this.jq.find("input.password");
                var password = passQuery.val();
                //clear password if needed
                if (jabberwerx._config.baseReconnectCountdown == 0) {
                    passQuery.val('');
                }

                var submitQuery = this.jq.find("input.auth_submit");
                
                //setup username if domain is not set
                var domainname;
                if (!this.domain) {
                    var atIdx = (this.allowAtSymbol) ?
                        // if allowing @, look for last instance
                        username.lastIndexOf('@') :
                        // otherwise look for first
                        username.indexOf('@');
                    domainname = username.substring(atIdx + 1);
                    username = username.substring(0, atIdx);
                }
                
                //Check the flag to see if @ is allowed
                if ( !this.allowAtSymbol && /@/.test(username)) {
                    throw new jabberwerx.JID.InvalidJIDError("invalid characters found");
                }
                
                var userJid;
                
                //first attempt to create the JID object without JID escaping
                try {
                    userJid = new jabberwerx.JID({
                        "node" : username,
                        "domain" : domainname || this.domain
                    });
                } catch (ex) {
                    userJid = new jabberwerx.JID({
                        "node" : username,
                        "domain" : domainname || this.domain,
                        "unescaped" : true
                    });
                }
                
                if (!userJid.getNode()) {
                    throw new jabberwerx.ui.AuthenticationView.MissingJIDNode();
                }

                var that = this;
                var onSuccess = function() {
                    userQuery.removeAttr("disabled");
                    passQuery.removeAttr("disabled");                    
                    submitQuery.removeAttr("disabled");
                    that._updateStatus();
                }
                var onError = function(evt) {
                    userQuery.removeAttr("disabled");
                    passQuery.removeAttr("disabled");
                    submitQuery.removeAttr("disabled");
                    that._updateStatus(evt);
                }
                var arg = {
                    successCallback: onSuccess,
                    errorCallback: onError,
                    register:   this._register
                };

                userQuery.attr("disabled", "disabled");
                passQuery.attr("disabled", "disabled");
                submitQuery.attr("disabled", "disabled");

                this.client.connect(userJid,password, arg);

            } catch (ex) {
                userQuery.removeAttr("disabled");
                passQuery.removeAttr("disabled");
                submitQuery.removeAttr("disabled");
                this._updateStatus(ex);
            }

            return false;
        },

        /**
         * @private
         * <p>Finds the status section and updates it according to the argument
         * passed in.</p>
         *
         * @param error Determines what the error status will be. If nothing is
         * passed in, clears out the status section.
         */
        _updateStatus: function(error) {
            var statusArea = this.jq.find("span.auth_status");
            if (!error) {
                statusArea.text("\u00A0");
            } else if (error instanceof jabberwerx.JID.InvalidJIDError) {
                statusArea.text(jabberwerx.ui.AuthenticationView.invalid_user);
            } else  {
                var msg = jabberwerx.errorReporter.getMessage(error);
                statusArea.text(msg);
            } 
        },

        willBeSerialized: function() {
            //save off whatever we want persisted
            if (this.jq) {
                this._persistedUsername = this.jq.find('input.username').val();
                //check reconnectcout to see if password should be serialized. Unit tests and demo's set this value on the
                //fly, most distributions will "brand" the value, making this check unwanted but there ya go
                if (jabberwerx._config.baseReconnectCountdown != 0) {
                    this._persistedPassword = jabberwerx.util.encodeSerialization(this.jq.find('input.password').val());
                }
                this._persistedError = this.jq.find("span.auth_status").text();
            }
            return this._super();
        },

        _persistedUsername: null,
        _persistedPassword: null,
        _persistedError: null,

        /**
         * @ private
         * This flag is purely for testing in-band registration logic and should not be utilized for any other purpose.
         */
        _register: false,
        
        /**
         * <p>The client used to connect to the XMPP server.</p>
         *
         * @type jabberwerx.Client
         */
        client: null,

        /**
         * <p>The domain of the XMPP server.</p>
         *
         * @type String
         */
        domain: null,
        
        /**
         * Dictates if usernames with the @ character are allowed by this view.
         * <b>NOTE:</B> The user may still enter @ if {@link #domain} is not
         * set, in which case the username is treated as 'username@domain'.
         *
         * @type Boolean
         */
        allowAtSymbol: false
        }, 'jabberwerx.ui.AuthenticationView');

        /**
         * <p>Invalid user error message: "Invalid username"</p>
         * @type String
         * @constant
         */
        jabberwerx.ui.AuthenticationView.invalid_user = "Invalid username.";
        /**
         * <p>The default error message "Login failed".</p>
         * @type String
         * @constant
         */
        jabberwerx.ui.AuthenticationView.default_error_message = "Login failed.";
        /**
         * @class jabberwerx.AuthenicationView.MissingDomain
         * <p>Error to indicate that the user must enter a JID with a node and
         * domain. Can only occurs if a domain has not been specified in the
         * AuththenicationView constructor.</p>
         *
         * @extends jabberwerx.util.Error
         */
        jabberwerx.ui.AuthenticationView.MissingJIDNode = jabberwerx.util.Error.extend(
            "A node and domain must be specified in the JID (user@domain).");
    }
})();
