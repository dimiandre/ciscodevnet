/**
 * filename:        TabbedView.js
 * created at:      2009/07/23T14:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.TabbedView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.TabbedView.prototype */{
        /**
         * @class
         * <p>Tabbed view implementation.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView.html">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.TabbedView.html">jabberwerx.ui.TabbedView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new TabbedView.</p>
         *
         * @constructs  jabberwerx.ui.TabbedView
         * @extends     jabberwerx.ui.JWView
         */
        init: function() {
            this._super();

            this._tabList = [];

            this.applyEvent("tabActivated");
            this.applyEvent("tabDeactivated");
        },
        /**
         * <p>Destroys this TabbedView. This implementation first destroys
         * all tab currently contained, then calls the superclass'
         * implementation.</p>
         */
        destroy: function() {
            jabberwerx.$.each(this._tabList.concat(), function() {
                this.remove();
            });

            this._super();
        },

        /**
         * <p>Adds a new tab to this TabbedView.</p>
         *
         * <p>A new tab is created with the given id and content, then
         * appended to the end of the tablist. If this is the first tab,
         * it is automatically activated (if this TabbedView is
         * rendered).</p>
         *
         * @param   {String} id The identifier for the tab
         * @param   {jabberwerx.ui.JWView} content The tab content
         * @returns {jabberwerx.ui.TabbedView.Tab} The added tab
         * @throws  {TypeError} if {id} not a non-empty string; or if
         *          {content} is not an instance of jabberwerx.ui.JWView
         * @throws  {jabberwerx.ui.TabbedView.TabAlreadyExistsError} If a tab
         *          for {id} is already present.
         */
        addTab: function(id, content) {
            var     tab = this.getTab(id);

            if (tab) {
                throw new jabberwerx.ui.TabbedView.TabAlreadyExistsError(
                        "tab for " + id + " already exists");
            }

            tab = new jabberwerx.ui.TabbedView.Tab(this, content, id);

            this._tabList[id] = tab;
            //TODO: sorted insert?
            this._tabList.push(tab);
            if (this.jq) {
                var tc = this.jq.find(".tabcontent");
                tab._wrapper.render().appendTo(tc).hide();

                var tl = this.jq.find(".tablist");
                tab.render().appendTo(tl);

                if (this._tabList.length == 1) {
                    tab.activate();
                }
            }

            return tab;
        },
        /**
         * <p>Removes the requested tab from this TabbedView.</p>
         *
         * @param   {String} id The id of the tab to remove
         * @returns {Boolean} <tt>true</tt> if a tab was removed
         * @throws  {TypeError} If {id} is not a non-empty string
         */
        removeTab: function(id) {
            var     tab = this.getTab(id);

            if (tab) {
                //remove from the UI
                tab.remove();

                return true;
            }

            return false;
        },

        /**
         * @private
         */
        _removeFromTabList: function(tab) {
            //remove from the set
            var pos = jabberwerx.$.inArray(tab, this._tabList);
            this._tabList.splice(pos, 1);
            delete this._tabList[tab.id];
        },

        /**
         * <p>Retrieves the given tab in this TabbedView.</p>
         *
         * @param   {String} id The id of the tab to retrieve
         * @returns {jabberwerx.ui.TabbedView.Tab} The tab for {id}, or
         *          <tt>null</tt> if none available
         * @throws  {TypeError} if {id} is not a non-empty string
         */
        getTab: function(id) {
            if (!(id && typeof(id) == "string")) {
                throw new TypeError("id is not valid");
            }

            return this._tabList[id];
        },
        /**
         * <p>Retrieves the active tab for this TabbedView. A tab is active
         * if its contents are what is currently being displayed by the
         * TabbedView.</p>
         *
         * @returns {jabberwerx.ui.TabbedView.Tab} The active tab, or
         *          <tt>null</tt> if none
         */
        getActiveTab: function() {
            if (!this.jq) {
                return null;
            }

            var activeDOM = this.jq.find(".tablist .tab.active").get(0);
            return (activeDOM && activeDOM.jw_view) || null;
        },
        /**
         * <p>Retrieves the number of tabs in this TabbedView. This method
         * less expensive than calling {@link #getAllTabs} if only the number
         * of tabs is needed.</p>
         *
         * @returns {Number} The number of tabs in this TabbedView
         */
        getTabCount: function() {
            return this._tabList.length;
        },
        /**
         * <p>Retrieves all of the tabs in this TabbedView. This method
         * returns a snapshot of the tabs at the time this method is
         * called. Any tabs added or removed since this method is called
         * are not reflected in the returned array.</p>
         *
         * @returns {[abberwerx.ui.TabbedView.Tab[]} The tabs in this
         *          TabbedView
         */
        getAllTabs: function() {
            return this._tabList.concat();
        },

        /**
         * <p>Retrieves or changes the size (width) of the tab list.</p>
         *
         * @returns {Number|jabberwerx.ui.TabbedView} size The current tab size
         *          (if getting), or this TabbedView (if setting)
         * @throws  {jabberwerx.ui.TabbedView.tabListSizeInvalidError} If {size}
         *          is specified but is not a positive number
         */
        tabListSize: function(size) {
            if (size === undefined) {
                return this._tabListSize;
            } else if (!(typeof(size) == "number" && size > 0)) {
                throw new jabberwerx.ui.TabbedView.tabListSizeInvalidError();
            }

            this._tabListSize = size;
            this.width(this.width());

            return this;
        },

        /**
         * <p>Retrieves or changes the width of this view. This implementation
         * extends the superclass to adjust the content and tablist to match
         * the new width.</p>
         *
         * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
         *          retrieve the current width)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#width
         */
        width: function(w, noupdate) {
            var result = this._super(w, true);
            if (w !== undefined && this.jq ) {
                //retrieve updated *pixel* values
                var w = this.width();

                var tl = this.jq.find(".tablist");
                var tc = this.jq.find(".tabcontent");
                var tabListSize = this.tabListSize();
                tl.width(tabListSize);
                var delta = jabberwerx.$.browser.msie ? -2 : (tc.outerWidth() - tc.width()) - 2;
                w = w  - tabListSize - delta
                tc.width(w);
                //adjust active tabs h & w
                if (this.getActiveTab()) {
                    this.getActiveTab().update();
                }

                if (!noupdate) {
                    this.update();
                }
            }
            return result;
        },

        /**
         * <p>Retrieves or changes the height of this view. This implementation
         * extends the superclass to adjust the content and tablist to match
         * the new height.</p>
         *
         * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
         *          retrieve the current height)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#height
         */
        height: function(h, noupdate) {
            var result = this._super(h, true);
            if (h !== undefined && this.jq) {
                //retrieve updated *pixel* values
                //has been updated with our css border info, h<=h here
                var h = this.height();
                var tl = this.jq.find(".tablist");
                var delta = tl.outerHeight() - tl.height();
                tl.height(h - delta);
                var tc = this.jq.find(".tabcontent");
                delta = tc.outerHeight() - tc.height();
                tc.height(h - delta);
                //adjust active tabs h & w
                if (this.getActiveTab()) {
                    this.getActiveTab().update();
                }

                if (!noupdate) {
                    this.update();
                }
            }
            return result;
        },

        /**
         * @private
         */
        _calculateContentHeight: function() {
            if (this.jq) {
                var container = this.jq.find(".tabcontent");
                var size = container.height();
                return size;
            }

            return 0;
        },
        /**
         * @private
         */
        _calculateContentWidth: function() {
            if (this.jq) {
                var container = this.jq.find(".tabcontent");
                var size = container.width();
                return size;
            }

            return 0;
        },

        /**
         * <p>Updates this TabbedView. This implementation ensures all
         * tabs (and their content) are rendered. If there are any tabs,
         * but none are active, it will automatically activate the first
         * one.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the view is updatable
         */
        update: function() {
            if (!this._super()) {
                return false;
            }
            var tc = this.jq.find(".tabcontent");
            var tl = this.jq.find(".tablist");
            jabberwerx.$.each(this._tabList, function() {
                if (!this.jq) {
                    this._wrapper.render().appendTo(tc).hide();
                    this.render().appendTo(tl);
                }
            });
            if (!this.getActiveTab() && this._tabList.length) {
                this._tabList[0].activate();
            }
            
            return true;        
        },

        /**
         * <p>Creates the DOM representation of the TabbedView. This
         * implementation produces the following:</p>
         * <pre class='code'>
         *      &lt;div class='jabberwerx tabpane'&gt;
         *          &lt;div class='tabcontent'/&gt;
         *          &lt;div class='tablist'/&gt;
         *      &lt;/div&gt;
         * </pre>
         *
         * @param   {Document} doc The document to create the DOM with
         * @returns {Element} The DOM representing this TabbedView
         */
        createDOM: function(doc) {
            var builder = jabberwerx.$("<div/>", doc).
                    addClass("jabberwerx tabpane");

            var content = jabberwerx.$("<div/>").appendTo(builder).
                    addClass("tabcontent");
            var tablist = jabberwerx.$("<div/>").appendTo(builder).
                    addClass("tablist");

            return builder.get(0);
        },
       /**
        * <p>Restore the DOM for a TabbedView </p>
        *
        * @param {Document} doc The document to use for creating content.
        * @returns {jQuery} The element containing the view.
        */ 
        restoreDOM: function(doc) {
            var res = jabberwerx.$(this.createDOM(doc));
            //restore all existing tabs
            var tc = res.find(".tabcontent");
            var tl = res.find(".tablist");
            jabberwerx.$.each(this._tabList, function() {
                //add like addTab without eventing
                this._wrapper.render().appendTo(tc).hide();
                this.render().appendTo(tl);
            });
            return res.get(0);
        },
        /**
         *<p>Shows the TabbedView if hidden in the document.</p>
         */ 
        show: function() {
            this._super();
            var t = this.getActiveTab();
            if (t) {
                t.show(); //showing tab, not tab's jq
                t._wrapper.show(); //showing wrapper, not wrapper's jq
                t.update();
            }
        },

        /**
         * @private
         * <p>The tab size.</p>
         *
         * @type    Number
         */
        _tabListSize:   150
    }, "jabberwerx.ui.TabbedView");

    /**
     * <p>Error thrown when attempting to add a tab with an ID already present
     * in the TabbedView.</p>
     */
    jabberwerx.ui.TabbedView.TabAlreadyExistsError = jabberwerx.util.Error.extend("the identified tab already exists");
    /**
     * <p>Error thrown when the requested tab size is not valid (not a positive
     * integer).</p>
     */
    jabberwerx.ui.TabbedView.tabListSizeInvalidError = jabberwerx.util.Error.extend("the tabListSize is invalid");

    jabberwerx.ui.TabbedView.ContentWrapper = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.TabbedView.ContentWrapper.prototype */{
        /**
         * @private
         * @class
         * <p>Wraps the content of a tabbed view in both HTML and JavaScript.
         * For HTML, wraps the content to provide better positioning/sizing.
         * For JavaScript, delegates most operations to the wrapped content.</p>
         *
         */
        init: function(tabview, content, id) {
            this._super();

            this.tabview = tabview;
            this.content = content;
            this.id = id;
        },
        destroy: function() {
            this.content.destroy();
            this.content = null;

            this._super();
        },

        isActive: function() {
            return Boolean(this.jq && this.jq.hasClass("active"));
        },
        _activate: function() {
            if (this.jq && !this.isActive()) {
                this.jq.addClass("active");
                this.show();
                this.content.event("viewActivated").trigger({
                    "type": "focus"
                });
            }
        },
        _deactivate: function() {
            if (this.jq && this.isActive()) {
                this.jq.removeClass("active");
                this.hide(); //hiding this.jq, content hidden by this hide
                this.content.event("viewDeactivated").trigger({
                    "type": "blur"
                });
            }
        },

        dimensions: function(dim) {
            return this.content.dimensions(dim);
        },
        height: function(h, noupdate) {
            return this.content.height(h, noupdate);
        },
        width: function(w, noupdate) {
            return this.content.width(w, noupdate);
        },

        show: function() {
            this._super();
            this.content.show();
            this.update(); //set size and update content
            return this;
        },

        hide: function() {
            this.content.hide();
            return this._super();
        },

        update: function() {
            if (!this._super()) {
                return false;
            }
            this.dimensions({
                height: this.tabview._calculateContentHeight(),
                width: this.tabview._calculateContentWidth()
            });

            return true;
        },

        destroyDOM: function() {
            this.content && this.content.remove();

            this._super();
        },
        createDOM: function(doc) {

            var builder = jabberwerx.$("<div/>", doc).
                    addClass("tabdata").
                    attr("id", "jabberwerx_tabdata_" + this.id);

            this.content.render().appendTo(builder);
            return builder.get(0);
        },
        restoreDOM: function(doc) {
            var res = jabberwerx.$(this.createDOM(doc));//.hide();
            if (this._persistedIsActive) {
                res.addClass("active");
                //add acrtive to content dom as well for some reason.
                this.content.jq.addClass("active");
                this._persistedIsActive = null;
            }
            return res.get(0);
        },

        willBeSerialized: function() {
            if (this.isActive()) {
                this._persistedIsActive = true;
            }
            return this._super();
        }

    }, "jabberwerx.ui.TabbedView.ContentWrapper");

    jabberwerx.ui.TabbedView.Tab = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.TabbedView.Tab.prototype */{
        /**
         * @class
         * <p>View of the tab control in a TabbedView. The tab is what selects
         * the contents to display in a TabbedView.</p>
         *
         * <p><b>NOTE:</b> This type should not be created directly. Instead,
         * it is created via {@link jabberwerx.ui.TabbedView#addTab}.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView.html">jabberwerx.ui.JWView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new Tab with the given tabview, content, and
         * identifier.</p>
         *
         * @param       {jabberwerx.ui.TabbedView} tabview The owning
         *              TabbedView
         * @param       {jabberwerx.ui.JWView} content The contents view associated with
         *              this tab
         * @param       {String} id The identifier of this tab
         * @throws      {TypeError} If {id} is not a non-empty string; or if
         *              {content} is not an instance of jabberwerx.ui.JWView.
         * @constructs  jabberwerx.ui.TabbedView.Tab
         * @extends     jabberwerx.ui.JWView
         */
        init: function(tabview, content, id) {
            this._super();

            if (!(id && typeof(id) == "string")) {
                throw new TypeError("id must be a non-empty string");
            }

            if (content instanceof jabberwerx.ui.TabbedView.ContentWrapper) {
                // do nothing
            } else if (content instanceof jabberwerx.ui.JWView) {
                // wrap it
                content = new jabberwerx.ui.TabbedView.ContentWrapper(tabview, content, id);
            } else {
                throw new TypeError("content is not an instance of jabberwerx.ui.JWView");
            }

            this.tabview = tabview;
            this._wrapper = content;
            this.content = content.content;
            this.id = id;

            if (jabberwerx.$.isFunction(this.content.setTabControl)) {
                this.content.setTabControl(this);
            }
            //tab html can be serialized, content serizlized elsewhere
            this.persistOptions(jabberwerx.ui.JWView.persist_html);
        },
        /**
         * <p>Destroys this Tab. This implementation ensures the tab content
         * is also destroyed.</p>
         */
        destroy: function() {
            this._wrapper.destroy();

            this._super();
        },
        /**
         * <p>Retrieves or changes the label for this Tab. If the label is
         * being changed, the view is also updated.</p>
         *
         * @param   {String} [label] The new label, or <tt>undefined</tt>
         *          to retrieve the current label
         * @returns {String|jabberwerx.ui.TabbedView.Tab} The current label
         *          (if getting), or this Tab (if setting)
         */
        label: function(val) {
            if (val === undefined) {
                return this._labelTxt;
            }

            if (this._labelTxt != val) {
                this._labelTxt = val;
                this.update();
            }

            return this;
        },


        /**
         * <p>Makes this tab's header (and contents) hidden. If this tab is
         * active, the closest visible tab is activated.</p>
         *
         * @returns {jabberwerx.ui.TabbedView.Tab} This tab
         */
        hide: function() {
            this._activateAdjacentTab();
            this._wrapper.hide();
            this._super();
            return  this
        },

        /**
         * <p>Determines if this Tab is active.</p>
         *
         * <p>This method returns <tt>false</tt> if the DOM has not been
         * created.</p>
         *
         * @returns {Boolean} <tt>true</tt> If this tab is active
         */
        isActive: function() {
            return Boolean(this.jq && this.jq.hasClass("active"));
        },
        /**
         * <p>Activates this tab within its owning TabbedView. If this
         * tab is renderd and not already active, it does the following:</p>
         * <ol>
         * <li>Deactivate the currently active tab (if any)</li>
         * <li>Adds the "active" CSS class to this tab's DOM</li>
         * <li>Shows the content for this tab within the TabbedView</li>
         * <li>Triggers the "tabActivated" event on the owning TabbedView, with
         * this Tab as the data</li>
         * </ol>
         *
         * <p>Regardless of this tab's previous active state, any pending
         * notification is canceled.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the tab selection changed
         */
        activate: function() {
            var retval = this.jq && !this.isActive();
            if (retval) {
                var prev = this.tabview.getActiveTab();
                prev && prev._deactivate();

                this.jq.addClass("active");
                this._wrapper._activate();

                this.tabview.event("tabActivated").trigger(this);
            }
            this.cancelAttention();
            return retval;
        },
        /**
         * @private
         * <p>Deactivates this tab. If this tab is rendered and active, it
         * does the following:</p>
         * <ol>
         * <li>Removes the "active" CSS class from this tab's DOM</li>
         * <li>Hides the content for this tab within the TabbedView</li>
         * <li>Triggers the "tabDeactivated" event on the owning TabbedView,
         * with this Tab as the data</li>
         * </ol>
         *
         * @returns {Boolean} <tt>true</tt> if the tab selection changed
         */
        _deactivate: function() {
            if (this.isActive()) {
                this.jq.removeClass("active");
                this._wrapper._deactivate();

                this.tabview.event("tabDeactivated").trigger(this);

                return true;
            }
            return false;
        },

        /**
         * <p>Requests the user's attention on this tab. This implementation
         * causes the tab's display to switch between the "attention" state
         * and its normal state every second.</p>
         *
         * <p>The "attention" state adds the "attn" CSS class to this tab's
         * DOM, and applies the given data (if any) to the &lt;span
         * class='notification'/&gt; within this tab's DOM.</p>
         *
         * @param   {jQuery|Element|String} [data] The data to display as
         *          part of the attention request
         * @returns {Boolean} <tt>true</tt> if this request caused a visual
         *          change
         */
        requestAttention: function(data) {
            if (!this.isActive() && this.jq && !this._attentionInfo) {
                if (data) {
                    //convert to a jQuery in some manner
                    if (data instanceof jabberwerx.$) {
                        // do nothing
                    } else if (jabberwerx.isElement(data)) {
                        data = jabberwerx.$(data);
                    } else {
                        data = jabberwerx.$("<div/>").
                                append(data).
                                contents();
                    }
                }

                var notify = {
                    id: "",
                    data: data
                };

                this._attentionInfo = notify;
                this._flashAttention(false);

                return true;
            }

            return false;
        },
        /**
         * <p>Cancels the attention request. This implementation reverts the
         * tab to its normal state.</p>
         */
        cancelAttention: function() {
            if (this._attentionInfo) {
                window.clearTimeout(this._attentionInfo.id);
                this._flashAttention(true);
                delete this._attentionInfo;
            }
        },
        /**
         * @private
         */
        _flashAttention: function(cancel) {
            var notify = this._attentionInfo;
            if (!notify) {
                //no flashing, no work!
                return;
            }

            cancel = Boolean(cancel || this.jq.hasClass("attn"));
            if (cancel) {
                //remove notification
                this.jq.removeClass("attn");
                if (notify.data) {
                    notify.data.remove();
                }
            } else {
                //add notification
                this.jq.addClass("attn");
                if (notify.data) {
                    this.jq.find(".notification").append(notify.data);
                }
            }

            // TODO: make interval configurable?
            notify.id = window.setTimeout(
                    this.invocation("_flashAttention"),
                    1000);
        },

        /**
         * <p>Updates this tab's view. This implementation updates the
         * tab's label to the value of {@link #label}, then updates its
         * content.</p>
         *
         * @returns {Boolean} <tt>true</tt> if this view is updatable
         */
        update: function() {
            if (!this._super()) {
                return false;
            }

            var label = this.label() || this.id;
            this.jq.attr("title", label);
            this.jq.find(".tab_label").text(label);
            this._wrapper.update();

            return true;
        },

        /**
         * @private
         */
        _handleTabClick: function(evt) {
            this.activate();

            return false;
        },
        /**
         * @private
         */
        _handleClose: function(evt) {
            this.destroy();

            return false;
        },

        /**
         * <p>Creates this Tab's DOM representation. This method creates
         * the following:</p>
         * <pre class='code'>
         *  &lt;div class='tab' id='jabberwerx_tabctrl_{tab.id}'&gt;
         *      &lt;img src='{path to theme images}/icon-close.png'
         *              alt='Close'
         *              title='Close'
         *              class='closer'/&gt;
         *      &lt;span class='notification'&gt;&amp;nbsp;&lt;/span&gt;
         *      &lt;a href='#' class='tab_link'&gt;
         *          &lt;span class='tab_label'&gt;{tab.label() || tab.id}&lt;/span&gt;
         *      &lt;/a&gt;
         *  &lt;/div&gt;
         * </pre>
         *
         * @param   {Document} doc The document to create this DOM with
         * @returns {Element} This Tab's DOM representatation
         */
        createDOM: function(doc) {
            var builder = jabberwerx.$("<div/>", doc).
                    addClass("tab").
                    bind("click", this.invocation("_handleTabClick")).
                    attr("id", "jabberwerx_tabctrl_" + jabberwerx.util.slugify(this.id));

            var link = jabberwerx.$("<a href='#'/>").appendTo(builder).
                    bind("click", this.invocation("_handleTabClick")).
                    text(this.label() || this.id).
                    addClass("tab_link tab_label");

            jabberwerx.$("<img/>").appendTo(builder).
                    attr("src", jabberwerx.ui.getThemeImageURL("icon-close.png")).
                    attr("alt", "Close").
                    attr("title", "Close").
                    bind("click", this.invocation("_handleClose")).
                    addClass("closer");
            return builder.get(0);
        },
       /**
        * <p>Restore the DOM for this Tab </p>
        *
        * @param {Document} doc The document to use for creating content.
        * @returns {jQuery} The element containing the tab.
        */ 
        restoreDOM: function(doc) {
            var res = jabberwerx.$(this._super(doc)); //serialized html
            //remap html events
            res.find(".tab").bind("click", this.invocation("_handleTabClick"));
            res.find("a").bind("click", this.invocation("_handleTabClick"));
            res.find("img").bind("click", this.invocation("_handleClose"));
            if (this._wrapper && this._wrapper._isActive) {
                res.addClass("active");
            }
            return res;
        },

        /**
         * <p>Destroys this view's DOM. This method does the following:</p>
         * <ol>
         * <li>Cancels any pending attention request</li>
         * <li>If this tab is active, an adjacent tab is activated
         * (if any)</li>
         * <li>If the content is still rendered and implements
         * {@link jabberwerx.ui.Tabbable#setTabControl}, the method is called
         * passing <tt>null</tt> (to indicate the content is being removed
         * from a TabbedView)</li>
         * <li>If the content is still rendered, it's {@link jabberwerx.ui.JWView#remove} is
         * called</li>
         * <li>Removes this tab from its owning TabbedView</li>
         * </ol>
         */
        destroyDOM: function() {
            this.cancelAttention();
            this._activateAdjacentTab();

            if (this.content.parent() === this._wrapper) {
                // The content is still attached to this tab, remove it!
                if (jabberwerx.$.isFunction(this.content.setTabControl)) {
                    this.content.setTabControl(null);
                }
                this._wrapper.remove();
                this.content = null;
            }
            this.tabview._removeFromTabList(this);

            this._super();
        },

        /**
         * @private
         */
        _activateAdjacentTab: function() {
            if (this.isActive()) {
                var tab = this.jq.next(".tab:not(:hidden)");
                if (!tab.length) {
                    tab = this.jq.prev(".tab:not(:hidden)");
                }

                if (tab.length) {
                    //activate the next available tab in the list...
                    tab.get(0).jw_view.activate();
                } else {
                    //just mark us as deactivated (trigger events, etc)
                    this._deactivate();
                }
            }
        },




        /**
         * @private
         * <p>Attention information structure. This propery is only set
         * if there is a pending attention request. The structure is the
         * following:</p>
         * <pre class='code'>
         *  {
         *      id: Object,     //window.timeout result
         *      data: Object    //additional notification data
         *  }
         * </pre>
         */
        /**
         * @private
         * <p>The wrapper around the actual content. <b>NOTE:</b> This value
         * should not be changed.</p>
         *
         * @type    jabberwerx.ui.TabbedView.ContentWrapper
         */
        _wrapper: null,
        /**
         * <p>The content this tab refers to.</p>
         *
         * @type    jabberwerx.ui.JWView
         */
        content: null,
        /**
         * <p>The identifier of this tab</p>
         *
         * @type    String
         */
        id: null,
        /**
         * <p>The owning TabbedView.</p>
         *
         * @type    jabberwerx.ui.TabbedView
         */
        tabview: null
    }, "jabberwerx.ui.TabbedView.Tab");

    /**
     * @class
     * <p>A mixin for better integration with {@link jabberwerx.ui.TabbedView}.
     * Mixing Tabbable into a jabberwerx.ui.JWView instance allows the TabbedView to get and
     * set the Tab control from/to that view.</p>
     *
     * @see JWBase.mixin
     */
    jabberwerx.ui.Tabbable = {
        /**
         * <p>Retrieves the tab.</p>
         *
         * @returns {jabberwerx.ui.TabbedView.Tab} The current tab, or
         *          <tt>null</tt> if not set
         * @lends   jabberwerx.ui.Tabbable#
         */
        getTabControl: function() {
            return this._tabCtrl || null;
        },
        /**
         * <p>Changes or removes the tab. This method is called by
         * {@link jabberwerx.ui.TabbedView} when a new tab is created for
         * this view or the view is being removed from the TabbedView.</p>
         *
         * <p>Mixed views may override this method to provide additonal
         * functionality (such as setting the default label), but should
         * call this._super().</p>
         *
         * @param   {jabberwerx.ui.TabbedView.Tab} The new tab, or
         *          <tt>null</tt> remove the tab.
         * @lends   jabberwerx.ui.Tabbable#
         */
        setTabControl: function(tab) {
            if (tab && !(tab instanceof jabberwerx.ui.TabbedView.Tab)) {
                throw new TypeError("tab is not a TabbedView.Tab");
            }
            this._tabCtrl = tab;
        }
    };
}
