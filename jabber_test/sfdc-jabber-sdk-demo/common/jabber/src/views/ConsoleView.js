/**
 * filename:        ConsoleView.js
 * created at:      2010/03/16T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */


;(function(){
    if (jabberwerx && jabberwerx.ui) {

        jabberwerx.ui.ConsoleView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.ConsoleView.prototype */{
            /**
             * @class
             * <p>A debug console view.</p>
             *
             * @description
             * <p>Creates a new ConsoleView.</p>
             *
             * @param {jabberwerx.Client} client
             *
             * @throws {TypeError} if {client} is not valid
             *
             * @constructs jabberwerx.ui.ConsoleView
             * @extends jabberwerx.ui.JWView
             */
            init: function(client) {
                this._super();

                if (!client || !(client instanceof jabberwerx.Client)) {
                    throw new TypeError("Client must be a jabberwerx.client");
                }

                this._client = client;

                //attach stream listeners
                this._client._stream.event("streamOpened").bind(this.invocation("_streamOpenedHandler"));
                this._client._stream.event("streamClosed").bind(this.invocation("_streamClosedHandler"));
                this._client._stream.event("streamElementsReceived").bind(this.invocation("_streamReceivedHandler"));
                this._client._stream.event("streamElementsSent").bind(this.invocation("_streamSentHandler"));

                this._opts = {};//placeholder for future construction configuration options.

                //mixin jabberwerx.util.debug logging methods
                this._mixinLogMethods();

                //cached refs to speedup access to interesting nodes
                this._messageTemplate = null;
                this._templates = [];
                this._filters = [];
            },
            /**
             * <p>Destroys this ConsoleView. This unbinds all of the bound events.</p>
             */
            destroy: function() {
                this._client._stream.event("streamOpened").unbind(this.invocation("_streamOpenedHandler"));
                this._client._stream.event("streamClosed").unbind(this.invocation("_streamClosedHandler"));
                this._client._stream.event("streamElementsReceived").unbind(this.invocation("_streamReceivedHandler"));
                this._client._stream.event("streamElementsSent").unbind(this.invocation("_streamSentHandler"));
                jabberwerx.util.debug.consoleDelegate = null;

                this._super();
            },

            /**
             * <p>Overrides jabberwerx.ui.JWView's height. Fits the console within the given height.</p>
             *
             * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
             *          retrieve the current height)
             * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
             *          <b>NOT</b> be updated
             * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
             *          jabberwerx.ui.ConsoleView (if changing)
             */
            height: function(h, noupdate) {
                var result = this._super(h, true);
                if ((h !== undefined) && this.jq) {
                    var entryPaneHeight = (jabberwerx.$("#console-xml-entry").length ? jabberwerx.$("#console-xml-entry").outerHeight(true) : 0);
                    var filterBarHeight = (jabberwerx.$("#filter-bar").length ? jabberwerx.$("#filter-bar").outerHeight(true) : 0);
                    jabberwerx.ui.ConsoleView._forceFit(jabberwerx.$("#console-log-div"), {height: this.jq.height() - entryPaneHeight - filterBarHeight});
                    if (!noupdate) {
                        this.update();
                    }
                }
                return result;
            },
            /**
             * <p>Overrides jabberwerx.ui.JWView's width. Fits the console within the given width,
             * including border and margins.</p>
             *
             * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
             *          retrieve the current width)
             * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
             *          <b>NOT</b> be updated
             * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
             *          jabberwerx.ui.ConsoleView (if changing)
             */
            width: function(w, noupdate) {
                var result = this._super(w, true);
                if ((w !== undefined) && this.jq) {
                    var wo = {width: this.jq.width()};
                    jabberwerx.ui.ConsoleView._forceFit(jabberwerx.$("#filter-bar", this.jq), wo);
                    jabberwerx.ui.ConsoleView._forceFit(jabberwerx.$("#console-log-div", this.jq), wo);
                    jabberwerx.ui.ConsoleView._forceFit(jabberwerx.$("#console-xml-entry", this.jq), wo);

                    if (!noupdate) {
                        this.update();
                    }
                }
                return result;
            },

            /**
             * <p>Triggers an update of this ConsoleView. View completely
             * fills its container.</p>
             * <p>Restores persisted log messages. This method is invoked once the console's DOM has
             * been appended to its parent, ensuring propper css is in place (witdth for example).</p>
             */
            update: function() {
                if (!this._super()) {
                    return false;
                }
                //restore persisted xml and log messages if they exist.

                if (this._persistedXMLEntry && this._persistedXMLEntry.length) {
                    jabberwerx.$("#console-xml-entry", this.jq).val(this._persistedXMLEntry);
                }
                delete this._persistedXMLEntry;
                if (this._persistedLog) {
                    for (var oneLog in this._persistedLog) {
                        if (this._persistedLog.hasOwnProperty(oneLog)) {
                            //use jquery to appened persisted html, don't unserialize and append dom (ie html dom)
                            jabberwerx.$("#console-log-table", this.jq).append(this._persistedLog[oneLog]);
                        }
                    }
                }
                delete this._persistedLog;

                return true;
            },

            /**
             * <p>Override to persist anything in xml entry. Super class will persist the console.</p>
             *
             * @returns {Boolean} Always <tt>true</tt>
             */
            willBeSerialized: function() {
                this._persistedXMLEntry = this.text();
                //serialize last persistedLogCount messages
                this._persistedLog = {};
                var pcount = jabberwerx._config.persistedLogCount !== undefined ? jabberwerx._config.persistedLogCount : 5;

                var log = jabberwerx.$("#console-log-table", this.jq).find("tr.log_message");
                for (var i=Math.max(log.length - pcount, 0); i < log.length; ++i) {
                    //trying to serialize HTML in IE, use outerHTML (will fail serializeXML)
                    var tstr = jabberwerx.util.serializeXML(log[i]) || log[i].outerHTML || "";
                    this._persistedLog[i] = tstr;
                }
                this._super();

                this._messageTemplate = null;
                this.templates = null;

                return true;
            },

            /**
             * <p>Clears the console log pane.</p>
             */
            clear: function() {
                if (this.jq) {
                    jabberwerx.$(".log_message", jabberwerx.$("#console-log-table")).remove();
                }
            },

            /**
             * @private
             * Get the current time as hh:mm:ss:mmm
             */
            _nowTimeString: function() {
                var pad0 = function(ti, nd) {
                    var retstr = ti.toString();
                    while(retstr.length < nd) {
                        retstr = "0" + retstr;
                    }
                    return retstr;
                }

                var d = new Date();
                var result = pad0(d.getHours(), 2) + ":";
                result += pad0(d.getMinutes(), 2) + ":";
                result += pad0(d.getSeconds(), 2) + ":";
                result += pad0(d.getMilliseconds(), 3);

                return result;
            },

            /**
             * @private
             */
            _xmpp2html: function(node, parent) {
                var repeatChar = function(rchar, num) {
                    var result = [];
                    for (var i = 0; i < num; ++i) {
                        result.push(rchar)
                    }
                    return result.join("");
                };
                var escapeHTML = function(val) {
                    switch (val) {
                        case '<':   return "&lt;";
                        case '>':   return "&gt;";
                        case '&':   return "&amp;";
                    }

                    return val;
                };

                switch (node.get(0).nodeType) {
                    case 1: { //element
                        //nDiv is our open tag line
                        var htmlDOM = (node.contents().length ?
                                       this._templates["element"].clone() :
                                       this._templates["simple-element"].clone());
                        parent.append(htmlDOM);
                        htmlDOM.find("#element-name").text(node.get(0).nodeName);

                        //populate attributes, alpha sort with xml attribs on top
                        var attrs = [];
                        var spaces = [];
                        for (var i = 0;i < node.get(0).attributes.length; ++i) {
                            var aName = node.get(0).attributes[i].nodeName;
                            if (aName.indexOf("xml") === 0) {
                                spaces.push(aName);
                            } else {
                                attrs.push(aName)
                            }
                        }
                        attrs.sort();
                        spaces.sort();
                        attrs = spaces.concat(attrs);
                        if (attrs.length) {
                            var attrInsert = htmlDOM.find("#attributes");
                            var oneAttr = this._templates["attribute"].clone();
                            oneAttr.addClass("xml_first_attrib");
                            oneAttr.find("#spacer:first").text("\u00A0");
                            oneAttr.find("#attrib-val:first").text(node.attr(attrs[0]));
                            oneAttr.find("#attrib-name:first").text(attrs[0]);
                            attrInsert.append(oneAttr);
                            var offset = repeatChar("\u00A0", (node.get(0).nodeName.length + 1)*2 + 1);
                            for (var i = 1; i < attrs.length; ++i) {
                                attrInsert.append(jabberwerx.$("<br/>"));
                                oneAttr = this._templates["attribute"].clone();
                                oneAttr.find("#spacer:first").text(offset);
                                oneAttr.find("#attrib-name:first").text(attrs[i]);
                                oneAttr.find("#attrib-val:first").text(node.attr(attrs[i]));
                                attrInsert.append(oneAttr);
                            }
                        }

                        //recurse children
                        var childContainer = htmlDOM.find("#child-container");
                        var that = this;
                        node.contents().each(function(idx, element) {
                            that._xmpp2html(jabberwerx.$(element), childContainer);
                        });
                        break;
                    };
                    case 3: { //text
                        jabberwerx.$("<div/>").
                            addClass("xml_text").
                            text(node.text().replace(/[<>&]/g, escapeHTML)).
                            appendTo(parent);
                        break;
                    };
                }
            },

            /**
             *@private
             */
            _setupFilterCriteria: function(message, mType, msgContainer) {
                //copy filter selector criteria from message to logMsg attributes
                //to or from full jids, node names and message type
                msgContainer.attr("mtype", mType);
                if ((mType == "sent_xml") || (mType == "received_xml")) {
                    message = jabberwerx.$(message);
                    msgContainer.attr("node-name", message.get(0).nodeName);
                    //to may not exist on all sent packets
                    if ((mType == "sent_xml") && message.attr("to")) {
                        msgContainer.attr("jid", message.attr("to"))
                    } else if (mType == "received_xml") {
                        msgContainer.attr("jid", message.attr("from"));
                    }
                }
            },
            /**
             *@private
             */
            _makeFilterLinks: function(mType, htmlDOM) {
                //link first nodename encountered <span id="element-name" class=element_name>name</span>
                var linkDOM = htmlDOM.find("#element-name:first");
                linkDOM.addClass("filter_element");
                var filter = {filterBy: 'node-name', criteria: linkDOM.text()};
                var that = this;
                linkDOM.click(function() {
                    that._filter(filter);
                });
                //link to value if sending message, from value if received
                var attrName = (mType == "sent_xml") ? "to" : "from";
                linkDOM = htmlDOM.find("#attributes:first").find("#attrib-template").map(function(idx, ele) {
                    ele = jabberwerx.$(ele);
                    if (ele.find("#attrib-name:first").text() == attrName) {
                        return ele.find("#attrib-val:first");
                    } else {
                        return null;
                    }
                });
                //sent may not contain a jid, need to check if we found anything
                if (linkDOM.length) {
                    linkDOM = linkDOM.get(0);
                    linkDOM.addClass("filter_jid");
                    var jidFilter = {filterBy: 'jid', criteria: linkDOM.text()};
                    linkDOM.click(function() {
                        that._filter(jidFilter);
                    });
                }
            },
            /**
             * @private
             */
            _filterRow: function(row) {
                var hideRow = false;
                for (var i = 0; i < this._filters.length; ++i) {
                    hideRow = hideRow || (row.attr(this._filters[i].filterBy) != this._filters[i].criteria);
                }
                if (hideRow) {
                    row.hide();
                } else {
                    row.show();
                }
                return hideRow;
            },
            /**
             *@private
             */
            _filter: function(filter) {
                //push onto the filter list if not already there
                var inList = false;
                for (var i = 0; i < this._filters.length; ++i) {
                    inList = inList || ((this._filters[i].filterBy == filter.filterBy) && (this._filters[i].criteria == filter.criteria));
                }
                if (!inList) {
                    this._filters.push(filter);
                    //apply the filter against current visible rows
                    jabberwerx.$("tr[" + filter.filterBy + "!='" + filter.criteria + "']:visible", jabberwerx.$("#console-log-table")).hide();
                    this._updateBreadcrumbs();
                    this._scrollToBottom();
                }
                return false;
            },
            /**
             *@private
             */
            _clearFilters: function() {
                this._filters = [];
                jabberwerx.$("tr", jabberwerx.$("#console-log-table")).show();
                this._updateBreadcrumbs();
                this._scrollToBottom();
            },

            /**
             *@private
             */
            _updateBreadcrumbs: function() {
                fstr = [];
                for (var i = 0; i < this._filters.length; ++i) {
                    fstr.push(this._filters[i].criteria);
                }
                jabberwerx.$("#active-filters").text(fstr.join(" >> "));
            },

            /**
             * @private
             * mType reflects css, should be one of sent_xml, received_xml, jwa_log or jwa_console (default)
             */
            _addConsoleMessage: function(message, mType) {
                var dType = "Unknown";
                if (mType == "sent_xml") {
                    dType = "Sent";
                } else if (mType == "received_xml") {
                    dType = "Recv";
                } else if (mType == "jwa_log") {
                    dType = "Log";
                } else {
                    mType = "jwa_log"; //force to log css if unknown mType
                }

                var logMsg = this._messageTemplate.clone();
                this._setupFilterCriteria(message, mType, logMsg);

                var detailDOM = jabberwerx.$("<span/>").addClass("detail-top");
                if (mType == "jwa_log") {
                    detailDOM.text(message);
                } else {
                    this._xmpp2html(jabberwerx.$(message), detailDOM);
                    this._makeFilterLinks(mType, detailDOM);
                }
                detailDOM.appendTo(logMsg.children(".details")[0]);

                logMsg.children(".type").text(dType);
                logMsg.children(".timestamp").text(this._nowTimeString());
                logMsg.children(".details").addClass(mType);

                //scroll only if cursor is at bottom of console, *very* annoying otherwise
                var shouldScroll = jabberwerx.$("#console-log-div").get(0);
                shouldScroll = shouldScroll.scrollTop >=
                    (shouldScroll.scrollHeight - shouldScroll.clientHeight);
                logMsg.appendTo(jabberwerx.$("#console-log-table"));
                var showing = this._filterRow(logMsg);
                if (!showing) {
                    this._scrollToBottom();
                }
            },

            /**
             * @private
             * <p>scroll to bottom of console log div</p>
             */
            _scrollToBottom: function() {
                var ld = jabberwerx.$("#console-log-div");
                if (ld.length) {
                    ld.get(0).scrollTop = ld.get(0).scrollHeight;
                }
            },

            /**
             * <p>Creates the DOM for ConsoleView consisting of a log area,
             * a widget area and a TextInput widget.</p>
             *
             * @param {Document} doc The document to use for creating content.
             * @returns {jQuery} The element containing the widgets.
             */
            createDOM: function(doc) {
                var consoleView = jabberwerx.$("<div/>", doc).addClass("jabberwerx console_view").attr("id","jwa-console-view");
                consoleView[0].consoleView = this; //ref back to this view for filterlinks onclick

                jabberwerx.$("<div/>").
                    appendTo(consoleView).
                    addClass("console_toolbar").
                    attr("id","filter-bar").
                    append(jabberwerx.$("<input type='button' value='" + jabberwerx._("Clear Filter") + "'/>").
                            addClass("console_toolbar_btn").
                            attr("id", "clear-filter-btn").
                            bind("click", this.invocation("_clearFilters"))).
                    append(jabberwerx.$("<input type='button' value='" + jabberwerx._("Clear Console") + "'/>").
                            addClass("console_toolbar_btn").
                            attr("id", "clear-console-btn").
                            bind("click", this.invocation("clear"))).
                    append(jabberwerx.$("<span/>").
                            addClass("active_filters").
                            attr("id", "active-filters"));


                jabberwerx.$("<div/>").
                    appendTo(consoleView).
                    addClass("log_div").
                    attr("id","console-log-div");
                jabberwerx.$("<table/>").
                    appendTo(jabberwerx.$("#console-log-div", consoleView)).
                    append(jabberwerx.$("<col/>").attr("width", "25px")).
                    append(jabberwerx.$("<col/>").attr("width", "50px")).
                    append(jabberwerx.$("<col/>").attr("width", "100%")).
                    append(jabberwerx.$("<tbody/>").attr("id", "console-log-table"));

                //don't create xml entry if configured that way
                if (!this._opts.noXMLEntry) {
                    jabberwerx.$("<textarea/>").
                        appendTo(consoleView).
                        attr("id", "console-xml-entry").
                        bind("keypress", this.invocation("_handleKeyPress"));
                }

                //console message template, cloned in addConsoleMessage, persisted as part of DOM
                var template = jabberwerx.$("<table/>").addClass("template").attr("id","console-message-template");
                template.appendTo(consoleView);
                this._messageTemplate = jabberwerx.$("<tr/>").addClass("log_message");
                this._messageTemplate.appendTo(template);
                jabberwerx.$("<td/>").addClass("type").attr("valign", "top").appendTo(this._messageTemplate);
                jabberwerx.$("<td/>").addClass("timestamp").attr("valign", "top").appendTo(this._messageTemplate);
                jabberwerx.$("<td/>").addClass("details").attr("valign", "top").appendTo(this._messageTemplate);
                //xml2html templates
                template.append(jabberwerx.$("<tr/>").
                    append(jabberwerx.$("<span/>").addClass("xml_attrib").attr("id", "attrib-template").
                            append(jabberwerx.$("<span/>").attr("id","spacer")).
                            append(jabberwerx.$("<span/>").addClass("xml_attrib_name").attr("id","attrib-name")).
                            append(jabberwerx.$("<span/>").addClass("xml_char").text("='")).
                            append(jabberwerx.$("<span/>").addClass("xml_attrib_val").attr("id","attrib-val")).
                            append(jabberwerx.$("<span/>").addClass("xml_char").text("'"))));
                template.append(jabberwerx.$("<tr/>").
                    append(jabberwerx.$("<div/>").addClass("xml_element").attr("id", "simple-element-template").
                            append(jabberwerx.$("<span/>").addClass("xml_char").text("<")).
                            append(jabberwerx.$("<span/>").addClass("xml_element_name").attr("id","element-name")).
                            append(jabberwerx.$("<span/>").addClass("xml_attributes").attr("id", "attributes")).
                            append(jabberwerx.$("<span/>").addClass("xml_char").text("/>"))));
                template.append(jabberwerx.$("<tr/>").
                    append(jabberwerx.$("<span/>").attr("id", "element-template").
                            append(jabberwerx.$("<div/>").addClass("xml_element").attr("id", "element-open").
                                append(jabberwerx.$("<span/>").addClass("xml_char").text("<")).
                                append(jabberwerx.$("<span/>").addClass("xml_element_name").attr("id", "element-name")).
                                append(jabberwerx.$("<span/>").addClass("xml_attributes").attr("id", "attributes")).
                                append(jabberwerx.$("<span/>").addClass("xml_char").text(">")).
                                append(jabberwerx.$("<span/>").addClass("xml_child_container").attr("id", "child-container"))).
                            append(jabberwerx.$("<div/>").addClass("xml_element").attr("id", "element-close").
                                append(jabberwerx.$("<span/>").addClass("xml_char").text("</")).
                                append(jabberwerx.$("<span/>").addClass("xml_element_name").attr("id", "element-name")).
                                append(jabberwerx.$("<span/>").addClass("xml_char").text(">")))));
                this._templates = [];
                this._templates["attribute"] = jabberwerx.$("#attrib-template", template);
                this._templates["simple-element"] = jabberwerx.$("#simple-element-template", template);
                this._templates["element"] = jabberwerx.$("#element-template", template);

                //logging state, mixin log methods do not persist
                this._mixinLogMethods();
                return consoleView;
            },
            /**
             * <p>Gets or sets the text in the text xml entry field.</p>
             *
             * <p>If {val} is not defined, this method returns the current
             * text in the xml entry field. Otherwise the text is changed
             * to the value of {val}.
             *
             * @param {String} [val] The new text to be put in the text input
             * field.
             * @returns {String|jabberwerx.ui.ConsoleView} The current text in the text input field
             *          or this jabberwerx.ui.ConsoleView if the text is being set.
             */
            text: function(val) {
                if (val === undefined) {
                    return (this.jq && this.jq.find("#console-xml-entry").val()) || "";
                }
                if (this.jq) {
                    this.jq.find("#console-xml-entry").val(val);
                }

                return this;
            },

            /**
             * @private
             */
            _handleKeyPress: function(evt) {
                if (evt.keyCode == 13) {
                    //parse to insure this is valid xml
                    var tstr = jabberwerx.$.trim(this.text());
                    if (tstr == "") {
                        return false;//bail on empty string
                    }
                    try {
                        //insure jabber:client ns if no ns is defined
                        var sendxml = jabberwerx.$(jabberwerx.util.unserializeXML("<nswrapper xmlns='jabber:client'>" + tstr + "</nswrapper>"));
                        sendxml = jabberwerx.$(sendxml.children()[0]).clone()[0]; //element, not jq
                        //insure this is at least somewhat valid xmpp by checking nodename
                        if ((sendxml.nodeName != "iq") && (sendxml.nodeName != "message") && (sendxml.nodeName != "presence")) {
                            this._addConsoleMessage(jabberwerx._("Attempted to send an invalid XMPP stanza (must be iq, message or presence), make sure the entered value is a valid XMPP stanza and try again."), "jwa_log");
                            return false;
                        }
                        try {
                            sendxml = new jabberwerx.Stanza(sendxml);
                            this._client.sendStanza(sendxml);
                            this.text("");
                        } catch (ex) {
                            this._addConsoleMessage(jabberwerx._("An unhandled error occurred attempting to send the stanza: {0}.", ex.message), "jwa_log");
                        }
                    } catch (ex) {
                        this._addConsoleMessage(jabberwerx._("Attempted to send invalid XML, make sure the entered value is valid XML and try again."), "jwa_log");
                    }

                    return false;
                }
                return true;
            },

            /**
             * @private streamOpened handler. passed stream features, logs the features to console
             */
            _streamOpenedHandler: function(evt) {
                this._addConsoleMessage(jabberwerx._("[Console] Stream opened with {0} {1}", this._client.connectedServer.jid.toString(), jabberwerx.util.serializeXML(evt.data)), "jwa_log");
            },
            /**
             * @private streamClosed handler. passed an optional error element on forced disconnects.
             */
            _streamClosedHandler: function(evt) {
                var cmsg = jabberwerx._("[Console] Stream closed") + (evt && evt.data ? jabberwerx._("with error:") + jabberwerx.errorReporter.getMessage(evt.data) + " (" + jabberwerx.util.serializeXML(evt.data) + ')': ".");
                this._addConsoleMessage(cmsg, "jwa_log");
            },
            /**
             * @private streamElementsReceived handler. Passed an array of elements
             */
            _streamReceivedHandler: function(evt) {
                var elements = jabberwerx.$(evt.data);
                for (var i = 0; i < elements.length; ++i) {
                    this._addConsoleMessage(elements.get(i), "received_xml");
                }
            },
            /**
             * @private streamElementsSent handler. passed san array of elements
             */
            _streamSentHandler: function(evt) {
                var elements = jabberwerx.$(evt.data);
                for (var i = 0; i < elements.length; ++i) {
                    this._addConsoleMessage(elements.get(i), "sent_xml");
                }
            },

            /**
             * @private
             * Mixin jabberwerx.util.debug logging methods, make this view the jabberwerx.util.debug.consoleDelegate
             */
            _mixinLogMethods: function() {
                var that = this;
                jabberwerx.$.each(['log', 'dir', 'warn', 'error', 'info', 'debug'], function(i, e) {
                    that[e] = function(a) {
                        if (!jabberwerx.util.isString(a)) {
                            a = jabberwerx._("Unknown log message type ({0}) passed to ConsoleView. {1}, Console view only supports String messages.", (typeof  a), e);
                        }
                        that._addConsoleMessage(a, "jwa_log");
                    }
                });
                jabberwerx.util.debug.consoleDelegate = this;
            }
        }, 'jabberwerx.ui.ConsoleView');

        /**
        * @private static helper to fit a jQuery entirely (borders + margins) within height/width
        */
        jabberwerx.ui.ConsoleView._forceFit = function(ele, dim) {
            if (!ele || !ele.length) {
                return;
            }
            if (dim.width !== undefined) {
                ele.width(dim.width);
                ele.width(ele.width() - Math.max(ele.outerWidth(true) - ele.width(), 0));
            }
            if (dim.height !== undefined) {
                ele.height(dim.height);
                ele.height(ele.height() - Math.max(ele.outerHeight(true) - ele.height(), 0));
            }
        }
    }
})();
