/**
 * filename:        SubscriptionView.js
 * created at:      2009/07/23T13:30:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
 
;(function() {
    if ((jabberwerx) && (jabberwerx.ui)) {
        jabberwerx.ui.SubscriptionView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.SubscriptionView.prototype */{
        /**
         * @class
         * <p>View for accepting or denying subscription requests.</p>
         *
         * <p>This allows the user to accept, deny or ignore (defer until later) a subscription
         * request. An instance of this class should be created upon a subscription request been
         * recieved, provided auto accept is not enabled in the RosterController. The user can
         * specify the contacts nickname and the group to which they should be added. It also 
         * facilitates the creation of a new group on the fly.</p>
         *
         * <p>When the accept action is undertaken, a subscription to the contacts presence status
         * is automatically invoked. When the deny action is undertaken, the contact, if present, is
         * automatically removed from the roster.</p>
         * 
         * <p>An event (actionComplete) will be triggered when an action has been undertaken. The 
         * action will have been to either accept, deny or ignore the subscription request. More
         * details below.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.SubscriptionView">jabberwerx.ui.SubscriptionView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a SubscriptionView object.</p>
         * @param {jabberwerx.RosterController} roster The roster
         * @param {String|jabberwerx.JID} contact The jid of the user from which the subscription
         * request was received.
         * @param {Object} [defaults]
         * @param {String} [defaults.nickname] The default nickname to populate the input with
         * @param {String} [defaults.group] The default group to select. If this group does not
         * exist then an entry for it will be inserted in the group drop down box.
         * @throws {TypeError} If roster is not valid
         * @throws {jabberwerx.JID.InvalidJIDError} if contact isn't a valid jid.
         * @constructs jabberwerx.ui.SubscriptionView
         * @extends jabberwerx.ui.JWView
         */
        init: function(roster, contact, defaults) {
            this._super();

            if (!(roster && roster instanceof jabberwerx.RosterController)) {
                throw new TypeError("roster must be a jabberwerx.RosterController");
            }

            this.roster = roster;
            this._contact = jabberwerx.JID.asJID(contact).getBareJID();            
            this._defaultNick = ( (defaults && typeof defaults.nickname == "string")
                            ? defaults.nickname : jabberwerx.JID.unescapeNode(this._contact.getNode()));
            this._defaultGroup = defaults && typeof defaults.group == "string" && defaults.group;
            
            this.applyEvent("actionComplete");
        },

        /**
         * <p>Creates the DOM to display.
         *
         * <p>Binds the input elements to their respective action methods.</p>
         * <p>Creates the DOM to display. This method creates the following
         * DOM:</p>
         * <pre class="code">&lt;div class="jabberwerx subscription_view"&gt;
         *      &lt;div class="contact_div"&gt;
         *          &lt;label for="contact"&gt;Contact:&lt;/label&gt;
         *          &lt;span class="contact" id="contact"&gt;<i>defaults.contact</i>&lt;/span&gt;
         *      &lt;/div&gt;
         *      &lt;div class="nick_div"&gt;
         *          &lt;label for="nick"&gt;Nickname:&lt;/label&gt;
         *          &lt;input type="text" class="nick" id="nick"&gt;<i>defaults.nickname</i>&lt;/input&gt;
         *      &lt;/div&gt;
         *      &lt;div class="group_div"&gt;
         *          &lt;label for="group"&gt;Group:&lt;/label&gt;
         *          &lt;select class="group_select" id="group"&gt;
         *              <i>Group option tags</i>
		 *              &lt;option class="group_option" value="New..."&gt;New...&lt;/option&gt;
         *          &lt;/select&gt;
         *      &lt;/div&gt;
         *      &lt;div class="action_div"&gt;
         *          &lt;input class="action_button accept" type="button" val="Accept"/&gt;
         *          &lt;input class="action_button deny" type="button" val="Deny"/&gt;
         *          &lt;input class="action_button ignore" type="button" val="Ignore"/&gt;
         *      &lt;/div&gt;
         * &lt;/div&gt;</pre>
         *
         * <p>It also binds a callback for "change" events on the
         * &lt;select/&gt; element.</p>
         * @param {Document} doc The document to use for creating content.
         * @returns {Element} The generated element to display
         */
        createDOM: function(doc) {
            // Create the overall container div
            var builder = jabberwerx.$("<div/>", doc)
                .addClass("jabberwerx subscription_view");
            
            // Create the contact label and span
            var contactDiv = jabberwerx.$("<div/>").
                addClass("contact_div").
                appendTo(builder);
            jabberwerx.$("<label/>").
                attr("for", "contact").
                text(jabberwerx._("Contact:")).
                appendTo(contactDiv);
            jabberwerx.$("<span/>").
                addClass("contact").
                attr("id", "contact").
                attr("title", this._contact.toDisplayString()).
                text(this._contact.toDisplayString()).
                appendTo(contactDiv);
            
            // Create the nickname label and text input
            var nickDiv = jabberwerx.$("<div/>").
                addClass("nick_div").
                appendTo(builder);
            jabberwerx.$("<label/>").
                attr("for", "nick").
                text(jabberwerx._("Nickname:")).
                appendTo(nickDiv);
            var nickInput = jabberwerx.$('<input type="text"/>').
                addClass("nick").
                attr("id", "nick").
                appendTo(nickDiv);
            if (this._defaultNick) {
                nickInput.val(this._defaultNick);
            }
            
            // Create the group label and selector
            var groupDiv = jabberwerx.$("<div/>").
                addClass("group_div").
                appendTo(builder);
            jabberwerx.$("<label/>").
                attr("for", "group").
                text(jabberwerx._("Group:")).
                appendTo(groupDiv);
            // Text input field for entering new group. Hidden by default.
            jabberwerx.$('<input type="text"/>').
                addClass("group_input").
                attr("id", "new_group").
                blur(this.invocation("_groupEntered")).
                keyup(this.invocation("_groupKeyup")).
                appendTo(groupDiv).
                hide();
            // Group select for selecting group. Displayed by default.
            var groupSelect = jabberwerx.$("<select/>").
                addClass("group_select").
                attr("id", "group").
                change(this.invocation("_groupSelected")).
                appendTo(groupDiv);
            
            // Get all groups for the entitySet. Then add the default group if it exists and is not
            // already present in the group set retrieved.
            var groups = this.roster.client.entitySet.getAllGroups();
            if (this._defaultGroup && jabberwerx.$.inArray(this._defaultGroup, groups) == -1) {
                groups.push(this._defaultGroup);
            }
            
            var that = this;
            jabberwerx.$.each(groups, function(key, val) {
                that._createGroupOption(val).
                    appendTo(groupSelect);
            });
            
            // Append the New... selection
            this._createGroupOption(jabberwerx._("New...")).
                appendTo(groupSelect);
            
            this._sortGroupSelection(groupSelect);
            
            // Auto select the default group if present, otherwise leave it to it's default choice
            if (this._defaultGroup) {
                groupSelect.val(this._defaultGroup);
            } else {
                this._defaultGroup = (groupSelect.val() != jabberwerx._("New...") ? groupSelect.val() : null);
            }
            
            // Create the action buttons
            var actionDiv = jabberwerx.$("<div/>").
                addClass("action_div").
                appendTo(builder);
                jabberwerx.$("<input/>").
                addClass("action_button").
                addClass("accept").                
                attr("type", "button").
                val(jabberwerx._("Accept")).
                click(this.invocation("acceptSubscription")).
                appendTo(actionDiv);
            jabberwerx.$("<input/>").
                addClass("action_button").
                addClass("deny").
                attr("type", "button").
                val(jabberwerx._("Deny")).
                click(this.invocation("denySubscription")).
                appendTo(actionDiv);
            jabberwerx.$("<input/>").
                addClass("action_button").
                addClass("ignore").
                attr("type", "button").
                val(jabberwerx._("Ignore")).
                click(this.invocation("ignoreSubscription")).
                appendTo(actionDiv);
            
            // Return the root div.subscription_view
            return builder.get(0);
        },

        /**
         * <p>Shows the form if hidden in the document.</p>
         */
        show: function() {
            if (this.jq) {
                this.jq.show();
            }
        },

        /**
         * <p>Hides the form in the document.</p>
         */
        hide: function() {
            if (this.jq) {
                this.jq.hide();
            }
        },
        
        /**
         * <p>Accepts the subscription. Will also attempt to add the contact to our roster using the
         * currently selected group and nickname. This is the equivalent of clicking the accept
         * button in the UI.</p>
         */
        acceptSubscription: function() {
            this.roster.acceptSubscription(this._contact, this.getNickname(), this.getGroup());
            this.event("actionComplete").trigger(
                {action: jabberwerx.ui.SubscriptionView.ACTION_ACCEPT});
        },
        
        /**
         * <p>Denies the subscription. This is the equivalent of clicking the deny button in the
         * UI.</p>
         */
        denySubscription: function() {
            this.roster.denySubscription(this._contact);
            this.event("actionComplete").trigger(
                {action: jabberwerx.ui.SubscriptionView.ACTION_DENY});
        },
        
        /**
         * <p>Ignores the subscription. No response will be returned to the subscribe request and
         * the server will notify the client of the same subscription request upon their next log
         * in. This is the equivalent of clicking the ignore button in the UI.</p>
         */
        ignoreSubscription: function() {    
            this.event("actionComplete").trigger(
                {action: jabberwerx.ui.SubscriptionView.ACTION_IGNORE});
        },
        
        /**
         * <p>Get the currently set nickname</p>
         * @returns {String} the nickname
         */
        getNickname: function() {
            return (this.jq && this.jq.find("#nick").val()) || this._defaultNick || "";
        },
        
        /**
         * <p>Get the currently set group</p>
         * @returns {String} the group
         */
        getGroup: function() {
            return (this.jq && this.jq.find("#group").val()) || this._defaultGroup || "";
        },
        
        /**
         * @private
         * Triggered upon the group selection changing
         */
        _groupSelected: function(evt) {
            var groupSelect = jabberwerx.$(evt.target);
            if (groupSelect.val() == jabberwerx._("new")) {
                // Hide the group selection element
                groupSelect.hide();
                // Show and set focus to the new group input text field
                this.jq.find("#new_group")
                    .val("")
                    .show()[0]
                    .focus();                
            }
        },
        
        /**
         * @private
         * Triggered upon a blur or keyup (where the return key is pressed) event in the group text
         * input
         */
        _groupEntered: function(evt) {
            var groupInput = jabberwerx.$(evt.target);
            var groupSelectElement = this.jq.find("#group");
            if (groupInput.val() && groupInput.val() != jabberwerx._("new")) {
                // Insert the new group name if not already present
                if ( !(jabberwerx.$("[value='" + groupInput.val() + "']", groupSelectElement).length) ) {
                    groupSelectElement.
                        append(this._createGroupOption(groupInput.val()));
                    this._sortGroupSelection(groupSelectElement);
                }
                // Hide new group text input and show group select element
                groupInput.hide();
                groupSelectElement.
                    val(groupInput.val()).
                    show();
            } else {
                // Don't allow empty strings or "New..." for group names - clear input
                groupInput.val("");
            }
        },
        
        /**
         * @private
         * Triggered upon a keyup event in the group text input
         */
        _groupKeyup: function(evt) {
            // Only process the input if the user presses enter
            if ( (evt.which && evt.which == 13) ||
                  evt.keyCode == 13) {
                this._groupEntered(evt);
            }
        },
        
        /**
         * @private
         * Sorts a group selectors option elements alphabetically. Doesn't actually move the option 
         * elements around but instead overwrites their value and text properties as needed. Assumes
         * value and text properties are the same string for each option.
         */
        _sortGroupSelection: function(selection) {
            var arr = [];
            selection.contents().each(function() {
                arr.push(this.value);
            });
            // Remove "New..." option
            var newPos = jabberwerx.$.inArray("New...", arr);
            if (newPos != -1) {
                arr.splice(newPos, 1);
            }
            // Sorts using case by default; we ignore case
            arr.sort(function(a,b) {
                a = a.toLowerCase();
                b = b.toLowerCase();
                if (a>b) return 1;
                if (a<b) return -1;
                return 0;
            });
            selection.contents().each(function() {
                // Reattches "New..." option when arr is empty
                var val = arr.shift() || jabberwerx._("new");
                this.value = val;
                this.text = val;
            });
        },
        
        /**
         * @private
         * Creates a new group option in jQuery format.
         * @param {String} groupName Used to populate the value and text properties
         * @return {jQuery} The option element
         */
        _createGroupOption: function(groupName) {
            return jabberwerx.$("<option/>").
                addClass("group_option").
                val(groupName).
                text(groupName);
        },
        
        /**
         * <p>The roster object used to accept or deny subscriptions.</p>
         * @type jabberwerx.RosterController
         */
        roster: null,

        /**
         * @private
         * <p>The jid of the subscribing user.</p>
         * @type jabberwerx.JID
         */
        _contact: null,
        
        /**
         * @private
         * <p>The default nickname of the subscribing user. Revert back to this value when the
         * nickname entered evaluates to false or this.jq is not present.</p>
         * @type String
         */
        _defaultNick: null,
        
        /**
         * @private
         * <p>The group to place the subscribing user into. Revert back to this value when the
         * group selected evaluates to false or this.jq is not present.</p>
         * @type String
         */
        _defaultGroup: null
        }, 'jabberwerx.ui.SubscriptionView');
        
        /**
         * <p>Returned as part of the event object in the actionComplete event when accept is the
         * action which has been taken.</p>
         * @type String
         */
        jabberwerx.ui.SubscriptionView.ACTION_ACCEPT = "accept";
        /**
         * <p>Returned as part of the event object in the actionComplete event when deny is the
         * action which has been taken.</p>
         * @type String
         */
        jabberwerx.ui.SubscriptionView.ACTION_DENY = "deny";
        /**
         * <p>Returned as part of the event object in the actionComplete event when ignore is the
         * action which has been taken.</p>
         * @type String
         */
        jabberwerx.ui.SubscriptionView.ACTION_IGNORE = "ignore";
    }
})();
