/**
 * filename:        MUCSearchView.js
 * created at:      2010/02/04T15:15:00+01:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
 

if ((jabberwerx) && (jabberwerx.ui)) {
    jabberwerx.ui.MUCSearchView = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MUCSearchView.prototype */{

        /**
         * @class
         * <p>View for displaying MUC search.</p>
         *
         * <p>This view displays the XDataForm received from the server,
         * both the search criteria and the results.</p>
         *
         * @description
         * <p>Creates a MUCSearchView object.</p>
         *
         * @param  {jabberwerx.MUCController} controller The MUC controller used to search.
         * @param  {jabberwerx.JID|String} muc The MUC server to search against.
         * @throws {TypeError} if {controller} isn't a MUCController
         * @throws {jabberwerx.JID.InvalidJIDError} if {muc} is not an instance of
         *         jabberwerx.JID and cannot be converted to one.
         * @constructs jabberwerx.ui.MUCSearchView
         * @extends jabberwerx.ui.JWView
         */
        init: function(controller, muc) {
            this._super();

            if (!(controller && controller instanceof jabberwerx.MUCController)) {
                throw new TypeError("controller must be a jabberwerx.MUCController.");
            }

            this.controller = controller;
            this.muc = jabberwerx.JID.asJID(muc);

            this.applyEvent("actionComplete");
        },

        /**
         * <p>Destroys this view.</p>
         *
         */
        destroy: function() {
            this._super();
        },

        /**
         * <p>Creates the DOM for MUCSearchView.
         *  
         * @param   {Element} doc Element for embedding the view           
         * @returns {Element} The generated element to display
         */
        createDOM: function(doc) {
            var div = jabberwerx.$("<div/>", doc).
                       addClass("jabberwerx muc_search_view");

            jabberwerx.$("<div/>"). 
                addClass("muc_search_criteria").
                appendTo(div);

            jabberwerx.$("<div/>"). 
                addClass("muc_search_error").
                appendTo(div);

            var buttons =  jabberwerx.$("<div/>"). 
                addClass("muc_search_buttons").
                appendTo(div);  

            jabberwerx.$("<input type='button'/>").
                addClass("muc_search_button_search").
                attr("value", jabberwerx._("Search")).
                attr("disabled", "disabled").
                click(this.invocation("_search")).
                appendTo(buttons);                                 

            jabberwerx.$("<input type='button'/>").
                addClass("muc_search_button_join").
                attr("value", jabberwerx._("Join")).
                click(this.invocation("_join")).
                attr("disabled", "disabled").
                appendTo(buttons);                                 

            jabberwerx.$("<input type='button'/>").
                addClass("muc_search_button_cancel").
                attr("value", jabberwerx._("Cancel")).
                click(this.invocation("_cancel")).
                appendTo(buttons);  

            jabberwerx.$("<div/>"). 
                addClass("muc_search_results").
                appendTo(div);

            this.hide();

            this.controller.startSearch(this.muc,
                this.invocation('_handleSearchCriteria'));

            return div[0]; 
        }, 

        /**
         * Get's invoked when the search criteria XDataForm is received.
         * @private
         */
        _handleSearchCriteria: function(form, error) {
            if (form) {
                var formView = new jabberwerx.ui.XDataFormView(form);
                var searchCriteriaDiv = jabberwerx.$(".muc_search_criteria", this.jq);
                formView.render().appendTo(searchCriteriaDiv);
                formView.dimensions({width:this.jq.width(),
                                     height: searchCriteriaDiv.height()});

                this.jq.find(".muc_search_button_search").removeAttr("disabled");
            } else {
                jabberwerx.$(".muc_search_button_search", this.jq).attr("disabled","disabled");                    
                this._showError(error);
            }
            this.show();
        },

        /**
         * Get's invoked when iq result for submitting the form is received (applyConfig callback).
         * @private
         */
        _handleConfigSubmit: function (form, error) {
            if (form) {
                var formView = new jabberwerx.ui.XDataFormView(form);
                var that = this;
                formView.event("xdataItemSelected").bind(function(evt) {
                    that.jq.find(".muc_search_button_join").removeAttr("disabled");

                    var resultTable = that.jq.find(".muc_search_results table.result_table");
                    resultTable.find("tr.selected").removeClass("selected");

                    that._selectedItem = evt.data.selected;
                    resultTable.find("tr#"+evt.data.selected._guid).addClass("selected");
                });

                var searchResultsDiv = jabberwerx.$(".muc_search_results", this.jq);
                searchResultsDiv.empty();
                this.update();
                var dim = {
                    width: searchResultsDiv.width(),
                    height: searchResultsDiv.height()
                };
                formView.render().appendTo(searchResultsDiv);
                formView.dimensions(dim);
            } else {
                this._showError(error);
            }
        },

        /**
         * <p>Updates view based on server response. Adjusts heights for the view elements 
         * based on server response. If error is received, the height for the XDataForm 
         * element is reduced to accomodate error element. Also sets the dimensions for
         * XDataFormView . </p>
         *
         * @returns {Boolean} <tt>true</tt> if the view is updatable
         */                                
        update: function() {
            var retval = this._super();
            if (retval) {
                var criteria = jabberwerx.$(".muc_search_criteria", this.jq);
                criteria.height(this._criteriaViewSize);
                criteria.width(this.jq.width() - (this.jq.outerWidth(true) - this.jq.width()));

                var height = this.jq.get(0).clientHeight -
                             jabberwerx.$(".muc_search_criteria").outerHeight(true) -
                             jabberwerx.$(".muc_search_buttons").outerHeight(true) -
                             (this.jq.outerHeight(true) - this.jq.height());
                jabberwerx.util.debug.log("search results height: " + height);
                var results = jabberwerx.$(".muc_search_results", this.jq);
                results.height(height);
                results.width(this.jq.width() - (this.jq.outerWidth(true) - this.jq.width()));
            }
            return retval;
        },

        /**
         * <p>Returns the size of the criteria view (usually this is the height).</p>
         * 
         * @returns {Number} Size of the criteria view.
         */
        getCriteriaViewSize: function() {
            return _criteriaViewSize;
        },

        /**
         * <p>Sets the size of the criteria view (usually this is the height).</p>
         *
         * @param {Number} size The new size of the criteria view.
         */
        setCriteriaViewSize: function(size) {
            this._criteriaViewSize = size;
        },

        /**
         * <p>Shows the form if hidden in the document.</p>
         */
        show: function() {
            if (this.jq) {
                this.jq.show();
            }
        },

        /**
         * <p>Hides the form in the document.</p>
         */
        hide: function() {
            if (this.jq) {
                this.jq.hide();
            }
        },

        /**
         * @private
         */
        _search: function() {
            jabberwerx.$(".muc_search_error", this.jq).empty();
            var form_view = jabberwerx.$("div.muc_search_criteria .xdata_form_view",
                                         this.jq).get(0).jw_view;
            var form = null;
            if (form_view) {
                try {
                    form = form_view.submit();
                }
                catch(e) {
                    this._showError(e);
                    return; 
                }
            }
            //If form is null, apply config will send the form type "cancel" request.
            this.controller.submitSearch(this.muc,
                                         form,
                                         this.invocation('_handleConfigSubmit'));
        }, 

        /**
         * @private
         */
        _join: function() {
            var room = this.controller.room(this._selectedItem.getFieldByVar("jid").getValues()[0]);
            var action =
                {action: jabberwerx.ui.MUCSearchView.ACTION_SUBMITTED,
                 submitted: room};

            this.event("actionComplete").trigger(action);
        },

        /**
         * @private
         */
        _cancel: function() {
            this.destroy();
        },  

        /**
         * <p>Shows error text.</p>
         * @private
         */
        _showError: function(error) {
            var msg;
            if (error instanceof jabberwerx.Stanza.ErrorInfo) {
                msg = jabberwerx.errorReporter.getMessage(error.getNode());
                if (!msg) {
                    msg = error.getNode().xml;
                }
            } 
            else if (error instanceof TypeError) {
                msg = error.message;
            }
            else {
                msg = error;
            }           
            jabberwerx.$(".muc_search_error", this.jq).text(msg);
            //This will allow only adjust height for the error handling once and
            //will not shrink the height on any consequtive error display
            if (!this._error) {
                var height = jabberwerx.$(".muc_search_criteria", this.jq).height() -
                             jabberwerx.$(".muc_search_error", this.jq).height(); 
                jabberwerx.$(".muc_search_criteria", this.jq).height(height);
                if ((jabberwerx.$(".xdata_form_view", this.jq).get(0) &&
                     jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view)) {
                    jabberwerx.$(".xdata_form_view", this.jq).get(0).jw_view.height(height);                   
                } 
            }                  
            this._error = true;
        },

        /**
         * <p> The MUCController passed in to the constructor.</p>
         * @type jabberwerx.MUCController
         */
        controller: null,

        /**
         * <p>The muc component passed in the constructor.</p>
         * @type    {jabberwerx.JID}
         */
        muc: null,

        /**
         * @private
         */
        _criteriaViewSize: 150,

        /**
         * <p>Error flag.</p>
         * @type    {Boolean}
         * @private
         */
        _error: false,

        /**
         * @private
         * @type {jabberwerx.XDataFormItem}
         * The XDataFormItem reference to the selected MUC room search result
         */
        _selectedItem: null

    }, 'jabberwerx.ui.MUCSearchView');        

    /**
     * <p>Returned as part of the event object in the actionComplete event</p>
     * @type String
     */
    jabberwerx.ui.MUCSearchView.ACTION_SUBMITTED = "submitted";
}
