/**
 * filename:        jabbwerwerx.cisco.js
 * created at:      2009/10/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
;(function() {
    var jabberwerx = window.jabberwerx;

    //if jw is not loaded yet or ui extensions have already been added, bail
    if (!jabberwerx) {
        throw new Error("jabberwerx not defined. jabbwerwerx.cisco extension could not load");
        return;
    }

    if ('cisco' in jabberwerx) {
        throw new Error("jabbwerwerx.cisco extension is already defined. Most likely a programatic error");
        return;
    }
	/**
	 * @namespace
	 * @description
	 * <p>Cisco AJAX XMPP Library is an easy to use, AJAX-based XMPP client. This namespace
	 * contains quick contacts subscription and controls.<p>
	 *
	 * ## Configuration 
     * See {@link jabberwerx}
	 */
    jabberwerx.cisco = {
        /**
        * Cisco extensions version
        * @property {String} version
        */ 
    version: '8.5.0.22677',

        /**
         * Internal config settings.
         *
         * @private
         */
        _config: {},

        /**
         * if library needs to do something interesting on load
         *
         * @private
         */
        _init: function() {
            this._inited = true;
        }
    };
    
    /**
     * Adding table module to allowed XHTML
     */
    jabberwerx.$.extend(jabberwerx.xhtmlim.allowedTags,{
        caption:    ["style"],
        table:      ["style", "border", "cellpadding", "cellspacing", "frame", "summary", "width"],
        td:         ["style", "align", "char", "charoff", "valign", "abbr", "axis", "colspan", "headers", "rowspan", "scope"],
        th:         ["style", "abbr", "axis", "colspan", "headers", "rowspan", "scope"],
        tr:         ["style", "align", "char", "charoff", "valign"],
        col:        ["style", "align", "char", "charoff", "valign", "span", "width"],
        colgroup:   ["style", "align", "char", "charoff", "valign", "span", "width"],
        tbosy:      ["style", "align", "char", "charoff", "valign"],
        thead:      ["style", "align", "char", "charoff", "valign"],
        tfoot:      ["style", "align", "char", "charoff", "valign"]
    });
    
    window.jabberwerx.cisco = jabberwerx.cisco;
    jabberwerx.$(document).ready(function() {
        jabberwerx.cisco._init();
    });
})();
