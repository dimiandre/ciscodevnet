/*
 * A JavaScript implementation of the Secure Hash Algorithm, SHA-1, as defined
 * in FIPS 180-1
 * Version 2.2 Copyright Paul Johnston 2000 - 2009.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for details.
 *
 * Modified for jabbwerwerx AJAX 
 *      moved functions under the jabberwerx.util.crypto namespace
 *      Eliminated functions not called directly by jwa
 *      made sha1 and md5 heklper functions inner to relevant global funcs
 *      replaced b64 encoding/decoding functions
 *      added jsdocs documentation, keeping private
 */
 
if (window.jabberwerx) {
    //ensure our namespaces exist
    window.jabberwerx.util = window.jabberwerx.util || {};
    window.jabberwerx.util.crypto = window.jabberwerx.util.crypto || {};    
    
    /**
     * @private
     * <p> Encodes the given string into base64.</p>
     *
     * <p><b>NOTE:</b> {input} is assumed to be UTF-8; only the first
     * 8 bits of each {input} element are significant.</p>
     *
     * @param   {String} input The string to convert to base64
     * @returns {String} The converted string
     */
    jabberwerx.util.crypto.b64Encode = function(input) {                
        var table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var output = "";
        
        for (var idx = 0; idx < input.length; idx += 3) {
            var data =  input.charCodeAt(idx) << 16 |
                        input.charCodeAt(idx + 1) << 8 |
                        input.charCodeAt(idx + 2);
                        
            //assume the first 12 bits are valid
            output +=   table.charAt((data >>> 18) & 0x003f) +
                        table.charAt((data >>> 12) & 0x003f);
            output +=   ((idx + 1) < input.length) ?
                        table.charAt((data >>> 6) & 0x003f) :
                        "=";
            output +=   ((idx + 2) < input.length) ?
                        table.charAt(data & 0x003f) :
                        "=";
        }
        
        return output;
    };
    
    /**
     * @private
     * <p>Decodes the given base64 string.</p>
     *
     * <p><b>NOTE:</b> output is assumed to be UTF-8; only the first
     * 8 bits of each output element are significant.</p>
     *
     * @param   {String} input The base64 encoded string
     * @returns {String} Decoded string
     */
    jabberwerx.util.crypto.b64Decode = function(input) {
        var table = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
        var output = "";
        
        for (var idx = 0; idx < input.length; idx += 4) {
            var h = [
                table.indexOf(input.charAt(idx)),
                table.indexOf(input.charAt(idx + 1)),
                table.indexOf(input.charAt(idx + 2)),
                table.indexOf(input.charAt(idx + 3))
            ];
            
            var data = (h[0] << 18) | (h[1] << 12) | (h[2] << 6) | h[3];
            if          (input.charAt(idx + 2) == '=') {
                data = String.fromCharCode(
                    (data >>> 16) & 0x00ff
                );
            } else if   (input.charAt(idx + 3) == '=') {
                data = String.fromCharCode(
                    (data >>> 16) & 0x00ff,
                    (data >>> 8) & 0x00ff
                );
            } else {
                data = String.fromCharCode(
                    (data >>> 16) & 0x00ff,
                    (data >>> 8) & 0x00ff,
                    data & 0x00ff
                );
            }
            output += data;
        }
        
        return output;
    };
    
    /**
     * @private
     * <p>Encodes the given utf-16 string into utf-8.</p>
     *
     * @param   {String} input The utf-16 string to encode
     * @returns {String} The utf-8 encoding
     */    
    jabberwerx.util.crypto.utf8Encode = function (input) {
      var output = "";
      var i = -1;
      var x, y;

      while(++i < input.length) {
        /* Decode utf-16 surrogate pairs */
        x = input.charCodeAt(i);
        y = i + 1 < input.length ? input.charCodeAt(i + 1) : 0;
        if(0xD800 <= x && x <= 0xDBFF && 0xDC00 <= y && y <= 0xDFFF) {
          x = 0x10000 + ((x & 0x03FF) << 10) + (y & 0x03FF);
          i++;
        }

        /* Encode output as utf-8 */
        if(x <= 0x7F)
          output += String.fromCharCode(x);
        else if(x <= 0x7FF)
          output += String.fromCharCode(0xC0 | ((x >>> 6 ) & 0x1F),
                                        0x80 | ( x         & 0x3F));
        else if(x <= 0xFFFF)
          output += String.fromCharCode(0xE0 | ((x >>> 12) & 0x0F),
                                        0x80 | ((x >>> 6 ) & 0x3F),
                                        0x80 | ( x         & 0x3F));
        else if(x <= 0x1FFFFF)
          output += String.fromCharCode(0xF0 | ((x >>> 18) & 0x07),
                                        0x80 | ((x >>> 12) & 0x3F),
                                        0x80 | ((x >>> 6 ) & 0x3F),
                                        0x80 | ( x         & 0x3F));
      }
      return output;
    }
    /**
     * @private
     * <p>Decodes the given utf-8 string into utf-16.</p>
     *
     * @param   {String} input The utf-8 string to encode
     * @returns {String} The utf-16 encoding
     */
    jabberwerx.util.crypto.utf8Decode = function (input) {
        var output = "";
        
        for (idx = 0; idx < input.length; idx++) {
            var c = [
                input.charCodeAt(idx),
                input.charCodeAt(idx + 1),
                input.charCodeAt(idx + 2)
            ];
            var pt;
            
            if          (c[0] < 128) {
                pt = c[0];
            } else if   (c[0] > 191 && c[0] < 224) {
                pt = ((c[0] & 0x001f) << 6 ) |
                     (c[1] & 0x003f);
                idx += 1;
            } else {
                pt = ((c[0] & 0x000f) << 12) |
                     ((c[1] & 0x001f) << 6) |
                     (c[2] & 0x003f);
                idx += 2;
            }
            
            output += String.fromCharCode(pt);
        }
        
        return output;
    };

    /**
     * @private
     * <p>Convert a raw string to a hex string.</p>
     *
     * @param   {String} input The string to hexify
     * @params {Boolean} [useUpperCase] Use uppercase hex characters
     * @returns {String} The hex representation iof string's bytes
     */    
    jabberwerx.util.crypto.str2hex = function(input, useUpperCase) {
        var hex_tab = useUpperCase ? "0123456789ABCDEF" : "0123456789abcdef";
        var output = "";
        var x;
        for(var i = 0; i < input.length; i++) {
            x = input.charCodeAt(i);
            output += hex_tab.charAt((x >>> 4) & 0x0F)
                   +  hex_tab.charAt( x        & 0x0F);
        }
        return output;
    }
    
    /**
     * @private
     * <p>Applies sha1 to given strng and encodes as Base64</p>
     *
     * @param   {String} input The utf-8 string to encode
     * @returns {String} The base64 encoded sha1 hash of input
     */    
    jabberwerx.util.crypto.b64_sha1 = function(input) {
        return jabberwerx.util.crypto.b64Encode(jabberwerx.util.crypto.str_sha1(input));
    }
    
    /**
     * @private
     * <p>Applies sha1 to given utf-8 string and encodes as String</p>
     * <p><b>NOTE:</b> {input} is assumed to be UTF-8; only the first
     * 8 bits of each {input} element are significant. 
     * Make sure to call utf8Encode as needed before invoking this function.</p>
     * @param   {String} input The utf-8 String to encode
     * @returns {String} The base64 encoded sha1 hash of input
     */    
    jabberwerx.util.crypto.str_sha1 = function(input) {
        //Convert a raw string to an array of big-endian words
        //Characters >255 have their high-byte silently ignored.
        var rstr2binb = function (input) {
            var output = Array(input.length >> 2);
            for(var i = 0; i < output.length; i++)
                output[i] = 0;
            for(var i = 0; i < input.length * 8; i += 8)
                output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (24 - i % 32);
            return output;
        }
        //Convert an array of big-endian words to a string
        var binb2rstr = function(input) {
            var output = "";
            for (var i = 0; i < input.length * 32; i += 8) {
                output += String.fromCharCode((input[i>>5] >>> (24 - i % 32)) & 0xFF);
            }
            return output;
        }

        //Add integers, wrapping at 2^32. This uses 16-bit operations internally
        //to work around bugs in some JS interpreters.
        var safe_add = function(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }

        //Bitwise rotate a 32-bit number to the left.
        var bit_rol = function(num, cnt) {
            return (num << cnt) | (num >>> (32 - cnt));
        }
 
        var sha1_ft = function(t, b, c, d) {
            if (t < 20) return (b & c) | ((~b) & d);
            if (t < 40) return b ^ c ^ d;
            if (t < 60) return (b & c) | (b & d) | (c & d);
            return b ^ c ^ d;
        }

        var sha1_kt = function(t) {
            return (t < 20) ?  1518500249 : (t < 40) ?  1859775393 :
                    (t < 60) ? -1894007588 : -899497514;
        }
        
        //Calculate the SHA-1 of an array of big-endian words, and a bit length
        var binb_sha1 = function(x, len) {
            /* append padding */
            x[len >> 5] |= 0x80 << (24 - len % 32);
            x[((len + 64 >> 9) << 4) + 15] = len;

            var w = Array(80);
            var a =  1732584193;
            var b = -271733879;
            var c = -1732584194;
            var d =  271733878;
            var e = -1009589776;

            for (var i = 0; i < x.length; i += 16) {
                var olda = a;
                var oldb = b;
                var oldc = c;
                var oldd = d;
                var olde = e;

                for (var j = 0; j < 80; j++) {
                    if (j < 16) {
                        w[j] = x[i + j];
                    } else {
                        w[j] = bit_rol(w[j-3] ^ w[j-8] ^ w[j-14] ^ w[j-16], 1);
                    }
                    var t = safe_add(safe_add(bit_rol(a, 5), sha1_ft(j, b, c, d)),
                                     safe_add(safe_add(e, w[j]), sha1_kt(j)));
                    e = d;
                    d = c;
                    c = bit_rol(b, 30);
                    b = a;
                    a = t;
                }

                a = safe_add(a, olda);
                b = safe_add(b, oldb);
                c = safe_add(c, oldc);
                d = safe_add(d, oldd);
                e = safe_add(e, olde);
            }
            return Array(a, b, c, d, e);
        }
        return binb2rstr(binb_sha1(rstr2binb(input), input.length * 8));
    }
    
    /**
     * @private
     * <p>Applies sha1 to given strng and encodes as Base64</p>
     *
     * <p><b>NOTE:</b> {input} is assumed to be UTF-8; only the first
     * 8 bits of each {input} element are significant. 
     * Make sure to call utf8Encode as needed before invoking this function.</p>
     * @param   {String} input The utf-8 string to encode
     * @returns {String} The base64 encoded sha1 hash of input
     */    
    jabberwerx.util.crypto.hex_md5 = function(input) { 
        return jabberwerx.util.crypto.str2hex(jabberwerx.util.crypto.rstr_md5(input)); 
    }

    /**
     * @private
     * <p>Calculate the MD5 of a raw string/p>
     *
     * <p><b>NOTE:</b> {input} is assumed to be UTF-8; only the first
     * 8 bits of each {input} element are significant. 
     * Make sure to call utf8Encode as needed before invoking this function.</p>
     * @param   {String} input The utf-8 string to encode
     * @returns {String} The base64 encoded sha1 hash of input
     */    
    jabberwerx.util.crypto.rstr_md5 = function(input) {
        function md5_cmn(q, a, b, x, s, t) {
            return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
        }
        function md5_ff(a, b, c, d, x, s, t) {
            return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
        }
        function md5_gg(a, b, c, d, x, s, t) {
            return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
        }
        function md5_hh(a, b, c, d, x, s, t) {
            return md5_cmn(b ^ c ^ d, a, b, x, s, t);
        }
        function md5_ii(a, b, c, d, x, s, t) {
            return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
        }
        //Add integers, wrapping at 2^32. This uses 16-bit operations internally
        //to work around bugs in some JS interpreters.
        var safe_add = function(x, y) {
            var lsw = (x & 0xFFFF) + (y & 0xFFFF);
            var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
            return (msw << 16) | (lsw & 0xFFFF);
        }
        //Bitwise rotate a 32-bit number to the left.
        var bit_rol = function(num, cnt) {
            return (num << cnt) | (num >>> (32 - cnt));
        }
        
        //Convert a raw string to an array of little-endian words
        //Characters >255 have their high-byte silently ignored.
        var rstr2binl = function(input) {
            var output = Array(input.length >> 2);
            for(var i = 0; i < output.length; i++) {
                output[i] = 0;
            }
            for(var i = 0; i < input.length * 8; i += 8) {
                output[i>>5] |= (input.charCodeAt(i / 8) & 0xFF) << (i%32);
            }
            return output;
        }
        //Convert an array of little-endian words to a string
        var binl2rstr = function(input) {
            var output = "";
            for (var i = 0; i < input.length * 32; i += 8) {
                output += String.fromCharCode((input[i>>5] >>> (i % 32)) & 0xFF);
            }
            return output;
        }
        //Calculate the MD5 of an array of little-endian words, and a bit length.
        var binl_md5 = function(x, len) {
          //append padding
          x[len >> 5] |= 0x80 << ((len) % 32);
          x[(((len + 64) >>> 9) << 4) + 14] = len;

          var a =  1732584193;
          var b = -271733879;
          var c = -1732584194;
          var d =  271733878;

          for (var i = 0; i < x.length; i += 16) {
            var olda = a;
            var oldb = b;
            var oldc = c;
            var oldd = d;

            a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
            d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
            c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
            b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
            a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
            d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
            c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
            b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
            a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
            d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
            c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
            b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
            a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
            d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
            c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
            b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);

            a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
            d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
            c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
            b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
            a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
            d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
            c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
            b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
            a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
            d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
            c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
            b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
            a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
            d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
            c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
            b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);

            a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
            d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
            c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
            b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
            a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
            d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
            c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
            b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
            a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
            d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
            c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
            b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
            a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
            d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
            c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
            b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);

            a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
            d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
            c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
            b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
            a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
            d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
            c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
            b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
            a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
            d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
            c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
            b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
            a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
            d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
            c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
            b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);

            a = safe_add(a, olda);
            b = safe_add(b, oldb);
            c = safe_add(c, oldc);
            d = safe_add(d, oldd);
          }
          return Array(a, b, c, d);
        }
        
        return binl2rstr(binl_md5(rstr2binl(input), input.length * 8));
    }
    /**
     * @private
     * <p>Generates a random UUID as per rfc4122</p>
     *
     * @returns {String} A random UUID
     */
     jabberwerx.util.crypto.generateUUID = function() {
        // Based on RFC 4122
        // time_lo:  bytes 0-3   (bits 0-31)
        // time_mid: bytes 4-5   (bits 32-47)
        // time_hi:  bytes 6-7   (bits 48-63)
        // clock_hi: bytes 8     (bits 64-71)
        // clock_lo: bytes 9     (bits 72-79)
        // node:     bytes 10-15 (bits 80-127)
        
        // start with "random" data
        var parts = [];
        for (var idx = 0; idx < 16; idx++) {
            parts[idx] = Math.floor(Math.random() * 256);
        }
        
        // set version to 4 (bits 12-15 of clock_hi)
        parts[6] = (parts[6] & 0x0f) | 0x40;
        
        // set clock_seq_and_reserved bits 6,7 to 0,1
        parts[8] = (parts[8] & 0x3f) | 0x80;
        
        // Assemble UUID as printed string
        var result = "";
        for (var idx = 0; idx < parts.length; idx++) {
            var val = parts[idx];
            if (idx == 4 || idx == 6 || idx == 8 || idx == 10) {
                // separators after time_lo, time_mid, time_hi, clock_lo
                result += "-";
            }
            
            result += ((val >>> 4) & 0x0f).toString(16);
            result += (val & 0x0f).toString(16);
        }
        
        return result;
    }
}
