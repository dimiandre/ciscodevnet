/**
 * filename:        validate.js
 * created at:      2010/08/10T13:50:00-07:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 * 
 * A utility class to validate Types of complex objects
 */
 
if (window.jabberwerx) {
    //ensure our namespaces exist
    window.jabberwerx.util = window.jabberwerx.util || {};
    window.jabberwerx.util.validate = window.jabberwerx.util.validate || {};    
    
    /**
     * @private
     * <p> Checks if the given input is a valid IP address.</p>
     *
     * @param {String} IP Address
     * @returns <tt>true</tt> If given input is a valid IP Address
     * @type Boolean
     */
    jabberwerx.util.validate.isIPAddress = function(input) {
        var regex = /^(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9])\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[1-9]|0)\.(25[0-5]|2[0-4][0-9]|[0-1]{1}[0-9]{2}|[1-9]{1}[0-9]{1}|[0-9])$/;
            if (regex.test(input))
                return true;
            else
                return false;

    };

}
