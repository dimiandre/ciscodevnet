/**
 * filename:        JWView.js
 * created at:      2009/06/25T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) { 
    jabberwerx.ui.JWView = jabberwerx.JWModel.extend(/** @lends jabberwerx.ui.JWView.prototype */{
        /**
         * @class
         * <p>Abstract base class for views.</p>
         *
         * <p>Subclasses MUST override the following:</p>
         * <ul>
         * <li>{@link #createDOM} to generate the content</li>
         * </ul>
         *
         * <p>Subclasses SHOULD override the following:</p>
         * <ul>
         * <li>{@link #update} to update the view.</li>
         * </ul>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">JWView</a></li>
         * </ul>
         *
         * @abstract
         * @description
         * <p>Creates a new jabberwerx.ui.JWView instance.</p>
         *
         * @constructs jabberwerx.ui.JWView
         * @extends jabberwerx.JWModel
         */
        init: function() {
            this._super();
            
            this.applyEvent("viewRendered");
            this.applyEvent("viewRemoved");
            this.applyEvent("viewActivated");
            this.applyEvent("viewDeactivated");
        },
        /**
         * <p>Destroys this view. This implementation calls {@link #remove}
         * before calling the superclass' implementation.</p>
         *
         */
        destroy: function() {
            this.remove();
        
            this._super();
        },
        
        /**
         * <p>Retreives the parent jabberwerx.ui.JWView for this instance. This method
         * locates the parent by recursing through the parent DOM elments
         * until a jabberwerx.ui.JWView instance is found.</p>
         *
         * @returns {jabberwerx.ui.JWView} The parent view, or <tt>null</tt> if not found
         */
        parent: function() {
            var found = null;
            var pn = this.jq && this.jq.get(0).parentNode;
            
            while (!found && pn) {
                found = pn.jw_view || null;
                pn = pn.parentNode;
            }
            
            return found;
        },
        
        /**
         * <p>Retrieves or changes the dimensions of this view. This method
         * is a convenience for calling {@link #width} and {@link #height}
         * within a single method call, although {@link #update} is only
         * called once.</p>
         *
         * <p>If this method is called without any arguments, the current
         * dimensions are returned. Otherwise, the dimensions are changed
         * and this jabberwerx.ui.JWView is returned.</p>
         *
         * <p>The return value (or value of {dim}) is an object with the
         * following properties:</p>
         * <pre class="code">{
         *   width: {Number|String},    //the pixel width (if Number), or CSS
         *                              //value (if String)
         *   height: {Number|String},   //the pixel height (if Number), or CSS
         *                              //value (if String)
         * }</pre>
         *
         * @param   {Object} [dim] The new dimensions, or <tt>undefined</tt>
         *          to retrieve the current dimensions
         * @returns  {Object|jabberwerx.ui.JWView} The current dimensions (if {dim} is
         *          <tt>undefined</tt>); or this jabberwerx.ui.JWView if the dimensions were
         *          changed
         */
        dimensions: function(dim) {
            if (!dim) {
                return {
                    width: this.width() || 0,
                    height: this.height() || 0
                };
            }
            
            if (this.jq) {
                if (dim.width) {
                    this.width(dim.width, true);
                }
                if (dim.height) {
                    this.height(dim.height, true);
                }
                
                this.update();
            }
            
            return this;
        },
        /**
         * <p>Retrieves or changes the width of this view.</p>
         *
         * <p>If {w} is not defined, this method returns the current width of
         * this view, in pixels. Otherwise the width is changed to the value of
         * {w} in pixels (if a Number) or the CSS value (if a String),
         * {@link #update} is called (unless {noupdate} is <tt>true</tt>), and
         * this jabberwerx.ui.JWView is returned.</p>
         *
         * <p>If this view is not currently rendered, this method returns 0 for
         * retrieval, and ignores {w} for changes.</p>
         *
         * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
         *          retrieve the current width)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     #dimensions
         */
        width: function(w, noupdate) {
            if (w === undefined) {
                return (this.jq && this.jq.width()) || 0;
            }
            if (this.jq) {
                //preset
                this.jq.width(w);
                w = this.jq.width();
                //how much width for margins/padding currently?
                var delta = jabberwerx.$.browser.msie ?
                            0 :
                            this.jq.outerWidth(true) - this.jq.width();
                //leave room for margins/padding
                this.jq.width(w - Math.max(delta, 0));
                !noupdate && this.update();
            }
            
            return this;
        },
        /**
         * <p>Retrieves or changes the height of this view.</p>
         *
         * <p>If {h} is not defined, this method returns the current height of
         * this view, in pixels. Otherwise the height is changed to the value of
         * {h} in pixels (if a Number) or the CSS value (if a String),
         * {@link #update} is called (unless {noupdate} is <tt>true</tt>), and
         * this jabberwerx.ui.JWView is returned.</p>
         *
         * <p>If this view is not currently rendered, this method returns 0 for
         * retrieval, and ignores {h} for changes.</p>
         *
         * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
         *          retrieve the current height)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     #dimensions
         */
        height: function(h, noupdate) {
            if (h === undefined) {
                return (this.jq && this.jq.height()) || 0;
            }
            if (this.jq) {
                //preset
                this.jq.height(h);
                h = this.jq.height();
                //margins/padding px
                var delta = jabberwerx.$.browser.msie ?
                            (0) :
                            (this.jq.outerHeight(true) - this.jq.height());
                //leave room for current margins/padding
                this.jq.height(h - Math.max(delta, 0));
                !noupdate && this.update();
            }
            
            return this;
        },
        
        /**
         * <p>Renders this jabberwerx.ui.JWView. This method performs the following:</p>
         * <ol>
         * <li>Calls {@link #createDOM} to generate the content to render</li>
         * <li>Stores the content as a jQuery-wrapped object in {@link #jq}.</li>
         * <li>Triggers the "viewRendered" event with the jQuery-wrapped object
         *     as data.</li>
         * <li>Calls {@link #update}.</li>
         * <li>Returns {@link #jq}.</li>
         * </ol>
         *
         * <p>If this view has already been rendered, then {@link #update} is
         * called and {@link #jq} is returned.</p>
         *
         * @param   {Document} [doc] The document to use for rendering;
         *          defaults to the global document object is not defined.
         * @returns {jQuery} The jQuery-wrapped content to be inserted
         * @throws  {TypeError} if {doc} is not a valid Document.
         * @throws  {ReferenceError} if the generated DOM is not valid
         */
        render: function(doc) {
            if (!this.jq) {
                if (!doc) {
                    doc = document;
                } else if (!jabberwerx.isDocument(doc)) {
                    throw new TypeError("document is not valid");
                }
                
                var data = this.createDOM(doc);
                if (!data) {
                    throw new ReferenceError("data not created");
                }
                
                this.jq = jabberwerx.$(data);
                this.jq.get(0).jw_view = this;
                this.event("viewRendered").trigger(this.jq);
                
                this.update();
            }
            
            return this.jq;
        },
        
        
    /**
         * <p>Shows the view if hidden in the document.
         * View state is kept in {@link #jq}  so this 
         * method does nothing if called before {@link #render} .</p>
         *
         * @returns {jabberwerx.ui.JWView} self refernce
         */
        show: function() {
            if (this.jq && !this._isVisible()) {
                this.jq.show();
            }
            return this;
        },

    /**
         * <p>Hides the view if visible in the document.
         * View state is kept in {@link #jq}  so this 
         * method does nothing if called before {@link #render} .</p>
         *
         * @returns {jabberwerx.ui.JWView} self refernce
         */
        hide: function() {
            if (this.jq && this._isVisible()) {
                this.jq.hide();
            }
            return this;
        },

    /**
         * <p>Called by {@link #render} to generate the content for this View.
         * Subclasses <b>MUST</b> override this method, returning the DOM element
         * that is to be this view's content.</p>
         *
         * <p><b>NOTE:</b> This method should not be called directly; instead it
         * is called by {@link #render} when necessary.</p>
         *
         * @param   {Document} doc The Document to use for creating content
         * @returns {Element|jQuery} The content element (either as a DOM
         *          element or the jQuery-wrapped DOM element)
         * @see     #render
         */
        createDOM: function(doc) {
            return null;
        },
        
        /**
         * <p>Removes this jabberwerx.ui.JWView's contents. This method performs the
         * following:</p>
         * <ol>
         * <li>Calls {@link #destroyDOM}.</li>
         * <li>Removes {@link #jq} from its parent</li>
         * <li>Deletes {@link #jq}.</li>
         * <li>Triggers the "viewRemoved" event with the previous value of
         * {@link #jq} as the data.</li>
         * </ol>
         *
         * <p>If {@link #jq} is not valid when this method is called, no
         * actions are taken.</li>
         *
         * @returns {jabberwerx.ui.JWView} This jabberwerx.ui.JWView instance
         */
        remove: function() {
            if (this.jq) {
                var jq = this.jq;
                this.destroyDOM();
                
                this.jq = null;
                jq.remove();
                this.event("viewRemoved").trigger(jq);
            }
            
            return this;
        },
        /**
         * <p>Called by {@link #remove} to destroy the DOM content of this
         * view. The current implementation does nothing.</p>
         *
         * <p>Subclasses MAY override this method to perform any necessary
         * cleanup. The value of {@link #jq} has not yet been deleted.</p>
         *
         * <p><b>NOTE:</b> This method should not be called directly; instead it
         * is called by {@link #remove} when necessary.</p>
         */
        destroyDOM: function() {
        },
        
        /**
         * <p>Updates this view. This method is called by {@link #dimensions},
         * {@link #height}, and {@link #width} automatically, but may also be
         * called directly to force an update of the view.</p>
         *
         * <p>Subclasses SHOULD override this method to perform any necessary
         * changes, but SHOULD call the super-classes implementation.</p>
         *
         * @returns {Boolean} <tt>true</tt> if the view is updatable (e.g. if
         *          {@link #jq} is valid).
         */
        update: function() {
            return Boolean(this.jq);
        },
                
        /**
         * <p>Determines if this view should be saved in object graphs.
         * Default behavior is to include the view</p>
         *
         * @returns {Boolean} Always <tt>true</tt>
         */
        shouldBeSavedWithGraph: function() { return true; },
        

        
        /**
         * <p>Check {@link #persistOptions} and save the current JQ's
         * xml as needed. Sets JQ to null to prevent any further serialization
         *  of that object.</p>
         *
         * @returns {Boolean} Always <tt>true</tt>
         */
        willBeSerialized: function() {
            this._super();
            if (this.jq && this.persistOptions() == jabberwerx.ui.JWView.persist_html) {
                //use xml if available, html if not
                if (this.jq.get(0).xml){
                    this._persistedHTML = this.jq.get(0).xml;
                } else {
                    this._persistedHTML = this.jq.get(0).outerHTML;
                }
            }
            this.jq = null; //do not persist jq,  meaningless
            return true;
        },

        
        /**
         * <p>Called when this instance has been rehydrated. Subclass changes {@link #render} 
         * to a {@link #restoreRender} reference. This allows special handling of the first render after 
         * a rehydration.
         *
         * @returns {Boolean} Always <tt>true</tt>
         */
        wasUnserialized: function() {
            this._super();
            
            //switch render func to one time call restoreRender
            this._savedRender = this.render;
            this.render = this.restoreRender;
            
            this.jq = null; //force to null so next render rebuilds, should be null anyway
            return true;
        },

        /**
         * <p>Renders this jabberwerx.ui.JWView the first time after rehydration.
         * <li>Calls {@link #restoreDOM} to generate the content to render</li>
         * <li>Stores the content as a jQuery-wrapped object in {@link #jq}.</li>
         * <li>Calls {@link #restoreUpdate} to allow special handling after rehydration</li>
         * <li>Reassigns {@link #render} to original method so future calls invoke
         *that method.</li>
         * <li>Returns {@link #jq}.</li>
         * </ol>
         *
         * <p>This method is called the first time the view is rendered after rehydration.
         * Unlike {@link #render} this method invokes {@link restoreDOM}, {@link restoreUpdate}
         * and does not trigger any events.</p>
         *
         * @param   {Document} [doc] The document to use for rendering;
         *          defaults to the global document object is not defined.
         * @returns {jQuery} The jQuery-wrapped content to be inserted
         * @throws  {TypeError} if {doc} is not a valid Document.
         * @throws  {ReferenceError} if the generated DOM is not valid
         */
        
        restoreRender: function(doc) {
            if (!this.jq) {
                if (!doc) {
                    doc = document;
                } else if (!jabberwerx.isDocument(doc)) {
                    throw new TypeError("document is not valid");
                }
                
                var data = this.restoreDOM(doc);
                if (!data) {
                    throw new ReferenceError("data not created");
                }

                this.jq = jabberwerx.$(data);
                this.jq.get(0).jw_view = this;
                this.render = this._savedRender;
                this.restoreUpdate();
            }        
            return this.jq;
        },
        
    /**
         * <p>Called by {@link #restoreRender} to generate the content for this View
         * from previously persisted state data.
         * Subclasses may override this method, returning the DOM element
         * that is to be this view's content. Default behavior is to return the
         * result of a {@link #createDOM}  call if {@link #persist_html}  was 
         * not in {@link #persistOptions}.
         *
         * If HTML was persisted this base class returns the DOM element
         * built from it. .</p>
         *
         * <p><b>NOTE:</b> This method should not be called directly; instead it
         * is called by {@link #restoreRender} when necessary.</p>
         *
         * @param   {Document} doc The Document to use for creating content
         * @returns {Element|jQuery} The content element (either as a DOM
         *          element or the jQuery-wrapped DOM element)
         * @see     #restoreRender
         */
        restoreDOM: function(doc) {
            var res = null;
            if (this._persistedHTML) {
                res = jabberwerx.$(this._persistedHTML, doc).get(0);
                this._persistedHTML = null;
            } else {
                res = this.createDOM(doc);
            }            
            return res;
        },
        
        
    /**
         * <p>Called by {@link #restoreRender} when the view's jq member has been assigned.
         * Gives subclasses a way of initializing the DOM, above and beyond {@link #update}.
         * Update is used to refresh the view from its current state (redraw) *or* from  associated 
         * models, a behavior undeeded and unwanted when rehydrating. 
         </p>
         *
         * <p><b>NOTE:</b> This method should not be called directly; instead it
         * is called by {@link #restoreRender} when necessary.</p>
         *
         * @returns { jabberwerx.ui.JWView} self reference   
         * @see     #restoreRender
         */
        restoreUpdate: function() {
            return true;
        },
        
        /**
         * <p>Set or retrieve built in persistence flags. 
         * The jabberwerx.ui.JWView base class can serialize its JQ reference
         * by storing the JQ's innerHTML. The default is 
         * {@link #persist_none}. If {@link #persist_html} is set
         * the base class will store and reload the JQ's html. /p>
         *
         * @param   {Integer} opts Serialization options implemented
         *      implemented in the base class. {@link #persist_html}.
         * @returns {jabberwerx.ui.JWView} self reference
         */        
        persistOptions: function(opts) {
            if (opts === undefined) {
                return this._persistOpts;
            }        
            this._persistOpts = opts;
            return this;
        },
        
        /** private persistence option flag **/
        _persistOpts: 0,
        
        /**
         * <p>Get the current visibility state of object's {@link #jq}  reference.</p>
         *
         * @returns (Boolean} visibility state
         */
        _isVisible: function() {
            return this.jq && this.jq.is(":visible");
        },

        /**
         * <p>The jQuery-wrapped content element for this jabberwerx.ui.JWView.</p>
         *
         * <p><b>NOTE:</b> Do not modify this value directly; instead it is
         * created via {@link #render} and deleted via {@link #remove}.</p>
         *
         * @type jQuery
         */
        jq: null
    }, "JWView");

        /**
         * No automatic persistence by base class
         * @constant
         * @see #persistOptions
         */
    jabberwerx.ui.JWView.persist_none = 0;
    
        /**
         * Base class automatically saves and loads the innerHTML of the
         * view's current {@link #jq}
         * @constant
         * @see #persistOptions         
         */
    jabberwerx.ui.JWView.persist_html = 1;
    
}
