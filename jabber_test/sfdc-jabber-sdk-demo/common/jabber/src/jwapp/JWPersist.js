/**
 * filename:        JWPersist.js
 * created at:      2010/04/10
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx && jabberwerx.util) {

/**
 * @deprecated do not use
 */
jabberwerx.util.config = {
    maxGraphAge: 30 
};

/**
 * The global graph registry.
 * @todo: make this associated with an app or "document" or something, so it's not global.
 */
jabberwerx.util._markGraph = function(tag, root) {
    if (jabberwerx.util._graphRegistry) {
        if (root && (!root.shouldBeSerializedInline || !root.shouldBeSerializedInline())) {
            /*DEBUG-BEGIN*/
                jabberwerx.util.debug.log('marking ' + tag + ' in registry');
            /*DEBUG-END*/
        
            jabberwerx.util._graphRegistry[tag] = {
                timestamp: new Date(),
                value: root,
                graph: jabberwerx.util.findReachableGUIDs(root)
            };
        } else {
            /*DEBUG-BEGIN*/
                jabberwerx.util.debug.log('unmarking ' + tag + ' from registry');
            /*DEBUG-END*/
            delete jabberwerx.util._graphRegistry[tag];
        }
    }
};

/**
 * Adapted from dojo.toJson
 * <p>Serialize an object; references to JW objects are replaced
 * by their GUID. Non-JWApp-object cycles will cause inifinite
 * recursion.</p>
 *
 * <p>Any property values in jabberwerx.JWBase objects that are bare references to
 * other JW objects will be replaced with the referent's GUID.
 * Any bare references in arrays will be also replaced with referent's
 * GUID. This method does not recurse into JW objects; you'll
 * have to serially serialize them to get a complete graph.</p>
 *
 * @param {Object} it Object at which to start traversing the graph.
 * @param {Boolean} prettyPrint Whether to include newlines and tabs in the output.
 * @param {String} _indentStr Private to the recursion; clients shouldn't pass anything.
 * @returns {String} the JSON-serialization of the object
 */
jabberwerx.util.serialize = function(it, prettyPrint, _indentStr) {
    var f = function(it, prettyPrint, _indentStr) {
        if(it === undefined) {
            return "undefined";
        }
        var objtype = typeof it;
        if(objtype == "number" || objtype == "boolean") {
            return it + "";
        }
        if(it === null){
            return "null";
        }
        if(jabberwerx.util.isString(it)) {        
            return jabberwerx.util._escapeString(it);
        }
        if((typeof it.nodeType == 'number') && (typeof it.cloneNode == 'function')) { // isNode
            // the original code here from dojo (below) yielded invalid JSON.
            return '{}';
        }

        var recurse = arguments.callee;
        // short-circuit for objects that support "json" serialization
        // if they return "self" then just pass-through...
        var newObj;
        _indentStr = _indentStr || "";
        var nextIndent = prettyPrint ? _indentStr + "\t" : "";
        if(typeof it.__json__ == "function"){
            newObj = it.__json__();
            if(it !== newObj){
                return recurse(newObj, prettyPrint, nextIndent);
            }
        }
        if(typeof it.json == "function"){
            newObj = it.json();
            if(it !== newObj){
                return recurse(newObj, prettyPrint, nextIndent);
            }
        }

        var sep = prettyPrint ? " " : "";
        var newLine = prettyPrint ? "\n" : "";
        var val;

        // array
        if(jabberwerx.util.isArray(it)){
            var res = jabberwerx.util.map(it, function(obj){
                if (jabberwerx.util.isJWObjRef(obj)) {
                    // replace reference with guid
                    val = jabberwerx.util._escapeString(obj._guid);
                }
                else {
                    val = recurse(obj, prettyPrint, nextIndent);
                }
                if(typeof val != "string"){
                    val = "undefined";
                }
                return newLine + nextIndent + val;
            });
            return "[" + res.join("," + sep) + newLine + _indentStr + "]";
        }
        if(objtype == "function"){
            if (jabberwerx.util.isJWInvocation(it)) {
                // strip it of its function-ness
                return recurse({ object: it.object, methodName: it.methodName, _jwinvocation_: true }, prettyPrint, nextIndent);
            }
            else {
                return null;
            }
        }
        // generic object code path
        var output = [];
        if (!('responseText' in it) && !('responseXML' in it)) {
            // don't do anything with xmlhttprequest objects
            try {
                // ie is doing something psychotic here.
                for (var key in it) { break; }
            }
            catch (e) {
                // suppressed so we can all get on with our lives.
                return "null";
            }
            for (var key in it) {
                var keyStr;
                if(typeof key == "number"){
                    keyStr = '"' + key + '"';
                }else if(typeof key == "string"){
                    keyStr = jabberwerx.util._escapeString(key);
                }else{
                    // skip non-string or number keys
                    continue;
                }

                if (jabberwerx.util.isJWObjRef(it[key])) {
                    if (    !it[key].shouldBeSerializedInline ||
                            !it[key].shouldBeSerializedInline()) {
                        // replace reference with guid
                        val = jabberwerx.util._escapeString(it[key]._guid);
                    } else {
                        if (it[key].willBeSerialized) {
                            it[key].willBeSerialized();
                        }
                        val = recurse(it[key], prettyPrint, nextIndent);
                    }
                }
                else {
                    val = recurse(it[key], prettyPrint, nextIndent);
                }

                if(typeof val != "string"){
                    // skip non-serializable values
                    continue;
                }
                output.push(newLine + nextIndent + keyStr + ":" + sep + val);
            }
        }

        return "{" + output.join("," + sep) + newLine + _indentStr + "}"; // String
    };
    return f(it, prettyPrint, _indentStr);
}

/**
 * Traverses the object graph from the passed object and records
 * the guids of evey object reachable from the passed object.
 *
 * @param {CFBase} start The object at which to start traversing.
 * @returns {String[]} An array of guids.
 */
jabberwerx.util.findReachableGUIDs = function(start) {
    var traversedGUIDs = {};
    (function f(root, depth) {
        if (typeof root == 'object' && root != null) {
            var s = ''; for (var p = 0; p < depth; p++) { s += '   '; }
            if (jabberwerx.util.isArray(root)) {
                // array
                for(var i = 0; i < root.length; i++) {
                    if (root[i]) {
                        f(root[i], depth + 1);
                    }
                }
            }
            else if (jabberwerx.util.isJWObjRef(root)) {
                // jw object
                if (traversedGUIDs[root._guid] === undefined) {
                    traversedGUIDs[root._guid] = root;
                    for (var p in root) {
                        if (root[p]) {
                            f(root[p], depth + 1);
                        }
                    }
                }
            }
            else if (root.constructor == Object){
                // regular object
                for (var p in root) {
                    if (root[p] && typeof root[p] == 'object') {
                        f(root[p], depth + 1);
                    }
                }
            }
        }
    })(start, 0);
    return traversedGUIDs;
};

/**
 * Save an object graph.
 *
 * <p>Pass an object to act as the "root" of the graph. Any objects you need to save should be
 * reachable from this root. This is the object you will get back when you call {@link jabberwerx.util.loadGraph}.</p>
 *
 * @param {CFBase} root A "root" object for the graph.
 * @param {String} tag A name for the stored graph.
 * @param {Function} callback A callback to be invoked when the save is complete.
 * @throws {jabberwerx.util.JWStorageRequiresjQueryjStoreError} if `_jw_store_` does not exist, or if it is not ready.
 */
jabberwerx.util.saveGraph = function(root, tag, callback) {
    if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
        throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
    }
    /*DEBUG-BEGIN*/
        jabberwerx.util.debug.log('saving graph ' + tag, 'persistence', 'persistence');
    /*DEBUG-END*/

    var reachableGUIDs = jabberwerx.util.findReachableGUIDs(root);
    if (!reachableGUIDs[tag]) {
        root = jabberwerx.util.isJWObjRef(root) ?
                { reference : root,
                  timestamp : new Date().getTime() } :
                root;
        reachableGUIDs[tag] = root;
    }

    var     serialization = {};
    for (var key in reachableGUIDs) {
        var val = reachableGUIDs[key];

        if (val.shouldBeSavedWithGraph && !val.shouldBeSavedWithGraph()) {
            continue;
        }

        if (val.willBeSerialized) {
            val.willBeSerialized();
        }
        
        var     json = jabberwerx.util.serialize(val, false);
        
        if (jabberwerx.$.jStore.CurrentEngine.type.toLowerCase() == 'flash') {
            //Flash engine requires json to double escaped
            json = json.replace(/\\\"/g, '\\\\\"').replace(/\\f/g, "\\\\f").
                replace(/\\b/g, "\\\\b").replace(/\\n/g, "\\\\n").
                replace(/\\t/g, "\\\\t").replace(/\\r/g, "\\\\r");
        }    
        
        /*DEBUG-BEGIN*/
            jabberwerx.util.debug.log('saving ' + key + ' == "' + json + '"...', 'persistence');
        /*DEBUG-END*/
        jabberwerx.$.jStore.store('_jw_store_.' + key, json);
        serialization[key] = json;
        jabberwerx.util._markGraph(key, val);
    }

    //finally, save TS for given tag
    jabberwerx.$.jStore.store('_jw_store_timestamp_.' + tag, new Date().getTime());

    if (callback) {
        callback(serialization);
    }

    return serialization;
};

/**
 * Returns a ref to the root object originally stored with this tag, connected
 * to its stored graph.
 *
 * @param {String} tag Tag name stored with root object.
 * @returns {CFBase} The root object originally stored with this tag.
 * @throws {jabberwerx.util.JWStorageRequiresjQueryjStoreError} if `_jw_store_` does not exist, or if it is not ready.
 */
jabberwerx.util.loadGraph = function(tag) {
    if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
        throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
    }

    var reg = {};
    var loader, knitter;
    //knitter recursively walks the objects, and:
    //  1) changes GUID references to the JWObject it refers to
    //     (as found in the registry or via loader())
    //  2) regenerates JWInvocation instances
    knitter = function(arg) {
        if (!arg) {
            return arg;
        }

        switch(typeof arg) {
            case 'string':
                if (jabberwerx.util.isJWObjGUID(arg)) {
                    var tmp = (reg[arg]) ?
                            reg[arg] :
                            loader(arg);
                    if (tmp) {
                        return tmp;
                    }
                }

                break;
            case 'object':
                if (jabberwerx.util.isJWInvocation(arg)) {
                    var typeObject = knitter(arg.object);
                    arg = jabberwerx.util.generateInvocation(typeObject, arg.methodName);
                } else if (arg._className) {
                    //This is probably a JWObject, rehydrate it
                    var tmp = reg[arg._guid];
                    if (tmp) {
                        //short-cut for already hydrated/-ing objects
                        return tmp;
                    } if (arg instanceof jabberwerx.JWBase) {
                        reg[arg._guid] = arg;
                    } else {
                        arg = eval('new ' +
                                arg._className +
                                '("__jw_rehydrate__", arg)');
                        reg[arg._guid] = arg;
                    }
                }

                for (var key in arg) {                    
                    if (key == '_guid') {
                        continue;
                    }
                    
                    arg[key] = knitter(arg[key]);

                }
                if (arg.wasUnserialized) {
                    arg.wasUnserialized();
                }

                break;
            case 'array':
                for (var i in val) {
                    val[i] = knitter(val[i]);
                }

                break;
        }

        return arg;
    };
    //loader does the following:
    //  1) grabs the value for the given tag from storgae
    //  2) evaluates the value into an object (creating JWObjects as needed)
    //  3) knits JWObject references together via knitter()
    loader = function(ltag) {
        var graph;

        if (reg[ltag]) {
            return reg[ltag];
        }

        /*DEBUG-BEGIN*/
            jabberwerx.util.debug.log('grabbing ' + ltag + ' from storage', 'persistence');
        /*DEBUG-END*/
        graph = jabberwerx.$.jStore.store('_jw_store_.' + ltag);

        if (!graph) {
            return graph;
        }

        /*DEBUG-BEGIN*/
            jabberwerx.util.debug.log('evaluating object for ' + ltag, 'persistence');
        /*DEBUG-END*/
        try {
            // If jStore 1.2 determines that the stored string value is in a json format, 
            // it'll eval the string value before returning it. Otherwise, the string is returned.
            if (typeof graph == "string") {
                graph = eval('(' + graph + ')');
            }
            graph = knitter(graph);
        }
        catch (e) {
            jabberwerx.util.debug.log('exception evalualuating ' + ltag + ": " + e);
            throw e;
        }
        reg[ltag] = graph;
        jabberwerx.util._markGraph(ltag, graph);

        return graph;
    };

    //check timestamp for given graph, load only if within allowed age or timestamp does not exist
    var ts = jabberwerx.util.getLoadedGraphAge(tag);
    if (ts && (ts.getTime() > jabberwerx.util.getMaxGraphAge()*1000)) {
        return null;
    }

    loader(tag);

    var key;
    for (key in reg) {
        if (reg[key] && reg[key].graphUnserialized) {
            reg[key].graphUnserialized();
        }
    }

    var retval = reg[tag];
    if (retval &&
        !jabberwerx.util.isJWObjRef(retval) &&
        jabberwerx.util.isJWObjRef(retval.reference)) {
        retval = retval.reference;
    }

    return retval;
};

/**
 * Returns true if the storage engine reports that the passed tag has a value
 * stored in the namespace `_jw_store_`.
 *
 * @param {String} tag Tag name for the stored graph.
 * @returns {Boolean} <tt>true</tt> if the storage engine reports that the passed tag has a value
 * stored in the namespace `_jw_store_`, otherwise false.
 * @throws {jabberwerx.util.JWStorageRequiresjQueryjStoreError} if `_jw_store_` does not exist, or if it is not ready.
 */
jabberwerx.util.isGraphSaved = function(tag) {
    if (!jabberwerx.$.jStore || !jabberwerx.$.jStore.isReady || !jabberwerx.$.jStore.CurrentEngine.isReady) {
        throw new jabberwerx.util.JWStorageRequiresjQueryjStoreError();
    }

    /*DEBUG-BEGIN*/
        jabberwerx.util.debug.log('checking for existence of store with tag ' + tag, 'persistence');
    /*DEBUG-END*/
    
    return jabberwerx.$.jStore.store('_jw_store_.' + tag) != null;    
};

/**
 * Clears storage associated with the namespace `_jw_store_`.
 *
 * If no tag is passed, the entire namespace is cleared. Otherwise, only the store
 * marked by the passed tag is cleared.
 *
 * @param {String} [tag] Optionally specify a specific, tagged store to clear.
 */
jabberwerx.util.clearGraph = function(tag) {
    /*DEBUG-BEGIN*/
        jabberwerx.util.debug.log('clearing storage' + (tag ? ' with tag ' + tag : ''));
    /*DEBUG-END*/
    if (!tag && !jabberwerx.util._graphRegistry) return; //nothing to do, no global graph, no tag to load
    
    var guids = {};    
    if (tag && (!jabberwerx.util._graphRegistry || !jabberwerx.util._graphRegistry[tag])) {
        guids = jabberwerx.util.findReachableGUIDs(jabberwerx.util.loadGraph(tag));
    } else {
        var gnode = tag ? jabberwerx.util._graphRegistry[tag] : jabberwerx.util._graphRegistry;
        guids = gnode.graph;
        if (tag && !guids[tag]) {
            guids[tag] = gnode.value;
        }
    }
    
    for (var key in guids) {
        jabberwerx.$.jStore.remove('_jw_store_.' + key);
        jabberwerx.util._markGraph(key);
        /*DEBUG-BEGIN*/
            jabberwerx.util.debug.log('removed ' + key + ' from storage...');
        /*DEBUG-END*/
    }
};

/**
 * Return the date at which a graph was saved. A graph must have been loaded
 * via {@link jabberwerx.util.loadGraph}.
 *
 * @param {String} tag Tag name for the stored graph.
 * @returns {Date} Date at which a graph was saved. Returns null if no graph was found for passed in tag value.
 */
jabberwerx.util.getLoadedGraphTimestamp = function(tag) {
    //check timestamp for given graph, load only if within allowed age
    if (tag) {
        var ts = jabberwerx.$.jStore.store('_jw_store_timestamp_.' + tag);
        if (ts) {
            var rd = new Date();
            rd.setTime(ts);
            return rd;
        }
    }
    return null;
};

/**
 * Return the delta in dates between when the graph was saved and now.
 *
 * @param {String} [tag] The tag of the saved graph (returns null if  null/undefined)
 * @returns {Date} The delta in dates between when the graph was saved and now. Returns null if tag is null/undefined, or if no store with stored tag could be found.
 */
jabberwerx.util.getLoadedGraphAge = function(tag) {
    var t = jabberwerx.util.getLoadedGraphTimestamp(tag);
    if (t) {
        return new Date(new Date().getTime() - t.getTime());
    }
    return null;
};

/**
 * Retrieve the maximum age a persisted graph can be before it is ignored and cleared on load
 *
 * @returns {Number} The maximum age
 */
 jabberwerx.util.getMaxGraphAge = function() {
    if (typeof(jabberwerx._config.persistDuration) != "number") {
        jabberwerx._config.persistDuration  = parseInt(jabberwerx._config.persistDuration);
    }
    return jabberwerx._config.persistDuration;
 }

/**
 * Set the maximum age a persisted graph can be before it is ignored and cleared on load
 *
 * @param {Number} [age] The maximum age in seconds, ignored if undefined or <= 0.
 * @returns {Number} The maximum age
 */
 jabberwerx.util.setMaxGraphAge = function(age) {
    if (age && typeof age == "number" && age > 0) {
        jabberwerx._config.persistDuration = age;
    }
    return jabberwerx.util.getMaxGraphAge();
 }
 
 /**
 * Thrown when persistence functions are invoked and Dojo storage is not loaded.
 * @class jabberwerx.util.JWStorageRequiresjQueryjStoreError
 */
jabberwerx.util.JWStorageRequiresjQueryjStoreError = jabberwerx.util.Error.extend('JW storage features require jQuery-jStore.');


/**
 * @private
 * _graphRegistry is an array of all jabberwerx.JWBase objects that have been serialized/rehydrated. Since 1.1 writes/reads object serializations
 * from the store (individually keyed). No need to write/read an entire graph at a time, making this data structure dubious at
 * best (JJF opinion).  Disable (default) by initializing to null, enable by initializing to an empty object {}
 *
 *@see  #_markGraph 
*/
jabberwerx.util._graphRegistry = null;

jabberwerx.$(document).ready(function() {

    // Storage engine perference order, engine with lowest array index is the
    // most desirable engine.
    jabberwerx.$.jStore.EngineOrder = ['local', 'flash', 'html5', 'gears', 'ie'];
	
    var flashFilePath = "";
    try {
        // Generate file path using jabberwerx core library install path
        flashFilePath = jabberwerx._getInstallPath("jabberwerx");  
    } catch(e) {
        // If jabberwerx core library is not loaded use jabberwerx ui 
        // library to generate the file path
        flashFilePath = jabberwerx._getInstallPath("jabberwerx.ui");
          
    }

    jabberwerx.$.extend(jabberwerx.$.jStore.defaults,{
        flash: flashFilePath + "jStore.Flash.html"
    });

    if (!jabberwerx.$.jStore.isReady) {
        jabberwerx.$.jStore.ready(window.jabberwerx._handleStorageReady);
        jabberwerx.$.jStore.load();
    }
    else {
        window.jabberwerx._handleStorageReady(jabberwerx.$.jStore.CurrentEngine);
    }
});

}