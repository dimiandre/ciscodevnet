;
/**
 * filename:        JWCore.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */
/**
 * @namespace
 * @description
 * A namespace for the JWApp framework. The framework is a general Javascript
 * application framework, built for but not tied to JabberWerx.
 *
 * Note: The jabberwerx.JWBase class does not live in this namespace.
 * Note: The JW framework depends on jQuery; jQuery must be loaded before this code runs.
 */

if (jabberwerx) {
if (!jabberwerx.util) {
    /**
     * @namespace
     * @minimal
     */
    jabberwerx.util = {}
    }

if (jabberwerx) {
   (function(){
    var initializing = false;

    /**
     * @class jabberwerx.JWBase
     * @minimal
     *
     * <p>The base class for objects in the JWApp framework.</p>
     *
     * <p>Objects derived from jabberwerx.JWBase get the following features:</p>
     *
     * <p><b>## Object Graph Client-Side Persistence ##</b></p>
     * <p>A graph of JWBase-derived objects can be serialized, stored, and re-loaded
     * via {@link jabberwerx.util.saveGraph} and {@link jabberwerx.util.loadGraph}. Cycles and uniqueing are handled via an
     * object registry and per-object guids. Note that cycles among non-JWBase objects
     * will result in infinite recursion, as per usual.</p>
     *
     * <p>FYI, there are two steps to loading a serialized graph: unserializing and
     * "rehydrating". We unserialize the object registry via eval(). Rehydrating
     * involves turning the resulting bare JS objects with keys and values into full JW objects,
     * with appropriate methods and prototype chain in place. For this to work, the
     * serialization must include the objects' class name. For now this is accomplished
     * in the class definition/declaration: {@link jabberwerx.JWBase.extend} takes a string parameter
     * that must be the fully-qualified name of the class being defined.</p>
     *
     * <p>Client-side storage is handled via dojo storage. By default any given JW object
     * in a graph will NOT be saved. Objects that want to be saved must implement
     * {@link jabberwerx.JWBase#shouldBeSavedWithGraph} and return true.</p>
     *
     * <p>Typically only model objects should be saved to a persistent store. See
     * {@link jabberwerx.JWModel}, which does return true for `shouldBeSavedWithGraph`.</p>
     *
     * <p><b>## Object-Method Invocation Objects ##</b></p>
     * <p>A jabberwerx.JWBase object can generate portable function objects that, when invoked, are
     * scoped automatically to their generating instance. The graph storage engine
     * treats these function objects specially, so the observer relationships among
     * stored objects can be serialized and restored automatically.</p>
     *
     * @description
     * Creates a new jabberwerx.JWBase object.
     *
     * @constructs jabberwerx.JWBase
     */
    jabberwerx.JWBase = function(){};     // The base class constructor (does nothing; used to generate a prototype)

    /**
     * This method is called to initialize the JWBase-derived instance.
     */
    jabberwerx.JWBase.prototype.init = function() {
    };
    /**
     * This method is offered as a way to release resources that were acquired by an object
     * during its lifetime. However, as Javascript does not have any built-in way to trigger code
     * when an object is about to be garbage-collected, or when a temporary/stack-based object
     * goes out of scope, this method will have to be called by hand.
     */
    jabberwerx.JWBase.prototype.destroy = function() {
        // currently not tracking observees; subclasses must unregister by hand
        // ... not that there's any way to get destructors to run anyway ... :(

        return null;
    };

    /**
     * Get the class name of this object.
     *
     * @type String
     * @returns The fully-qualified name of the object's class.
     */
    jabberwerx.JWBase.prototype.getClassName = function() {
        return this._className || '';
    };

    /**
     * Returns the objects' classname, in brackets.
     * Override if desired.
     *
     * @type String
     * @returns A string representation of this object.
     */
    jabberwerx.JWBase.prototype.toString = function() {
        return '[' + this.getClassName() + ']';
    };

    /**
     * By default JW objects will not be saved with the object graph.
     * Subclasses can override this behavior by implementing this method
     * and having it return true.
     *
     * The jabberwerx.JWModel class does this for you; in general, model objects are
     * the only objects that should be saved in a persistent store.
     *
     * @type Boolean
     * @returns Whether this object should be saved with the object graph.
     */
    jabberwerx.JWBase.prototype.shouldBeSavedWithGraph = function() {
        return false;
    };

    /**
     * By default JW objects will not be serialized inline from their references.
     * Subclasses can override this behavior by implementing this method
     * and having it return true.
     *
     * @type Boolean
     * @returns Whether this object should be serialized inline with its references.
     */
    jabberwerx.JWBase.prototype.shouldBeSerializedInline = function() {
        return false;
    };

    /**
     * A hook for objects to prepare themselves for serialization. Subclasses
     * should use this to do any custom serialization work. Typically this will
     * involve serializing by hand any foreign (eg, non-JWModel-based) object
     * references that will be necessary for the object's functioning when restored
     * from serialization.
     *
     * It's important to note that there is no guarantee of the order in which
     * objects will have this method invoked, relative to other objects in the
     * graph. Basically, you can't depend on other object's `willBeSerialized`
     * having been called or not called when you're in this method.
     * @see {@link #wasUnserialized}
     */
    jabberwerx.JWBase.prototype.willBeSerialized = function() {
    };

    /**
     * A hook for objects to undo any custom serialization work done
     * in {@link #willBeSerialized}.
     *
     * As with that method, there's no guarantee of the order in which
     * objects will have this method invoked. When this method is invoked,
     * every object in the graph will have been rehydrated into a fully-fleshed-out
     * JW object, with data and prototype chain in place, but there is no
     * guarantee that any other object will have had its `wasUnserialized` invoked.
     */
    jabberwerx.JWBase.prototype.wasUnserialized = function() {
        // whack any observers that did not survive serialization
        jabberwerx.$.each(this._observerInfo, function(eventName, info) {
            info.observers = jabberwerx.$.grep(info.observers, function(observer, i) { return typeof observer.target != 'undefined'; });
        });
    };

    /**
     * A chance for recently-unserialized objects to do something and be assured that
     * every object in the graph has run its custom post-serialization code.
     */
    jabberwerx.JWBase.prototype.graphUnserialized = function() {
    }

    /**
     * Subclasses can call this with one of their method names to get back a storable, portable,
     * and invokable function object. The result can be passed to anything expecting a bare callback.
     *
     * @see jabberwerx.util.generateInvocation
     * @param {String} methodName The name of this object's method to wrap in an invocation object.
     * @param {Array} [boundArguments] Arguments can be bound to the eventual invocation of your object
     * method here at the invocation creation. These arguments will **preceed** in the argument list any
     * arguments that are passed to your method at the actual call site.
     * @type Function
     * @returns A bare callback
     */
    jabberwerx.JWBase.prototype.invocation = function(methodName, boundArguments) {
        return jabberwerx.util.generateInvocation(this, methodName, boundArguments);
    };

    /**
     * @private
     * Creates the override chain.  If {base} and {override} are both
     * functions, then this method generates a new function that:
     * <ol>
     * <li>Remembers any current superclass method (e.g. overriding an
     * override)</li>
     * <li>Sets this._super to {base}</li>
     * <li>Calls {override}, remembering its return value (if any)</li>
     * <li>Sets this._super back to its previous value (if any).</li>
     * <li>Returns the result from {override}</li>
     * </ol>
     *
     * If {override} is undefined and {base} is defined (regardless
     * of its type), {base} is returned as-is. If {override} and {base} are
     * not functions, or if {base} is not a function, {override} is returned
     * as-is.</p>
     *
     * @param   base The base method or property (may be undefined)
     * @param   override The overriding method or property (may be undefined)
     * @returns The function providing the override chain, or {base} or
     *          {override} as appropriate
     */
    var __jwa__createOverrideChain = function(base, override) {
        if (base !== undefined && override === undefined) {
            return base;
        }

        if (    !jabberwerx.$.isFunction(base) ||
                !jabberwerx.$.isFunction(override)) {
            return override;
        }

        return function() {
            var tmp = this._super;

            this._super = base;
            var retval = override.apply(this, arguments);
            this._super = tmp;

            return retval;
        };
    };

    /**
     * Provide mixin support to Javascript objects. This method applies all of
     * the properties and methods from {prop} to this type. A copy of {prop}
     * is made before it is applied, to ensure changes within this type do
     * not impact the mixin definition.</p>
     *
     * <p><b>NOTE:</b> This method should not be called for jabberwerx.JWBase directly.
     * Instead, specific subclasses of jabberwerx.JWBase may use it to include new
     * functionality.</p>
     *
     * <p>Mixin properties are shadowed by the jabberwerx.JWBase class in the same way
     * as super class properties are. In this case, the mixin is considered
     * the super class, and any properties defined in the class override
     * or shadow those with the same name in the mixin.</p>
     *
     * <p>Mixin methods may be overridden by the jabberwerx.JWBase class in the same
     * manner as super class methods are.  In this case, the mixin's method
     * is considered to be the "_super":</p>
     *
     * <p><pre class='code'>
        AMixin = {
            someProperty: "property value",
            doSomething: function() {
                jabberwerx.util.debug.log("something is done");
            }
        };
        MyClass = jabberwerx.JWBase.extend({
            init: function() {
                this._super();  //calls JWBase.prototype.init
            },
            doSomething: function() {
                jabberwerx.util.debug.log("preparing to do something");
                this._super();  //calls AMixin.doSomething
                jabberwerx.util.debug.log("finished doing something");
            }
        }, "MyClass");

        MyClass.mixin(AMixin);
     * </pre></p>
     *
     * @param   {Object} prop The mixin to include
     */
    jabberwerx.JWBase.mixin = function(prop) {
        // create a deep copy of the mixin, to prevent corruption of the Mixin
        prop = jabberwerx.$.extend(true, {}, prop);

        // Apply the mixin's properties and methods, treating the mixin methods
        // as if coming from a super-class (the opposite of extend!)
        for (var name in prop) {
            this.prototype[name] = __jwa__createOverrideChain(
                    prop[name],
                    this.prototype[name]);
        }
    };

    /**
     * Provide intercept support to Javascript objects. This method applies all of
     * the properties and methods from {prop} to this type. A copy of {prop}
     * is made before it is applied, to ensure changes within this type do
     * not impact the intercept definition.</p>
     *
     * <p><b>NOTE:</b> This method should not be called for jabberwerx.JWBase directly.
     * Instead intercept a specific subclasses of jabberwerx.JWBase by adding new
     * or overriding exisiting functions and properties.</p>
     *
     * <p>Intercept functions are inserted into the top of the super class call stack,
     * that is a intercept function's _super call will invoke the original, overridden
     * method. Other properties are "overridden" by changing the property directly.</p>
     *
     * <p><pre class='code'>
     * MyClass = jabberwerx.JWBase.extend({
     *        someProperty: "MyClass property"
     *        init: function() {
     *            this._super();  //calls JWBase.prototype.init
     *        },
     *        doSomething: function() {
     *            jabberwerx.util.debug.log("something is done");
     *        }
     *    }, "MyClass");
     *    AnIntercept = {
     *        someProperty: "AnIntercept property",
     *        doSomething: function() {
     *            jabberwerx.util.debug.log("preparing to do something");
     *            this._super(); //call MyClass.doSomething
     *            jabberwerx.util.debug.log("post something");
     *        }
     *    };
     * 
     * MyClass.intercept(AnIntercept);
     * </pre></p>
     *
     * @param   {Object} prop The intercept to include
     */
    jabberwerx.JWBase.intercept = function(prop) {
        prop = jabberwerx.$.extend(true, {}, prop);
        for (var name in prop) {
            this.prototype[name] =
                __jwa__createOverrideChain(this.prototype[name], prop[name]);
        } 
    };
    
    /**
     * Provide classical inheritance to Javascript objects.
     *
     * Following John Resig's Class,
     * <a href="http://ejohn.org/blog/simple-javascript-inheritance/">http://ejohn.org/blog/simple-javascript-inheritance/</a>
     * Inspired by base2 and Prototype
     *
     * One important addition to Resig's code: we provide a quasi-"copy constructor"
     * that will take a bare javascript object with the data and classname of a
     * JW object, and rehydrate it into a full object with prototype chain and object
     * methods in place. Clients/sub-classes probably won't need to use it; it's used
     * by {@link jabberwerx.util.loadGraph}.
     *
     * Within any object method, the superclass's version may be invoked via the variable
     * named `_super`.
     *
     * Ex:
     *
     * <p><pre class='code'>
        MyClass = jabberwerx.JWBase.extend({
            init: function() {
                this._super()
            },
            someMethod: function() {
                doSomething();
            }
        }, 'MyClass');

        AnotherClass = MyClass.extend({ ... })
     * </pre></p>
     *
     * @param {Object} prop The "subclass definition", an object with which to extend the parent class.
     * @param {String} className The fully-qualified name of the class.
     */
    //I haven't found a way to avoid passing the fully-qualified name of the classs.
    jabberwerx.JWBase.extend = function(prop, className) {
        var _super = this.prototype;

        // Instantiate a base class (but only create the instance,
        // don't run the init constructor)
        initializing = true;
        var prototype = new this();
        initializing = false;

        // Copy the properties over onto the new prototype
        for (var name in prop) {
            // Check if we're overwriting an existing function
            prototype[name] = __jwa__createOverrideChain(_super[name], prop[name]);
        };

        var _superClass = jabberwerx.$.extend({}, this);

        // The dummy class constructor
        function JWBase() {
            if ( !initializing ) {
                // a JW object is "dehydrated" if its data has been unserialized but
                // it does not have the methods or prototype chain of a real object yet.
                // Rehydrating is done by invoking
                //
                //      new ClassName('__jw_rehydrate__', obj);
                //
                // Where obj is the unserialized raw data object. The returned instance
                // will be a shallow copy of the passed object (including its GUID),
                // but with methods and prototype chain in place. Sort of a "copy constructor".
                if (arguments.length == 2 && typeof arguments[0] == 'string' && arguments[0] == '__jw_rehydrate__') {
                    /*DEBUG-BEGIN*/
                    jabberwerx.util.debug.log('rehydrate constructor, ' + arguments[1]._className, 'persistence');
                    /*DEBUG-END*/
                    // make a SHALLOW copy of the passed argument.
                    // it's assumed that the passed object is dehydrated; ie, no methods or
                    // prototype chain.
                    var obj = arguments[1];
                    for (var p in obj) {
                        this[p] = obj[p];
                    }
                }
                else {
                    // regular construction of a new instance
                    this._guid = jabberwerx.util.newObjectGUID(className || "");
                    this._jwobj_ = true;    // cheap way to say "i'm a jw object!"
                    //this._serialized = false;
                    this._className = (typeof className == 'undefined' ? null : className); // REALLY wish there were a better way!
                    this._observerInfo = {};
                    //this._className = arguments.callee.name;
                    // give ourselves private copies of declared arrays and objects
                    for (var p in this) {
                        if (typeof this[p] != 'function') {
                            this[p] = jabberwerx.util.clone(this[p]);
                        }
                    }
                    //[this._guid] = this;
                    if (this.init) {
                        // All construction is actually done in the init method
                        //jabberwerx.util.debug.log('regular init, argument list is length ' + arguments.length, 'persistence');
                        this.init.apply(this, arguments);
                    }
                }
            }
        };

        // Include class-level methods and properties
        for (var name in _superClass) {
            JWBase[name] = _superClass[name];
        }

        // Populate our constructed prototype object
        JWBase.prototype = prototype;

        // Enforce the constructor to be what we expect
        prototype.constructor = JWBase;

        return JWBase;
    };
})();
}
/**
 * @class
 * @minimal
 * JWApp's "native" error object. Just a wrapper/namespace so we can extend
 * Error objects pleasantly.
 *
 * @description
 * <p>Creates a new jabberwerx.util.Error with the given message.</p>
 *
 * @param {String} message The error message.
 * @extends Error
 */
jabberwerx.util.Error = function(message) {
    this.message = message;
};
jabberwerx.util.Error.prototype = new jabberwerx.util.Error();


/**
 * Create a new Error type.
 *
 * You can define the message and extension at declaration time (ie, defining the error class)
 * and then override it at creation time (ie, at the throw site) if desired.
 *
 * @param {String} message Becomes the base Error object's message.
 * @param {Object} extension Properties in this object are copied into the new error type's prototype.
 * @type Function
 */
jabberwerx.util.Error.extend = function(message, extension) {
    var f = function(message, extension) {
        this.message = message || this.message;
        for (var p in extension) {
            this[p] = extension[p];
        }
    };
    
    f.prototype = new this(message);
    for (var p in extension) {
        f.prototype[p] = extension[p];
    }
    
    return f;
}

/**
 * @class
 * @minimal
 * <p>Error thrown when an object has, but does not support,
 * a method or operation.</p>
 *
 * @extends jabberwerx.util.Error
 */
jabberwerx.util.NotSupportedError = jabberwerx.util.Error.extend("This operation is not supported");


/**
 * @private
 * Internal cache of invocation objects; two calls to
 * {@link jabberwerx.util.generateInvocation} will return the same function object.
 */
jabberwerx.util._invocations = {};

/**
 * Invocations are function objects that do two nifty things.
 *
 * <p><ul>
 * <li>They wrap an (optional) JW object reference and method name, and
 *   an invocation of the bare invocation object reference is secretly
 *   an invocation of the named method on the JW object.</li>
 *
 * <li>We treat invocation objects specially during serialization and
 *   unserialization. They carry their object GUID and method name along with
 *   them into persistence-land, and we rehydrate that info into a real function
 *   when we load the graph.</li>
 * </ul></p>
 *
 * <p>The former nifty thing means you can pass an invocation object as a callback
 * to any service expecting a bare function reference, and, when invoked,
 * the callback will be applied within object scope, rather than global scope.</p>
 *
 * <p>We also cache invocation objects, so you will always get the same function object
 * back from each call to generateInvocation when passing the same object and
 * method name. This is useful for registering an invocation as a handler
 * for some service that registers/unregisters handlers by function reference,
 * (eg, dom or jQuery events) and then unregistering it later.</p>
 *
 * <p>The latter nifty thing means that callback handlers among JW objects will be
 * preserved across graph loading and storing and automatically re-connected when
 * the graph is rehydrated. That's assuming, of course, that the target object
 * was stored in the graph to begin with. Since models are generally stored,
 * callback networks among model objects can be expected to be stored, while
 * callbacks involving other kinds of objects will have to be re-created after
 * unserialization. {@link jabberwerx.JWBase.wasUnserialized} and
 * {@link jabberwerx.JWBase.graphUnserialized} are usually good places to do this.</p>
 *
 * @param {jabberwerx.JWBase} object Any JW object
 * @param {String} methodName The name of the method this invocation represents.
 * @param {Array} [boundArguments] An optional array of arguments to pass to the invocation. These will PRECEED any arguments passed to the invocation at the actual call site.
 * @returns {function} object An "invocation"-type function object.
 */
jabberwerx.util.generateInvocation = function(object, methodName, boundArguments) {
    var objectTag = '_global_';
    if (jabberwerx.util.isJWObjRef(object)) {
        objectTag = object._guid;
    }
    // we don't support any objects that aren't JW objects or global (window)

    var f = jabberwerx.util._invocations[objectTag + '.' + methodName]
    if (typeof f != 'undefined') {
        return f;
    }
    if (typeof boundArguments != 'undefined') {
        if (typeof boundArguments != 'object' || !(boundArguments instanceof Array)) {
            boundArguments = [boundArguments];
        }
    }
    else {
        boundArguments = [];
    }

    var f = function() {
        return jabberwerx.util.invoke.apply(arguments.callee, [arguments.callee].concat(boundArguments, Array.prototype.slice.call(arguments)));
    };
    f.object = object;
    f.methodName = methodName;
    f._jwinvocation_ = true;
    jabberwerx.util._invocations[objectTag + '.' + methodName] = f;
    return f;
};


/**
 * Invoke an invocation function object. Clients shouldn't need to call this.
 *
 * The first argument is the invocation object, remaining arguments are arguments
 * to pass through to the method.
 *
 * @param {Object} invocationObject The invocation object.
 * @param {Anything} [...] Remaining arguments are passed on to the invocation method.
 */
jabberwerx.util.invoke = function() {
    var invocation = arguments[0];
    var args = Array.prototype.slice.call(arguments, 1);
    if (typeof invocation.object == 'undefined' || !invocation.object) {
        // assume a global method
        return window[invocation.methodName].apply(window, args);
    }
    else {
        return invocation.object[invocation.methodName].apply(invocation.object, args);
    }
};

/**
 * @private
 * This is an optional saftey measure to try to avoid observer event name
 * collisions. There's no requirement that you register custom event names;
 * just be careful.
 */
jabberwerx.util.eventNames = [
    'jw_valueChanged',
    'jw_collectionChanged',
];
/**
 * @private
 * Register an observer event name. There's no requirement
 * that you register events; it's like wearing a bike helmet.
 *
 * @param {String} name The name of the observable/event.
 * @throws jabberwerx.util.EventNameAlreadyRegisteredError
 */
jabberwerx.util.registerEventName = function(name) {
    if (jabberwerx.util.eventNames.indexOf(name) == -1) {
        jabberwerx.util.eventNames.push(name);
    }
    else {
        throw new jabberwerx.util.EventNameAlreadyRegisteredError('JW event name ' + name + ' already registered!');
    }
};

/**
 * @private
 */
jabberwerx.util.EventNameAlreadyRegisteredError = jabberwerx.util.Error.extend('That event name is already registered!');

/**
 * @private
 */
jabberwerx.util._objectUIDCounter = 0;

/**
 * Generate a quasi-guid for object tracking.
 *
 * @param {String} className Class name of object.
 * @returns {String} a new guid
 */
jabberwerx.util.newObjectGUID = function(className) {
    jabberwerx.util._objectUIDCounter = (jabberwerx.util._objectUIDCounter + 1 == Number.MAX_VALUE) ? 0 : jabberwerx.util._objectUIDCounter + 1;
    return '_jwobj_' + className.replace(/\./g, "_") + '_' + (new Date().valueOf() + jabberwerx.util._objectUIDCounter).toString();
};


/**
 * @private
 * Adapted from dojo._escapeString.
 * Adds escape sequences for non-visual characters, double quote and
 * backslash and surrounds with double quotes to form a valid string
 * literal.
 *
 * @param {String} str String to escape
 * @returns {String} escaped string
 */
jabberwerx.util._escapeString = function(str){
    return ('"' + str.replace(/(["\\])/g, '\\$1') + '"').
        replace(/[\f]/g, "\\f").replace(/[\b]/g, "\\b").replace(/[\n]/g, "\\n").
        replace(/[\t]/g, "\\t").replace(/[\r]/g, "\\r"); // string
};

/**
 * Adapted from dojo.isString
 * <p>Checks if the parameter is a String.</p>
 *
 * @param {Object} it Object to check.
 * @returns {Boolean} <tt>true</tt> if object is a string, false otherwise.
 */
jabberwerx.util.isString = function(it){
    return !!arguments.length && it != null && (typeof it == "string" || it instanceof String); // Boolean
};

/**
 * Adapted from dojo.isArray
 * <p>Checks if the parameter is an Array.</p>
 *
 * @param {Object} it Object to check.
 * @returns {Boolean} <tt>true</tt> if object is an array, false otherwise.
 */
jabberwerx.util.isArray = function(it){
    return it && (it instanceof Array || typeof it == "array"); // Boolean
};

/**
 * @private
 * Dependency from dojo.map.
 *
 * @returns {Array} array of stuff used in dojo.map
 */
jabberwerx.util._getParts = function(arr, obj, cb){
    return [
        jabberwerx.util.isString(arr) ? arr.split("") : arr,
        obj || window,
        // FIXME: cache the anonymous functions we create here?
        jabberwerx.util.isString(cb) ? new Function("item", "index", "array", cb) : cb
    ];
};

/**
 * Adapted from dojo.map
 * <p>Applies callback to each element of arr and returns an Array with
 * the results. This function corresponds to the JavaScript 1.6 Array.map() method.
 * In environments that support JavaScript 1.6, this function is a
 * passthrough to the built-in method.</p>
 * <p>For more details, see:
 * <a href = "http://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/map">
 * http://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Array/map
 * </a></P>
 *
 * @param {Array|String} arr The array to iterate on.  If a string, operates on individual characters.
 * @param {Function|String} callback The function is invoked with three arguments: item, index, and array and returns a value
 * @param {Object} [thisObject] May be used to scope the call to callback
 *
 * @returns {Array} passed in array after the callback has been applied to each item
 */
jabberwerx.util.map = function(arr, callback, thisObject){
    var _p = jabberwerx.util._getParts(arr, thisObject, callback); arr = _p[0];
    var outArr = (arguments[3] ? (new arguments[3]()) : []);
    for(var i=0;i<arr.length;++i){
        outArr.push(_p[2].call(_p[1], arr[i], i, arr));
    }
    return outArr; // Array
};

/**
 * Determines if the passed reference is a GUID for a JWBase-derived object.
 * @param {Object} ref Reference to check.
 * @returns {Boolean} <tt>true</tt> if passed reference is a GUID for a JWBase-derived object, otherwise false.
 */
jabberwerx.util.isJWObjGUID = function(ref) {
    return (typeof ref == 'string' && (ref.indexOf('_jwobj_') == 0 || ref.indexOf('"_jwobj_') == 0));
}

/**
 * Determines if the passed reference is a JWBase-derived object.
 * @param {Object} ref Reference to check.
 * @returns {Boolean} <tt>true</tt> if passed reference is a JWBase-derived object, otherwise false.
 */
jabberwerx.util.isJWObjRef = function(ref) {
    return (ref && typeof ref == 'object' && typeof ref._jwobj_ == 'boolean' && ref._jwobj_);
}

/**
 * Determines if the passed reference is one of our invocation objects.
 * @param {Object} ref Reference to check.
 * @returns {Boolean} <tt>true</tt> if passed reference is one of our invocation objects, otherwise false.
 */
jabberwerx.util.isJWInvocation = function(ref) {
    return (ref && (typeof ref._jwinvocation_ != 'undefined'));
};

/**
 * Depth-first recursively clone passed argument. Cyclical references will
 * result in infinite recursion. Will shallow-copy an argument's
 * prototype if it exists, and will shallow-copy functions.
 *
 * @param {arg} The object/array/whatever to clone.
 * @returns {Anything} The new cloned whatever.
*/
jabberwerx.util.clone = function(arg) {
    if (typeof arg == 'object' && arg != null) {
        if (arg instanceof Array) {
            var copy = [];
            for (var i = 0; i < arg.length; i++) {
                copy.push(jabberwerx.util.clone(arg[i]));
            }
        }
        if (typeof copy == 'undefined') {
            var copy = {};
        }
        for (var p in arg) {
            copy[p] = jabberwerx.util.clone(arg[p]);
        }
        if (typeof arg.prototype != 'undefined') {
            copy.prototype = arg.prototype;
        }
    }
    else {
        var copy = arg;
    }
    return copy;
};

/**
 * Almost, but not quite, like WordPress's sanitize_title_with_dashes.
 * <a href = "http://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes">http://codex.wordpress.org/Function_Reference/sanitize_title_with_dashes</a>
 * <p>The difference with this implementation is that the seperator can be specified as an input parameter.</p>
 * 
 * @param {String} string String which to slugify
 * @param {String} separator String value which to replace '-' with in string.
 * @returns {String} string but with all instances of '-' replaced with seperator.
 */
jabberwerx.util.slugify = function(string, separator) {
    return string.toLowerCase().replace('-', separator).replace(/[^%a-z0-9 _-]/g, '').replace(/\s+/g, (typeof separator != 'undefined' ? separator : '-'));
};

//utf8 safe encoding
/**
 * <p>Encodes a string into an obfuscated form.</p>
 *
 * @param   {String} s The string to encode
 * @returns {String} The obfuscated form of {s}
 */
jabberwerx.util.encodeSerialization = function(s) {
    if (s) {
        return jabberwerx.util.crypto.b64Encode(jabberwerx.util.crypto.utf8Encode(s));
    }
    return '';
}

/**
 * <p>Decodes a string from an obfuscated form.</p>
 *
 * @param   {String} s The string to decode
 * @returns {String} The un-obfuscated form of {s}
 */
jabberwerx.util.decodeSerialization = function(s) {
    if (s) {
        return jabberwerx.util.crypto.utf8Decode(jabberwerx.util.crypto.b64Decode(s));
    }
    return '';
}


/**#nocode+
 * IE awesomeness -- if indexOf is not defined, define it.
 */
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(elt /*, from*/) {
        var len = this.length;

        var from = Number(arguments[1]) || 0;
        from = (from < 0) ? Math.ceil(from) : Math.floor(from);
        if (from < 0)
            from += len;

        for (; from < len; from++) {
            if (from in this && this[from] === elt) return from;
        }
        return -1;
    };
}
/**#nocode- */

/**
 * Pass an xml string, get back an xml document. Optionally pass
 * the name of a tag in which to wrap the result.
 *
 * @param {String} s The XML to convert into DOM objects
 * @param {String} [wrap] The name of a tag in which you'd like your xml wrapped.
 * @returns {Document} An XML document.
 */
jabberwerx.util.unserializeXMLDoc = function(s, wrap) {
    if (!s && !wrap) {return null;}

    if(typeof wrap == 'string') {
        s = '<' + wrap + '>' + s + '</' + wrap + '>';
    }
    var builder = new jabberwerx.NodeBuilder("nbwrapper");
    builder.xml(s);
    var unwrapped = builder.data.childNodes[0];    
    builder.data.removeChild(unwrapped);
    builder.document.removeChild(builder.data);
    builder.document.appendChild(unwrapped);
    return builder.document;
};

/**
 * Parses the passed XML and returns the document element.
 * <p>Similar to {@link jabberwerx.util.unserializeXMLDoc}</p>
 * 
 * @param {String} s The XML to convert into DOM objects
 * @param {String} [wrap] The name of a tag in which you'd like your xml wrapped.
 * @returns {Element} An XML document element.
 */
jabberwerx.util.unserializeXML = function(s, wrap) {
    var d = jabberwerx.util.unserializeXMLDoc(s, wrap);
    return (d ? d.documentElement : d);
};
/**
 * Generates XML for the given Node.
 *
 * @param {XML DOM Node} n XML node
 * @returns {String} XML for given node n. null if n is undefined.
 */
jabberwerx.util.serializeXML = function(n) {
    if (typeof(XMLSerializer) != "undefined") {
        return new XMLSerializer().serializeToString(n);
    } else if (n.xml) {
        return n.xml;
    }
    
    return null;
};

//Overload to provide Node.xml
if (    typeof(XMLSerializer) != "undefined" &&
        typeof(Node) != "undefined" &&
        Node.prototype &&
        Node.prototype.__defineGetter__) {
    Node.prototype.__defineGetter__(
            "xml",
            function() {
                return jabberwerx.util.serializeXML(this);
            });
}

/**
 * <p>Jabberwerx debug console. Exposes a subset of Firebug methods
 * including log, warn, error, info, debug and dir. Jabberwerx console
 * methods may only be passed one log message (Firebug allows formatted
 * strings and values, ala printf) and a "stream". Streams are message types and allow
 * finer filtering of log messages.</p>
 *
 * <p>For example jabberwerx.util.debug.log("my foo", "bar") will log
 * "my foo" if the stream "bar" is enabled see {@link jabberwerx.util.setDebugStream}.</p>
 *
 * <p>If the built in console (window.console) does not support a particular method
 * the given message is not logged.</p>
 * 
 */
jabberwerx.util.debug = {
    /** an external console implementing the same logging methods as jabbrwerx.util.debug **/
    consoleDelegate: null,
    /** The built in window console **/
    console: window.console || null
}

// jabberwerx.util.debug.log, jabberwerx.util.debug.dir, etc ...
// second argument is a stream name; will only log when that stream is turned on in jabberwerx.util.debug.streams
jabberwerx.$.each(['log', 'dir', 'warn', 'error', 'info', 'debug'], function(i, e) {
    jabberwerx.util.debug[e] = function(a, streamName) {
        //no logging if all logging disabled or the given stream is disabled
        if (!jabberwerx._config.debug.on ||
            (jabberwerx.util.isString(streamName) && !jabberwerx._config.debug[streamName])) {
            return;
        }

        if (jabberwerx.util.isString(streamName)) {
            a = '[' + streamName + '] '  + a;
        }
        
        //built in console may have been destroyed or may not support this method. Don't log.
        try {
            jabberwerx.util.debug.console[e](a);
        } catch (ex) {}

        //console delegates should support the same interface jabberwerx.util.debug implements.
        //throw exception if it does not
        if (jabberwerx.util.debug.consoleDelegate) {
            jabberwerx.util.debug.consoleDelegate[e](a);
        }
    }
});

/**
 * Add or overwrite the setting on a debug stream.
 *
 * @param {String} streamName Debug stream name
 * @param {String} value Debug stream value to set.
 */
jabberwerx.util.setDebugStream = function(streamName, value) {
    jabberwerx._config.debug[streamName] = (typeof value == 'undefined' ? true : value);
};

}
