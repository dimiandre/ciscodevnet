/**
 * filename:        JWApp.js
 * created at:      2009/09/21T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

;(function(){
    if (!jabberwerx.util.app) {
        jabberwerx.util.app = {};
        jabberwerx.app = {};
    }

    jabberwerx.ui.JWApp = jabberwerx.JWBase.extend(/** @lends jabberwerx.ui.JWApp.prototype */{
        /**
         * @class
         * <p>A top-level "Application" base class object used to simplify persistence. 
         *  Applications are loaded from the store or created if no serialization
         *  exists. jabberwerx.ui.JWApp init is only called if a new instance is created. This class
         *  divides the initialization into two sperate steps. appCreated and appInitialized,
         *  simplifying the lifetime management. </P>
         * 
         *
         * @description
         * <p>Creates a new jabberwerx.ui.JWApp  Subclasses should <b>not</b> override this method. Override
         *  appCreate instead.</p>
         *
         * @constructs jabberwerx.ui.JWApp
         * @extends jabberwerx.JWBase
         */
        init: function() {
            this._super();
            this.appCreate();
        },
        
        /**
         * <p>jabberwerx.ui.JWApp are persisted by default.</p>
         *
         * @returns   {Boolean} Always <tt>true</tt>
         */
        shouldBeSavedWithGraph: function() {
            return true;
        },

        /**
         * <p>Called once on object creation. jabberwerx.ui.JWApp subclasses should override this method 
         *  rather than init. jabberwerx.JWBase object creation should occurr here. JWA Event handlers may be registered
         * if using Invocations. Essentially, any objects that may be persisted should be created here.</p>
         */
        appCreate: function() {
        },

        /**
         * <p>Called when the application has been sucessfully created or loaded from the store.
         *  jabberwerx.ui.JWApp subclasses should override this method and should render and map HTML events within this
         *  method. Any sort of state that is not explicitly serialized (ie non Invocation events) should be set here.</p>
         */
        appInitialize: function() {
        }
    }, "JWApp");

         /**
         * <p>Set or retrieve the primary application class. This function manages the lifetime of the given application class. Creating, initializing, serializing and
         *  rehydrating as needed. Sets jabberwerx.util.app._jwappclass and jabberwerx.util.app._jwappinst singletons as a side effect.
         *  TODO acessors to these singletons are currently not defined</p>
         *
         * @param   {String|null} [appClass] The name of the jabberwerx.ui.JWApp subclass that should be created or loaded. appClass is used
         *      as the storage key.
         *  @return {String} the managed applicartion classname
         */   
    jabberwerx.app.persistedApplicationClass = function(appClass) {
        if (!appClass) {
            return jabberwerx.util.app._jwappclass;
        }
        jabberwerx.util.app._jwappclass = appClass;
        jabberwerx.util.app._jwappinst = null;
        
        jabberwerx.$(document).bind("ready", function() {
            // Attempt to load the app once the jStore engine is ready
            jabberwerx.$.jStore.ready(function(engine){
                engine.ready(function(){
                    jabberwerx.util.app._jwappinst = jabberwerx.app.loadApp();
                });
            });
            return true;
        });
        
        jabberwerx.$(window).bind("unload", function() {
            try {
                jabberwerx.app.saveApp();
            } catch(e) {
                jabberwerx.util.debug.log('Exception persisting application : ' + e.message);
            }
            jabberwerx.util.app._jwappinst = null; //refs are no longer valid
        });
        return appClass;
    }

         /**
         * <p>Load or create className jabberwerx.ui.JWApp class.  Insures {@link jabberwerx.ui.JWApp#appInitialize} is called.
         *  Applications that do not want their lifetime managed via {@link jabberwerx.app#persistedApplicationClass}
         *  should use this function and <b>not</b> call {@link jw#loadGraph} directly. </p>
         *
         * @param   {String|null} [appClass] The name of the jabberwerx.ui.JWApp subclass that should be created or loaded. appClass is used
         *      as the storage key. If null this method attempts to load the managed application.
         *  @return {jabberwerx.JWBase} The newly loaded or created application
         *  @see jabberwerx.app#persistedApplicationClass
         */   
    jabberwerx.app.loadApp = function(className) {
        var appInst = null;
        var cn = className;
        //if no class given, assume global
        if (!cn) {
            cn = jabberwerx.util.app._jwappclass;
        }
        if (cn) {
            appInst = jabberwerx.util.loadGraph(cn);
            if (!appInst) {
                eval('appInst = new ' + cn + '()');
            }
            appInst.appInitialize();
        }
        return appInst;
    };

         /**
         * <p>save the given jabberwerx.ui.JWApp instance to store using object's classname as key. </p>
         *
         * @param   {jabberwerx.JWBase|null} [appInst] The JWBase object instance. SHould be jabberwerx.ui.JWApp instance but
         *  nothing prevents other jabberwerx.JWBase (persistable) objects from being stored. Uses {@link JWBase#classname} as
         *  store key.
         *  @see jabberwerx.app#persistedApplicationClass
         */   
    jabberwerx.app.saveApp = function (appInst){
        //if no app inst given,. assume global
        var ai = appInst;
        if (!ai) {
            ai = jabberwerx.util.app._jwappinst;
        }
        if (ai) {
            jabberwerx.util.saveGraph(ai, ai.getClassName());
            if (!appInst) {
                jabberwerx.util.app._jwappinst = null;
            }
        }
    };

    if (jabberwerx.util.app) {
        jabberwerx.util.app._jwappclass = '';
        jabberwerx.util.app._jwappinst = null;
    }
})();
