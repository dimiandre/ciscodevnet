/**
 * filename:        JWModel.js
 * created at:      2008/10/31T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2010 Cisco Systems, Inc.  All Rights Reserved.
 */
if (jabberwerx) {

    jabberwerx.JWModel = jabberwerx.JWBase.extend(/** @lends jabberwerx.JWModel.prototype */{
        /**
         * @class
         * @minimal
         * <p>Base class for Model objects.</p>
         *
         * @description
         * Creates a new jabberwerx.JWModel object.
         * <p>A jabberwerx.JWModel object assumes it will be persisted in any saved object
         * graphs, and assumes it is a source of events.</p>
         *
         * @see jabberwerx.JWBase#shouldBeSavedWithGraph
         * @constructs jabberwerx.JWModel
         * @extends jabberwerx.JWBase
         */
        init: function() {  },
        /**
         * Determines if this object should be persisted in object graphs.
         * This method always returns <tt>true</tt>.
         *
         * @return  {Boolean} Always <tt>true</tt>
         */
        shouldBeSavedWithGraph: function() { return true; }
    }, 'JWModel');
};
