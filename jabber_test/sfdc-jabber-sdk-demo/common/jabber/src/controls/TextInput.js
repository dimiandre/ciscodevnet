/**
 * filename:        TextInput.js
 * created at:      2009/06/25T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.TextInput = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.TextInput.prototype */{
        /**
         * @class
         * <p>A TextInput widget is a GUI mechanism to capture input from
         * the user and allow the containing View to do the appropriate
         * action with it.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.TextInput">jabberwerx.ui.TextInput</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new TextInput.</p>
         *
         * @constructs jabberwerx.ui.TextInput
         * @extends jabberwerx.ui.JWView
         */
        init: function() {
            this._super();

            this.applyEvent("textSend");
            this.applyEvent("textTypingStarted");
            this.applyEvent("textTypingEnded");
            this.applyEvent("textTypingIdled");
        },
       /**
         * <p>Retrieves or changes the width of this view.</p>
         *
         * <p>If {w} is not defined, this method returns the current width of
         * this view, in pixels. Otherwise the width is changed to the value of
         * {w} in pixels (if a Number) or the CSS value (if a String),
         * {@link #update} is called (unless {noupdate} is <tt>true</tt>), and
         * this jabberwerx.ui.JWView is returned.</p>
         *
         * <p>If this view is not currently rendered, this method returns 0 for
         * retrieval, and ignores {w} for changes.</p>
         *
         * @param   {Number|String} [w] The new width (or <tt>undefined</tt> to
         *          retrieve the current width)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current width (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#dimensions
         */
        width: function(w, noupdate) {
            var res = this._super(w, true);
            if (w !== undefined) {
                this._configureControl();
                !noupdate && this.update();
            }

            return res;
        },
        /**
         * <p>Retrieves or changes the height of this view.</p>
         *
         * <p>If {h} is not defined, this method returns the current height of
         * this view, in pixels. Otherwise the height is changed to the value of
         * {h} in pixels (if a Number) or the CSS value (if a String),
         * {@link #update} is called (unless {noupdate} is <tt>true</tt>), and
         * this jabberwerx.ui.JWView is returned.</p>
         *
         * <p>If this view is not currently rendered, this method returns 0 for
         * retrieval, and ignores {h} for changes.</p>
         *
         * @param   {Number|String} [h] The new height (or <tt>undefined</tt> to
         *          retrieve the current height)
         * @param   {Boolean} [noupdate] <tt>true</tt> if the view should
         *          <b>NOT</b> be updated
         * @returns {Number|jabberwerx.ui.JWView} The current height (if retrieving); or this
         *          jabberwerx.ui.JWView (if changing)
         * @see     jabberwerx.ui.JWView#dimensions
         */
        height: function(h, noupdate) {
            var res = this._super(h, true);
            if (h !== undefined) {
                this._configureControl();
                !noupdate && this.update();
            }
            return res;
        },

        /**
         * <p>This adjusts the size of the widget to display
         * correctly.</p>
         */
        update: function() {
            if (!this._super()) {
                return false;
            }
            this._configureControl();
            return true;
        },

        /**
         * <p>Creates the DOM for the TextInput widget which consists of
         * a text input field and a button.</p>
         *
         * @param {Document} doc The document to use for creating content.
         * @returns {jQuery} The element containing the widget.
         */
        createDOM: function(doc) {
            var builder = jabberwerx.$("<div/>", doc).addClass("chat_input_wrapper");

            jabberwerx.$("<div/>").appendTo(builder).
                        addClass("send_button").
                        text("Send").
                        click(this.invocation("_send"));

            jabberwerx.$("<textarea/>").appendTo(builder).
                             addClass("chat_input").
                             attr("rows", "3").
                             bind("keypress", this.invocation("_handleKeyPress")).
                             bind("keyup", this.invocation("_handleKeyUp"));

            return builder;
        },
        /**
         * <p>Restores the DOM for the TextInput widget.</p>
         *
         * @param {Document} doc The document to use for creating content.
         * @returns {jQuery} The element containing the widget.
         */
        restoreDOM: function(doc) {
            var res = this.createDOM(doc);
            res.find(".chat_input").val(this._persistedText);
            return res;
        },

        /**
         * <p>Clears out all of the objects created by createDOM.</p>
         */
        destroyDOM: function() {
            if (this._timer) {
                clearTimeout(this._timer);
                this._timer;
            }
        },

        willBeSerialized: function() {
            if (this.jq) {
                this._persistedText = this.jq.find(".chat_input").val();
            } else this._persistedText = "";

            //reset typing indicator on a save/load
            if (this._timer) {
                clearTimeout(this._timer);
                this._timer = null;
            }
            this._count = 0;
            return this._super();
        },

        /**
         * <p>Gets or sets the text in the text input field.</p>
         *
         * <p>If {val} is not defined, this method returns the current
         * text in the text input field. Otherwise the text is changed
         * to the value of {val}.
         *
         * @param {String} [val] The new text to be put in the text input
         * field.
         * @returns {String|jabberwerx.ui.JWView} The current text in the text input field
         *          or this jabberwerx.ui.JWView if the text is being set.
         */
        text: function(val) {
            var textArea = this.jq.find(".chat_input");
            if (val === undefined) {
                return (textArea && textArea.val()) || "";
            }
            if (textArea) {
                textArea.val(val);
                this.update();
            }

            return this;
        },

        /**
         * @private
         */
        _send: function() {
            var textArea = this.jq.find(".chat_input");
            var data = textArea.val();
            var result = data.length > 0 && jabberwerx.reduce(
                    this.event("textSend").trigger(data) || [],
                    function(item, val) {
                        return val || Boolean(item);
                    },
                    false);

            if (result) {
                if (this._timer) {
                    clearTimeout(this._timer);
                    this._timer = null;
                }
                this._count = 0;
                textArea.val("");

            }

            textArea.focus();
        },

        /**
         * @private
         */
        _handleKeyPress: function(evt) {
            if (evt.keyCode == 13) {
                //enter key pressed
                this._send();
                return false;
            }

            return true;
        },

        /**
         * @private
         */
        _handleKeyUp: function(evt) {
            var data = this.jq.find(".chat_input").val();
            var count = data.length;

            if (this._count == 0 && count != 0) {
                //started typing
                this.event("textTypingStarted").trigger(data);
                if (this._timer) {
                    clearTimeout(this._timer);
                    this._timer = null;
                }
                if (this.idleTime > 0) {
                    this._timer = setTimeout(
                            this.invocation("_handleIdle"),
                            this.idleTime * 1000);
                }
            } else if (this._count != 0 && count == 0) {
                //stopped typing
                this.event("textTypingEnded").trigger(data);
                if (this._timer) {
                    clearTimeout(this._timer);
                    this._timer = null;
                }
            }

            this._count = count;
        },

        /**
         * @private
         */
        _handleIdle: function(evt) {
            this.event("textTypingIdled").trigger();
            this._timer = null;
            this._count = 0;
        },

        _configureControl: function() {
            if (!this.jq) return this;

            var w = this.width(), h = this.height();
            if (!w || !h) return this; //if 0 in one or more dims,  ignore
            // recalculate the left/right of internal elements...
            var delta = (this.jq.outerWidth(true) - w) / 2;

            var textArea = this.jq.find(".chat_input");
            textArea.css("left", delta);

            var sendButton = this.jq.find(".send_button");
            sendButton.css("right", delta);

            // recalculate the bottom of internal elements...
            delta = (this.jq.outerHeight(true) - h) / 2;
            textArea.css("bottom", delta);
            sendButton.css("bottom", delta);

            // recalculate the width of internal elements...
            w = w - sendButton.outerWidth(true);

            delta = (sendButton.outerWidth(true) - sendButton.width()) +
                    (textArea.outerWidth(true) - textArea.width());

            delta += (jabberwerx.$.browser.msie ? 4 : 2);
            textArea.width(w - delta);

            // recalculate height of internal components...
            delta = this.jq.outerHeight(true) - h;
            textArea.height(h - delta - (jabberwerx.$.browser.msie ? 0 : 2));
            
            sendButton.height(textArea.height());
            sendButton.css("line-height", textArea.height() + "px");

            return this;
        },


        _persistedText: null,
        /**
         * @private
         */
        _timer: null,
        /**
         * @private
         */
        _count: 0,

        /**
         * <p>The amount of time left before the chatState changes to Idle.</p>
         */
        idleTime: 30
    }, "jabberwerx.ui.TextInput");
}
