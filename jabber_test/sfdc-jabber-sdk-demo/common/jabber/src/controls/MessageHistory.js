/**
 * filename:        MessageHistory.js
 * created at:      2009/06/25T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */

if (jabberwerx && jabberwerx.ui) {
    jabberwerx.ui.MessageHistory = jabberwerx.ui.JWView.extend(/** @lends jabberwerx.ui.MessageHistory.prototype */{
        /**
         * @class
         * <p>A MessageHistory widget is a GUI mechanism to display past
         * messages for a given View.</p>
         *
         * <p>
         * This class provides the following events:
         * <ul>
         * <li><a href="../jabberwerxEvents.html#jabberwerx.ui.JWView">jabberwerx.ui.JWView</a></li>
         * </ul>
         *
         * @description
         * <p>Creates a new MessageHistory.</p>
         *
         * @constructs jabberwerx.ui.MessageHistory
         * @extends jabberwerx.ui.JWView
         */
        init: function() {
            this._super();
            this.persistOptions(jabberwerx.ui.JWView.persist_html);

            this.applyEvent("historyMessageAppended");
        },

        /**
         * <p>Clears out all of the messages in this widget.</p>
         */
        clearHistory: function() {
            this.jq && this.jq.find("div.chat_incoming").empty();
        },
        
        /**
         * <p>Adds a message to the MessageHistory widget.<p>
         *
         * @param {jabberwerx.Entity} [entity] The entity associated with the message.
         * @param {jabberwerx.Stanza} [msg] The message to be added to this widget.
         */
        addMessage: function(entity, msg) {
            var message = "";
            var cssClassName = "messageView";
            if (msg.isError()) {
                var errorNode = msg.getNode().getElementsByTagName('error').item(0);
                if (errorNode) {
                    message = jabberwerx.errorReporter.getMessage(errorNode);
                }

                cssClassName = cssClassName + " error";
            } else {
                message = msg.getHTML();
                if (!message) {
                    message = msg.getBody();
                }
            }
                
            var msgView = new jabberwerx.ui.MessageView({
                content: message,
                displayName: entity.getDisplayName(),
                timestamp: msg.timestamp,
                cssClassName: cssClassName
            });

            var msgViewHtml = msgView.render();

            this.event("historyMessageAppended").trigger(msgViewHtml);

            with (this.jq.find("div.chat_incoming")) {
                append(msgViewHtml);
            }        
            this._scrollToBottom();
        },

        /**
         * <p>Changes or removes the badge for this MessageHistory. The badge
         * is extra state information to alert the user to (e.g. chat state
         * changes).</p>
         *
         * @param   {String|jQuery|Element|null} [data] The badge data, or
         *          <tt>null</tt> to remove badge
         */
        setBadge: function(data) {
            if (this.jq) {
                var badge = this.jq.find("div.chat_badge");

                if (!data) {
                    badge.remove();
                } else {
                    if (badge.length) {
                        badge.empty();
                    } else {
                        badge = jabberwerx.$("<div/>", this.jq.get(0).ownerDocument).
                            addClass("chat_badge").
                            appendTo(this.jq);
                    }
                    badge.append(data);
                }
            }
        },

        /**
         * <p>This adjusts the size of the widget to display
         * correctly.</p>
         * @returns <tt>false</tt> if inherited returned false (if update was called before DOM has been created)
         */
        update: function() {
            if (!this._super()) {
                return false;
            }
            this._configureControl();
            this._scrollToBottom();
            return true;
        },

        /**
         * <p>Creates the DOM for the MessageHistory widget.</p>
         *
         * @param {Document} [doc] The document to use for creating content.
         * @returns {jQuery} The element containing the newly created content.
         */
        createDOM: function(doc) {
            var builder = jabberwerx.$("<div/>", doc).addClass("chat_wrapper");
            jabberwerx.$("<div/>").addClass("chat_incoming").
                    appendTo(builder);

            return builder.get(0);
        },
        /**
         * <p>Restores the size of the widget to display correctly.</p>
         * @returns Always <tt>true</tt>
         */
        restoreUpdate: function() {
            this._configureControl();
            this._scrollToBottom();
            return true;
        },
        
        /**
         * @private
         * <p>scroll to bottom of chat div</p>
         */
        
        _scrollToBottom: function() {
            if (this.jq) {
                with (this.jq.find("div.chat_incoming")) {
                    get(0).scrollTop = get(0).scrollHeight;
                }                    
            }
        },
        
        /**
         * @private
         * <p>update hieght and width of chat div to account for css</p>
         */
        _configureControl: function() {
            if (!this.jq) return this;
            var w = this.jq.width(), h = this.jq.height();
            if (!w || !h) return this;

            //padding & margins
            var div = this.jq.find("div.chat_incoming");

            var delta = div.outerHeight(true) - div.height();
            div.height(h - Math.max(delta, 0));

            delta = div.outerWidth(true) - div.width();
            div.width(w - Math.max(delta, 0));
        }

    }, "jabberwerx.ui.MessageHistory");
}
