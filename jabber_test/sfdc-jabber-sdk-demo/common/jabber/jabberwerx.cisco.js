/**
 * filename:        jabbwerwerx.cisco.js
 * created at:      2009/10/09T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
/**
 *
 *A dependency loader that loads the actual cisco extension in the current document.
 *   This file is replaced by the build during compilation as dependancies are
 *
**/
;(function() {
    // our build script will read this list and use it to order the compiled js file.
    var loader = {
        dependencies : function () {
            var result = [];
            result = result.concat([
                'src/jabberwerx.cisco.js', //actual cisco extension impl
                'src/controller/QuickContactController.js',
                'src/model/QuickContact.js'
            ]);
            return result;
        },
        path: function(ssFile) {
            var scripts = document.getElementsByTagName("script");
            var result =  './';

            for (var i = 0; i < scripts.length; i++) {
                if (scripts.item(i).src && scripts.item(i).src.match(ssFile)) {
                    result = scripts.item(i).src.replace(ssFile,'');
                    break;
                }
            }
            return result;
        },
        load: function() {
            var deps = this.dependencies();
            if (deps.length > 0) {
                var srcRoot = this.path('jabberwerx.cisco.js');

                for (var i = 0; i < deps.length; i++) {
                    if (!deps[i]) {
                        continue;
                    }                
                    document.write('<script type="text/javascript" src="' + srcRoot + deps[i] +
                                    '"></script>');
                }
            }
        }
    };
        
    loader.load();
})();
