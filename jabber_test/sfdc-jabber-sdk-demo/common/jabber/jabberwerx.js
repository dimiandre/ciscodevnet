/**
 * filename:      jabberwerx.js
 * created at:      2009/??/??T00:00:00-06:00
 *
 * Copyrights
 *
 * Portions created or assigned to Cisco Systems, Inc. are
 * Copyright (c) 2009 Cisco Systems, Inc.  All Rights Reserved.
 */
/**
 *
 *	A dependency loader that loads the actual jabberwerx library in the current document. 
 *    	This file is replaced by the build during compilation as dependancies are 
 *
**/
;(function() {
	// our build script will read this list and use it to order the compiled js file.	
    var loader = {
        dependencies : function () {
            var result = [].concat([
                'src/third_party/jquery/jquery-1.4.2.js',			
                'src/third_party/jquery/jquery.jstore-all.js',
                'src/jabberwerx.js',
                'src/util/crypt.js',
                'src/util/validate.js',
                'src/jwapp/JWCore.js',
                'src/jwapp/JWPersist.js',
                'src/jwapp/JWModel.js',
                'src/model/SASL.js',
                'src/model/SASLMechs.js',
                'src/model/Translator.js',
                'src/model/JID.js',
                'src/model/Events.js',
                'src/model/Entity.js',
                'src/model/Stanza.js',
                'src/model/User.js',
                'src/model/Stream.js',
                'src/model/Client.js',
                'src/model/MUCRoom.js',
                'src/model/Server.js',
                'src/model/Contact.js',
                'src/model/TemporaryEntity.js',
                'src/model/ErrorReporter.js',
                'src/model/ChatSession.js',
                'src/model/PrivacyList.js',
                'src/model/PubSubNode.js',
                'src/model/PEPNode.js',
                'src/model/XDataForm.js',
                'src/model/XDataFormField.js',                
                'src/model/XDataFormItem.js',                
                'src/model/MUCInvite.js',
                'src/controller/Controller.js',
                'src/controller/RosterController.js',
                'src/controller/CapabilitiesController.js',
                'src/controller/ChatController.js',
                'src/controller/MUCController.js',
                'src/controller/PrivacyListController.js',
                'src/controller/PrivateStorageController.js',
                'src/controller/DiscoController.js',
                'src/controller/PubSubController.js',
                'src/controller/PEPController.js'
            ]);
            
            return result;
        },
        path: function(ssFile) {
            var scripts = document.getElementsByTagName("script");
            var result =  './';
            
            for (var i = 0; i < scripts.length; i++) {	    
                if (scripts.item(i).src && scripts.item(i).src.match(ssFile)) {				
                    result = scripts.item(i).src.replace(ssFile,'');
                    break;
                }
            }
            return result;	
        },        
        load: function() {
            var deps = this.dependencies();
            if (deps.length > 0) {
                var srcRoot = this.path('jabberwerx.js');
                
                for (var i = 0; i < deps.length; i++) {
                    if (!deps[i]) {
                        continue;
                    }                
                    document.write('<script type="text/javascript" src="' + srcRoot + deps[i] + 
                                    '"></script>');
                }
            }
        }
    };
	
	loader.load();
})();
