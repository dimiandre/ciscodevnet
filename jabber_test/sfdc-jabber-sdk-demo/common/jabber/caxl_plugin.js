
/**
 jQuery plugin to wrap CAXL behaviour.
 
 
  
 */


(function( $ ){
	var client = new jabberwerx.Client();
	jabberwerx._config.httpBindingURL = "http://gwydlvm151:9080/hub20/alphahttpbinding";
	jabberwerx._config.unsecureAllowed = true;
    jabberwerx._config.persistDuration = 0;
	
	
	phone.setOnCallEnded( function(callInfo) {
		callMap.get(callInfo.callId).trigger('callended.CWC', callInfo);
	});
	
	phone.setOnCallUpdated( function(args) {
		// Match the top-level DOM element by call id and store for later.
		var $call = callMap.get(args.call.callId);
		// Defensively check to see if user has remembered to bind their div to the call
		if($call.size() == 0) {
			console.error("A call update event has been fired for call ID: " + args.call.callId + 
                         " but there are no DOM elements bound to that call ID." + 
                         " Please use bindCall() to associate your DOM element with the call.");
            return;
		}	
		
		// Trigger a callupdated event. If no-one is listening, log a warning.
		if($call.trigger('callupdated.CWC', args.call).size() == 0) {
			console.warn("A call update event has been fired for call ID: " + callInfo.callId + 
						 " but there are no DOM elements bound to that call ID." + 
						 " Please use bindCall() to associate your DOM element with the call.");
		}
		
	});
	
	// Raise a call created event. This method is usd by both the onCallCreated handler
	// and the onAnswered handler since we want our users' UI code to behave the same way.
	var raiseCallCreatedEvent = function( $this, callInfo) {
		// Defensively trigger a callupdate event but if there are call divs open already for
		// this call then do nothing. This is because we can get multiple callCreated events from CWC
		if(callMap.get(callInfo.callId).trigger('callupdated.CWC', callInfo).size() > 0) return;
		$this.trigger('callcreated.CWC', callInfo);
	};
	
	// The public methods available in the plugin
	var methods = {
		// Make a call to dn
		makeCall: function( dn ) {
			phone.originate( dn );
			//Note: doesn't return 'this' because it doesn't need to be chainable
		},
		// End Call
		endCall: function( callId ) {
			phone.endCall(); // TODO: No IDs in API yet
			//Note: doesn't return 'this' because it doesn't need to be chainable
		},
		answerCall: function( callId ) {
			phone.answer(); // TODO: No IDs in API yet
			//Note: doesn't return 'this' because it doesn't need to be chainable
		},
		// Associate the callInfo with the DOM node
		bindCall : function( callInfo ) {
			// TODO: Check callInfo
			return this.each(function() {
				callMap.put(callInfo.callId, this);
			}); 
		},
		// Insert the webphone instance, and configure it with config
		insert : function( config ) {
			$(document.body).append(phone.getObjectTag());
			setTimeout( function() {
				phone.start(config);
			},500);
			return this.each(function() {
				var $this = $(this);
				phone.setOnCallCreated( function(callInfo) {
					raiseCallCreatedEvent($this, callInfo);	
				});
				phone.setOnRinging( function(callInfo) {	
					$this.trigger('incomingcall.CWC', callInfo);					
				});
				
				phone.setOnRegistrationSuccess( function() {
					$this.trigger('loginsuccess.CWC');
				});
				
				phone.setOnLoaded( function() {
					phone.shortLogin();
				});
					
				phone.setOnCallAnswered( function(callInfo) {
					raiseCallCreatedEvent($this, callInfo);		
				});
			});
		}
	};
	
	$.fn.CAXL = function( method ) { 
		// Method calling logic
		if ( methods[method] ) {
			return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		} else {
			$.error( 'Method ' +  method + ' does not exist on jQuery.CAXL' );
		}    		
	};
})( jQuery );