$_org(document).ready(
	function() {
		/** CAXL init **/
		
		loadJabberUsers();
		
		client = initCAXL();
		
		controller = new jabberwerx.RosterController(client);
		chatController = new jabberwerx.ChatController(client);
		chatController.event("chatSessionOpened").bind (function (ev) {
			ev.data.chatSession.event("chatReceived").bind(function(ev) {				
				var msg = ev.data; 
				var jid = msg.getFromJID().getBareJIDString();
				openChat(jid, jid);								
				
				var txt = null;
				var cebt = false;
				var xmlDoc = msg.getDoc();
				
				var cebtNodesList = xmlDoc.getElementsByTagNameNS ?
					cebtNodesList = xmlDoc.getElementsByTagNameNS('http://www.cisco.com/cebt/platform', 'notification') :
					cebtNodesList =  xmlDoc.getElementsByTagName("notification");
					
				if (cebtNodesList && cebtNodesList.length > 0) {
					if (cebtNodesList[0] && 
				    	    cebtNodesList[0].firstChild && 
				    	    $_org(cebtNodesList[0]).attr("xmlns") == 'http://www.cisco.com/cebt/platform') {
						txt = $_org(cebtNodesList[0].firstChild).text();
					}
				}
				
				if (!txt) {
					txt = msg.getHTML() ? msg.getHTML() : msg.getBody();
				}
				else {
					cebt = true;
				}

				printMessageOnPage(getTabsUIObject(jid), txt, false, cebt);					
			});
			
			var jid = ev.data.chatSession.jid;
			var entity = client.entitySet.entity(ev.data.chatSession.jid.getBareJIDString());			
			openChat(entity ? entity.getDisplayName() : jid.getBareJIDString(), jid.getBareJIDString(), ev.data.chatSession);
		});
		client.event("presenceReceived").bind(function(evt) {
			var jidStr = evt.data.getFromJID().getBareJIDString();
			var user = getUserByJID(jidStr);
			if (user) {
				//$_org(".presence", user.container).each( 
				$_org("." + user.jabberId.replace(/[@\.]+/g, "") + " > .presence").each(
					function() {
						var presenceData = translatePresence(evt.data);							
						$_org(this).removeClass().addClass(presenceData.clazz + " presence");
						$_org(this).attr("title", presenceData.type + " - " + user.jabberId);
					}
				);
			}
		});
		
		
		$_org( "#chatDialog" ).dialog({
			autoOpen: false,
			show: "",
			hide: "",
			width: "400",
			height: "600",
			resizable: false,
			dialogClass: "chatDialog"
			//,
			//position: [-1000, -1000]
		});
		
		
		$_org( "#chatTabs" ).tabs(
		{
			//options
			panelTemplate : $_org(".tab-panel-template").val(),
			tabTemplate : $_org(".tab-button-template").val(),

			//event handlers
			add: function(event, ui) {				
				var jid = $_org(".chat-tab-label-text", ui.tab).text();
				
				var user = getUserByJID(jid);
				var phone = null;
				
				var entity = client.entitySet.entity(jid);				 
				var displayName = entity ? entity.getDisplayName() : jid;
				
				if (user) {
					phone = user.phone;
					if (phone) {
						$_org(ui.tab).parent().data("phone", phone);
						$_org(ui.panel).data("phone", phone);						

						$_org(".phone-number", $_org(ui.panel)).html(phone);
						$_org(".phone-action > button", $_org(ui.panel)).html("Dial");
						$_org(".phone-action > button", $_org(ui.panel)).click(function() {
							makeCall(displayName, jid, phone);
						});	
					}
				}
				if (!user || !phone) {
					$_org(".phone-number", $_org(ui.panel)).html("&lt;phone not set&gt;&nbsp;&nbsp;");
					$_org(".phone-action > button", $_org(ui.panel)).html("Dial");
					$_org(".phone-action > button", $_org(ui.panel)).attr("disabled", "true");
				}
				 
				$_org(".chat-tab-label-text", ui.tab).text(displayName);
				var presenceContainer = buildPresenceContainer(jid, client.getPrimaryPresenceForEntity(jid));
				$_org(ui.tab).prepend(presenceContainer);	
				
				$_org(ui.panel).data("jid", jid);
				$_org(ui.tab).parent().data("jid", jid);
								
				
				$_org( "span.ui-icon-close", $_org(ui.tab).parent() ).click(function(event) {
					//prevent adding #tab-XYZ to the url
					event.preventDefault();
					
					var tabs = $_org( "#chatTabs" );
					var index = $_org( "li", tabs ).index( $_org( this ).closest("li"));
					$_org( "#chatTabs" ).tabs( "remove", index );
				});
				
				$_org("input.chat-tab-content-input", $_org(ui.panel)).keypress(function(evt) {     
					if(evt.keyCode == 13) 
					{
						var msg = $_org(this).val();
						$_org(this).val('');
						printMessageOnPage(ui, msg, true);						
						var session = chatController.getSession(jid);
						session.sendMessage(msg);
						
					}
				});				
				$_org( "#chatTabs" ).tabs("select", ui.index);
			}
		});
		

		
		/** Audio init **/
		
		initCWIC('#phonecontainer');
		
		$_org('#phonecontainer')
			.bind('conversationStart.cwic', handleConversationStart)			
			.bind('conversationIncoming.cwic', handleConversationIncoming)			
		 	.bind('system.cwic', handleSystemEvent);		
		
		$_org(document).bind('error.cwic', handleError);

		
		 $_org('input:radio[name=showlocalvideo]').click(switchPreviewVideo);

		$_org('#callcontainer')
			.bind('conversationUpdate.cwic', handleConversationUpdate)
			.bind('conversationEnd.cwic', handleConversationEnd);
		 
		enterOnHookState();
});


function execute(source, command) {
	var jsComm = $_org.parseJSON(unescape(command));
	if (jsComm.type == "openchat") {
		openChat(jsComm.user.name, jsComm.user.jabberId);
	}
	if (jsComm.type == "makecall") {
		makeCall(jsComm.user.name, jsComm.user.jabberId, jsComm.user.phone);
	}
}

function openChat(name, jid, session) {
	if (!session && !chatController.getSession(jid)) {
		try {
			session = chatController.openSession(jid);
		}
		catch (exception) {
			alert('Cannot open chat session with ' + jid);
			return;
		}
	}
	var ui = getTabsUIObject(jid);
	if (!ui) {
		var randomNum = Math.floor(Math.random() * 100000000);
		var url = "tab-" + randomNum;
		$_org( "#chatTabs" ).tabs('add', '#' + url, jid);
	}
	else {
		$_org( "#chatTabs" ).tabs("select", ui.index);
	}
	setTimeout(function() {
		$_org( "#chatDialog" ).dialog({height:600}); 
		$_org( "#chatDialog" ).dialog("open");	
	}, 10);	
}

function getTabsUIObject(jid) {
	var ui = {
		index : -1,
		tab : null,
		panel : null	
	};
	$_org("li", $_org( "#chatTabs" )).each(function (idx, obj) {
		if ( jid == $_org(this).data("jid") ) {
			ui.index = idx;
			ui.tab = this;
			ui.panel =$_org($_org("a", this).attr("href"))[0];
		}
	});
	return ui.index == -1 ? null : ui; 
}

function styleCEBT(container, ui) {
	$_org( ".cebt-action-button" , container).button();
	$_org( ".cebt-action-button" , container).click(function() {
		var notificationId = $_org(".cebt-notification", container).attr("data-notification-id");
		var actionComment = $_org(this).attr("data-action-comment");
		var actionId = $_org(this).attr("data-action-id");
		//alert(notificationId + "." + actionId);		
		var jid = $_org(ui.panel).data("jid");
		var session = chatController.getSession(jid);
		session.sendMessage(notificationId + "." + actionId + "." + (!actionComment || actionComment == 'undefined'? "" : actionComment));		
	});
	
	$_org(".cebt-recipient-container > .recipient", container).each( function() {
		var userName = $_org(this).text();
		var user = getUser(userName);
		if (user) {
			//var presenceContainer = buildPresenceContainer(user.jabberId, client.getPrimaryPresenceForEntity(user.jabberId));
			var presenceContainer = buildJabberContainer(user, client.getPrimaryPresenceForEntity(user.jabberId));
			$_org(presenceContainer).appendTo(this);
		}
	});
}

function printMessageOnPage(ui, msg, isMe, isCEBT) {
	var p = document.createElement("p");
	if (isCEBT) {
		$_org(p).html(msg);
		styleCEBT(p, ui);
	}
	else {
		//$_org(p).html((isMe ? "me" : "them")  + ": ");
		//$_org(p).html('<img  style="margin-right: 5px;" height="16" width="13" src="./images/'+ (isMe ? 'me' : 'contact') + '.png"/>');
		$_org(p).addClass(isMe ? "chat-me" : "chat-contact");
		$_org(p).append(msg);
	}
	
	$_org(".chat-tab-content-chat", $_org(ui.panel)).append(p);
	$_org(".chat-tab-content-chat", $_org(ui.panel)).scrollTop($_org(".chat-tab-content-chat", $_org(ui.panel)).prop("scrollHeight"));
}


function buildJabberContainer(user, presence) {
	if (!user.jabberId) {
		return;
	}
	
	user.container = document.createElement("span");
	$_org(user.container).addClass("jabber-container");	
	$_org(user.container).addClass(user.jabberId.replace(/[@\.]+/g, ""));
	$_org(user.container).data("jid", user.jabberId);
	$_org(user.container).data("name", name);
	
	var presenceData = translatePresence(presence);	
	var presence = document.createElement("span");
	$_org(presence).html('&nbsp;');
	$_org(presence).addClass(presenceData.clazz + " presence");
	$_org(presence).attr("title", presenceData.type + " - " + user.jabberId);
	$_org(presence).on("click", function(event) {
		event.preventDefault();
		event.stopPropagation();
	});			
	$_org(user.container).append(presence);
	
	
	if (user.phone) {
		var clickcall = document.createElement("span");
		$_org(clickcall).html('&nbsp;');
		$_org(clickcall).addClass("clickcall");
		$_org(clickcall).attr("title", "Call to " + user.phone ? user.phone : "<number not set>");
		$_org(clickcall).on("click", function(event) {
			event.preventDefault();
			event.stopPropagation();
			makeCall(user.name, user.jabberId, user.phone);
			return false;
		});
		$_org(user.container).append(clickcall);
	}

	var clickim = document.createElement("span");
	$_org(clickim).html('&nbsp;');
	$_org(clickim).addClass("clickim");	
	$_org(clickim).attr("title", "Click To Chat");
	$_org(clickim).on("click", function(event) {
		event.preventDefault();
		event.stopPropagation();
		openChat(user.name, user.jabberId);
		
		return false;
	});
	
	$_org(user.container).append(clickim);
	return user.container;
}

function buildPresenceContainer(jid, presence) {
	var container = document.createElement("span");
	$_org(container).addClass("jabber-container");	
	$_org(container).addClass(jid.replace(/[@\.]+/g, ""));
	$_org(container).data("jid", jid);
	$_org(container).data("name", name);
	
	var presenceData = translatePresence(presence);	
	var presence = document.createElement("span");
	$_org(presence).html('&nbsp;');
	$_org(presence).addClass(presenceData.clazz + " presence");
	$_org(presence).attr("title", presenceData.type + " - " + jid);
	$_org(container).append(presence);	
	return container;
}

/***** PHONE *****/
var videoObject = null;
var previewVideoObject = null;
var showLocalVideo = false;

function switchPreviewVideo(evt) {
    if(showLocalVideo !== evt.target.value) {
    	var previewVidContainer = $_org("#localvidcontainer");
        showLocalVideo = evt.target.value;        
        if(showLocalVideo === "On") {
        	console.log('localvideo - on');
        	previewVidContainer.show();
            $_org(document).cwic('addPreviewWindow',{previewWindow: previewVideoObject});
        } else {
        	console.log('localvideo - off');        	
        	$_org(document).cwic('removePreviewWindow',{previewWindow: previewVideoObject});
        	previewVidContainer.hide();
        }
    }
}


function buildVideoWindows() {
	$_org('#remotevideocontainer').css('display', '');
	$_org('#localPreviewContainer').css('display', '');
	
	$_org('#remotevideocontainer').cwic('createVideoWindow',{id: 'videocallobject', success: function(id) {
        //videoObject = $_org('#'+id)[0];
        videoObject = $_org('#remotevideocontainer').children()[0];
        videoObject.addEventListener('videofocus', function() {
            console.log("caught video object focus");
            setTimeout(function() {$_org('#videocontainer').focus();}, 250);
        },true);
   }});
	$_org('#localvidcontainer').cwic('createVideoWindow',{id: 'localPreviewVideo', success: function(id) {
		//previewVideoObject = $_org('#'+id)[0];
		previewVideoObject = $_org('#localvidcontainer').children()[0];
		previewVideoObject.addEventListener('videofocus', function() {
           console.log("caught local video object focus");                	
           setTimeout(function() {$_org('#localvidcontainer').focus();}, 250);

       },true);
   }});
}

function removeVideoWindows() {
	$_org('#localvidcontainer').empty();
	$_org('#remotevideocontainer').empty();
	
	$_org('#remotevideocontainer').css('display', 'none');
	$_org('#localPreviewContainer').css('display', 'none');
}

function initCWIC (id) {
    var settings = {
        ready: /* Callback when phone is ready for use */ function(defaults,phoneRegistered, phoneMode) {
        	$_org(this).cwic('registerPhone', {        	
				user: _ucm_user,
				password:  _ucm_password,
				//password:  "",
				cucm: _ucm_address,
				success: function() {	
					alert("Phone available");
					$_org(document).cwic('addPreviewWindow',{previewWindow: previewVideoObject});
				}
			});	
        },
        error: /* Error callback */ function() {
        	alert('Phone cannot be initialized');
        },
        encodeBase64: ciscobase.util.crypto.b64Encode,
        verbose: true,
        log: function (msg, context) {
            //console.trace();
            if (typeof console !== "undefined" && console.log) {
                console.log(msg);
                if (context) {
                    if(console.dir) {
                        console.log(context);
                    } else if(window.JSON && window.JSON.stringify) {
                        console.log(JSON.stringify(context,null,2));
                    } else {
                        console.log(context);
                    }
                }
            };
        }
	};	
	$_org(id).cwic('init', settings);
	return $_org(id);
}

function makeCall(name, jid, phone) {
	$_org( "#chatTabs" ).tabs("select", 0);
	$_org( "#chatDialog" ).dialog("open");
	
	buildVideoWindows();
	
	var phonePanel = $_org("#phone-panel", $_org( "#chatTabs" ));
	$_org(".phone-number-input", phonePanel).val(phone);
	if (!phone || isNaN(phone)) {
		alert("Invalid number");
		return;
	}
	
	//enterInCallState();
	callObj = { participant: { recipient: phone }, videoDirection : 'SendRecv' };
	 if(videoObject && videoObject.windowhandle && videoObject.windowhandle !== "00000000") {
		 callObj.remoteVideoWindow = videoObject;
     } else {
    	 callObj.remoteVideoWindow = 'videocallobject';
     }

	$_org("#callcontainer").cwic('startConversation', callObj);	
}

function enterInCallState(conversation) {
	var phonePanel = $_org("#phone-panel", $_org( "#chatTabs" ));
	$_org(".phone-number-input", phonePanel).attr("disabled", "disabled");
	var actionBtn = $_org(".phone-action > button");
	$_org(actionBtn).off("click");
	$_org(actionBtn).click(function() {
		if (conversation) {
			$_org("#callcontainer").cwic('endConversation', conversation.id);
		}
	});
	$_org("#phone-status").html(conversation ? conversation.state : "");
	actionBtn.html("Disconnect");
    $_org('#callcontainer').show();
}

function enterOnHookState(conversation) {
	var phonePanel = $_org("#phone-panel", $_org( "#chatTabs" ));
	$_org(".phone-number-input", phonePanel).removeAttr("disabled");
	var actionBtn = $_org(".phone-action > button");
	$_org(".phone-action > button").html("Dial");
	$_org(actionBtn).off("click");
	$_org(actionBtn).click(function() {
		var number = $_org(".phone-number-input").val();
		if (number == '') {
			return;
		}
		makeCall(null, null, number);
	});
	$_org("#phone-status").html(conversation ? conversation.state : "");
	actionBtn.html("Call");
}

function handleConversationUpdate(event, conversation, containerdiv) {
	$_org("#phone-status").html(conversation.state);
	 if(conversation && conversation.callState === "Connected" && (conversation.videoDirection === "RecvOnly" ||  conversation.videoDirection === "SendRecv")) {
		 if(videoObject) {
             $_org('#callcontainer').cwic('updateConversation',{'addRemoteVideoWindow':videoObject});
         }
     }
}

function handleConversationStart(event, conversation, containerdiv) {
	enterInCallState(conversation);
}

function handleConversationEnd(event, conversation, containerdiv) {
	//containerdiv.remove();
	enterOnHookState(conversation);	
	removeVideoWindows();
}


function handleConversationIncoming(event, conversation, containerdiv) {
	containerdiv
    .bind('conversationUpdate.cwic', function(event, conversation) {
        console.log('state in update=' + conversation.state);
    });
	
	$_org('body').append(containerdiv);
    containerdiv.bind('conversationEnd.cwic', function(event, conversation) {
       containerdiv.unbind().remove();
       console.log('Unknon Div Ended call id:' + conversation.id);
	   $_org('#callcontainer').hide();
   });

    var answerObject = $_org.extend(conversation,{videoDirection: 'SendRecv'});
    if(videoObject) {
        answerObject.remoteVideoWindow = videoObject;
    } else {
        answerObject.remoteVideoWindow = 'videocallobject';
    }
    
	$_org( "#chatTabs" ).tabs("select", 0);
	$_org('#callcontainer').show();
	$_org( "#chatDialog" ).dialog("open");
	
	buildVideoWindows();
    
    $_org('#callcontainer').cwic('startConversation', answerObject);
    enterInCallState(conversation);
}

function handleError() {
	alert("error");
}

function handleSystemEvent(event) {
    //alert('got here');
	var reason = event.phone.status || null;
    console.log('system event with reason=' + reason);
    if (reason == 'eRecoveryPending') {
        console.log('recovery pending, end any active call and disable make call');
    }
    else if (reason == 'eIdle') {
        //handleLoggedOut();
    	console.log('eIdle');
    }
    else if (reason == 'eReady') {
        console.log('phone is ready, enable make call');
        //setUILoggedIn();
    }
	else if (reason == 'ePhoneModeChanged') {
		$_org('#mode').val(event.phone.registration.mode);
	}
    else {
        console.log('ignoring system.cwic event with reason=' + reason);
    }
};