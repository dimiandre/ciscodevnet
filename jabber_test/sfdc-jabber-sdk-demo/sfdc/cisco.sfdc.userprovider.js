var cisco_users = new Array();

function loadJabberUsers() {
	sforce.connection.sessionId = _my_sfdcSession;
	var result = sforce.connection.query('select name, 	Jabber_ID__c, 	Jabber_Password__c, Extension from User');
	it = new sforce.QueryResultIterator(result);
	while (it.hasNext()) {
		var rec = it.next();
		//cisco_users[rec.Name] = { 
		cisco_users.push({
				name : rec.Name,
				jabberId : rec.Jabber_ID__c ? rec.Jabber_ID__c : null,
				jabberPwd : rec.Jabber_Password__c ? rec.Jabber_Password__c : null,
				phone : rec.Extension ? rec.Extension : null,
				container : null
			});
	}
}

function getUserPhoneByJID(jid) {
	for (var i = 0; i < cisco_users.length; i++) {
		if (cisco_users[i].jabberId == jid) {
			return cisco_users[i].phone;
		}
	}
	return null;
}

function getUserByJID(jid) {
	for (var i = 0; i < cisco_users.length; i++) {
		if (cisco_users[i].jabberId == jid) {
			return cisco_users[i];
		}
	}
	return null;
}


function getUser(name) {
	for (var i = 0; i < cisco_users.length; i++) {
		if (cisco_users[i].name == name) {
			return cisco_users[i];
		}
	}
	return null;
}

function usersIterator(containerFactory) {
	$_org(".dataCell:has(a), .dataCol:has(a), .data2Col:has(a), .userMru, .actorentitylink, .hov-id005d00000014UIO, .chat_buddy_list_name").each(
			function(idx) {				
				var user = isUser($_org(this).text());
				if (user) {
					var container = containerFactory(user);
					if (container) {
						alert('b');
						$_org(this).append(container);
					}
				}
			});}

function isUser(txt) {	
	for (var i = 0; i < cisco_users.length; i++) {
		if (txt.indexOf(cisco_users[i].name) == 0) {
			return cisco_users[i];
		}
	}
	return null;
}
