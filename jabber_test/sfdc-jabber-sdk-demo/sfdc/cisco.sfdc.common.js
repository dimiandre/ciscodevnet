var $_org = jQuery.noConflict();

$_org.extend({
	getUrlVars : function() {
		var vars = [], hash;
		var hashes = window.location.href.slice(
				window.location.href.indexOf('?') + 1).split('&');
		for ( var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			vars.push(hash[0]);
			vars[hash[0]] = hash[1];
		}
		return vars;
	},
	getUrlVar : function(name) {
		return $_org.getUrlVars()[name];
	},
	
	stringify : function stringify(obj) {
		var t = typeof (obj);
		if (t != "object" || obj === null) {
			// simple data type
			if (t == "string")
				obj = '"' + obj + '"';
			return String(obj);
		} else {
			// recurse array or object
			var n, v, json = [], arr = (obj && obj.constructor == Array);

			for (n in obj) {
				v = obj[n];
				t = typeof (v);
				if (obj.hasOwnProperty(n)) {
					if (t == "string")
						v = '"' + v + '"';
					else if (t == "object" && v !== null)
						v = jQuery.stringify(v);
					json.push((arr ? "" : '"' + n + '":') + String(v));
				}
			}
			return (arr ? "[" : "{") + String(json) + (arr ? "]" : "}");
		}
	}
});


function initCAXL (eventBindings) {
	client = new jabberwerx.Client(resource);
	jabberwerx._config.httpBindingURL = _cups_address;
	jabberwerx._config.unsecureAllowed = true;
    jabberwerx._config.persistDuration = 0;
    client.connect(_cups_jid, _ucm_password, 
    	{
	    	successCallback: function() {
	    		client.sendPresence("chat", "Using Jabber SDK Client in SFDC");
	    	}
    	}
    );

	return client;
}

function translatePresence(presenceStz) {
	var res = {
		clazz : "offline",
		type : "Offline",
		status : "Offline"
	};
	
	if(presenceStz && (typeof presenceStz != "undefined")) {
		
		if(!presenceStz || (presenceStz.getType() == "unavailable")) {
			res.clazz = "offline";
		}
		else {
			switch(presenceStz.getShow()) {
				case jabberwerx.Presence.SHOW_NORMAL:
				case jabberwerx.Presence.SHOW_CHAT:
					res.clazz = "online";
					res.type = "Online";
					break;
				case jabberwerx.Presence.SHOW_DND:
					res.clazz = "dnd";
					res.type = "Do not disturb";
					break;
				case jabberwerx.Presence.SHOW_AWAY:
				case jabberwerx.Presence.SHOW_XA:
					res.clazz = "away";
					res.type = "Away";
					break;
			}
		}
	}
	return res;
};

