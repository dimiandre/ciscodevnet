$_org(document).ready(
	function() {
		loadJabberUsers();
		usersIterator(buildJabberContainer);
		
		client = initCAXL();
		
		controller = new jabberwerx.RosterController(client);
		chatController = new jabberwerx.ChatController(client);
		client.event("presenceReceived").bind(function(evt) {
			var jidStr = evt.data.getFromJID().getBareJIDString();
			var user = getUserByJID(jidStr);
			if (user) {
				$_org("." + user.jabberId.replace(/[@\.]+/g, "") + " > .presence").each(
					function() {
						var presenceData = translatePresence(evt.data);							
						$_org(this).removeClass().addClass(presenceData.clazz + " presence");
						$_org(this).attr("title", presenceData.type + " - " + user.jabberId);
					}
				);
			}
		});
	}
);


function buildJabberContainer(user) {	
	if (!user || !user.jabberId) {
		return;
	}
	
	user.container = document.createElement("span");
	$_org(user.container).addClass("jabber-container");	
	$_org(user.container).addClass(user.jabberId.replace(/[@\.]+/g, ""));
	$_org(user.container).data("jid", user.jabberId);
	$_org(user.container).data("name", name);
	
	var presenceData = translatePresence(null);	
	var presence = document.createElement("span");
	$_org(presence).html('&nbsp;');
	$_org(presence).addClass(presenceData.clazz + " presence");
	$_org(presence).attr("title", presenceData.type + " - " + user.jabberId);
	$_org(presence).on("click", function(event) {
					event.preventDefault();
					event.stopPropagation();

		//makeCall(user);
	});			
	$_org(user.container).append(presence);
	
	
	if (user.phone) {
		var clickcall = document.createElement("span");
		$_org(clickcall).html('&nbsp;');
		$_org(clickcall).addClass("clickcall");
		$_org(clickcall).attr("title", "Call to " + user.phone ? user.phone : "<number not set>");
		$_org(clickcall).on("click", function(event) {
			event.preventDefault();
			event.stopPropagation();
			
			makeCall(user);
			return false;
		});
		$_org(user.container).append(clickcall);
	}

	var clickim = document.createElement("span");
	$_org(clickim).html('&nbsp;');
	$_org(clickim).addClass("clickim");	
	$_org(clickim).attr("title", "Click To Chat");
	$_org(clickim).on("click", function(event) {
		event.preventDefault();
		event.stopPropagation();

		openChat(user);
		
		return false;
	});
	
	$_org(user.container).append(clickim);
	return user.container;
}

function openChat(user) {	
	sendCommand("openchat", {
		name : user.name,
		jabberId : user.jabberId,
		phone : user.phone
	});
}

function makeCall(user) {
	sendCommand("makecall", {
		name : user.name,
		jabberId : user.jabberId,
		phone : user.phone
	});
};

function sendCommand(_type, _data, callback) {
	var command = {
		type : _type,
		user : _data				
	};			
	$_org("#jsCommandContainer").append('<IFRAME id="myId">');
	//$_org('iframe#myId').attr('src', commandFrameSrc + "?source=" + escape(document.location.href) + "&command=" + escape($_org.stringify(command)));
	$_org('iframe#myId').attr('src', commandFrameSrc + "?source=X&command=" + escape($_org.stringify(command)));
	
	$_org('iframe#myId').load(function() {
		$_org(this).remove();
		if (callback) {
			callback();
		}
	});
};